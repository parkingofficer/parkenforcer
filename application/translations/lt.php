<?php

$lang['CV'] = 'CV';

$lang['name'] = 'Vardas';
$lang['surname'] = 'Pavardė';
$lang['email'] = 'El. paštas';
$lang['message'] = 'Žinutė';
$lang['send'] = 'Siųsti';
$lang['SUBMIT_AN_APLICATION'] = 'Siųsti';

$lang['back'] = 'Atgal';

$lang['news'] = 'naujienos';
$lang['read_more'] = 'Daugiau';
$lang['rezults'] = 'Rezultatai';
$lang['view'] = 'Peržiūrėti';
$lang['more_news'] = 'Daugiau naujienų';
$lang['les'] = 'Sutraukti tekstą';
$lang['more'] = 'Daugiau';
$lang['contact us'] = 'Susisiekite su mumis';
$lang['email_not_valid'] = 'Klaidingas el.paštas';
$lang['email not valid'] = 'Klaidingas el.paštas';
$lang['field_is_required'] = 'Laukelį būtina užpildyti';
$lang['field is required'] = 'Laukelį būtina užpildyti';
$lang['message_send_success'] = 'Žinutė išsiųsta sėkmingai!';
$lang['message send success'] = 'Žinutė išsiųsta sėkmingai!';

return $lang;
