<?php

$lang['back'] = 'back';
$lang['news_search_results'] = 'News search results';
$lang['pages_search_results'] = 'Articles search results';
$lang['team_search_results'] = 'Team search results';
$lang['not_found'] = 'No results found';
$lang['to_meny_results'] = 'Too many results were found). Please be more specific';
$lang['client'] = 'client';
$lang['year'] = 'year';
$lang['status'] = 'status';
$lang['projects'] = 'Projects';
$lang['rezults'] = 'Results';
$lang['projects_search_results'] = 'Projects';
$lang['name'] = 'Name';
$lang['surname'] = 'Surname';
$lang['email'] = 'E-mail';
$lang['CV'] = 'CV';
$lang['message'] = 'Message';
$lang['send'] = 'Send';
$lang['email_not_valid'] = 'E-mail not valid';
$lang['field_is_required'] = 'Field is required';
$lang['SUBMIT_AN_APLICATION'] = 'Send CV';
$lang['news'] = 'News';
$lang['more'] = 'More';
$lang['search'] = 'search';
$lang['message send success'] = 'Message send success';

return $lang;
