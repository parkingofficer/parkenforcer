<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initAutoload()
    {
        $moduleLoader = new Zend_Application_Module_Autoloader([
            'namespace' => '',
            'basePath' => APPLICATION_PATH,
        ]);

        $moduleLoader->addResourceType('services', 'services/', 'Services');

        return $moduleLoader;
    }

    protected function _initConfigs()
    {
        // Config
        $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/application.ini', APPLICATION_ENV);
        Zend_Registry::set('config', $config);

        $config = new Zend_Config_Xml(APPLICATION_PATH.'/configs/cmsEnv.xml', 'production', true);
        Zend_Registry::set('cmsEnv', $config);

        $resource = $this->getPluginResource('db');
        $db = $resource->getDbAdapter();
        $db->query("SET NAMES 'utf8' ");
        Zend_Registry::set('db', $db);
    }

    /*
    protected function _initLang() {
        // LANG
        $lang = substr( $_SERVER ['REQUEST_URI'], 1, 2 );
        if (!in_array( $lang, array (
            'lt',
            'en'
        ) )) {
            $lang = 'en';
        }

        define( 'DEFAULT_LANG', $lang );
        define( 'LANG', $lang );
        $editor_lang = Zend_Registry::isRegistered('editor_lang') ? Zend_Registry::get('editor_lang') : $lang;

        try {
            $translator = new Core_Translate(array(
                'adapter' => 'Core_Translate',
                'content' => APPLICATION_PATH . '/translations/' . $lang . '.php',
                'locale'  => $lang,
            ));
        } catch (Exception $e) {
            var_dump($e);die();
        }

        Zend_Registry::set( 'translator', $translator );
        Zend_Registry::set( 'not_translated', array () );

    }*/
    protected function _initRoutes()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/routes.ini');
        $router->addConfig($config, 'routes');
    }

    protected function _initRegisterRegistry()
    {
    }

    protected function _initRegisterPlugins()
    {
        //$this->bootstrap( 'frontController' );
        //$front = $this->getResource( 'frontController' );
        //$front->registerPlugin( new Plugin_Init() );
        //$front->registerPlugin( new Plugin_Navigation() );
    }

    protected function _initViewHelpers()
    {
        $this->bootstrap('layout');
        $layout = $this->getResource('layout');
        $view = $layout->getView();

        ZendX_JQuery_View_Helper_JQuery::enableNoConflictMode();
        $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $view->addHelperPath('ZendX/JQuery/View/Helper/JQuery', 'ZendX_JQuery_View_Helper_JQuery');
        $view->addHelperPath('Core/Form/Element', 'Core_Form_Element');
        $view->addHelperPath('Core/View/Helper', 'Core_View_Helper');
        $view->addScriptPath(APPLICATION_PATH.'/views/scripts/');

        $view->doctype('XHTML1_STRICT');
        $view->headMeta()->appendHttpEquiv('ContentType', 'text/html;charset=utf-8');
        $view->headTitle()->setSeparator(' - ');
        $view->headTitle('ParkEnforce');
    }

    protected function _initCache()
    {
    }

    protected function _initDefaults()
    {
        defined('GALLERY_PATH') || define('GALLERY_PATH', APPLICATION_PATH.'/../uploads/images/gallery');
        defined('GALLERY_URL') || define('GALLERY_URL', '/uploads/images/gallery');

        defined('TMP_FILES_PATH') || define('TMP_FILES_PATH', APPLICATION_PATH.'/../files/tmp');
        defined('FINES_PHOTOS_PATH') || define('FINES_PHOTOS_PATH', APPLICATION_PATH.'/../files/fines');
        defined('FINES_PHOTOS_URL') || define('FINES_PHOTOS_URL', '/files/fines');

        defined('UPLOAD_SALT') || define('UPLOAD_SALT', 'xk0q2389rCcn98N*yr2398ryuc23c$%adf');
        defined('DEFAULT_PAGINATION_SCRIPT') || define('DEFAULT_PAGINATION_SCRIPT', '_partial/select_paging.phtml');
        defined('DEFAULT_PAGINATION_ITEMS') || define('DEFAULT_PAGINATION_ITEMS', 50);
    }

    //protected function _initComposer(){
    //require APPLICATION_PATH.'/../library/vendor/autoload.php';
    //}

    protected function _initWhoops()
    {
        if (APPLICATION_ENV == 'development' || (int) $_GET['debug'] == 1) {
            // this line forces Zend Framework to throw Exceptions as they are and letting us handle them
            Zend_Controller_Front::getInstance()->throwExceptions(true);
            // creating new Whoops object
            $whoops = new \Whoops\Run();
            // adding an error handler, pretty one ;)
            $whoops->pushHandler(new Whoops\Handler\PrettyPageHandler());
            // and now final touch: setting Whoops as default error handler
            $whoops->register();
        }
    }
}
