<?php

/**
 * @name Api_BlackListController
 * @author darius.matulionis
 * @since  : 2013.07.09, 15:34:11
 */
class Api_FinesController extends Core_Controller_Action_Api
{
    public function init()
    {
        parent::init();
        $this->checkSession();
    }

    /**
     * @dec    For testing
     * @url http://{:host}/api/fines/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json   $return_success
     *                    (boolean) success: true # Is request success;
     *                    (double) version:  api_version_number # API version number;
     * @param json   $return_error
     *                    (boolean) success: false # Is request success;
     *                    (boolean) has_session: false # Is device authenticated;
     *                    (double) version: api_version_number # API version number;
     *                    (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
        $this->view->success = true;
        $this->view->msg = 'Test success';
    }

    /**
     * @dec    Check Plate
     * @url http://{:host}/api/fines/check-plate/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array  $_POST
     *                    adm_device_id: (string) # Device ID;
     *                    plate: (string) # Plate number;
     *                    lat: (double) # (optional) GPS LAT Format (xx.xxxxxxx);
     *                    lon: (double) # (optional) GPS LON Format (yy.yyyyyyy);
     * @param json   $return_success
     *                    (boolean) success: true # Is request success;
     *                    (double) version:  api_version_number # API version number;
     *                    (boolean) paid:  true/false # Is paid for parking;
     *                    (string) zone:  zone # Zone letter;
     *                    (date) paid_from:  datetime # Paid from (format Y-m-d H:i:s);
     *                    (date) paid_to:  datetime # Paid till (format Y-m-d H:i:s);
     * @param json   $return_error
     *                    (boolean) success: false # Is request success;
     *                    (boolean) has_session: false # Is device authenticated;
     *                    (double) version: api_version_number # API version number;
     *                    (string) msg: error_message # Error message;
     *                    (double) has_session: false # Has user session for this device ID;
     */
    public function checkPlateAction()
    {
        $log = new Model_PlateCheckLog();
        if ($this->authenticated) {
            $gps = null;
            $lat = null;
            $lon = null;

            if ((double)$this->_request->getParam('lat') && (double)$this->_request->getParam('lon')) {
                $lat = (double)$this->_request->getParam('lat');
                $lon = (double)$this->_request->getParam('lon');
                $gps = $lat . ', ' . $lon;
            }

            $log_id = $log->save([
                'adm_device_id' => $this->_request->getParam('adm_device_id'),
                'plate' => $this->_request->getParam('plate'),
                'time' => date('Y-m-d H:i:s'),
                'gps' => $gps,
                'request' => json_encode($this->_request->getParams()),
            ]);
            $plate = $this->_request->getParam('plate');
            if (!$plate) {
                $this->view->success = false;
                $this->view->msg = 'No plate number';
            } else {
                $model = new Model_Parking();
                $select = $model->select();
                $select->where('plate = ?', $plate);
                $select->where('date_start >= ?', date('Y-m-d 00:00:00'));
                $select->order('date_end DESC');
                $select->limit(1);
                $payment = $model->fetchRow($select);

                if ($payment) {
                    $now = time();
                    $payment_start = strtotime($payment->date_start);
                    $payment_end = strtotime($payment->date_end);
                    if ($payment_end >= $now) {
                        $this->view->success = true;
                        $this->view->paid = true;
                        $this->view->paid_from = $payment->date_start;
                        $this->view->paid_to = $payment->date_end;
                        $this->view->zone = $payment->zone;
                        $this->view->msg = 'Paid';

                        $b_model = new Model_BlackList();
                        $select = $b_model->select();
                        $select->where('plate_no = ?', $plate);
                        $select->where('`delete` = 0');
                        $plate_in_black_list = $b_model->fetchRow($select);
                        if ($plate_in_black_list) {
                            $plate_in_black_list->delete = 1;
                            $plate_in_black_list->deleted_by = $this->authenticated_admin;
                            $plate_in_black_list->delete_time = date('Y-m-d H:i:s');
                            $plate_in_black_list->save();
                            $this->view->msg = 'Plate deleted from black list. Payment was found';
                        }
                    } else {
                        if (!$this->checkWhiteList($plate)) {
                            $b_model = new Model_BlackList();
                            $b_model->save([
                                'cause_id' => 2,
                                'plate_no' => $plate,
                                'time_checked' => date('Y-m-d H:i:s'),
                                'gps' => $gps,
                                'lat' => $lat,
                                'lon' => $lon,
                            ]);

                            $this->view->success = true;
                            $this->view->paid = false;
                            $this->view->add_fine = true;
                            $this->view->added_to_balck_list = true;
                            $this->view->paid_from = $payment->date_start;
                            $this->view->paid_to = $payment->date_end;
                            $this->view->request_time = date('Y-m-d H:i:s', $now);
                            $this->view->msg = 'Time ended. Adding to black list';
                        }
                    }
                } elseif (!$this->checkWhiteList($plate)) {
                    $b_model = new Model_BlackList();
                    $select = $b_model->select();
                    $select->where('plate_no = ?', $plate);
                    $select->where('`delete` = 0');
                    $plate_in_black_list = $b_model->fetchRow($select);
                    if ($plate_in_black_list) {
                        $this->view->success = true;
                        $this->view->paid = false;
                        $this->view->plate_in_balck_list = true;
                        $this->view->cause_id = $plate_in_black_list->cause_id;
                        $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                        $this->view->msg = 'Plate is in black list';
                    } else {
                        $f_model = new Model_Fines();
                        $select = $f_model->select();
                        $select->where('plate = ?', $plate);
                        $select->where('date >= ?', date('Y-m-d 00:00:00'));
                        $select->where('date <= ?', date('Y-m-d 23:59:59'));
                        $fine = $f_model->fetchRow($select);
                        if ($fine) {
                            $this->view->success = true;
                            $this->view->paid = false;
                            $this->view->fine_is_created = true;
                            $this->view->msg = 'Fine is already created';
                        } else {
                            $b_model->save([
                                'cause_id' => 2,
                                'plate_no' => $plate,
                                'time_checked' => date('Y-m-d H:i:s'),
                                'gps' => $gps,
                                'lat' => $lat,
                                'lon' => $lon,
                            ]);

                            $this->view->success = true;
                            $this->view->paid = false;
                            $this->view->added_to_balck_list = true;
                            $this->view->msg = 'No plate number found in database. Adding to black list';
                        }
                    }
                }
            }
            //$log->update(array('response'=>json_encode($this->view)),'id = '.$log_id);
            $log_id->response = json_encode($this->view);
            $log_id->save();
        }
    }

    /**
     * @dec    Get Fine Causes
     * @url http://{:host}/api/fines/get-fine-causes/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array  $_POST
     *                    adm_device_id: (string) # Device ID;
     * @param json   $return_success
     *                    (boolean) success: true # Is request success;
     *                    (double) version:  api_version_number # API version number;
     *                    (array) fine_causes:  [;
     *                    \t {;
     *                    \t \t (int) id : fine_causes_id # Fine Causes ID;
     *                    \t \t (string) title : cause_title # Fine Cause;
     *                    \t };
     *                    ] # List fine Causes;
     * @param json   $return_error
     *                    (boolean) success: false # Is request success;
     *                    (boolean) has_session: false # Is device authenticated;
     *                    (double) version: api_version_number # API version number;
     *                    (string) msg: error_message # Error message;
     *                    (double) has_session: false # Has user session for this device ID;
     */
    public function getFineCausesAction()
    {
        if ($this->authenticated) {
            $model = new Model_BlackListCauses();
            $this->view->fine_causes = $model->fetchAll('visible = 1')->toArray();
            $this->view->success = true;
        }
    }

    /**
     * @dec    Get Fine Statuses
     * @url http://{:host}/api/fines/get-fine-statuses/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array  $_POST
     *                    adm_device_id: (string) # Device ID;
     * @param json   $return_success
     *                    (boolean) success: true # Is request success;
     *                    (double) version:  api_version_number # API version number;
     *                    (array) fine_statuses:  [;
     *                    \t {;
     *                    \t \t (int) id : fine_status_id # Fine Causes ID;
     *                    \t \t (string) title : status_title # Fine Status;
     *                    \t };
     *                    ] # List fine Causes;
     * @param json   $return_error
     *                    (boolean) success: false # Is request success;
     *                    (boolean) has_session: false # Is device authenticated;
     *                    (double) version: api_version_number # API version number;
     *                    (string) msg: error_message # Error message;
     *                    (double) has_session: false # Has user session for this device ID;
     */
    public function getFineStatusesAction()
    {
        if ($this->authenticated) {
            $this->view->fine_statuses = Model_Fines::statuses()->map(function ($status, $key) {
                return [
                    'id' => (int)$key,
                    'title' => $status,
                ];
            })->toArray();
            $this->view->success = true;
        }
    }

    /**
     * @dec    Insert Fine
     * @url http://{:host}/api/fines/insert/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array  $_POST
     *                    adm_device_id: (string) # Device ID;
     *                    causes_id: (int) # Fine Cause ID. Get list from
     *                    http://{:host}/api/fines/get-fine-causes/{:key}; status_id: (int) # Fine Status ID. Get list
     *                    from http://{:host}/api/fines/get-fine-statuses/{:key}; plate: (string) # Plane number; car:
     *                    (string) # Car model and manufacture; date: (string) # Fine date. Format:(Y-m-d H:i:s);
     *                    first_photo_date: (string) # First photo date. Format:(Y-m-d H:i:s); start: (string) # Start
     *                    date time. Format:(Y-m-d H:i:s); end: (string) # End date time. Format:(Y-m-d H:i:s); lat:
     *                    (double) # GPS LAT Format (xx.xxxxxxx); lon: (double) # GPS LON Format (yy.yyyyyyy); street:
     *                    (string) # Street;
     * @param json   $return_success
     *                    (boolean) success: true # Is request success;
     *                    (double) version:  api_version_number # API version number;
     *                    (int) fine_id: fine_id # Fine ID;
     * @param json   $return_error
     *                    (boolean) success: false # Is request success;
     *                    (boolean) has_session: false # Is device authenticated;
     *                    (double) version: api_version_number # API version number;
     *                    (string) msg: error_message # Error message;
     *                    (double) has_session: false # Has user session for this device ID;
     */
    public function insertAction()
    {
        if ($this->authenticated) {
            $model = new Model_Fines();

            try {
                $model->getAdapter()->beginTransaction();
                $data = $this->_request->getPost();

                $photo_files = [
                    'first_photo',
                    'plate_photo',
                    'front_photo',
                    'back_photo',
                    'left_side_photo',
                    'right_side_photo',
                ];
                $can_be_empty = array_merge([
                    'id',
                    'gps',
                    'car',
                    'bank_payment_no',
                    'notification_sent_time',
                    'notification_sent_to',
                    'zone',
                    'date',
                    'change_status_date',
                    'start',
                    'end',
                    'admin_id',
                    'municipality_id',
                    'id_public',
                    'amount',
                    'default_amount',
                    'rate',
                    'paid',
                    'first_photo_date',
                    'import_xlsx',
                    'import_xlsx_file_name',
                    'name',
                    'surname',
                    'u_email',
                    'u_phone_number',
                    'u_comment',
                    'billing_id'
                ], $photo_files);
                $is_valid_post = true;
                $not_asigned_fields = null;
                foreach ($model->fetchNew() as $k => $v) {
                    if (!in_array($k, $can_be_empty)) {
                        if (!isset($data[$k]) || !$data[$k]) {
                            $is_valid_post = false;
                            $not_asigned_fields[$k] = '';
                        }
                    }
                }

                $this->view->not_asigned_fields = $not_asigned_fields;

                if ($is_valid_post) {
                    $data['adm_device_id'] = $this->_request->getParam('adm_device_id');
                    $data['gps'] = $data['lat'] . ', ' . $data['lon'];
                    $data['plate'] = strtoupper(str_replace(' ', '', $data['plate']));
                    $fine = $model->save($data);

                    $b_model = new Model_BlackList();
                    $select = $b_model->select();
                    $select->where('plate_no = ?', $fine->plate);
                    $plate_in_black_list = $b_model->fetchRow($select);
                    if ($plate_in_black_list) {
                        $plate_in_black_list->delete = 1;
                        $plate_in_black_list->deleted_by = $this->authenticated_admin;
                        $plate_in_black_list->delete_time = date('Y-m-d H:i:s');
                        $plate_in_black_list->save();
                        $this->view->msg = 'Plate deleted from black list and new fine inserted';
                    }

                    $this->view->fine = $fine->toArray();
                    $this->view->fine_id = $fine->id;
                    $model->getAdapter()->commit();
                } else {
                    $model->getAdapter()->rollBack();
                    $this->view->success = false;
                    $this->view->msg = 'Not all required fields are posted';
                }
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                $this->view->msg = $e->getMessage();
            }
        }
    }

    /**
     * @dec    Upload Fine Photo
     * @url http://{:host}/api/fines/upload-fine-photo/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array  $_POST
     *                    adm_device_id: (string) # Device ID;
     *                    fine_id: (int) # Fine ID;
     *                    photo: (file) # Photo Format (JPG/PNG);
     *                    photo_name: (string) # Which photo is uploading
     *                    (first_photo/plate_photo/front_photo,back_photo/left_side_photo/right_side_photo);
     * @param json   $return_success
     *                    (boolean) success: true # Is request success;
     *                    (double) version:  api_version_number # API version number;
     *                    (string) photo_url: photo_url # Photo url;
     * @param json   $return_error
     *                    (boolean) success: false # Is request success;
     *                    (boolean) has_session: false # Is device authenticated;
     *                    (double) version: api_version_number # API version number;
     *                    (string) msg: error_message # Error message;
     *                    (double) has_session: false # Has user session for this device ID;
     */
    public function uploadFinePhotoAction()
    {
        if ($this->authenticated) {
            $model = new Model_Fines();
            $model->getAdapter()->beginTransaction();

            $photo_files = [
                'first_photo',
                'plate_photo',
                'front_photo',
                'back_photo',
                'left_side_photo',
                'right_side_photo',
            ];
            $data = $this->_request->getPost();

            if (!isset($data['fine_id']) || empty($data['fine_id'])) {
                $this->view->success = false;
                $this->view->msg = 'No fine ID';
            } elseif (!isset($data['photo_name']) || empty($data['photo_name']) || !in_array($data['photo_name'],
                    $photo_files)) {
                $this->view->success = false;
                $this->view->msg = 'No photo name';
            } elseif (count($_FILES) != 1) {
                $this->view->success = false;
                $this->view->msg = 'One photo file should be uploaded';
            } else {
                $fine = $model->find((int)$data['fine_id'])->current();
                if (!$fine) {
                    $this->view->success = false;
                    $this->view->msg = 'No Fine found';
                } else {
                    //$this->view->file = $_FILES;
                    $fv = new Zend_Validate_File_MimeType(['image/jpeg', 'image/png']);
                    if (!isset($_FILES['photo']) || empty($_FILES['photo']['tmp_name']) || $fv->isValid($_FILES['photo']['type'])) {
                        $this->view->success = false;
                        $this->view->msg = 'Bad file';
                    } else {
                        try {
                            $upload = new Core_Utils_Upload();
                            $upload->uploadFile(FINES_PHOTOS_PATH . '/' . $data['fine_id'] . '/', 'md5', 10, $_FILES);
                            if (!$upload->_files['photo']) {
                                $this->view->success = false;
                                $this->view->msg = 'File upload Error';
                            } else {
                                $fine->{$data['photo_name']} = $upload->_files['photo'];
                                $fine->save();
                                $model->getAdapter()->commit();
                                $this->view->msg = 'File upload success';
                            }
                        } catch (Exception $e) {
                            $model->getAdapter()->rollBack();
                            $this->view->success = false;
                            $this->view->msg = $e->getMessage();
                        }
                    }
                }
            }
        }
    }

    protected function checkWhiteList($plate)
    {
        $isInWhiteList = false;
        $model = new Model_WhiteList();
        $white_list = $model->checkWhiteList($plate);
        if ($white_list && date('Y-m-d') <= $white_list->date_to) {
            $this->view->success = $isInWhiteList = true;
            $this->view->paid = true;
            $this->view->paid_from = $white_list->date_from;
            $this->view->paid_to = $white_list->date_to;
            $this->view->white_list_group = $white_list->group;
            $this->view->white_list_address = $white_list->address;
            $this->view->msg = 'Plate is in white list';
        }

        return $isInWhiteList;
    }

    public function getFineAction()
    {
        if ($this->authenticated) {


            if (!$publicId = $this->getParam('publicId')) {
                $this->view->msg = 'No id';
                return;
            }

            if (!$plate = $this->getParam('plate')) {
                $this->view->msg = 'No plate';
                return;
            }

            $paid = Model_Fines::STATUS_FINE_PAYED;
            $generated = Model_Fines::STATUS_FINE_GENERATED;
            $dismissed = Model_Fines::STATUS_FINE_DISMISSED;

            $model = new Model_Fines();
            $fine = $model->fetchRow($model->select()
                ->where('id_public  = ?', $publicId)
                ->where('plate  = ?', $plate)
                ->where("status_id not in('$paid','$generated','$dismissed')"));
            if (!empty($fine)) {
                $finalAmount = $model::finalFineAmount($fine['amount'], $fine['rate']);
                $amountToPay = (float)($finalAmount) - (float)($fine['paid']);
                $missingAmount = number_format($amountToPay, 2);

                if ($missingAmount > 0 ) {
                    $this->view->status = true;
                    $this->view->amount = $missingAmount;
                    $this->view->paid = false;
                }else{
                    $this->view->status = false;
                    $this->view->msg = $publicId . ' - not found.';
                }
                return;
            } else {
                $this->view->status = false;
                $this->view->msg = $publicId . ' - not found.';

                return;

                /*else {

                $fines = $model->fetchAll($model->select()->where(" plate  = '$publicId' and status_id not in('$paid','$generated','$dismissed')"));

                if (count($fines) > 0) {
                    $overallAmount = 0;
                    foreach ($fines as $fine) {

                        $finalAmount = $model::finalFineAmount($fine['amount'], $fine['rate']);
                        $amountToPay = (float)($finalAmount) - (float)($fine['paid']);
                        $missingAmount = number_format($amountToPay, 2);
                        $overallAmount +=$missingAmount;
                    }

                    $overallAmountConverted = number_format($overallAmount, 2);
                    $this->view->status = true;
                    $this->view->amount = $overallAmountConverted;
                    $this->view->paid = true;
                    if ($overallAmountConverted > 0) {
                        $this->view->paid = false;
                    }

                }else{
                    $this->view->status = false;
                    $this->view->msg = $publicId . ' - not found.';

                    return;
                }*/

            }
        }
    }

    public function payFineAction()
    {
        if ($this->authenticated) {

            $paid = Model_Fines::STATUS_FINE_PAYED;
            $generated = Model_Fines::STATUS_FINE_GENERATED;
            $dismissed = Model_Fines::STATUS_FINE_DISMISSED;

            $publicId = $this->getParam('publicId');
            if (!$publicId) {
                $this->view->msg = 'No id';
                return;
            }

            $amount = $this->getParam('amount');
            if (!$amount) {
                $this->view->msg = 'No amount';
                return;
            }
            $model = new Model_Fines();

            $fine = $model->fetchRow($model->select()->where(" id_public  = '$publicId' and status_id not in('$paid','$generated','$dismissed')"));

            if (!empty($fine)) {

                if ($fine['id'] && $amount) {
                    try {
                        $convertedAmount = number_format($amount, 2);
                        (new Model_Fines())->addManualPayment($fine['id'], date('Y-m-d'), $convertedAmount, $this->authenticated_admin);
                        $fine = $model->fetchRow($model->select()->where(' id  = ?', $fine['id']));
                        $finalAmount = $model::finalFineAmount($fine['amount'], $fine['rate']);
                        $amountToPay = (float)($finalAmount) - (float)($fine['paid']);
                        $missingAmount = number_format($amountToPay, 2);

                        $paid = true;
                        if ($missingAmount > 0) {
                            $paid = false;
                        }

                        $this->_helper->json([
                            'status' => true,
                            'amount' => $missingAmount,
                            'paid' => $paid
                        ]);


                    } catch (\Exception $e) {
                        $this->_helper->json([
                            'success' => false,
                            'error' => true,
                            'message' => $e->getMessage()
                        ]);
                    }
                }


                exit;
            } else {

                $fines = $model->fetchAll($model->select()->where(" plate  = '$publicId' and status_id not in('$paid','$generated','$dismissed')"));
                if (count($fines) > 0) {
                    foreach ($fines as $fine) {
                        if ($fine['id'] && $amount) {
                            try {

                                $finalAmount = $model::finalFineAmount($fine['amount'], $fine['rate']);
                                $amountToPay = (float)($finalAmount) - (float)($fine['paid']);

                                $missingAmount = number_format($amountToPay, 2);
                                if ($amount >= $missingAmount) {
                                    (new Model_Fines())->addManualPayment($fine['id'], date('Y-m-d'), $missingAmount, $this->authenticated_admin);
                                    $amount = $amount - $missingAmount;
                                    $updatedFine = $model->fetchRow($model->select()->where(' id  = ?', $fine['id']));
                                    $finalAmount = $model::finalFineAmount($updatedFine['amount'],
                                        $updatedFine['rate']);
                                    $amountToPay = (float)($finalAmount) - (float)($updatedFine['paid']);
                                    $missingAmount = number_format($amountToPay, 2);
                                }

                            } catch (\Exception $e) {
                                $this->_helper->json([
                                    'success' => false,
                                    'error' => true,
                                    'message' => $e->getMessage()
                                ]);
                            }

                        }
                    }
                    if ($amount > 0) {
                        (new Model_Fines())->addManualPayment($fine['id'], date('Y-m-d'), $amount, $this->authenticated_admin);
                    }
                    $this->_helper->json([
                        'status' => true,
                        'amount' => $missingAmount,
                    ]);
                } else {
                    $this->view->status = false;
                    $this->view->msg = $publicId . ' - not found. Action reported';
                }
                return;
            }
        }
    }

    public function createFineAction()
    {
        if ($this->authenticated) {
            $model = new Model_Fines();

            try {
                $model->getAdapter()->beginTransaction();
                $data = $this->_request->getPost();

                $zone = $this->_request->getParam('parking_zone_id');
                $billing_id = $this->_request->getParam('billing_fine_id');

                $error = false;
                if ( !$data['default_amount'] ) {
                    $error = true;
                    $this->view->success = false;
                    $this->view->msg = 'No require default amount';
                }elseif ( $data['default_amount'] && !ctype_digit( $data['default_amount'] )){
                    $error = true;
                    $this->view->success = false;
                    $this->view->msg = 'The amount specified is not a numeric value';
                }
                elseif ( !$data['status_id'] ) {
                    $error = true;
                    $this->view->success = false;
                    $this->view->msg = 'No require fine status ID';
                }
                elseif ( !$data['reason_id'] ) {
                    $error = true;
                    $this->view->success = false;
                    $this->view->msg = 'No require fine reason ID';
                }
                elseif ( !$data['plate'] ) {
                    $error = true;
                    $this->view->success = false;
                    $this->view->msg = 'Plate not found';
                }

                $fineExist = false;
                if ( !$billing_id ) {
                    $this->view->success = false;
                    $this->view->msg = 'Billing id not found';
                } else {
                    $fineExist = $model->fetchRow('billing_id =' . $billing_id);
                }
                $Model_Zone = new Model_Zone();
                $zoneObject = $Model_Zone->fetchRow('billing_id = ' . $zone);

                if ( $zoneObject && !$fineExist && $billing_id && !$error) {
                    $model_city = new Model_City();
                    $cityObject = $model_city->fetchRow('id = ' . $zoneObject->city_id . '');

                    $fine_date = new DateTime($data['date']); //current date/time
                    $fine_date->add(new DateInterval("PT{$zoneObject['warning_pay_time']}H"));

                    $data['date'] = date('Y-m-d H:i:s');
                    $data['adm_device_id'] = $this->_request->getParam('adm_device_id');
                    $data['admin_id'] = $this->authenticated_admin_id;
                    $data['plate'] = strtoupper(str_replace(' ', '', $data['plate']));
                    $data['zone'] = $zoneObject->id;
                    $data['id_public'] = Model_Fines::uniqueFineNumber($cityObject);

                    $cause = $data['reason_id'];

                    $zcs_model = new Model_ZoneCauseRates();
                    $zoneCauseRate = $zcs_model->fetchRow($zcs_model->select()->where(" zone_id = '$zoneObject->id' and cause_id = '$cause' "));

                    //$data['amount'] = $zoneCauseRate['rate'];
                    $data['amount'] = number_format($data['default_amount']/100,2, '.', ' ');
                    $data['default_amount'] = number_format($data['default_amount']/100,2, '.', ' ');
                    $data['due_date'] = $fine_date->format('d.m.Y');
                    $data['operator_name'] = $this->authenticated_admin;

                    $data['billing_id'] = $billing_id;
                    $data['first_photo'] = date('Y-m-d H:i:s');
                    $data['first_photo_date'] = $data['first_photo_date'] ? $data['first_photo_date'] : date('Y-m-d H:i:s');
                    $data['causes_id'] = $data['reason_id'];
                    $data['lat'] = 1;
                    $data['lon'] = 1;
                    $data['gps'] = $data['lat'] . ', ' . $data['lon'];

                    //dump($data);die('miau');
                    $fine = $model->save($data);

                    $this->view->FineNumber = $fine->id_public;
                    $this->view->FineId = $fine->id;

                    if (!empty($_FILES)) {
                        foreach ($_FILES as $k => $info) {
                            if ($info['name'] && $info['error'] == 0) {
                                $upload = new Core_Utils_Upload();
                                $upload->uploadFile(
                                    FINES_PHOTOS_PATH . '/' . $fine->id . '/',
                                    'md5',
                                    10,
                                    [$k => $info]
                                );

                                if (!$upload->_files[$k]) {
                                    throw new \Exception('Upload failed');
                                } else {
                                    $fine->{$k} = $upload->_files[$k];
                                    $fine->save();
                                }
                            }
                        }
                    }

                    $model->getAdapter()->commit();
                } elseif ( !$zoneObject ) {
                    $model->getAdapter()->rollBack();
                    $this->view->success = false;
                    $this->view->msg = 'No Zone found';
                } elseif ( $fineExist ) {
                    $model->getAdapter()->rollBack();
                    $this->view->success = false;
                    $this->view->msg = 'Fine Billing id - '.$billing_id.' exist';
                }
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                //$this->view->msg = $e->getMessage();
                $this->view->msg = '500 error';
            }
        }
    }

     /**
      * @dec Delete Fine inserted from billing
      * @url http://{:host}/api/fines/delete-fine/{:key}
      * @action post
      *
      **/
    public function deleteFineAction()
    {
        if ($this->authenticated) {
            $model = new Model_Fines();

            try {
                $model->getAdapter()->beginTransaction();
                $billing_id = $this->_request->getParam('billing_fine_id');

                $error = false;
                $fineExist = false;

                if ( !$billing_id ) {
                    $error = true;
                    $this->view->success = false;
                    $this->view->msg = 'Billing id not found';
                } else {
                    $fineExist = $model->fetchRow('billing_id =' . $billing_id);
                }

                if ( $fineExist['billing_id'] === $billing_id && !$error && !empty( $fineExist ) ) {
                    $where = $model->getAdapter()->quoteInto('billing_id = ?', $fineExist['billing_id']);
                    $model->delete($where);
                    $model->getAdapter()->commit();

                    Model_FineLog::event(
                        $fineExist['id'],
                        sprintf('Fine from billint ID <strong>'.$fineExist['billing_id'].'</strong>is delteted'),
                        $this->authenticated_admin
                    );

                    $this->view->FineId = $fineExist['id'];
                    $this->view->msg = 'Fine from Billing id - '.$billing_id.' deleted';

                } elseif ( empty( $fineExist ) ) {
                    $this->view->success = false;
                    $this->view->msg = 'Fine Billing id - '.$billing_id.' not exist';
                } else {
                    $this->view->success = false;
                    $this->view->msg = 'Fine Billing id - '.$billing_id.' not deleted';
                }
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                //$this->view->msg = $e->getMessage();
                $this->view->msg = '500 error';
            }
         }
    }
}
