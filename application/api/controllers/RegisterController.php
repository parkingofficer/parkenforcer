<?php

class Api_RegisterController extends Core_Controller_Action_Api
{
    public function sendDataAction()
    {
        $registerData = [
            'phone_number' => $this->_request->getParam('phoneNumber'),
            'number_plate' => $this->_request->getParam('numberPlate'),
            'email' => $this->_request->getParam('email'),
        ];

        $b_model = new Model_Register();
        $b_model->save($registerData);
    }
}
