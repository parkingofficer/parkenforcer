<?php

/**
 * @name Api_BlackListController
 * @author darius.matulionis
 * @since : 2013.07.09, 15:34:11
 */
class Api_BlackListController extends Core_Controller_Action_Api
{
    public function init()
    {
        parent::init();
        $this->checkSession();
    }

    /**
     * @dec For testing
     * @url http://{:host}/api/black-list/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
    }

    /**
     * @dec Add new user
     * @url http://{:host}/api/black-list/get-list/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (array) balck_list:  [;
     *          \t {;
     *          \t \t (int) id : black_list_item_id # Black List ID;
     *          \t \t (int) cause_id : black_list_cause_id # Black List Cause ID;
     *          \t \t (string) plate_no : plate_no # Black List Plate No;
     *          \t \t (string) time_checked : time_checked # Then entered to Black List. Format (YYYY-MM-DD HH:ii:ss);
     *          \t \t (string) gps : gps # GPS coordinates. Format (xx.xxxxxxx, yy.yyyyyyy);
     *          \t \t (double) lat : lat # GPS LAT Format (xx.xxxxxxx);
     *          \t \t (double) lon : lon # GPS LON Format (yy.yyyyyyy);
     *          \t \t (string) cause : cause # Black list cause;
     *          \t };
     *      ] # List of black;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function getListAction()
    {
        if ($this->authenticated) {
            $model = new Model_BlackList();
            $this->view->balck_list = $model->getBlackList();
        }
    }

    /**
     * @dec Add new user
     * @url http://{:host}/api/black-list/delete-from-black-list/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     *        black_list_id: (int) # Black List ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function deleteFromBlackListAction()
    {
        if ($this->authenticated) {
            $id = $this->_request->getParam('black_list_id');

            if ($id) {
                $model = new Model_BlackList();
                $item = $model->find($id)->current();
                if ($item) {
                    $model->getAdapter()->beginTransaction();
                    try {
                        $item->delete = 2;
                        $item->delete_time = date('Y-m-d H:i:s');
                        $item->deleted_by = $this->authenticated_admin;
                        $item->save();
                        $model->getAdapter()->commit();
                        $this->view->success = true;
                        $this->view->msg = 'Item deleted from black list';
                    } catch (Exception $e) {
                        $model->getAdapter()->rollBack();
                        $this->view->success = false;
                        $this->view->msg = $e->getMessage();
                    }
                } else {
                    $this->view->success = false;
                    $this->view->msg = 'No Black List Item Found';
                }
            } else {
                $this->view->success = false;
                $this->view->msg = 'No Black List ID';
            }
        }
    }
}
