<?php

class Api_ModusappController extends Core_Controller_Action_Api
{
    public function getOrdersForModusAppAction()
    {
        $plate = $this->getParam('plate');
        if (! $plate) {
            return 'No plate number';
        }

        $model = new Model_Parking();
        $now = date('Y-m-d H:i:s');
        $where = [];
        $where[] = "plate = '$plate'";
        $where[] = "date_end >= '$now'";

        $orders = $model->getOrdersForModus($where);

        $arr = [];
        foreach ($orders as $order) {
            $arr [] = $order->toArray();
        }

        if ($arr) {
            $this->view->parking = $arr;
        } else {
            $this->view->parking = null;
        }
    }
}
