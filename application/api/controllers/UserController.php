<?php

/**
 * @name Api_UserController
 * @author darius.matulionis
 * @since : 2013.07.09, 15:34:11
 */
class Api_UserController extends Core_Controller_Action_Api
{
    /**
     * @dec For testing
     * @url http://{:host}/api/user/{:key}
     * @action get
     *
     * @param string $key Your API key
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
    }

    /**
     * @dec Add new user
     * @url http://{:host}/api/user/login/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_login: (string) # Login name;
     *        adm_password: (string) # Login password (sha1 hash);
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (int) user_id: user_id # user ID;
     *        (string) adm_device_id: adm_device_id  # Device ID;
     *        (string) name: user_name # user Name;
     *        (string) adm_last_login: adm_last_login # Last login date time (Y-m-d H:i:s);
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function loginAction()
    {
        $adm_login = $this->_request->getParam('adm_login');
        $adm_password = $this->_request->getParam('adm_password');
        $adm_device_id = $this->_request->getParam('adm_device_id');

        if ($adm_login && $adm_password && $adm_device_id) {
            $model = new Model_Admins();
            $select = $model->select();
            $select->where('adm_login = ?', $adm_login);
            $select->where('adm_password = ?', $adm_password);
            $admin = $model->fetchRow($select);
            if ($admin) {
                $admin->adm_device_id = $adm_device_id;
                $admin->adm_last_login = date('Y-m-d H:i:s');
                $admin->save();

                $this->view->user_id = $admin['id'];
                $this->view->adm_device_id = $adm_device_id;
                $this->view->name = $admin['adm_name'];
                $this->view->adm_last_login = $admin['adm_last_login'];
            } else {
                $this->view->success = false;
                $this->view->msg = 'Bad login credentials';
            }
        } else {
            $this->view->success = false;
            $this->view->msg = 'Not all required parameters was provided';
        }
    }

    /**
     * @dec Add new user
     * @url http://{:host}/api/user/has-session/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (int) user_id: user_id # user ID;
     *        (double) has_session: true # Has user session for this device ID (true | false);
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function hasSessionAction()
    {
        $adm_device_id = $this->_request->getParam('adm_device_id');

        if ($adm_device_id) {
            $model = new Model_Admins();
            $select = $model->select();
            $select->where('adm_device_id = ?', $adm_device_id);
            $admin = $model->fetchRow($select);
            if ($admin) {
                $admin->adm_last_login = date('Y-m-d H:i:s');
                $admin->save();

                $this->view->user_id = $admin['id'];
                $this->view->has_session = true;
                $this->view->adm_last_login = $admin['adm_last_login'];
            } else {
                $this->view->success = false;
                $this->view->has_session = true;
                $this->view->msg = 'Bad login credentials';
            }
        } else {
            $this->view->success = false;
            $this->view->has_session = true;
            $this->view->msg = 'Not all required parameters was provided';
        }
    }
}
