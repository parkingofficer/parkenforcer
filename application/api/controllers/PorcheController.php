<?php

/**
 * @name Api_PorcheController
 * @author Jarek Skuder
 */
class Api_PorcheController extends Core_Controller_Action_Api
{
    public function init()
    {
        parent::init();
        $this->checkSession();
    }

    /**
     * @dec For testing
     * @url http://{:host}/api/porche/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
    }

    /**
     * @dec Add new user
     * @url http://{:host}/api/porche/set/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (array) :  porsche[;
     *          \t {;
     *          \t \t (string) plate : order plate number;
     *          \t \t (datetime) date_from : order date from;
     *          \t \t (datetime) date_to : order date to;
     *          \t \t (string) flight_number : order flight number;
     *          \t \t (string) name : person name;
     *          \t \t (string) phone : person phone;
     *          \t \t (string) email : person email;
     *          \t \t (string) address : person address;
     *          \t \t (string) tech_doc_nr : person technical document number;
     *          \t \t (string) insurance_nr : person insurance number;
     *          \t \t (float) amount : order amount;
     *          \t \t (string) company : company name;
     *          \t \t (string) contract_nr : contract number;
     *          \t \t (string) payment_type : payment type;
     *          \t \t (int) status : status;
     *          \t \t (double) amount_pvm : amount pvm;
     *          \t \t (string) person_company_code : person company code;
     *          \t \t (int) shop_order_id : shop order id;
     *          \t };
     *      ] # Porche;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function setPorscheAction()
    {
        if ($this->authenticated) {
            $porscheDb = new Model_Porche();
            $orderServices = new Model_PorcheOrderServices();
            $porscheLogDb = new Model_PorcheLog();
            $request = json_decode($this->_request->getRawBody(), true);
            $plate = $request['plate'];
            $services = $request['services'];

            $log = $porscheLogDb->save([
                'adm_device_id' => $request['adm_device_id'],
                'plate' => $plate ? $plate : null,
                'request' => json_encode($request),
            ]);

            if( $plate ){
                $lang = $request['locale'];

                unset($request['services']);
                $dataSave = array_merge(
                    $request,
                    array(
                        'services' => json_encode( $services ),
                        "log_id" => $log->id,
                        "language" => $lang ? $lang : 'lt'
                        )
                );
                $porscheOrder = $porscheDb->save( $dataSave );

                $porscheServiceStatuses = new Model_PorcheServiceStatuses();
                foreach ($services as $s){
                    $status = $porscheServiceStatuses->getPrimaryServiceStatus( $s['service_id'] );
                    $orderServices->save([
                        'porche_id' => $porscheOrder['id'],
                        'service_id' => $s['service_id'],
                        'status_id' => $status,
                        'count_id' => $s['count_id']
                    ]);
                }

                $this->view->success = true;
                $this->view->msg = 'Porsche order set';
            }else{
                $this->view->success = false;
                $this->view->msg = 'No plate number';
            }

            $log->response = json_encode($this->view);
            $log->save();
        }
    }

    public function startAdditionalServicesAction()
    {
        if ($this->authenticated) {
            $porscheDb = new Model_Porche();
            $orderServices = new Model_PorcheOrderServices();
            $porscheLogDb = new Model_PorcheLog();
            $request = json_decode($this->_request->getRawBody(), true);
            $plate = $request['NumberPlate'];
            $services = $request['OrderedServices'];

            $log = $porscheLogDb->save([
                'adm_device_id' => $request['adm_device_id'],
                'plate' => $plate ? $plate : null,
                'request' => json_encode($request),
            ]);

            if( $plate ){
                //$lang = $request['locale'];
                $lang = 'lt';

                $servicesData = [];
                $i = 0;
                foreach ($services as $service){
                    $i++;
                    array_push($servicesData, array(
                        'service_id' => $service['Id'],
                        'count' => $service['Qty'],
                        'count_id' => $i
                    ));
                }

                $dataFromBilling = [
                    'date_from'     => $request['StartDate'],
                    'date_to'       => $request['ReturnDate'],
                    'plate'         => $plate,
                    'flight_number' => $request['FlightNumber'],
                    'payment_type'  => 3,
                    'services'      => json_encode( $servicesData ),
                ];
                $dataSave = array_merge(
                    $dataFromBilling,
                    array(
                        "log_id" => $log->id,
                        "language" => $lang ? $lang : 'lt'
                        )
                );
                $porscheOrder = $porscheDb->save( $dataSave );

                $porscheServiceStatuses = new Model_PorcheServiceStatuses();
                $count = 0;
                foreach ($services as $s){
                    $count++;
                    $status = $porscheServiceStatuses->getPrimaryServiceStatus( $s['Id'] );
                    $orderServices->save([
                        'porche_id' => $porscheOrder['id'],
                        'service_id' => $s['Id'],
                        'status_id' => $status,
                        'count_id' => $count
                    ]);
                }
                $this->view->OrderId = $porscheOrder['id'];

                // send information to info uniparkt
                $dbPS = new Model_PorcheStatuses();
                $porscheDb->sendMail( $porscheOrder['id'], $dbPS->getServiceMailType(1) , "", 1, 3);
                //
            }else{
                $this->view->success = false;
                $this->view->msg = 'No plate number';
            }

            $log->response = json_encode($this->view);
            $log->save();
        }
    }
}
