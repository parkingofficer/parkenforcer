<?php

/**
 * @name Api_PaymentsController
 * @author darius.matulionis
 */
class Api_PaymentsController extends Core_Controller_Action_Api
{
    public function init()
    {
        parent::init();
        $api = $this->getParam('SystemApi');
        $model = new Model_Admins();
        if ($api) {
            $select = $model->select();
            $select->where('adm_device_id = ?', $api);
            $admin = $model->fetchRow($select);
            if ($admin) {
                $this->admin = $admin;
                $this->authenticated = true;
            } else {
                $this->authenticated = false;
                $this->view->success = false;
                $this->view->msg = 'API Key Not Found';
            }
        } else {
            $this->authenticated = false;
            $this->view->success = 'false';
            $this->view->msg = 'No API Key';
        }
    }

    /**
     * @dec For testing
     * @url http://{:host}/api/payments/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
    }

    public function paymentRemoveAction()
    {
        if ($this->authenticated) {
            $pl_model = new Model_PaymentsLog();
            $p_log = $pl_model->save([
                'date' => date('Y-m-d H:i:s'),
                'request' => json_encode($this->_request->getParams()),
                'zone' => $this->getParam('Zone'),
            ]);
            $UniqueID = $this->getParam('UniqueID');
            if (! $UniqueID) {
                $this->view->msg = 'No Unique Payment ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $model = new Model_Parking();

            $parking = $model->fetchRow($model->select()->where(' id = ?', $UniqueID));
            if (! empty($parking)) {
                $parking->delete($parking);
                $p_log->response = json_encode($UniqueID.' - removed');
                $p_log->save();
                $this->view->msg = $UniqueID.' - removed';

                return;
            } else {
                $p_log->response = json_encode($UniqueID.' - not found.');
                $p_log->save();
                $this->view->msg = $UniqueID.' - not found. Action reported';

                return;
            }
        }
    }

    public function paymentInsertAction()
    {
        if ($this->authenticated) {
            $pl_model = new Model_PaymentsLog();
            $p_log = $pl_model->save([
                'date' => date('Y-m-d H:i:s'),
                'request' => json_encode($this->_request->getParams()),
                'zone' => $this->getParam('Zone'),
            ]);

            $Zone = $this->getParam('Zone');
            if (!$Zone) {
                $domain = new Model_Domain();
                $domainOptionsArray = $domain->options();
                $Zone = $domainOptionsArray['default_zone'];
            }

            $this->view->success = false;
            $SystemName = $this->getParam('SystemName');
            if (!$SystemName) {
                $this->view->msg = 'No System Name';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $UniqueID = $this->getParam('UniqueID');
            if (!$UniqueID) {
                $this->view->msg = 'No Unique Payment ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $dateValidate = new Zend_Validate_Date();

            $StartTime = $this->getParam('StartTime');
            if (!$StartTime) {
                $this->view->msg = 'No Start Time';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            /*            if(!$dateValidate->setFormat('Y-m-d H:i:s')->isValid($StartTime)){
                            $this->view->msg = 'Start Time is not valid. Format (Y-m-d H:i:s)';
                            $p_log->response = json_encode($this->view->msg);
                            $p_log->save();
                            return;
                        }*/

            $EndTime = $this->getParam('EndTime');
            if (!$EndTime) {
                $this->view->msg = 'No End Time';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            /*            if(!$dateValidate->setFormat('Y-m-d H:i:s')->isValid($EndTime)){
                            $this->view->msg = 'End Time is not valid. Format (Y-m-d H:i:s)';
                            $p_log->response = json_encode($this->view->msg);
                            $p_log->save();
                            return;
                        }*/

            $City = $this->getParam('City');
            if (!$City) {
                $City = '4';
            }

            $LicencePlate = $this->getParam('LicencePlate');
            if (!$LicencePlate) {
                $this->view->msg = 'No License plate';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $Amount = (int)$this->getParam('Amount');
            if (!$Amount) {
                $Amount = '0';
            }

            $LocationLat = (double)$this->getParam('LocationLat');
            if (!$LocationLat) {
                $this->view->msg = 'No Latitude. Format (xx.xxxxxx)';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $LocationLon = (double)$this->getParam('LocationLon');
            if (!$LocationLon) {
                $this->view->msg = 'No Longitude. Format (xx.xxxxxx)';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $MachineSerialNumber = (double)$this->getParam('MachineSerialNumber');
            if (!$MachineSerialNumber) {
                $this->view->msg = 'No Machine Serial Number';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $LicencePlate = str_replace('-', '', $LicencePlate);
            $LicencePlate = str_replace(' ', '', $LicencePlate);

            $zoneDb = new Model_Zone();
            $PaymentDb = new Model_FinesPayments();
            $WPrice = $zoneDb->getWPrice($Zone);

            //patikrinama ar yra bauda
            $model = new Model_Fines();
            $where = [];
            $where[] = "plate = '$LicencePlate'";
            $where[] = "zone = '$Zone'";
            $where[] = "(status_id != " . Model_Fines::STATUS_TIME_ELAPSED . " or status_id != " . Model_Fines::STATUS_PERSON_INFORMED . ")";
            $data = $model->getFines($where);

            if ($WPrice['warning_price'] * 100 == ($Amount) && count($data)) {
                try {
                    if (count($data)) {
                        $archive_no = $this->generateRandomString('11');

                        //pridedamas naujas irasas i fines_payments
                        //sugeneruojamas archivo numeris pav.: HEC_DATE(rand number 11 symbol)
                        $arr = [];
                        $arr['archive_no'] = $archive_no;
                        $arr['time'] = date('Y-m-d H:s');
                        $arr['amount_cnt'] = $Amount;
                        $arr['payment_type'] = 'C';
                        $arr['currency'] = 'EUR';
                        $arr['client_code'] = 'Hectronic';
                        $arr['payment_text'] = 'Hectronic ' . $LicencePlate;
                        $arr['payer_bank_code'] = 'Hectronic';
                        $arr['payer_bank'] = 'Hectronic';
                        $arr['payer_account'] = 'Hectronic';
                        $arr['payer_name'] = 'Hectronic';
                        $arr['payer_code'] = '1';
                        $arr['plate'] = $LicencePlate;
                        $arr['fine_id'] = null;
                        $arr['operation_type'] = '';
                        $arr['document_no'] = '';
                        $arr['payment_code'] = '';

                        $PaymentDb->save($arr,'document_no');
                        $model->associatePayment($archive_no,$data[0]['id']);
                        $response_array = array_merge(['fine_id' => $data[0]['id']], $arr);

                        $this->view->success = true;
                        $p_log->response = json_encode($response_array);
                        $p_log->save();
                    }
                } catch (Exception $e) {
                    $this->view->success = false;
                    $this->view->msg = $e->getMessage();
                    $p_log->response = json_encode($e->getMessage());
                    $p_log->save();
                }
            } else {
                $model = new Model_Parking();
                $model->getAdapter()->beginTransaction();
                try {

                    $data = [
                        'id' => $UniqueID,
                        'phone' => null,
                        'operator' => 4,
                        'operator_name' => strtolower($SystemName),
                        'plate' => $LicencePlate,
                        'bar_code' => $MachineSerialNumber,
                        'duration_hours' => DateTime::createFromFormat('Y-m-d H:i:s',
                            $StartTime)->diff(DateTime::createFromFormat('Y-m-d H:i:s', $EndTime))->h,
                        'count' => 1,
                        'price_cent' => $Amount,
                        'city' => $City,
                        'zone' => $Zone,
                        'reminder' => 0,
                        'date_inserted' => date('Y-m-d H:i:s'),
                        'date_start' => $StartTime,
                        'date_end' => $EndTime,
                    ];
                    $model->save($data);
                    $model->getAdapter()->commit();

                    $this->view->success = true;
                    $p_log->response = json_encode($data);
                    $p_log->save();

                    //save to billing export queue
                    $billingQueue = new Model_BillingQueue();
                    $billingQueue->save([
                        'type' => Model_BillingQueue::CREATE_TRANSACTION,
                        'system_id' => $data['id'],
                        'status' => 0,
                    ]);

                    $data['initiator'] = 'app';
                } catch (Exception $e) {
                    $model->getAdapter()->rollBack();
                    $this->view->success = false;
                    $this->view->msg = $e->getMessage();

                    $p_log->response = json_encode($e->getMessage());
                    $p_log->save();
                }

            }
        }
    }

    public function generateRandomString($length)
    {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = 'HEC_'.date('Ymdhs');
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    public function mobilePaymentInsertAction()
    {
        if ($this->authenticated) {
            $pl_model = new Model_PaymentsLog();

            $p_log = $pl_model->save([
                'date' => date('Y-m-d H:i:s'),
                'request' => json_encode($this->_request->getParams()),
                'zone' => $this->getParam('Zone'),
            ]);

            $this->view->success = false;
            $SystemName = $this->getParam('SystemName');
            if (! $SystemName) {
                $this->view->msg = 'No System Name';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $UniqueID = $this->getParam('UniqueID');

            $Zone = $this->getParam('Zone');
            if (! $Zone) {
                $this->view->msg = 'No Zone ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $City = $this->getParam('City');
            if (! $City) {
                $this->view->msg = 'No City ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            /*$Duration = $this->getParam('Duration');
            if(!$Duration){
                $this->view->msg = 'No Duration amount';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();
                return;
            }*/

            $Phone = $this->getParam('Phone');
            if (! $Phone) {
                $this->view->msg = 'No Phone number';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $dateValidate = new Zend_Validate_Date();

            $EndTime = $this->getParam('EndTime');
            if (! $EndTime) {
                $this->view->msg = 'No End Time';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $LicencePlate = $this->getParam('LicencePlate');
            if (! $LicencePlate) {
                $this->view->msg = 'No License plate';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $Amount = (int) $this->getParam('Amount');
            if (! $Amount) {
                $this->view->msg = 'No payment amount';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $model_zones = new Model_Zone();
            $zoneCode = $model_zones->fetchRow($model_zones->select()->where(' zone_code = ?', $Zone));

            $model = new Model_Parking();

            $select = $model->select();
            $select->where('plate = ?', $LicencePlate);
            $select->where('zone = ?', $zoneCode->id);
            $select->where('date_end > ?', date('Y-m-d H:i:s'));
            $select->order('date_end DESC');
            $select->limit(1);
            $order = $model->fetchRow($select);

            if (! empty($order)) {
                $date_end = date('Y-m-d H:i:s', strtotime($order->date_end.'+'.$EndTime.' minutes'));
                if ($order['operator_name'] !== strtolower($SystemName)) {
                    $date_start = $order->date_end;
                    $UniqueID = uniqid();
                    $Amount_order = $Amount;
                } else {
                    $date_start = $order->date_start;
                    if (! $UniqueID) {
                        $UniqueID = $order->id;
                        $Amount_order = (int) $order->price_cent + (int) $Amount;
                    } else {
                        $Amount_order = $Amount;
                    }
                }
            } else {
                if (! $UniqueID) {
                    $UniqueID = uniqid();
                }
                $date_end = date('Y-m-d H:i:s', strtotime('+'.$EndTime.' minutes'));
                $date_start = date('Y-m-d H:i:s');
                $Amount_order = $Amount;
            }
            $model->getAdapter()->beginTransaction();

            $LicencePlate = str_replace('-', '', $LicencePlate);
            $LicencePlate = str_replace(' ', '', $LicencePlate);

            $date_a = new DateTime(".$date_start.");
            $date_b = new DateTime(".$date_end.");
            if ($date_b >= $date_a) {
                $interval = date_diff($date_b, $date_a);
                $interval = $interval->format('%h');
            }

            try {
                $data = [
                    'id' => $UniqueID,
                    'phone' => $Phone,
                    'operator' => 4,
                    'operator_name' => strtolower($SystemName),
                    'plate' => $LicencePlate,
                    'duration_hours' => $interval,
                    'count' => 1,
                    'price_cent' => $Amount_order,
                    'city' => $City,
                    'zone' => $zoneCode->id,
                    'reminder' => 0,
                    'date_inserted' => date('Y-m-d H:i:s'),
                    'date_start' => $date_start,
                    'date_end' => $date_end,
                ];
                $model->save($data);
                $model->getAdapter()->commit();

                $this->view->success = true;
                $this->view->info = $date_end;
                $p_log->response = json_encode($data);
                $p_log->save();
                //save to billing export queue
                $billingQueue = new Model_BillingQueue();
                $billingQueue->save([
                    'type' => Model_BillingQueue::CREATE_TRANSACTION,
                    'system_id' => $data['id'],
                    'status' => 0,
                ]);
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                $this->view->msg = $e->getMessage();

                $p_log->response = json_encode($e->getMessage());
                $p_log->save();
            }
        }
    }

    public function paymentInsert2Action()
    {
        if ($this->authenticated) {
            $pl_model = new Model_PaymentsLog();
            $p_log = $pl_model->save([
                'date' => date('Y-m-d H:i:s'),
                'request' => json_encode($this->_request->getParams()),
            ]);

            $this->view->success = false;
            $SystemName = $this->getParam('SystemName');
            if (! $SystemName) {
                $this->view->msg = 'No System Name';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $UniqueID = $this->getParam('UniqueID');
            if (! $UniqueID) {
                $this->view->msg = 'No Unique Payment ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $zone = $this->getParam('Zone');
            if (! $zone) {
                $this->view->msg = 'No Zone ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $BillingZones = new Model_Zone();
            $zoneId = $BillingZones->fetchRow("billing_id = '$zone'");

            if (! $zoneId) {
                $zoneId = $BillingZones->fetchRow("id = '$zone'");

                if (! $zoneId) {
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();
                    $this->view->msg = 'No Zone ID';

                    return;
                }
            }

            $dateValidate = new Zend_Validate_Date();

            $StartTime = $this->getParam('StartTime');
            if (! $StartTime) {
                $parking = new Model_Parking();
                $begin = $parking->fetchRow($parking->select()->where(' id = ?', $UniqueID));
                $StartTime = $begin['date_start'];
                if (! $StartTime) {
                    $this->view->msg = 'No Start Time';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }
            }

            if (! $dateValidate->setFormat('Y-m-d H:i:s')->isValid($StartTime)) {
                $this->view->msg = 'Start Time is not valid. Format (Y-m-d H:i:s)';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $EndTime = $this->getParam('EndTime');

            if (! $EndTime) {
                $EndTime = null;
            }

            if (! empty($EndTime)) {
                $date_a = new DateTime(".$StartTime.");
                $date_b = new DateTime(".$EndTime.");
                if ($date_b >= $date_a) {
                    $interval = date_diff($date_b, $date_a);
                    $interval = $interval->format('%h');
                }

                if (! $dateValidate->setFormat('Y-m-d H:i:s')->isValid($EndTime)) {
                    $this->view->msg = 'End Time is not valid. Format (Y-m-d H:i:s)';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }
            }

            $LicencePlate = $this->getParam('LicencePlate');
            if (! $LicencePlate) {
                $this->view->msg = 'No License plate';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $Amount = (int) $this->getParam('Amount');
            if (! $Amount) {
                $amount = new Model_Parking();
                $price = $amount->fetchRow($amount->select()->where(' id = ?', $UniqueID));
                if (! $price) {
                    $Amount = '0';
                }
            }
        }

        $MachineSerialNumber = (double) $this->getParam('MachineSerialNumber');
        if (! $MachineSerialNumber) {
            $MachineSerialNumber = null;
        }

        $FreeTimeInMinutes = (int) $this->getParam('FreeTimeInMinutes');

        $model = new Model_Parking();
        $model->getAdapter()->beginTransaction();
        $LicencePlate = str_replace('-', '', $LicencePlate);
        $LicencePlate = str_replace(' ', '', $LicencePlate);

        try {
            $data = [
                'id' => $UniqueID,
                'billing_id' => $UniqueID,
                'phone' => $MachineSerialNumber,
                'operator' => 5,
                'operator_name' => strtolower($SystemName),
                'plate' => $LicencePlate,
                'bar_code' => null,
                'duration_hours' => $interval,
                'count' => 1,
                'price_cent' => $Amount,
                'city' => 4,
                'zone' => $zoneId['id'],
                'reminder' => 0,
                'date_inserted' => date('Y-m-d H:i:s'),
                'date_start' => $StartTime,
                'date_end' => $EndTime,
                'free_time_in_minutes' => $FreeTimeInMinutes ? $FreeTimeInMinutes : null
            ];
            $model->save($data);
            $model->getAdapter()->commit();

            $this->view->success = true;

            $p_log->response = json_encode($data);
            $p_log->save();

            $domain = new Model_Domain();
            $domainOptionsArray = $domain->options();
            //------------------------------------------------------------------------------------------------------------------------

            // CheckBalance =================================
// Setup cURL
        } catch (Exception $e) {
            $model->getAdapter()->rollBack();
            $this->view->success = false;
            $this->view->msg = $e->getMessage();

            $p_log->response = json_encode($e->getMessage());
            $p_log->save();
        }
    }

    public function getParkingZonesAction()
    {
        if ($this->authenticated) {
            $model = new Model_Zone();
            $zones = $model->fetchall();
            $z = [];
            foreach ($zones as $zone) {
                $z[] = [
                    'zone_code' => $zone['zone_code'],
                    'hour_price' => $zone['hour_price'],
                ];
            }
            $this->view->zones = $z;
        }
    }

    public function getParkingZonesUsmsAction()
    {
        if ($this->authenticated) {
            $zone = $this->getParam('Zone');
            if (! $zone) {
                $this->view->msg = 'No Zone ID';

                return;
            }

            $model = new Model_Zone();
            $zones = $model->fetchRow($model->select()->where('usms_zone = ?', $zone));
            if ($zones) {
                $this->view->zones = [
                    'zone_code' => $zones['zone_code'],
                    'zone_name' => $zones['zone_name'],
                    'billing_id' => $zones['billing_id'],
                    'pvm' => $zones['pvm'],
                    'work_time_begin' => $zones['work_time_begin'],
                    'work_time_end' => $zones['work_time_end'],
                    'hour_price' => $zones['hour_price'],
                ];
            } else {
                $this->view->zones = null;
            }
        }
    }

    public function getLastParkingAction()
    {
        if ($this->authenticated) {
            $phone = $this->getParam('Phone');
            if (! $phone) {
                $this->view->msg = 'No Phone';

                return;
            }

            $model = new Model_Parking();
            $zone_model = new Model_Zone();
            $plate = $model->fetchRow($model->select()->where('phone = ?', $phone)->order('date_inserted DESC'));

            if ($plate) {
                $zone = $zone_model->fetchRow('id ='.$plate['zone']);
                $this->view->parking = [
                    'plate' => $plate['plate'],
                    'date_start' => $plate['date_start'],
                    'zone' => $zone['usms_zone'],
                ];
            } else {
                $this->view->parking = null;
            }
        }
    }

    public function getParkingAction()
    {
        if ($this->authenticated) {
            $carplate = $this->getParam('Plate');
            $phone = $this->getParam('Phone');
            if (! $carplate) {
                //$this->view->msg = 'No Plate';
                //return;
                $plateStr = null;
            } else {
                $plateStr = "and plate = '$carplate'";
            }
            if (! $phone) {
                $this->view->msg = 'No Phone';

                return;
            }

            $currentTime = date('Y-m-d H:i:s');
            $model = new Model_Parking();
            $plate = $model->fetchAll($model->select()->where("phone = '$phone' $plateStr and (date_end >= '$currentTime' or date_end is null ) ")->order('date_inserted DESC'))->toArray();
            if ($plate) {
                $this->view->parking = $plate;
            } else {
                $this->view->parking = null;
            }
            //$this->view->parking = "plate = '$carplate' and date_end >= '$currentTime'";
        }
    }

    public function getLastActiveParkingAction()
    {
        if ($this->authenticated) {
            $phone = $this->getParam('Phone');
            if (! $phone) {
                $this->view->msg = 'No Phone';

                return;
            }
            $model = new Model_Parking();
            $plate = $model->getActiveParking($phone);
            if ($plate) {
                foreach ($plate as $park) {
                    $parking[] = [
                        'plate' => $park['plate'],
                        'date_start' => $park['date_start'],
                        'billing_id' => $park['billing_id'],
                        'zone' => $park['usms_zone'],
                    ];
                    $this->view->parking = $parking;
                }
            } else {
                $this->view->parking = null;
            }
        }
    }

    public function getParkingsByPeriodAction()
    {
        if ($this->authenticated) {
            $dateFrom = $this->getParam('dateFrom');
            $dateTo = $this->getParam('dateTo');
            if (! $dateFrom) {
                $this->view->success = false;
                $this->view->msg = 'No dateFrom format YYYY-MM-DD';

                return;
            }
            if (!$dateTo) {
                $this->view->success = false;
                $this->view->msg = 'No dateTo format YYYY-MM-DD';

                return;
            }

            $model_parkings = new Model_Parking();
            $admin = $this->admin;

            $id = $admin['id'];

            $parkings = $model_parkings->getParkingsByPeriodAndUser($dateFrom, $dateTo, $id);

            $listParkings = [];
            foreach ($parkings as $parking) {
                $listParkings [$parking['zone_name']][] = $parking->toArray();
            }

            if ($listParkings) {
                $this->view->parking = $listParkings;
            } else {
                $this->view->parking = null;
            }
        }
    }

    public function getParkingByIdAction()
    {
        if ($this->authenticated) {
            $parkingId = $this->getParam('parkingId');

            if (! $parkingId) {
                $this->view->success = false;
                $this->view->msg = 'No parkingId';

                return;
            }

            $model = new Model_Parking();
            $ZonesModel = new Model_Zone();
            $parking = $model->fetchRow("id = '$parkingId'");
            if ($parking) {
                $parking = $parking->toArray();
                $zone = $parking['zone'];
                $parkingZone = $ZonesModel->fetchRow($ZonesModel->select()->where("id = '$zone'"));
                $parking['zone_name'] = $parkingZone['zone_code'];
                $this->view->parking = $parking;
            } else {
                $this->view->success = false;
                $this->view->msg = 'No parking with given parkingId';
            }
        }
    }

    public function getParkingsByPeriodReconciliationAction()
    {
        if ($this->authenticated) {
            $dateFrom = $this->getParam('dateFrom');
            $dateTo = $this->getParam('dateTo');
            if (! $dateFrom) {
                $this->view->success = false;
                $this->view->msg = 'No dateFrom format YYYY-MM-DD';

                return;
            }
            if (! $dateTo) {
                $this->view->success = false;
                $this->view->msg = 'No dateTo format YYYY-MM-DD';

                return;
            }

            $model_parkings = new Model_Parking();
            $admin = $this->admin;

            $id = $admin['id'];

            $parkings = $model_parkings->getParkingsByPeriodAndUser($dateFrom, $dateTo, $id);

            $listParkings = [];
            $listParkings ['parkingsCount'] = count($parkings);
            $listParkings ['TotalParkingTime'] = 0;
            $listParkings ['TotalParkingAmount'] = 0;
            foreach ($parkings as $parking) {
                $listParkings ['TotalParkingTime'] = $listParkings ['TotalParkingTime'] + $parking['duration_hours'];
                $listParkings ['TotalParkingAmount'] = $listParkings ['TotalParkingAmount'] + $parking['price_cent'];
            }

            if ($listParkings) {
                $this->view->parking = $listParkings;
            } else {
                $this->view->parking = null;
            }
        }
    }

    function RigaParking($action, $post_data, $prod = true)
    {

        $client = "https://rsapi.azure-api.net";
        $segment = '/api';
        if (!$prod) {
            $segment = '/api-test';
        }

        if ($action == 'start') {
            $action = "/v1.0/Parking/Start";
        } else {
            $action = "/v1.0/Parking/Stop";
        }

        $pl_model = new Model_PaymentsLog();

        $zonesClient = new Zend_Http_Client($client . $segment . '/v1.0/Zone/GetZones');
        $zonesClient->setHeaders('Ocp-Apim-Subscription-key', '07f39b62aaf1481abc594051e5e550c9');
        $zonesClient->setHeaders('Content-type', 'application/json');
        $zonesClient->setParameterGet('date', date('Ymd'));
        $zonesClientResponse = $zonesClient->request(Zend_Http_Client::GET);
        //Die(var_dump($zones->getBody()));
        $zones = $zonesClientResponse->getBody();
        $zonesJson = json_decode($zones);
        $zonesArray = $zonesJson->zone;
        $zonesModel = new Model_Zone();
        $currentZone = $zonesModel->fetchRow('id =' . $post_data['zone']);
        $rigaZone = null;
        foreach ($zonesArray as $zone) {
            if ($zone->name == $currentZone['zone_code']) {
                $rigaZone = $zone->id;
            }
        }

        if ($rigaZone != null) {
            $post_data = json_encode(
                array(
                    "vrn" => $post_data['plate'],
                    "zoneId" => $rigaZone,
                    "phone" => isset($post_data['phone']) ? $post_data['phone'] : null,
                    "timeOfRequest" => str_replace('+00:00', 'Z', gmdate('c', strtotime(date('Y-m-d H:i:s')))),
                    "timeOfStart" => str_replace('+00:00', 'Z', gmdate('c', strtotime($post_data['date_start']))),
                    "initiator" => $post_data['initiator'],
                    "informative" => false,
                    "textMessage" => "id: " . $post_data['id'],
                    "timeOfStop" => isset($post_data['date_end']) ? str_replace('+00:00', 'Z',
                        gmdate('c', strtotime($post_data['date_end']))) : null,
                    "price" => isset($post_data['price_cent']) ? $post_data['price_cent'] / 100 : null
                )
            );


            $url = $client . $segment . $action;
            $client = new Zend_Http_Client($url);
            $client->setHeaders('Ocp-Apim-Subscription-key', '07f39b62aaf1481abc594051e5e550c9');
            $client->setHeaders('Content-type', 'application/json');
            $client->setRawData($post_data);
            //$client->setParameterPost('post',$post_data);
            $errors = array();

            try {
                // $result = $client->restPost($url, $post_data)->getHeader("Ocp-Apim-Subscription-key : 07f39b62aaf1481abc594051e5e550c9");
                $response = $client->request(Zend_Http_Client::POST);
                $p_log = $pl_model->save(array(
                    'date' => date('Y-m-d H:i:s'),
                    'request' => json_encode($client->getLastRequest()),
                    'response' => json_encode($response->getBody()),
                    'zone' => $currentZone['id']
                ));

            } catch (Zend_Rest_Client_Exception $e) {
                // catch Rest Client Exception
                $errors[] = '[' . $e->getCode() . ']:' . $e->getMessage();
            } catch (Exception $e) {
                // catch general exception if any occurs
                // like DB
                $errors[] = '[' . $e->getCode() . ']:' . $e->getMessage();
            }
        }
    }

    public function paymentInsertShopAction()
    {
        if ($this->authenticated) {
            $pl_model = new Model_PaymentsLog();
            $p_log = $pl_model->save([
                'date' => date('Y-m-d H:i:s'),
                'request' => json_encode($this->_request->getParams()),
                'zone' => $this->getParam('Zone'),
            ]);

            $this->view->success = false;
            $SystemName = $this->getParam('SystemName');
            if (! $SystemName) {
                $this->view->msg = 'No System Name';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $UniqueID = $this->getParam('UniqueID');
            if (! $UniqueID) {
                $this->view->msg = 'No Unique Payment ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $zone = $this->getParam('Zone');
            if (! $zone) {
                $this->view->msg = 'No Zone ID';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $BillingZones = new Model_Zone();
            $zoneId = $BillingZones->fetchRow("billing_id = '$zone'");
            if ( empty( $zoneId ) ) {
                $this->view->msg = 'No Zone ID in to PE';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $Phone = $this->getParam('Phone');
            if (! $Phone) {
                $this->view->msg = 'No Phone number';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $StartTime = $this->getParam('StartTime');
            if (!$StartTime) {
                $this->view->msg = 'No Start Time';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $date_from = strtotime( $StartTime );

            $EndTime = $this->getParam('EndTime');
            if (! $EndTime) {
                $this->view->msg = 'No End Time';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }
            $date_to = strtotime( $EndTime );

            $LicencePlate = $this->getParam('LicencePlate');
            if (! $LicencePlate) {
                $this->view->msg = 'No License plate';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $Amount = (int) $this->getParam('Amount');
            if (! $Amount) {
                $this->view->msg = 'No payment amount';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $LicencePlate = $this->getParam('LicencePlate');
            if (! $LicencePlate) {
                $this->view->msg = 'No License plate';
                $p_log->response = json_encode($this->view->msg);
                $p_log->save();

                return;
            }

            $MachineSerialNumber = (double) $this->getParam('MachineSerialNumber');
            if (! $MachineSerialNumber) {
                $MachineSerialNumber = null;
            }

            $model = new Model_Parking();
            $model->getAdapter()->beginTransaction();
            $LicencePlate = str_replace('-', '', $LicencePlate);
            $LicencePlate = str_replace(' ', '', $LicencePlate);

            try {
                $data = [
                    'id' => $UniqueID,
                    'billing_id' => null,
                    'phone' => $Phone,
                    'operator' => 4,
                    'operator_name' => strtolower($SystemName),
                    'plate' => $LicencePlate,
                    'bar_code' => null,
                    'duration_hours' => round( ( ($date_to - $date_from) / (60 * 60)), 0, PHP_ROUND_HALF_UP),
                    'count' => 1,
                    'price_cent' => $Amount,
                    'city' => $zoneId['city_id'],
                    'zone' => $zoneId['id'],
                    'reminder' => 0,
                    'date_inserted' => date('Y-m-d H:i:s'),
                    'date_start' => $StartTime,
                    'date_end' => $EndTime,
                ];
                $model->save($data);
                $model->getAdapter()->commit();

                $this->view->success = true;
                $p_log->response = json_encode($data);
                $p_log->save();
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                $this->view->msg = $e->getMessage();

                $p_log->response = json_encode($e->getMessage());
                $p_log->save();
            }
        }
    }
}