<?php

class Api_Bootstrap extends Zend_Application_Module_Bootstrap
{
    public function __construct($application)
    {
        parent::__construct($application);
    }

    protected function _initRestRoute()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $config = new Zend_Config_Ini(APPLICATION_PATH.'/api/configs/routes.ini');
        $router->addConfig($config, 'routes');
    }

    protected function _initRequest()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $request = $front->getRequest();
        if (null === $front->getRequest()) {
            $request = new Zend_Controller_Request_Http();
            $front->setRequest($request);
        }
        if (! $request->getParam('format')) {
            $request->setParam('format', 'json');
        }

        return $request;
    }
}
