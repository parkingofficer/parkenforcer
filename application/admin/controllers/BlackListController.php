<?php

class Admin_BlacklistController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    public function indexAction()
    {
        $is_delete = $this->_getParam('1');
        //$is_hid = $this->_getParam('hid');
        $translates = json_decode(Core_Locale::translate('export_headers_black_list'));
        $model = new Model_BlackList();

        if ($this->_request->isPost() && $is_delete) {
            $where[] = "id=. $is_delete";
            $id = $model->fetchRow($model->select()->where('id = ?', $is_delete));
            if ($id['delete'] == '0') {
                $adm = Core_Auth_Admin::getInstance()->getStorage()->read();

                $model->update([
                    'delete' => 3,
                    'delete_time' => date('Y-m-d H:i:s'),
                    'deleted_by' => $adm->adm_name.' '.$adm->adm_surname,
                ],
                    'id = '.$is_delete);

                //$this->_redirect($this->view->baseUrl('blacklist/'));
            }
        }

        $this->view->filter = $filter = $this->_request->getParam('filter');
        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        $this->view->city = $city = $this->getParam('city') ? $this->getParam('city') : null;
        if (! $date_from) {
            $this->view->date_from = $date_from = date('Y-m-d', strtotime('-2 days'));
        }
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $select = $model->getBlackListIndex();
        if ($date_from) {
            $select->where("time_checked >= '$date_from 00:00:00'");
        }
        if ($date_to) {
            $select->where("time_checked <= '$date_to 23:59:59'");
        }
        if ($filter) {
            $select->where("
                cause_id LIKE '%$filter%' OR
                plate_no LIKE '%$filter%' OR
                zone LIKE '%$filter%'

            ");
        }

        $ui = new Model_City();
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $zones = new Model_UserCities();
        $select2 = $zones->select()->from(['z' => $zones->info('name')],
            ['z.city_id'])->where('active = 1 and user_id = ?', (int) $id);//gaunu prisijungusio varotojo matomus miestus,

        $this->view->city_list = $ui->fetchAll(' id in ('.$select2.')');

        $model_city = new Model_City();

        $where = [];
        $adminGroup = new Model_AdminGroups();
        $operatorsGroupId = $adminGroup->getOperatorGroups();

        if ($city) {
            $select->where($model_city->info('name').'.id in ('.implode(',', $city).')');
        } else {
            $adm = new Model_Admins();
            $city_operators = $adm->fetchAll(' admin_groups_id in('.implode(',',
                    $operatorsGroupId).') AND adm_active = 1 and id =0');
            $this->view->operators_list = $city_operators;
        }

        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $select->where('user_id ='.$id);
        $select->where(' active = 1 ');
        $this->view->paginator_params = [
            'date_from' => $date_from,
            'date_to' => $date_to,
            'filter' => $filter,
        ];

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'time_checked DESC',
            $select
        );
        $this->view->table = $model->info('name');
        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;

        //EXPORTINGS =========================================================================

        if ($this->_request->isPost('xls') || $this->_request->isPost('pdf')) {
            $where = [];
            if ($date_from) {
                $where[] = "time_checked >= '$date_from 00:00:00'";
            }
            if ($date_to) {
                $where[] = "time_checked <= '$date_to 23:59:59'";
            }

            $model_zone = new Model_Zone();
            $user_info = $model_zone->getUserInfo($id);
            if ($city) {
                $city_ids = implode(',', $city);
                $where[] = "box_zone.city_id in($city_ids)";
            } else {
                $zoneArray = [];
                foreach ($user_info as $zone) {
                    $zoneArray[] = $zone['zone_id'];
                }
                $city_ids = implode(',', $zoneArray);
                $where[] = "box_zone.id in($city_ids)";
            }
            $data = $model->getBlackListCsv($where);

            $finalData = [];
            $i = 1;
            $finalData[] = $translates->csv;

            foreach ($data as $row) {
                $finalData[] = [
                    utf8_decode($i++),
                    iconv('UTF-8', 'windows-1252', $row['city']),
                    iconv('UTF-8', 'windows-1252', $row['zone_code']),
                    utf8_decode($row['plate_no']),
                    utf8_decode($row['cause']),
                    utf8_decode($row['time_checked']),
                    utf8_decode($row['gps']),
                    utf8_decode($row['delete']),
                    utf8_decode($row['delete_time']),
                    utf8_decode($row['deleted_by']),
                ];
            }
        }

        $this->view->table = $model->info('name');
        $is_csv = $this->_getParam('csv');

        //EXPORT TO CSV ======================================================================

        if ($this->_request->isPost() && $is_csv) {
            if ($date_from && $date_to) {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].$date_to.'.csv';
            } else {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].date('Y-m-d').'.csv';
            }
            $fp = fopen('php://output', 'w+');
            $where = [];
            if ($date_from) {
                $where[] = "time_checked >= '$date_from 00:00:00'";
            }
            if ($date_to) {
                $where[] = "time_checked <= '$date_to 23:59:59'";
            }
            $model_zone = new Model_Zone();
            $user_info = $model_zone->getUserInfo($id);
            if ($city) {
                $city_ids = implode(',', $city);
                $where[] = "box_zone.city_id in($city_ids)";
            } else {
                $zoneArray = [];
                foreach ($user_info as $zone) {
                    $zoneArray[] = $zone['zone_id'];
                }
                $city_ids = implode(',', $zoneArray);
                $where[] = "box_zone.id in($city_ids)";
            }
            $data = $model->getBlackListCsv($where);
            $i = 1;
            fputcsv($fp, $translates->csv);
            foreach ($data as $fields) {
                fputcsv($fp, [
                    $i++,
                    $fields['city'],
                    $fields['zone_code'],
                    $fields['plate_no'],
                    $fields['cause'],
                    $fields['time_checked'],
                    $fields['gps'],
                    $fields['delete'],
                    $fields['delete_time'],
                    $fields['deleted_by'],
                ]);
            }

            fclose($fp);

            if ($file1 !== false) {
                header('Content-type: text/plain');
                header('Content-Disposition: attachment; filename="'.$file1.'"');
                exit;
            }
        }
        //EXPORT TO EXCEL =====================================================================

        $is_xls = $this->_getParam('xls');
        if ($this->_request->isPost() && $is_xls) {
            set_time_limit(0);
            $model = new Model_BlackList();
            $filename = $_SERVER['DOCUMENT_ROOT'].'/tmp/excel-'.date('m-d-Y').'.xls';
            $handle = fopen('php://output', 'w+');
            if ($date_from && $date_to) {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].$date_to.'.xls';
            } else {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].date('Y-m-d').'.xls';
            }
            if ($city) {
                $city_ids = implode(',', $city);
                $where[] = "box_zone.city_id in($city_ids)";
            }
            foreach ($finalData as $finalRow) {
                fputcsv($handle, $finalRow, "\t");
            }

            fclose($handle);

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel')
                ->setRawHeader('Content-Disposition: attachment; filename='.$file1)
                ->setRawHeader('Content-Transfer-Encoding: binary')
                ->setRawHeader('Expires: 0')
                ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->setRawHeader('Pragma: public')
                ->sendResponse();

            exit;
        }

        // EXPORT TO PDF ========================================================================

        //require $_SERVER['DOCUMENT_ROOT'].'/library/fpdf/mc_table.php';
        $is_pdf = $this->_getParam('pdf');

        if ($this->_request->isPost() && $is_pdf) {
            set_time_limit(0);
            $model = new Model_BlackList();

            $pdf = new PDF_MC_Table();
            $pdf->Open();
            $pdf->AddPage();
            $pdf->SetWidths([10, 15, 12, 14, 25, 17, 35, 15, 15, 20]);
            srand(microtime() * 1000000);

            $o = 1;
            foreach ($finalData as $row) {
                if ($o == 1) {
                    $pdf->SetFont('Arial', 'B', '8');
                } elseif ($o != 1) {
                    $pdf->SetFont('Arial', '', '7');
                }
                $pdf->Row($row);
                $o++;
            }

            $pdf->Output();
        }
    }

    public function insertAction()
    {
        $id = (int) $this->_getParam('id');
        $model = new Model_BlackList();
        $form = new Admin_Form_BlackList();

        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $values = $form->getValues();
            //$values['gps'] = $values['lat'].", ".$values['lon'];
            $values['plate_no'] = str_replace(' ', '', $values['plate_no']);
            $values['zone'] = $values['zone_code'];
            $row = $model->save($values);
            $id = $row->id;
            $this->_redirect($this->view->baseUrl('blacklist/'));
        }
        $row = $model->find((int) $id)->current();
        if ($row) {
            $form->populate($row->toArray());
        }
        $this->view->form = $form;
    }

    public function tablePdfAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $this->_request->getParam('id');
        $model = new Model_Fines();
        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $data = $model->fetchAll();

        foreach ($data as $fields) {
        }
        $html = $this->view->render('pdf/print.phtml');
        $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 12, $default_font = '', $mgl = 25,
            $mgr = 10, $mgt = 20, $mgb = 0, $mgh = 9, $mgf = 9, $orientation = 'P');
        $pdf->SetDisplayMode('fullpage');
        $pdf->SetFooter('{PAGENO}/{nb}');
        $pdf->writeHTML($html);
        $tmpFileName = md5(time()).'-'.date('YmdHis',
                strtotime($date_from)).'-'.$date_to.'-Juodasis-sarasas.pdf.tmp';
        Core_Utils::serveFilePartial($pdf->Output(null, 'S'), $tmpFileName, 'notification-form'.'-'.date('YmdHis',
                strtotime($date_from)).'-'.$date_to.'-Juodasis-sarasas.pdf');
        die();
    }

    public function importAction()
    {
        if ($this->_request->isPost()) {
            $model = new Model_BlackList();
            if ($_FILES['blacklist']) {
                $content = file_get_contents($_FILES['blacklist']['tmp_name']);
                if (! empty($content)) {
                    $content = mb_convert_encoding($content, 'UTF-8', 'ISO-8859-13');
                    $content = explode(PHP_EOL, $content);
                    //var_dump($content);die();
                    $content[0] = 'zone,plate_no';
                    //var_dump($content);die();
                    $content = implode(PHP_EOL, $content);
                    file_put_contents($_FILES['blacklist']['tmp_name'], $content);
                    $content = Core_Utils::csv_to_array($_FILES['blacklist']['tmp_name'], ',');
                    if (! empty($content)) {
                        foreach ($content as $row) {
                            if ($row['plate_no'] && $row['zone']) {
                                $row['plate_no'] = str_replace(' ', '', $row['plate_no']);
                                $date_validator = new Zend_Validate_Date();

                                $row['cause_id'] = '2';
                                $row['time_checked'] = date('Y-m-d H:i:s');
                                $model->save($row);
                                /*
                                $old_row = $model->fetchRow("plate = '".$row['plate']."' AND group_id = ".$row['group_id']);
                                if($old_row){
                                    $old_row->setFromArray($row);
                                    $old_row->save();
                                }else{
                                    $model->save($row);
                                }*/
                            }
                        }
                        $this->_redirect($this->view->baseUrl('blacklist'));
                    }
                }
            }
        }
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('Black List'),
            'module' => 'admin',
            'controller' => 'blacklist',
            'action' => 'index',
            'class' => 'awe-th-list',
            'a_class' => 'no-submenu',
            'order' => 10,
            'params' => [
                'lang' => LANG,
            ],
            'pages' => [
                'black_list_insert' => [
                    'label' => Core_Locale::translate('Insert'),
                    'module' => 'admin',
                    'controller' => 'blacklist',
                    'action' => 'insert',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
            ],
        ];

        return $nav;
    }
}
