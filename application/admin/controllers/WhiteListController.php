<?php

class Admin_WhitelistController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    public function indexAction()
    {
        $model = new Model_WhiteList();
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $select = $model->getWhiteIndex($id);
        $this->view->filter = $filter = (string)trim($this->_request->getParam('filter'));
        $where = [];
        if ($filter) {
            $where[] =("
                plate LIKE '%$filter%' OR
                group_id LIKE '%$filter%' OR
                company LIKE '%$filter%' OR
                address LIKE '%$filter%'

            ");
        }

        $this->view->paginator_params = [
            'filter' => $filter
        ];

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'contract_date DESC',
            $model->getWhiteListQuery($where, $id)
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;

        $is_csv = $this->_getParam('csv');

        //EXPORT TO CSV ======================================================================

        if ($this->_request->isPost() && $is_csv) {
            $model_zone = new Model_Zone();
            $user_info = $model_zone->getUserInfo($id);

            $file1 = 'Baltasis sąrašas ' . date('Y-m-d') . '.csv';
            $fp = fopen('php://output', 'w+');
            $where = [];

            #$where[] = "user_id = '$id'";
            //$zoneArray = [];
            $cityArray = [];
            foreach ($user_info as $zone) {
                //$zoneArray[] = $zone['zone_id'];
                $cityArray[] = $zone['city_id'];
            }
            $cityIds = array_unique($cityArray);
            $city_ids = implode(',', $cityIds);
            $where[] = "box_zone.city_id in($city_ids)";

            if ($filter) {
                $where[] = "
                plate LIKE '%$filter%' OR
                group_id LIKE '%$filter%' OR
                company LIKE '%$filter%' OR
                address LIKE '%$filter%'

            ";
            }
            $data = $model->WhiteListCSV($where);
            $i = 1;
            fputcsv($fp, [
                'Eiles nr.',
                'Numeris',
                'Įmonė',
                'Data nuo',
                'Data iki',
                'Kontrakto data',
                'Kontrakto suma',
                'Adresas',
                'Grupė',
            ]);
            foreach ($data as $fields) {
                fputcsv($fp, [
                    $i++,
                    $fields['plate'],
                    $fields['company'],
                    $fields['date_from'],
                    $fields['date_to'],
                    $fields['contract_date'],
                    $fields['contract_sum'],
                    $fields['address'],
                    $fields['group'],
                ]);
            }

            fclose($fp);

            if ($file1 !== false) {
                header('Content-type: text/plain; charset=utf-8');
                header('Content-Disposition: attachment; filename="' . $file1 . '"');
                exit;
            }
        }
    }

    public function importAction()
    {
        if ($this->_request->isPost()) {
            $model = new Model_WhiteList();
            if ((int)$_POST['group'] && $_FILES['whitelist']) {
                $content = file_get_contents($_FILES['whitelist']['tmp_name']);
                if (!empty($content)) {
                    $content = mb_convert_encoding($content, 'UTF-8', 'ISO-8859-13');
                    $content = explode(PHP_EOL, $content);
                    //var_dump($content);die();
                    $content[0] = 'contract_no;plate;company;contract_date;date_from;date_to;contract_sum;company_code;pvm_code;address;email;phone;comment';
                    //var_dump($content);die();
                    $content = implode(PHP_EOL, $content);
                    file_put_contents($_FILES['whitelist']['tmp_name'], $content);
                    $content = Core_Utils::csv_to_array($_FILES['whitelist']['tmp_name'], ';');
                    if (!empty($content)) {
                        $model->delete('group_id = ' . (int)$_POST['group']);
                        foreach ($content as $row) {
                            if ($row['plate'] && $row['date_from'] && $row['date_to']) {
                                $row['company'] = str_replace(
                                    ['„', '“'],
                                    ['"', '"'],
                                    $row['company']
                                );
                                $row['plate'] = str_replace(' ', '', $row['plate']);

                                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row['date_from']);
                                $row['date_from'] = $myDateTime->format('Y-m-d H:i:s');

                                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row['date_to']);
                                $row['date_to'] = $myDateTime->format('Y-m-d H:i:s');

                                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row['contract_date']);
                                $row['contract_date'] = $myDateTime->format('Y-m-d');
                                $row['contract_sum'] = (float)str_replace([' ', 'EUR'], '',
                                    strtolower($row['contract_sum']));
                                $date_validator = new Zend_Validate_Date(array('format' => 'Y-m-d H:i:s'));
                                if ($row['plate'] && $date_validator->isValid($row['date_from']) && $date_validator->isValid($row['date_to'])) {
                                    $row['group_id'] = (int)$_POST['group'];
                                    $row['city'] = (int)$_POST['city'];
                                    $row['zone'] = (int)$_POST['zone'];
                                    $model->save($row);
                                    /*
                                    $old_row = $model->fetchRow("plate = '".$row['plate']."' AND group_id = ".$row['group_id']);
                                    if($old_row){
                                        $old_row->setFromArray($row);
                                        $old_row->save();
                                    }else{
                                        $model->save($row);
                                    }*/
                                }
                            }
                        }
                        $this->_redirect($this->view->baseUrl('whitelist'));
                    }
                }
            }
        }

        //tikrinimas kurias zonai priskirtas WLG gali matyti
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $uc = new Model_UserCities();
        $zone = new Model_Zone;
        $zone_wlg = new Model_ZonesWhitelists();
        $wlg_model = new Model_WhiteListGroups();
        $select2 = $uc->select()->from(['z' => $uc->info('name')], ['z.city_id'])->where('active = 1 and user_id = ?',
            (int) $id);
        $select3 = $zone->select()->from(['z' => $zone->info('name')],
            ['z.id'])->where('city_id in (' . $select2 . ')');
        $select4 = $zone_wlg->select()->from(['z' => $zone_wlg->info('name')],
            ['z.wg_list_id'])->where('zone_id in (' . $select3 . ')');

        $this->view->wlg_pairs = $wlg_model->getPairs($wlg_model->fetchAll('visible = 1 and id in(' . $select4 . ')'),
            'id', 'title', false);
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('White List'),
            'module' => 'admin',
            'controller' => 'whitelist',
            'action' => 'index',
            'class' => 'awe-list',
            'a_class' => 'no-submenu',
            'params' => [
                'lang' => LANG,
            ],
            'pages' => [
                'white_list_import' => [
                    'label' => Core_Locale::translate('Import'),
                    'module' => 'admin',
                    'controller' => 'whitelist',
                    'action' => 'import',
                    'visible' => false,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
            ],
        ];

        return $nav;
    }
}
