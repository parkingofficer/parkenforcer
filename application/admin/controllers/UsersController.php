<?php

class Admin_UsersController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    public function viewAction()
    {
        $id = $this->_getParam('id');
        $model = new Model_Admins();
        $this->view->admin = $model->find($id)->current();
    }

    public function insertAction()
    {
        $admins = new model_admins;

        $admin = $admins->fetchAll($admins->select());
        $id = (int) $this->_request->getParam('id');
        $form = new Admin_Form_Admin(['id' => $id]);
        $model = new Model_Admins();
        $g_model = new Model_AdminsHasAdminGroups();
        if ($this->_request->isPost()) {
            if ($form->isValid($_POST)) {
                $values = $form->getValues();

                if (! empty($values['adm_password'])) {
                    $values['adm_password'] = sha1($values['adm_password']);
                } else {
                    unset($values['adm_password']);
                }

                $admin = $model->save($values);
                if (isset($values ['groups']) && ! empty($values ['groups'])) {
                    $g_model->delete("admins_id = $admin->id");
                    foreach ($values ['groups'] as $key => $value) {
                        if ($value && $key != $admin->admin_groups_id) {
                            $g_model->save([
                                'admins_id' => $admin->id,
                                'admin_groups_id' => $key,
                            ]);
                        }
                    }
                }

                /*----------------City select for user---------------------------------------------------------------*/

                $c_model = new  Model_UserCities();
                $c_model->update(['active' => 0, 'deleted_on' => date('Y-m-d H:i:s')],
                    [' user_id='.$id.' and '.$c_model->info('name').'.active = 1']);

                $sql = ('SELECT max(id) FROM box_admins');
                $query = $admins->getAdapter()->query($sql);
                $result = $query->fetchAll();
                //  var_dump($result[0]['max(id)']);

                if (! empty($values['CityName'])) {
                    foreach ($values['CityName'] as $city_id) {
                        $line = $c_model->fetchRow($c_model->info('name').'.user_id = '.(int) $id.' AND '.$c_model->info('name').'.city_id = '.$city_id.' and '.$c_model->info('name').'.active = 1');

                        if (! $id) {
                            $c_model->save([
                                'user_id' => $result[0]['max(id)'],
                                'city_id' => $city_id,
                                'active' => '1',
                            ]);
                        } elseif (! $line) {
                            $c_model->save([
                                'user_id' => $id,
                                'city_id' => $city_id,
                                'active' => '1',
                            ]);
                        } elseif ($line) {
                            $c_model->save([
                                'user_id' => $id,
                                'city_id' => $city_id,
                                'active' => '1',
                            ]);
                        }
                        if (empty($values['CityName'])) {
                            foreach ($values['CityName'] as $city_id) {
                                $c_model->update(['active' => 0, 'deleted_on' => date('Y-m-d H:i:s')],
                                    ['city_id = '.$city_id.'and user_id='.$id.'and active = 1']);
                            }
                        }

                        /*if (empty($values['CityName'])) foreach ($values['CityName'] as $city_id){

                  if($line){
                                $c_model->update(array(
                                    "deleted"=>$line['id'],
                                    "deleted_on"=> time('Y-m-d H:i:s'),
                                ) , "user_id =".$id." AND city_id =".$city_id );
                        }
                   }*/
                    }
                }
                /*-------------------------------------------------------------------------------*/

                $this->_redirect($this->view->baseUrl('users/list'));
            } else {
                $this->view->error = true;
            }
        }

        if ($id) {
            $data = $model->find($id)->current()->toArray();
            //$data ['groups'] [$data [$this->view->type] ['admin_groups_id']] = 1;
            $groups = $g_model->fetchAll("admins_id = $id");
            if ($groups->count() > 0) {
                foreach ($groups as $key => $value) {
                    $data ['groups'] [$value ['admin_groups_id']] = 1;
                }
            }

            $row = $model->find((int) $id)->current();
            $c_model = new  Model_UserCities();
            if ($row) {
                $data = $row->toArray();
                $data['CityName'] = $c_model->getPairs($c_model->fetchAll('user_id = '.(int) $id." AND active='1'"),
                    'city_id', 'city_id');
            }
            $form->populate($data);
        }

        $this->view->form = $form;
    }

    public function listAction()
    {
        $model = new Model_Admins();
        $a_g_model = new Model_AdminGroups();

        $this->registerStandartActions($model);

        $adm_group = Core_Auth_Admin::getInstance()->getStorage()->read()->admin_groups_id;
        $group = $a_g_model->fetchRow('id ='.$adm_group);

        if ($group['city'] && $group['city'] != 0) {
            $groupArray = [];
            $groupsByCity = $a_g_model->fetchAll('city ='.$group['city'], 'title ASC');

            foreach ($groupsByCity as $groups) {
                array_push($groupArray, $groups['id']);
            }

            $this->view->items = $model->getUsersByGroup($groupArray);
        } else {
            $this->view->items = $model->getCityZone();
        }
    }

    public function adminGroupsListAction()
    {
        $model = new Model_AdminGroups();
        $status = $this->registerStandartActions($model);
        $this->view->items = $model->fetchAll()->toArray();
    }

    public function adminGroupInsertAction()
    {
        $id = (int) $this->_getParam('id');
        $model = new Model_AdminGroups();
        $form = new Admin_Form_AdminGroup();
        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $values = $form->getValues();

            foreach ($values as $v) {
                if (is_array($v) && ! empty($v)) {
                    foreach ($v as $k => $p) {
                        $values ['permisions'][$k] = $p;
                        $values ['hide_operators'][$k] = $p;
                        $values ['hide_dashboard'][$k] = $p;
                        $values ['hide_blitzinfo'][$k] = $p;
                        $values ['hide_appreg'][$k] = $p;
                    }
                }
            }

            $values ['permisions'] = serialize($values ['permisions']);
            $values ['hide_operators'] = $values['hide_operators']['hide_operators'];
            $values ['hide_dashboard'] = $values['hide_dashboard']['hide_dashboard'];
            $values ['hide_blitzinfo'] = $values['hide_blitzinfo']['hide_blitzinfo'];
            $values ['hide_appreg'] = $values['hide_appreg']['hide_appreg'];
            $row = $model->save($values);
            $id = $row->id;
            $this->_redirect($this->view->baseUrl("users/admin-group-insert/id/$id"));
        }
        $row = $model->find((int) $id)->current();
        if ($row) {
            $data = $row->toArray();
            $permisions = unserialize($data ['permisions']);
            if (! empty($permisions)) {
                $perms = [];
                foreach ($permisions as $k => $p) {
                    $group = explode('_', $k);
                    $group = $group[0];
                    $data[$group][$k] = $p;
                }
            }
            $form->populate($data);
        }
        $this->view->form = $form;
    }

    /**
     * Navigation.
     *
     * @return Array
     */
    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('Users'),
            'module' => 'admin',
            'controller' => 'users',
            'action' => 'list',
            'class' => 'awe-user',
            'order' => 100,
            'params' => [
                'lang' => LANG,
            ],
            'pages' => [
                'users_view' => [
                    'label' => Core_Locale::translate('User'),
                    'module' => 'admin',
                    'controller' => 'users',
                    'action' => 'view',
                    'visible' => false,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'users_list' => [
                    'label' => Core_Locale::translate('Users List'),
                    'module' => 'admin',
                    'controller' => 'users',
                    'route' => 'defaults',
                    'action' => 'list',
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'users_insert' => [
                    'label' => Core_Locale::translate('Add User'),
                    'module' => 'admin',
                    'controller' => 'users',
                    'action' => 'insert',
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'users_groups_list' => [
                    'label' => Core_Locale::translate('Users Groups List'),
                    'module' => 'admin',
                    'controller' => 'users',
                    'action' => 'admin-groups-list',
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'users_group_insert' => [
                    'label' => Core_Locale::translate('Add Users Group'),
                    'module' => 'admin',
                    'controller' => 'users',
                    'action' => 'admin-group-insert',
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
            ],
        ];

        return $nav;
    }
    /*
     *
     */
}
