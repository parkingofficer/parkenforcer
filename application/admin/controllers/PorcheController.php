<?php

class Admin_PorcheController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    public function indexAction()
    {
        $model = new Model_Porche();
        $services = new Model_PorcheServices();
        $statuses = new Model_PorcheStatuses();

        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $this->view->status = $status = $this->_request->getParam('status');
        $this->view->today = $today = (string) trim($this->_request->getParam('today'));

        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;

        $select = $model->select()->setIntegrityCheck(false)->from(['p' => $model->info('name')]);
        $select->joinLeft(
            $statuses->info('name').' as s',
            's.id = p.status',
            ['s.name as status_name']
        );

        $where = [];
        $select->order('p.id DESC');

        if ($today) {
            $select->where('date_to = ?', date('Y-m-d'));
        } else {
            if ($date_from) {
                $select->where('date_from >= ?', $date_from.' 00:00:00');
            }
            if ($date_to) {
                $select->where('date_to <= ?', $date_to.' 23:59:59');
            }
            if (!$date_from) {
                $this->view->date_from = $date_from = date('Y-m-d', strtotime('-2 days'));
            }

            $this->view->filter = $filter = (string) trim($this->_request->getParam('filter'));
            if ($filter) {
                $select->where("
                person LIKE '%$filter%' OR
                company LIKE '%$filter%' OR
                p.name LIKE '%$filter%' OR
                surname LIKE '%$filter%' OR
                payment_type LIKE '%$filter%' OR
                flight_number LIKE '%$filter%' OR
                status LIKE '%$filter%' OR
                plate LIKE '%$filter%'
            ");
            }

            if ($status) {
                $select->where("s.name = '$status'");
            }

            $this->view->paginator_params = [
                'status' => $status,
                'date_from' => $date_from,
                'date_to' => $date_to,
                'filter' => $filter,
            ];
        }
        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            $where,
            'id DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;

        //EXPORTINGS =========================================================================

        $where = [];

        if ($today) {
            $where[] = 'date_to = '.date('Y-m-d');
        } else {
            if ($date_from) {
                $where[] = "date_from >= '$date_from 00:00:00'";
            }
            if ($date_to) {
                $where[] = "date_to <= '$date_to 23:59:59'";
            }
            if ($status) {
                $where[] = "box_porche_statuses.name = '$status'";
            }
        }

        $this->view->table = $model->info('name');

        $is_csv = $this->_getParam('csv');

        //EXPORT TO CSV ======================================================================

        if ($this->_request->isPost() && $is_csv) {
            if ($date_from && $date_to) {
                $file1 = 'Porche_uzsakymai_'.$date_from.'_iki_'.$date_to.'.csv';
            } else {
                $file1 = 'Porche_uzsakymai_'.$date_from.'_iki_'.date('Y-m-d').'.csv';
            }

            $fp = fopen('php://output', 'w+');

            $data = $model->getPorcheExport($where);

            $i = 1;

            fputcsv($fp, [
                    utf8_decode('Eiles nr.'),
                    utf8_decode('Asmuo'),
                    utf8_decode('Pirkėjas'),
                    utf8_decode('Asmens / Įmonės kodas'),
                    utf8_decode('Apmokėjimo būdas'),
                    utf8_decode('Valst. Numeris'),
                    utf8_decode('Statusas'),
                    utf8_decode('Sutarties tipas'),
                    utf8_decode('Nuo'),
                    utf8_decode('Iki'),
                    utf8_decode('Suma su PVM'),
                ]
            );

            foreach ($data as $fields) {
                fputcsv($fp, [
                        $i++,
                        utf8_decode($fields['person']),
                        utf8_decode(! empty($fields['name_surname']) ? $fields['name_surname'] : $fields['company']),
                        utf8_decode(! empty($fields['person_code']) ? $fields['person_code'] : $fields['company_code']),
                        utf8_decode($fields['payment_type']),
                        utf8_decode($fields['plate']),
                        utf8_decode($fields['status_name']),
                        utf8_decode($fields['blic_type']),
                        utf8_decode($fields['date_from']),
                        utf8_decode($fields['date_to']),
                        utf8_decode($fields['amount_pvm']),
                    ]
                );
            }

            fclose($fp);

            if ($file1 !== false) {
                header('Content-type: text/plain');
                header('Content-Disposition: attachment; filename="'.$file1.'"');
                exit;
            }
        }
        //EXPORT TO EXCEL =====================================================================

        $is_xls = $this->_getParam('xls');
        if ($this->_request->isPost() && $is_xls) {
            set_time_limit(0);

            $filename = '/var/www/vhosts/'.$_SERVER['HTTP_HOST'].'/httpdocs/tmp/excel-'.date('m-d-Y').'.xls';

            if ($date_from && $date_to) {
                $file1 = 'Porche_uzsakymai_'.$date_from.'_iki_'.$date_to.'.xls';
            } else {
                $file1 = 'Porche_uzsakymai_'.$date_from.'_iki_'.date('Y-m-d').'.xls';
            }

            $filename = realpath($filename);

            $handle = fopen('php://output', 'w+');

            $data = $model->getPorcheExport($where);
            // $handle = fopen('php://output', 'w+');
            $finalData = [];
            $i = 1;
            $finalData[] = [
                utf8_decode('Eiles nr.'),
                utf8_decode('Asmuo'),
                utf8_decode('Pirkėjas'),
                utf8_decode('Asmens / Įmonės kodas'),
                utf8_decode('Apmokėjimo būdas'),
                utf8_decode('Valst. Numeris'),
                utf8_decode('Statusas'),
                utf8_decode('Sutarties tipas'),
                utf8_decode('Nuo'),
                utf8_decode('Iki'),
                utf8_decode('Suma su PVM'),
            ];

            foreach ($data as $row) {
                $finalData[] = [
                    utf8_decode($i++),
                    utf8_decode($row['person']),
                    utf8_decode(! empty($row['name_surname']) ? $row['name_surname'] : $row['company']),
                    utf8_decode(! empty($row['person_code']) ? $row['person_code'] : $row['company_code']),
                    utf8_decode($row['payment_type']),
                    utf8_decode($row['plate']),
                    utf8_decode($row['status_name']),
                    utf8_decode($row['blic_type']),
                    utf8_decode($row['date_from']),
                    utf8_decode($row['date_to']),
                    utf8_decode($row['amount_pvm']),
                ];
            }

            foreach ($finalData as $finalRow) {
                fputcsv($handle, $finalRow, "\t");
            }

            fclose($handle);

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel')
                ->setRawHeader('Content-Disposition: attachment; filename='.$file1)
                ->setRawHeader('Content-Transfer-Encoding: binary')
                ->setRawHeader('Expires: 0')
                ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->setRawHeader('Pragma: public')
                ->setRawHeader('Content-Length: '.filesize($filename))
                ->sendResponse();

            exit;
        }
    }

    public function porcheInsertAction()
    {
        $id = (int) $this->_request->getParam('id');
        $model = new Model_Porche();
        $model2 = new Model_PorcheOrderServices();
        $dbPSS = new Model_PorcheServiceStatuses();
        $dbPS = new Model_PorcheStatuses();
        $form = new Admin_Form_Porche(['id' => $id]);

        $row = $model->find((int) $id)->current();
        if ($row) {
            $this->view->date_from = $date_from = $row['date_from'];
            $this->view->date_to = $date_to = $row['date_to'];

            $servicesArray = json_decode($row['services']);
            $this->view->service_array = $servicesArray;

            $form->populate($row->toArray());

            if (!empty($_POST)) {

                $modelPorsche = new Model_Porche();
                $modelData = $row->toArray();

                // to billing data
                $dataStatusToBilling = [];
                $dataSSOneToBilling = [];
                $sendToBilling = 0;

                if ( $modelData['status'] != $_POST['status'] ) {
                    $smsType = $dbPS->getServiceSmsType( $_POST['status'] );
                    $mailType = $dbPS->getServiceMailType( $_POST['status'] );

                    switch ( $modelData['payment_type'] )
                    {
                        case 1:
                            if ( $smsType ) $modelPorsche->sendSMS( $id, $smsType, '', $_POST['status'] );
                            if ( $mailType ) $modelPorsche->sendMail( $id, $mailType, '', $_POST['status'], 1 );
                            break;
                        case 3:
                            $sendToBilling = 1;
                            $dataStatusToBilling = $modelPorsche->getDataStatusesToBilling( $id , $_POST['status'] );
                            if ( $mailType ) $modelPorsche->sendMail( $id, $mailType, '', $_POST['status'], 3 );
                            break;
                        default:
                            echo "false";
                            break;
                    }
                }

                $r = $model->getAdapter()->quoteInto('id = ?', $id);
                $model->update([
                    "plate" => $_POST['plate'],
                    "date_from" => $_POST['date_from'],
                    "date_to" => $_POST['date_to'],
                    "flight_number" => $_POST['flight_number'],
                    "name" => $_POST['name'],
                    "phone" => $_POST['phone'],
                    "email" => $_POST['email'],
                    "address" => $_POST['address'],
                    "birth_date" => $_POST['birth_date'],
                    "tech_doc_nr" => $_POST['tech_doc_nr'],
                    "insurance_nr" => $_POST['insurance_nr'],
                    "amount" => $_POST['amount'],
                    "mileage_before" => $_POST['mileage_before'],
                    "mileage_after" => $_POST['mileage_after'],
                    "company" => $_POST['company'],
                    "payment_type" => $_POST['payment_type'],
                    "status" => $_POST['status'],
                    "amount_pvm" => $_POST['amount_pvm'],
                    "person_company_code" => $_POST['person_company_code'],
                ], $r);

                foreach($servicesArray as $s){

                    $statusId = $_POST['service_status_'.$s->service_id.'_'.$s->count_id];
                    $POSStatusId = $model2->getServiceStatus( $id, $s->service_id );

                    if ( $statusId != $POSStatusId ) {
                        $smsType = $dbPSS->getServiceSmsType($statusId, $s->service_id);
                        $mailType = $dbPSS->getServiceMailType($statusId, $s->service_id);

                        switch ( $modelData['payment_type'] )
                        {
                            case 1:
                                if ( $smsType ) $modelPorsche->sendSMS( $id, $smsType, $s->service_id );
                                if ( $mailType ) $modelPorsche->sendMail( $id, $mailType, $s->service_id, "", 1 );
                                break;
                            case 3:
                                $sendToBilling = 1;
                                array_push ($dataSSOneToBilling, [ "Id" => $s->service_id , "Status" => $dbPSS->getBillingId( $statusId, $s->service_id ) ]);
                                if ( $mailType ) $modelPorsche->sendMail( $id, $mailType, $s->service_id, "", 3 );
                                break;
                            default:
                                echo "false";
                                break;
                        }
                    }

                    $where = array();
                    $where[] = $model2->getAdapter()->quoteInto('porche_id = ?', $id);
                    $where[] = $model2->getAdapter()->quoteInto('service_id = ?', $s->service_id);
                    $where[] = $model2->getAdapter()->quoteInto('count_id = ?', $s->count_id);
                    $model2->update(['status_id' => $statusId], $where);
                }

                if ( $sendToBilling )
                {
                    $dataSSToBilling = [ "Statuses" => $model2->getAllStatusDataToBilling( $id, $dataSSOneToBilling ) ];
                    $dataStatusToBilling = ( empty( $dataStatusToBilling )) ? $modelPorsche->getDataStatusesToBilling( $id ) : $dataStatusToBilling;
                    $dataToBilling = array_merge( $dataStatusToBilling, $dataSSToBilling );

                    $api = new Core_Api_Oxid();
                    $api->AdditionalServiceStatusChange( $dataToBilling );
                }

                $this->_redirect($this->view->baseUrl('porche'));
            }
        }

        $this->view->porche = $model->fetchrow('id = '.$id);
        $this->view->form = $form;
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('Porche'),
            'module' => 'admin',
            'controller' => 'porche',
            'action' => 'index',
            'class' => 'awe-th-list',
            'a_class' => 'no-submenu',
            'params' => [
                'lang' => LANG,
            ],
        ];

        return $nav;
    }
}
