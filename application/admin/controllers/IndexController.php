<?php

class Admin_IndexController extends Zend_Controller_Action
{
    public function init()
    {
        $adm_group = Core_Auth_Admin::getInstance()->getStorage()->read()->admin_groups_id;
        if ($adm_group == 16) {
            $this->_redirect($this->view->baseUrl('blitz/'));
            die();
        }
    }

    public function indexAction()
    {
        $this->view->dashboard = true;
        $this->view->date_from = $date_from = $this->getParam('date_from') ? $this->getParam('date_from') : date('Y-m-d 00:00');
        $this->view->date_to = $date_to = $this->getParam('date_to') ? $this->getParam('date_to') : date('Y-m-d H:i');
        $this->view->operators = $operators = $this->getParam('operators') ? $this->getParam('operators') : null;
        $this->view->city = $city = $this->getParam('city') ? $this->getParam('city') : null;

        $time_by = null;
        $time_cols = [];

        if (date('z', strtotime($date_from)) == date('z', strtotime($date_to))) {
            $time_by = 'G';
            for ($i = 8; $i <= 22; $i++) {
                $time_cols[$i] = 0;
            }
        } elseif (date('W', strtotime($date_from)) == date('W', strtotime($date_to))) {
            $time_by = 'N';
            for ($i = 1; $i <= 7; $i++) {
                $time_cols[$i] = 0;
            }
        } elseif (date('n', strtotime($date_from)) == date('n', strtotime($date_to))) {
            $time_by = 'j';
            for ($i = 1; $i <= 31; $i++) {
                $time_cols[$i] = 0;
            }
        } elseif (date('Y', strtotime($date_from)) == date('Y', strtotime($date_to))) {
            $time_by = 'n';
            for ($i = 1; $i <= 12; $i++) {
                $time_cols[$i] = 0;
            }
        } else {
            $time_by = 'Y';
            for ($i = date('Y', strtotime($date_from)); $i <= date('Y', strtotime($date_to)); $i++) {
                $time_cols[$i] = 0;
            }
        }

        //PLATES CHECK

        $info = new Model_UserCities();
        $ui = new Model_City();
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $zones = new Model_UserCities();
        $select2 = $zones->select()->from(['z' => $zones->info('name')],
            ['z.city_id'])->where('active = 1 and user_id = ?', (int) $id);//gaunu prisijungusio varotojo matomus miestus,

        //$where[] = "adm.id IN (".implode(',',$operators).")";

        $this->view->city_list = $ui->fetchAll(' id in ('.$select2.')');
        $pcl_model = new Model_PlateCheckLog();

        $plateCheckLogMessages = $pcl_model->getPlateCheckLogMessages();
        $where = [];
        $admin = new Model_Admins();
        $adminGroup = new Model_AdminGroups();
        $operatorsGroupId = $adminGroup->getOperatorGroups();
        if ($date_from) {
            $where[] = "time >= '$date_from'";
        }
        if ($date_to) {
            $where[] = "time <= '$date_to'";
        }
        if ($operators) {
            $where[] = 'adm.id IN ('.implode(',', $operatorsGroupId).')';
        }

        if ($city) {
            $zones_model = new Model_Zone();
            $where[] = $zones_model->info('name').'.city_id IN ('.implode(',', $city).')';
            $plate_check_data = $pcl_model->getPlateChecks($where, null);
            $citys = $info->select()->from(['x' => $info->info('name')],
                ['x.user_id'])->where('city_id in ('.implode(',',
                    $city).') and active = 1');// gaunu prisijungusio vartotojo matomu miestu id varototoju taip pat matanciu siuos miestus,

            $city_operators = $admin->fetchAll(' admin_groups_id in('.implode(',',
                    $operatorsGroupId).') AND adm_active = 1 and id in('.$citys.')');
            $this->view->operators_list = $city_operators;
        } else {
            $city_operators = $admin->fetchAll(' admin_groups_id in('.implode(',',
                    $operatorsGroupId).') AND adm_active = 1 and id =0');
            $this->view->operators_list = $city_operators;
        }
        //--------

        $model = new Model_Fines();
        $select = $model->select()->from(['p' => $model->info('name')])->setIntegrityCheck(false);
        $select->joinLeft($admin->info('name').' as adm', 'adm.adm_device_id = p.adm_device_id', 'adm.id as adm_id');

        if ($date_from) {
            $select->where("p.date >= '$date_from'");
        }
        if ($date_to) {
            $select->where("p.date <= '$date_to'");
        }
        if ($operators) {
            $select->where('adm.id IN ('.implode(',', $operators).')');
        }
        //
        $model_zone = new Model_Zone();

        $zone = new Model_Zone();
        $model = new Model_Fines();
        if ($city) {
            $array = $model_zone->select()->from(['z' => $model_zone->info('name')],
                ['z.id'])->where("city_id in('".implode(',', $city)."')");
            $select->where('zone in ('.$array.')');
            $this->view->operators_list = $city_operators;
            $select_payed_in_24_hours = clone $select;
            $select_payed_in_24_hours->where('status_id = ?', Model_Fines::STATUS_FINE_PAYED);
            $select_fined = clone $select;
            $select_fined->where('status_id = ?', Model_Fines::STATUS_FINE_GENERATED);
            $fined = $model->fetchAll($select_fined);
            $all_fined = $model->fetchAll($select);
            // Die(var_dump($array).'miestai: '.implode(',',$city));
        } else {
            $adm = new Model_Admins();
            $city_operators = $adm->fetchAll(' admin_groups_id in('.implode(',',
                    $operatorsGroupId).') AND adm_active = 1 and id =0');
            $this->view->operators_list = $city_operators;
            $select_payed_in_24_hours = clone $select;
            $select_payed_in_24_hours->where('status_id = ?', Model_Fines::STATUS_NOTICE);
            $select_fined = clone $select;
            $select_fined->where('status_id = ?', Model_Fines::STATUS_NOTICE);
            $fined = $model->fetchAll($select_fined);
            $all_fined = $model->fetchAll('id=0');
        }

        $payed_in_24_hours = $model->fetchAll($select_payed_in_24_hours);
        $payed_in_24_hours_report = $time_cols;
        $payed_in_24_hours_report_count = 0;
        if (! empty($payed_in_24_hours)) {
            foreach ($payed_in_24_hours as $i) {
                $payed_in_24_hours_report[date($time_by, strtotime($i['date']))] += 1;
                $payed_in_24_hours_report_count++;
            }
        }

        $fined_report = $time_cols;
        $fined_report_count = 0;
        if (! empty($fined)) {
            foreach ($fined as $i) {
                $fined_report[date($time_by, strtotime($i['date']))] += 1;
                $fined_report_count++;
            }
        }

        //$all_fined = clone $select;

        $all_fined_report = $time_cols;
        $all_fined_report_count = 0;
        if (! empty($all_fined)) {
            foreach ($all_fined as $i) {
                $all_fined_report[date($time_by, strtotime($i['date']))] += 1;
                $all_fined_report_count++;
            }
        }

        $plate_check_fined_report = $time_cols;
        $plate_check_fined_report_count = 0;
        if (! empty($plate_check_data['items'])) {
            foreach ($plate_check_data['items'] as $i) {
                $plate_check_fined_report[date($time_by, strtotime($i['time']))] += 1;
                $plate_check_fined_report_count++;
            }
        }

        $this->view->time_cols = $time_cols;
        $this->view->report_by_fine_types = [
            Core_Locale::translate('Viso patikrinimų ').'('.$plate_check_fined_report_count.')' => $plate_check_fined_report,
            Core_Locale::translate('Surašyta baudų ').'('.$all_fined_report_count.')' => $all_fined_report,
            Core_Locale::translate('Sumokėta per 24val. ').'('.$payed_in_24_hours_report_count.')' => $payed_in_24_hours_report,
            Core_Locale::translate('Išsiųsta savivaldybei ').'('.$fined_report_count.')' => $fined_report,
        ];

        $operators_log = [];
        if (! empty($plate_check_data['items'])) {
            foreach ($plate_check_data['items'] as $i) {
                if (! isset($operators_log[$i['admin']])) {
                    $operators_log[$i['admin']] = $plateCheckLogMessages;
                }
                if (isset($operators_log[$i['admin']][$i['msg']])) {
                    $operators_log[$i['admin']][$i['msg']] += 1;
                }
            }
        }
        $this->view->operators_log_x_axis = $time_cols;
        //unset($operators_log['']);
        $this->view->operators_log = $operators_log;
        $this->view->plateCheckLogMessages = $plateCheckLogMessages;
    }

    public function mapAction()
    {
        $this->view->map = true;
        $this->view->date_from = $date_from = $this->getParam('date_from') ? $this->getParam('date_from') : date('Y-m-d 00:00');
        $this->view->date_to = $date_to = $this->getParam('date_to') ? $this->getParam('date_to') : date('Y-m-d H:i');
        $this->view->operators = $operators = $this->getParam('operators') ? $this->getParam('operators') : null;
        $this->view->city = $city = $this->getParam('city') ? $this->getParam('city') : null;
        $this->view->show_operators = $show_operators = $this->getParam('show_operators') ? $this->getParam('show_operators') : null;
        $this->view->show_city = $show_city = $this->getParam('show_city') ? $this->getParam('show_city') : null;

        //default domain param

        $domain_model = new Model_Domain();
        $options = $domain_model->options();
        $this->view->default_map_gps = $options['default_map_gps'];
        $this->view->zones_gps_parameters = $options['zones_gps_parameters'];

        /* CheckBox kintamieji filtruojami pagal prisijungusio vartotojo galimus miestus*/
        $info = new Model_UserCities();
        $ui = new Model_City();
        $model2 = new Model_Admins();
        $adminGroup = new Model_AdminGroups();
        $operatorsGroupId = $adminGroup->getOperatorGroups();
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $zones = new Model_UserCities();
        $select2 = $zones->select()->from(['z' => $zones->info('name')],
            ['z.city_id'])->where("active = '1' and user_id = ?", (int) $id);//gaunu prisijungusio varotojo matomus miestus,
        //$citys = $info->select()->from(array('x' => $info->info('name')), array('x.user_id'))->where("city_id in (" . $select2 . ")");
        // $city_operators = $model2->fetchAll(' admin_groups_id in ('.implode(',',$operatorsGroupId).') AND adm_active = 1 and id in(' . $citys . ')');
        //  $this->view->operators_list = $city_operators;
        $this->view->city_list = $ui->fetchAll(' id in ('.$select2.')');
        //--------------END OF Chart checkbox------------------//

        $model = new Model_PlateCheckLog();
        $where = [];
        if ($date_from) {
            $where[] = "time >= '$date_from'";
        }
        if ($date_to) {
            $where[] = "time <= '$date_to'";
        }
        if ($operators) {
            $where[] = 'adm.id IN ('.implode(',', $operators).')';
        }
        if ($city) {
            $zones_model = new Model_Zone();
            $where[] = $zones_model->info('name').'.city_id'.' IN ('.implode(',', $city).')';
            $plate_check_data = $model->getPlateChecks($where, null);
            $citys = $info->select()->from(['x' => $info->info('name')],
                ['x.user_id'])->where('city_id in ('.implode(',',
                    $city).') and active = 1');// gaunu prisijungusio vartotojo matomu miestu id varototoju taip pat matanciu siuos miestus,
            $city_operators = $model2->fetchAll(' admin_groups_id in ('.implode(',',
                    $operatorsGroupId).') AND adm_active = 1 and id in('.$citys.')');
            $this->view->operators_list = $city_operators;
        } else {
            $city_operators = $model2->fetchAll(' admin_groups_id in ('.implode(',',
                    $operatorsGroupId).') AND adm_active = 1 and id =0');
            $this->view->operators_list = $city_operators;
        }

        // MAP
        $model = new Model_Fines();
        $model2 = new Model_Admins();
        $fines_select = $model->select()->from(['p' => $model->info('name')])->setIntegrityCheck(false);
        $fines_select->joinLeft($model2->info('name').' as adm', 'adm.adm_device_id = p.adm_device_id',
            ['adm.id as adm_id', 'CONCAT(adm.adm_name," ",adm.adm_surname) as admin']);
        if ($date_from) {
            $fines_select->where("p.date >= '$date_from'");
        }
        if ($date_to) {
            $fines_select->where("p.date <= '$date_to'");
        }
        if ($operators) {
            $fines_select->where('adm.id IN ('.implode(',', $operators).')');
        }

        if ($city) {
            $where[] = 'adm.city_id IN ('.implode(',', $city).')';
            $this->view->operators_list = $city_operators;
        } else {
            $city_operators = $model2->fetchAll(' admin_groups_id in ('.implode(',',
                    $operatorsGroupId).') AND adm_active = 1 and id =0');
            $this->view->operators_list = $city_operators;
        }

        if ($show_operators) {
            $markers = [
                'pointer_green',
                'pointer_red_inside',
                'pointer_green_inside',
                'pointer_red',
                'pointer_blue',
                'pointer_blue_inside',
            ];
            $this->view->operators_report = [];

            if (! empty($plate_check_data['items'])) {
                foreach ($plate_check_data['items'] as $i) {
                    if (in_array($i['admin_groups_id'], [implode(',', $operatorsGroupId)])) {
                        if (! isset($this->view->operators_report[$i['admin']]['marker'])) {
                            $this->view->operators_report[$i['admin']]['marker'] = array_pop($markers);
                        }
                        $this->view->operators_report[$i['admin']]['data'][] = [
                            'gps' => $i['gps'],
                            'plate' => $i['plate'],
                            'car' => $i['car'],
                            'msg' => $i['msg'],
                        ];
                    }
                }
            }

            $select_fines = clone $fines_select;

            $select_fines->where('p.status_id = ?', Model_Fines::STATUS_FINE_GENERATED);
            $select_fines->where('p.status_id = ?', Model_Fines::STATUS_WAITING_FOR_FINE_GENERATION);
            $fines = $model->fetchAll($select_fines);
            if ($fines->count()) {
                foreach ($fines as $i) {
                    $this->view->operators_report[$i['admin']]['data'][] = [
                        'gps' => $i['gps'],
                        'plate' => $i['plate'],
                        'car' => $i['car'],
                    ];
                }
            }

            $this->renderScript('index/operators_map.phtml');
        } else {
            $this->view->paid_log = [];
            $this->view->black_list_log = [];
            $this->view->white_list_log = [];
            $this->view->zone_time_end_log = [];

            if (! empty($plate_check_data['items'])) {
                foreach ($plate_check_data['items'] as $i) {
                    if ($i['msg'] == 'Paid') {
                        $this->view->paid_log[] = [
                            'gps' => $i['gps'],
                            'plate' => $i['plate'],
                            'car' => $i['car'],
                        ];
                    }
                    if ($i['msg'] == 'Plate is in black list') {
                        $this->view->black_list_log[] = [
                            'gps' => $i['gps'],
                            'plate' => $i['plate'],
                            'car' => $i['car'],
                        ];
                    }
                    if ($i['msg'] == 'Plate is in white list') {
                        $this->view->white_list_log[] = [
                            'gps' => $i['gps'],
                            'plate' => $i['plate'],
                            'car' => $i['car'],
                        ];
                    }
                    if ($i['msg'] == 'Zonos darbo laikas baigėsi') {
                        $this->view->zone_time_end_log[] = [
                            'gps' => $i['gps'],
                            'plate' => $i['plate'],
                            'car' => $i['car'],
                        ];
                    }
                }
            }

            if ($city) {
                $select_red = clone $fines_select;
                $select_blue = clone $fines_select;
                $select_red->where('p.status_id = ?', Model_Fines::STATUS_FINE_GENERATED);
                $select_blue->where('p.status_id = ?', Model_Fines::STATUS_WAITING_FOR_FINE_GENERATION);
            } else {
                $select_red = clone $fines_select;
                $select_blue = clone $fines_select;
                $select_red->where('p.status_id = ?', Model_Fines::STATUS_NOTICE);
                $select_blue->where('p.status_id = ?', Model_Fines::STATUS_NOTICE);
            }

            $this->view->map_red_parkings = $model->fetchAll($select_red);
            $this->view->map_blue_parkings = $model->fetchAll($select_blue);
        }
    }

    public function saveTranslationAction()
    {
        $model = new Model_Translate();
        $model->save($this->_request->getPost());
        die();
    }
}
