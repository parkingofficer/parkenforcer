<?php

class Admin_AppRegController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    public function indexAction()
    {
        $model = new Model_Register();

        $select = $model->getAppReg();

        $this->view->filter = $filter = (string) trim($this->_request->getParam('filter'));

        if ($filter) {
            $select->where("
                number_plate LIKE '%$filter%' OR
                phone_number LIKE '%$filter%' OR
                email LIKE '%$filter%'
                ");
        }

        $this->view->paginator_params = [
            'filter' => $filter,
        ];

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            null,
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('App Registrations'),
            'module' => 'admin',
            'controller' => 'appreg',
            'action' => 'index',
            'class' => 'awe-th-list',
            'a_class' => 'no-submenu',
            'order' => 10,
            'params' => [
                'lang' => LANG,
            ],
        ];

        return $nav;
    }
}
