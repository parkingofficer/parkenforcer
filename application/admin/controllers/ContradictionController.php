<?php
/**
 * Created by PhpStorm.
 * User: viktoras
 * Date: 19.4.17
 * Time: 10.16
 */

class Admin_ContradictionController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    protected $statusNew;
    protected $statusAccepted;
    protected $statusRejected;

    function __construct(
        Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = array()
    ) {
        parent::__construct($request, $response, $invokeArgs);

        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $keys = $config->getOption('contradiction');

        $this->statusNew = $keys['status_new'];
        $this->statusAccepted = $keys['status_accepted'];
        $this->statusRejected = $keys['status_rejected'];
    }

    public function indexAction()
    {
        /*$this->view->date_from = $date_from = $this->_request->getParam('date_from') ?: date('Y-m-d', strtotime('-2 days'));*/
        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $this->view->city = $city = $this->getParam('city') ?: [];
        $this->view->zone = $zone = $this->getParam('zone') ?: [];
        $this->view->status = $status = $this->getParam('status') ?: [];
        $this->view->keyword = $keyword = (string) trim($this->_request->getParam('keyword'));
        $model_contradiction = new Model_Contradiction();

        $userCities = (new Model_City())->userCities();
        $this->view->city_list = $userCities;
        $this->view->zones_list = (new Model_Zone())->getUserInfo(user()->id);
        $this->view->status_list = $this->statuses();
        $this->view->operators_list = (new Model_AdminGroups())->getUserOperators($userCities);

        $this->view->filter = $filter = trim($this->_request->getParam('keyword'));
        $select = (new \App\Models\Filters\ContradictionFilters($this->_request))->apply();

        $this->view->paginator_params = [
            'city' => $city,
            'zone' => $zone,
            'status' => $status,
            'filter' => $filter
        ];

        $paginator = Core_Paginator::create(
            $model_contradiction,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            $where,
            'add_date DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;
    }

    public function viewAction()
    {
        $id = (int) $this->_request->getParam('id');
        $status = (int) $this->_request->getParam('status');
        $car = $this->_request->getParam('car');
        $model = new Model_Fines();
        $domain_model = new Model_Domain();
        $options = $domain_model->options();
        $model_contradiction = new Model_Contradiction();

        $select = $model_contradiction->select();
        $select->where('fine_id = ?', $id);
        $select->where('`deleted_at` IS NULL');
        $contradiction = $model_contradiction->fetchRow($select);

        if ($this->_request->isPost() && $car) {
            (new Model_Fines)->changeCar($id, $car);
            die();
        }

        if ($status && $status == Model_Fines::STATUS_FINE_GENERATED) {
            (new Model_Fines)->markAsGenerated($id);
            $this->_redirect($this->view->baseUrl('contradiction/view/id/'.$id));
        }

        if ( $contradiction ){
            $contra_array = $contradiction->toArray();
        }
        $this->view->contradiction = $contra_array;
        $this->view->fine = $model->getFine($id);
        $this->view->locale = ($options['locale']);
        $this->view->fine_rates = collect((new Model_FineRates())->fetchAll())
            ->pluck('title', 'rate');

        $fp_model = new Model_FinesPayments();
        $this->view->fine_payments = $fp_model->fetchAll('fine_id = '.$id);

        if (! $this->view->fine_payments->count()) {
            $this->view->payments = $fp_model->fetchAll("fine_id IS NULL AND deleted IS NULL and payment_type = 'C'");
        }

        $this->view->log = (new Model_FineLog)->fetchAll('fine_id = '. $id, 'created_at DESC');
    }

    public function changeContradictionStatusAction()
    {
        $id = (int) $this->_request->getParam('fine_id');
        $status = (int) $this->_request->getParam('status');

        if ($this->_request->isPost() && $id && $status) {
            try {
                (new Model_Contradiction())->changeStatus($id, $status);
                $model_fine = new Model_Fines();

                $fineId = (new Model_Contradiction)->getContradictionFineId( $id );
                $fileName = (new Model_Contradiction)->getContradictionFileName( $id );

                if ( $fineId && $fileName && $status != $this->statusNew ) {
                    (new Model_Contradiction)->sendMail( $fineId );
                    if ( Model_Fines::STATUS_PERSON_INFORMED && $status == $this->statusRejected ) {
                        $fine = $model_fine->find($fineId)->current();
                        $fine->change_status_date = date('Y-m-d H:i:s');
                        $fine->save();
                    }
                }

                if ( $fineId && $status == $this->statusAccepted && $status != $this->statusNew ) {
                    $model_fine->changeStatus( $fineId,Model_Fines::STATUS_FINE_DISMISSED );
                }

                $this->_helper->json([
                    'success' => true,
                    'new_status' => $this::statuses($status)
                ]);

            } catch (\Exception $e) {
                $this->_helper->json([
                    'success' => false,
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        exit;
    }

    public function addFileAction()
    {
        $model = new Model_Contradiction();
        $fine_id = $this->_request->getParam('fine_id');

        $select = $model->select();
        $select->where('fine_id = ?', $fine_id);
        $select->where('`deleted_at` IS NULL');
        $contra_array = $model->fetchRow($select);
        $contradiction_id = $contra_array['id'];

        $contradiction = $model->find( $contradiction_id )->current();

        if ($this->_request->isPost()) {
            if (!empty($_FILES)) {
                $model->getAdapter()->beginTransaction();
                try {
                    foreach ($_FILES as $k => $info) {
                        if ($info['name'] && $info['error'] == 0) {
                            $upload = new Core_Utils_Upload();
                            $upload->uploadFile(
                                FINES_PHOTOS_PATH . '/' . $fine_id . '/',
                                'md5',
                                10,
                                [$k => $info]
                            );

                            if (!$upload->_files[$k]) {
                                throw new \Exception('Upload failed');
                            } else {
                                $contradiction->{$k} = $upload->_files[$k];
                                $contradiction->{'full_path'} = FINES_PHOTOS_PATH . '/' . $fine_id . '/';
                                $contradiction->{'file_type'} = $_FILES[$k]['type'];
                                $contradiction->save();
                            }
                        }
                    }

                    $model->getAdapter()->commit();

                    $status =  (new Model_Contradiction)->getContradictionStatus( $contradiction_id );
                    $fileName = (new Model_Contradiction)->getContradictionFileName( $contradiction_id );

                    if ( $fine_id && $fileName && $status != $this->statusNew ) {
                        (new Model_Contradiction)->sendMail( $fine_id );
                    }

                } catch (\Exception $e) {
                    $model->getAdapter()->rollBack();
                    dd($e->getMessage());
                    $this->view->success = false;
                    $this->view->msg = 'File upload Error';
                }
            }
        }

        $this->redirect('/admin/en/contradiction/view/id/'.$fine_id);
    }

    /**
     * @param null $key
     *
     * @return \Illuminate\Support\Collection|null
     */
    public static function statuses($key = null)
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $keys = $config->getOption('contradiction');

        $statuses = [
            $keys['status_new'] => \Core_Locale::translate('New contradiction'), //'Naujas',
            $keys['status_accepted'] => \Core_Locale::translate('Accepted contradiction'), //'Patvirtintas',
            $keys['status_rejected'] => \Core_Locale::translate('Rejected contradiction') //'Atmestas'
        ];

        if ($key) {
            return array_key_exists($key, $statuses) ? $statuses[$key] : null;
        }

        return collect($statuses);
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('Contradiction'),
            'module' => 'admin',
            'controller' => 'contradiction',
            'action' => 'index',
            'class' => 'awe-warning-sign',
            'a_class' => 'no-submenu',
            'params' => [
                'lang' => LANG,
            ],
            'pages' => [
                'contradiction_list' => [
                    'label' => Core_Locale::translate('Contradiction list'),
                    'module' => 'admin',
                    'controller' => 'contradiction',
                    'action' => 'index',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'contradiction_view' => [
                    'label' => Core_Locale::translate('View contradiction'),
                    'module' => 'admin',
                    'controller' => 'contradiction',
                    'action' => 'view',
                    'visible' => false,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
            ],
        ];

        return $nav;
    }
}