<?php

class Admin_ImageController extends Zend_Controller_Action
{
    public function indexAction()
    {
        die('image');
    }

    public function resizeAction()
    {
        //Zend_Debug::dump($this->_request->getParams());
        //Zend_Debug::dump($this->_request->getParam("type"));
        $type = $this->_request->getParam('type');
        $image = str_replace('//', '/', base64_decode($this->_request->getParam('image')));
        $thumb = Core_GdThumb_PhpThumbFactory::create($image);
        $size = explode('x', $type);
        $thumb->adaptiveResize($size[0], $size[1]);
        $thumb->show();
        die();
    }

    public function uploadToWebAction()
    {
        if (! empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = APPLICATION_PATH.'/../'.$_REQUEST['folder'].'/';
            //$targetFile =  str_replace('//','/',$targetPath) . $_FILES['Filedata']['name'];
            $targetFile = str_replace('//', '/', $targetPath).$_REQUEST['microtime'].$_FILES['Filedata']['name'];
            $_FILES['Filedata']['name'] = $_REQUEST['microtime'].$_FILES['Filedata']['name'];

            move_uploaded_file($tempFile, $targetFile);
            echo '1';
        } else {
            die();
        }
    }

    public function uploadToAdminAction()
    {
        if (! empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $targetPath = ZETS_PATH_ROOT_ADMIN.$_REQUEST['folder'].'/';
            $targetFile = str_replace('//', '/', $targetPath).substr(sha1(microtime()), 1,
                    6).$_FILES['Filedata']['name'];

            move_uploaded_file($tempFile, $targetFile);
            echo '1';
        } else {
            die();
        }
    }
}
