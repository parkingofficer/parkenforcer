<?php

class Admin_FinesController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    public function indexAction()
    {
        $model = new Model_Fines();
        $translates = json_decode(Core_Locale::translate('export_headers_fines_list'));
        $this->view->date_from = $date_from = $this->_request->getParam('date_from') ?: date('Y-m-d', strtotime('-2 days'));
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $this->view->city = $city = $this->getParam('city') ?: [];
        $this->view->zone = $zone = $this->getParam('zone') ?: [];
        $this->view->status = $status = $this->getParam('status') ?: [];
        $this->view->operator = $operator = $this->getParam('operator') ?: [];
        $this->view->keyword = $keyword = (string) trim($this->_request->getParam('keyword'));

        $select = (new \App\Models\Filters\FineFilters($this->_request))->apply();

        $userCities = (new Model_City())->userCities();
        $this->view->city_list = $userCities;
        $this->view->zones_list = (new Model_Zone())->getUserInfo(user()->id);
        $this->view->status_list = (new Model_Fines())->statuses();
        $this->view->operators_list = (new Model_AdminGroups())->getUserOperators($userCities);

        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;

        $domain_model = new Model_Domain();
        $options = $domain_model->options();

        $this->view->paginator_params = [
            'date_from' => $date_from,
            'date_to' => $date_to,
            'filter' => $keyword,
            'city' => $city,
            'zone' => $zone,
            'status' => $status,
            'operator' => $operator,
            'locale' => $options['locale']
        ];

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'date DESC',
            $select
        );

        $allData = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            '',
            null,
            'date DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');

        $this->view->paginator = $paginator;
        $this->view->allData = $allData;

        if ($this->_request->isPost()) {
            $zone_model = new Model_Zone();
            $now = time();
            $download_file_name = $translates->filename[0] . $date_from . $translates->filename[1] . date('Y-m-d') . '.xls';
            if ($date_from && $date_to) {
                $download_file_name = $translates->filename[0] . $date_from . $translates->filename[1] . $date_to . '.xls';
            }

            if ($this->_request->isPost('xls') || $this->_request->isPost('pdf')) {

                $data = $model->fetchAll($select);

                $finalData = [];
                $i = 1;
                foreach ($translates->csv as $headerRow) {
                    $header[] = $headerRow;
                }
                $finalData[] = $header;
                foreach ($data as $row) {
                    $zoneName = $zone_model->fetchRow("zone_code = '".$row['zone_code']."'");
                    $lastDay = strtotime($row['date']);
                    $datediff = $now - $lastDay;
                    $days = floor($datediff / (60 * 60 * 24));

                    $finalData[] = [
                        //utf8_decode($i++),
                        $row['id_public'],
                        Model_Fines::statuses($row['status_id']),
                        $row['date'],
                        $days*-1,//MATH
                        $row['city'],
                        $row['zone_code'],
                        $zoneName->zone_name,
                        $row['plate'],
                        $row['car'],
                        $row['cause'],
                        $row['default_amount'],
                        Model_Fines::finalFineAmount($row['amount'], $row['rate']),
                        $row['paid'],
                        $row['admin'],
                    ];
                }
            }
            //EXPORTINGS =========================================================================

            //EXPORT TO CSV ======================================================================
            if ($this->_getParam('csv')) {
                if ($date_from && $date_to) {
                    $download_file_name = $translates->filename[0] . $date_from . $translates->filename[1] . $date_to . '.csv';
                } else {
                    $download_file_name = $translates->filename[0] . $date_from . $translates->filename[1] . date('Y-m-d') . '.csv';
                }
                $fp = fopen('php://output', 'w+');
                fwrite($fp, chr(239) . chr(187) . chr(191)); // Write the BOM to the beginning of the file

                $data = $model->fetchAll($select);

                fputcsv($fp, $translates->csv);
                foreach ($data as $row) {
                    //$zoneName = $zone_model->fetchRow($zone_model->select('zone_code = ?', $row['zone_code']));
                    $zoneName = $zone_model->fetchRow("zone_code = '".$row['zone_code']."'");
                    $firstDay = new Zend_Date($now, 'YYYY-MM-dd');
                    $lastDay = new Zend_Date($row['date'], 'YYYY-MM-dd');
                    $diff = $lastDay->sub($firstDay)->toValue();
                    $days = (ceil($diff/60/60/24) +1)*-1;

                    fputcsv($fp, [
                        $row['id_public'],
                        Model_Fines::statuses($row['status_id']),
                        $row['date'],
                        $days,//MATH
                        $row['city'],
                        $row['zone_code'],
                        $zoneName->zone_name,
                        $row['plate'],
                        $row['car'],
                        $row['cause'],
                        $row['default_amount'],
                        $row['amount'],
                        $row['paid'],
                        $row['admin']
                    ]);
                }

                fclose($fp);

                if ($download_file_name !== false) {
                    header('Content-type: text/plain; charset=utf-8');
                    header('Content-Disposition: attachment; filename="' . $download_file_name . '"');
                    exit;
                }
            }

            //EXPORT TO EXCEL =====================================================================
            if ($this->_getParam('xls')) {

                set_time_limit(0);

                $filename = '/var/www/vhosts/' . $_SERVER['HTTP_HOST'] . '/httpdocs/tmp/excel-' . date('m-d-Y') . '.xls';
                $filename = realpath($filename);

                $handle = fopen('php://output', 'w+');
                fwrite($handle, chr(239) . chr(187) . chr(191)); // Write the BOM to the beginning of the file
                foreach ($finalData as $finalRow) {
                    fputcsv($handle, $finalRow, ";");
                }

                fclose($handle);

                $this->_helper->layout->disableLayout();
                $this->_helper->viewRenderer->setNoRender();

                $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel; charset=UTF-8LE')
                    ->setRawHeader('Content-Disposition: attachment; filename=' . $download_file_name)
                    ->setRawHeader('Expires: 0')
                    ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                    ->setRawHeader('Pragma: public')
                    ->setRawHeader('Content-Length: ' . filesize($filename))
                    ->sendResponse();

                exit;
            }

            // EXPORT TO PDF ========================================================================
            if ($this->_getParam('pdf')) {
                set_time_limit(0);

                $pdf = new PDF_MC_Table();
                $pdf->Open();
                $pdf->AddPage();

                $pdf->SetWidths([10, 10, 12, 15, 25, 25, 20, 17, 20]);
                srand(microtime() * 1000000);

                $o = 1;
                foreach ($finalData as $row) {
                    if ($o == 1) {
                        $pdf->SetFont('Arial', 'B', '8');
                    } elseif ($o != 1) {
                        $pdf->SetFont('Arial', '', '7');
                    }
                    $pdf->Row($row);
                    $o++;
                }

                $pdf->Output();
            }
            //EXPORT TO ATASKAITA ========================
            if ($this->_getParam( 'atas')) {
                ini_set('memory_limit', '1024M');
                set_time_limit(0);

                $where = [];
                if ($date_from) {
                    $where[] = "start >= '$date_from 00:00:00'";
                }
                if ($date_to) {
                    $where[] = "start <= '$date_to 23:59:59'";
                }
                if ($date_from && $date_to) {
                    $file1 = $date_from . '_' . $date_to . $translates->atas->filename . '.xls';
                } else {
                    $file1 = $date_from . '_' . date('Y-m-d') . $translates->atas->filename . '.xls';
                }

                if ($operator) {
                    $where[] = "operator_name = '$operator'";
                }

                $model_zone = new Model_Zone();
                $user_info = $model_zone->getUserInfo($id);
                if ($city) {
                    $city_ids = implode(',', $city);
                    $where[] = "box_zone.city_id in($city_ids)";
                } else {
                    $zoneArray = [];
                    foreach ($user_info as $zone) {
                        $zoneArray[] = $zone['zone_id'];
                    }
                    $city_ids = implode(',', $zoneArray);
                    $where[] = "box_zone.id in($city_ids)";
                }

                $data = $model->getCityZoneCsv($where);

                foreach ($data as $item) {
                    $z_name[$item['zone_name']] = $item['zone_name'];
                    $array[] = [
                        $item['zone_name'],
                        date_format(new DateTime($item['date']), 'Y-m-d'),
                        $item['fine'],
                        $item['paid']
                    ];
                }

                $objPHPExcel = new PHPExcel();
                $objPHPExcel->setActiveSheetIndex(0);
                $objPHPExcel->getActiveSheet()->SetCellValue('A1', $translates->atas->zone);
                $objPHPExcel->getActiveSheet()->SetCellValue('B1', $translates->atas->date);
                $objPHPExcel->getActiveSheet()->SetCellValue('C1', $translates->atas->amount);
                $objPHPExcel->getActiveSheet()->SetCellValue('D1', $translates->atas->other);

                if (empty($date_to)) {
                    $date_to = date('Y-m-d');
                }

                $date_to = date('Y-m-d H:i:s', strtotime($date_to . ' +1 day'));

                // Dates
                $period = new DatePeriod(
                    new DateTime($date_from),
                    new DateInterval('P1D'),
                    new DateTime($date_to)
                );
                foreach ($period as $date) {
                    $dateArray[] = $date->format('Y-m-d');
                }

                $zoneRowCount = 2;
                $dateRowCount = 2;
                $sumRowCount = 2;

                $mergeFrom = 2;
                $mergeTo = 2;

                foreach ($z_name as $zone) {
                    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $mergeFrom,
                        $zone)->getStyle('A' . $mergeFrom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                    foreach ($dateArray as $dateItem) {
                        $objPHPExcel->getActiveSheet()->SetCellValue('B' . $dateRowCount, $dateItem);
                        $a = 0;
                        $b = 0;
                        foreach ($array as $key => $item) {
                            if ($item['0'] == $zone) {
                                if ($item['1'] == $dateItem) {
                                    $a += $item['2'];
                                    $b += $item['3'];
                                    unset($array[$key]);
                                }
                            }
                        }

                        $objPHPExcel->getActiveSheet()->SetCellValue('C' . $sumRowCount, $a ? $a : 0);
                        $objPHPExcel->getActiveSheet()->SetCellValue('D' . $sumRowCount, $b ? $b : 0);
                        $sumRowCount++;
                        $dateRowCount++;
                        $mergeTo++;
                    }

                    $objPHPExcel->getActiveSheet()->mergeCells('A' . $mergeFrom . ':A' . ($mergeTo - 1));
                    $mergeFrom = $mergeTo;
                    $zoneRowCount++;
                }

                // Redirect output to a client’s web browser (Excel5)
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename=' . $file1);
                header('Cache-Control: max-age=0');

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
                $objWriter->save('php://output');
                exit;
            }

            if ($this->_getParam( 'zip')) {
                ini_set('memory_limit', '1024M');
                set_time_limit(0);

                $idArray = [];
                foreach ( $data as $item ){
                    $idArray [] = $item['id'];
                }

                $zip = new ZipArchive();
                $zipname="finesPdf_".date( 'YmdHis' ).".zip";
                $zip->open(TMP_FILES_PATH."/".$zipname, ZipArchive::CREATE);
                $files = $this->generateFinePdf ( $idArray );
                foreach ($files as $file) {
                    $zip->addFile(TMP_FILES_PATH."/".$file, $file);
                }
                $zip->close();

                header('Content-Type: application/zip');
                header('Content-disposition: attachment; filename='.$zipname);
                header('Content-Length: ' . filesize(TMP_FILES_PATH."/".$zipname));
                readfile(TMP_FILES_PATH."/".$zipname);

                $this->removeTmpfiles( array_merge( $files, array( $zipname ) ) );
                die();
            }

            if (
                $this->_getParam( 'change_fine_status') &&
                !empty($this->_getParam( 'fines')) &&
                $this->_getParam( 'changeStatus')
            ) {
                $status = $this->_getParam( 'changeStatus');
                $fineIdArray = $this->_getParam( 'fines');

                $model->changeArrayFineStatus( $fineIdArray, $status );
            }
        }
    }

    public function viewAction()
    {
        $id = (int) $this->_request->getParam('id');
        $status = (int) $this->_request->getParam('status');
        $car = $this->_request->getParam('car');
        $model = new Model_Fines();
        $domain_model = new Model_Domain();
        $options = $domain_model->options();

        if ($this->_request->isPost() && $car) {
            (new Model_Fines)->changeCar($id, $car);
            die();
        }

        if ($status && $status == Model_Fines::STATUS_FINE_GENERATED) {
            (new Model_Fines)->markAsGenerated($id);
            $this->_redirect($this->view->baseUrl('fines/view/id/'.$id));
        }

        if ( !empty( $zoneAddress['zone_address'] )) {
            $fine = $model->getFine($id);
            $zone_model = new Model_Zone();
            $zoneAddress = $zone_model->getZoneAddress($fine['zone']);
            $this->view->zone_address = $zoneAddress;
            $this->view->fine = $fine;
            $this->view->fine['street'] = $zoneAddress['zone_address'];
        } else {
            $this->view->fine = $model->getFine($id);
        }

        $this->view->locale = ($options['locale']);
        $this->view->fine_rates = collect((new Model_FineRates())->fetchAll())
            ->pluck('title', 'rate');

        $fp_model = new Model_FinesPayments();
        $this->view->fine_payments = $fp_model->fetchAll('fine_id = '.$id);

        if (! $this->view->fine_payments->count()) {
            $this->view->payments = $fp_model->fetchAll("fine_id IS NULL AND deleted IS NULL and payment_type = 'C'");
        }

        $this->view->log = (new Model_FineLog)->fetchAll('fine_id = '. $id, 'created_at DESC');
    }

    public function changePhotosAction()
    {
        $model = new Model_Fines;
        $fine_id = $this->_request->getParam('fine_id');
        $fine = $model->find($fine_id)->current();

        if ($this->_request->isPost()) {
            if (!empty($_FILES)) {
                $model->getAdapter()->beginTransaction();
                try {
                    foreach ($_FILES as $k => $info) {
                        if ($info['name'] && $info['error'] == 0) {
                            $upload = new Core_Utils_Upload();
                            $upload->uploadFile(
                                FINES_PHOTOS_PATH . '/' . $fine_id . '/',
                                'md5',
                                10,
                                [$k => $info]
                            );

                            if (!$upload->_files[$k]) {
                                throw new \Exception('Upload failed');
                            } else {
                                $fine->{$k} = $upload->_files[$k];
                                $fine->save();
                            }
                        }
                    }

                    $model->getAdapter()->commit();

                } catch (\Exception $e) {
                    $model->getAdapter()->rollBack();
                    dd($e->getMessage());
                    $this->view->success = false;
                    $this->view->msg = 'File upload Error';
                }
            }
        }

        $this->redirect('/admin/en/fines/view/id/'.$fine_id);
    }

    public function changeFineAmountAction()
    {
        $fine_id = (int) $this->_request->getParam('fine_id');
        $amount = (double) $this->_request->getParam('amount');

        if ($this->_request->isPost() && $fine_id && $amount && Model_Admins::isRoot()) {
            try {
                (new Model_Fines())->changeAmount($fine_id, $amount);

                $this->_helper->json([
                    'success' => true
                ]);

            } catch (\Exception $e) {
                $this->_helper->json([
                    'success' => false,
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        exit;
    }

    public function changeFineRateAction()
    {
        $fine_id = (int) $this->_request->getParam('fine_id');
        $rate = (double) $this->_request->getParam('rate');

        if ($this->_request->isPost() && $fine_id && $rate) {
            try {
                (new Model_Fines())->changeRate($fine_id, $rate);

                $this->_helper->json([
                    'success' => true
                ]);

            } catch (\Exception $e) {
                $this->_helper->json([
                    'success' => false,
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        exit;
    }

    public function changeFineStatusAction()
    {
        $fine_id = (int) $this->_request->getParam('fine_id');
        $status = (int) $this->_request->getParam('status');

        if ($this->_request->isPost() && $fine_id && $status) {
            try {
                (new Model_Fines())->changeStatus($fine_id, $status);

                $this->_helper->json([
                    'success' => true,
                    'new_status' => Model_Fines::statuses($status)
                ]);

            } catch (\Exception $e) {
                $this->_helper->json([
                    'success' => false,
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        exit;
    }

    public function dismissAction()
    {
        $fine_id = (int) $this->_request->getParam('fine_id');

        if ($this->_request->isPost() && $fine_id) {
            try {
                (new Model_Fines())->dismiss($fine_id);

                $this->_helper->json([
                    'success' => true
                ]);

            } catch (\Exception $e) {
                $this->_helper->json([
                    'success' => false,
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        exit;
    }

    public function addManualPaymentAction()
    {
        $fine_id = (int) $this->_request->getParam('fine_id');
        $date = date('Y-m-d', strtotime($this->_request->getParam('date')));
        $amount = (double) $this->_request->getParam('amount');

        if ($this->_request->isPost() && $fine_id && $date && $amount) {
            try {
                (new Model_Fines())->addManualPayment($fine_id, $date, $amount);

                $this->_helper->json([
                    'success' => true
                ]);

            } catch (\Exception $e) {
                $this->_helper->json([
                    'success' => false,
                    'error' => true,
                    'message' => $e->getMessage()
                ]);
            }
        }

        exit;
    }

    public function usePaymentAction()
    {
        $this->_helper->layout()->disableLayout();
        $archive_no = $this->getParam('archive_no');
        $fine_id = (int) $this->getParam('fine_id');
        $plate = $this->getParam('plate');

        if ($archive_no && $fine_id) {
            (new Model_Fines)->associatePayment($archive_no, $fine_id, $plate);
        }
        die();
    }

    public function plateCheckLogAction()
    {
        $translates = json_decode(Core_Locale::translate('export_headers_plate_check_log'));
        $model = new Model_PlateCheckLog();
        $this->view->message = $message = $this->_request->getParam('message');
        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        if (! $date_from) {
            $this->view->date_from = $date_from = date('Y-m-d', strtotime('-2 days'));
        }
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $this->view->filter = $filter = (string) trim($this->_request->getParam('filter'));
        $select = $model->getPlateCheckSelect();
        if ($date_from) {
            $select->where('time >= ?', $date_from.' 00:00:00');
        }
        if ($date_to) {
            $select->where('time <= ?', $date_to.' 23:59:59');
        }
        if ($message) {
            $select->where("response like '%$message%'COLLATE utf8_bin");
        }

        if ($filter) {
            $select->where("
                adm_name LIKE '%$filter%' OR
                adm_surname LIKE '%$filter%' OR
                plate LIKE '%$filter%' OR
                time LIKE '%$filter%' OR
                zone_code LIKE '%$filter%' OR
                CityName LIKE '%$filter%'
            ");
        }
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $select->where('user_id = ?', $id);
        $select->where('active = 1');
        $this->view->paginator_params = [
            'message' => $message,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'filter' => $filter,
        ];
        if ($this->_getParam('page') > 1) {
            $select->limit(DEFAULT_PAGINATION_ITEMS, DEFAULT_PAGINATION_ITEMS * ($this->_getParam('page') - 1));
        } else {
            $select->limit(DEFAULT_PAGINATION_ITEMS, DEFAULT_PAGINATION_ITEMS * ($this->_getParam('page')));
        }

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'time DESC',
            $select
        );
        $this->view->page = $this->_getParam('page');

        $this->view->paginator = $paginator;

        if ($this->_request->isPost('xls') || $this->_request->isPost('pdf')) {
            $where = [];
            if ($date_from) {
                $where[] = "time >= '$date_from 00:00:00'";
            }
            if ($date_to) {
                $where[] = "time <= '$date_to 23:59:59'";
            }
            //$where[] = "user_id = '$id'";
            //$where[] = "active = 1 ";
            $model_zone = new Model_Zone();
            $user_info = $model_zone->getUserInfo($id);
            $zoneArray = [];
            foreach ($user_info as $zone) {
                $zoneArray[] = $zone['zone_id'];
            }
            $city_ids = implode(',', $zoneArray);
            $where[] = "box_zone.id in($city_ids)";

            $data = $model->getPlateChecksCsv($where);

            $finalData = [];
            $i = 1;
            $finalData[] = $translates->csv;

            foreach ($data as $row) {
                $r = json_decode($row['response'], true);
                $finalData[] = [
                    utf8_decode($i++),
                    utf8_decode($row['admin']),
                    utf8_decode($row['plate']),
                    utf8_decode($row['time']),
                    utf8_decode($r['response'] ? 'FALSE' : 'TRUE'),
                    utf8_decode($r['msg']),
                    utf8_decode($row['gps']),
                    iconv('UTF-8', 'windows-1252', $row['zone_code']),
                    iconv('UTF-8', 'windows-1252', $row['city']),
                ];
            }
        }
        //EXPORTINGS =========================================================================

        $this->view->table = $model->info('name');
        $is_csv = $this->_getParam('csv');
        //EXPORT TO CSV ======================================================================
        $model = new Model_PlateCheckLog();
        if ($this->_request->isPost() && $is_csv) {
            if ($date_from && $date_to) {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].$date_to.'.csv';
            } else {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].date('Y-m-d').'.csv';
            }
            $fp = fopen('php://output', 'w+');

            $where = [];
            if ($date_from) {
                $where[] = "time >= '$date_from 00:00:00'";
            }
            if ($date_to) {
                $where[] = "time <= '$date_to 23:59:59'";
            }
            //$where[] = "user_id = '$id'";
            //$where[] = "active = 1 ";
            $model_zone = new Model_Zone();
            $user_info = $model_zone->getUserInfo($id);
            $zoneArray = [];
            foreach ($user_info as $zone) {
                $zoneArray[] = $zone['zone_id'];
            }
            $city_ids = implode(',', $zoneArray);
            $where[] = "box_zone.id in($city_ids)";

            $data = $model->getPlateChecksCsv($where);
            $i = 1;
            fputcsv($fp, $translates->csv);
            foreach ($data as $fields) {
                $r = json_decode($fields['response'], true);
                fputcsv($fp, [
                    $i++,
                    $fields['admin'],
                    $fields['plate'],
                    $fields['time'],
                    json_decode($r['response']) ? 'FALSE' : 'TRUE',
                    $r['msg'],
                    $fields['gps'],
                    $fields['zone_code'],
                    $fields['city'],
                ]);
            }

            fclose($fp);

            if ($file1 !== false) {
                header('Content-type: text/plain; charset=utf-8');
                header('Content-Disposition: attachment; filename="'.$file1.'"');
                exit;
            }
        }
        //EXPORT TO EXCEL =====================================================================

        $is_xls = $this->_getParam('xls');
        if ($this->_request->isPost() && $is_xls) {
            set_time_limit(0);
            if ($date_from && $date_to) {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].$date_to.'.xls';
            } else {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].date('Y-m-d').'.xls';
            }

            $handle = fopen('php://output', 'w+');

            foreach ($finalData as $finalRow) {
                fputcsv($handle, $finalRow, "\t");
            }

            fclose($handle);

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel; charset=utf-8')
                ->setRawHeader('Content-Disposition: attachment; filename='.$file1)
                ->setRawHeader('Content-Transfer-Encoding: binary')
                ->setRawHeader('Expires: 0')
                ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->setRawHeader('Pragma: public')
                ->sendResponse();

            exit;
        }

        // EXPORT TO PDF ========================================================================

        //require $_SERVER['DOCUMENT_ROOT'].'/library/fpdf/mc_table.php';
        $is_pdf = $this->_getParam('pdf');

        if ($this->_request->isPost() && $is_pdf) {
            set_time_limit(0);
            $pdf = new PDF_MC_Table();
            $pdf->Open();
            $pdf->AddPage();

            $pdf->SetWidths([10, 20, 15, 15, 22, 25, 35, 10, 15]);
            srand(microtime() * 1000000);

            $o = 1;
            foreach ($finalData as $row) {
                if ($o == 1) {
                    $pdf->SetFont('Arial', 'B', '8');
                } elseif ($o != 1) {
                    $pdf->SetFont('Arial', '', '7');
                }
                $pdf->Row($row);
                $o++;
            }

            $pdf->Output();
        }
    }

    public function notificationPdfAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $this->_request->getParam('id');

        $model = new Model_Fines();

        if ($id) {
            $fine = $model->getFine($id);
            $zones = new Model_Zone();
            $blacklistModel = new Model_BlackListCauses();
            $city = new Model_City();

            $domain_model = new Model_Domain();
            $options = $domain_model->options();

            $zone = $zones->fetchRow($zones->select()->where('id = ?', $fine['zone']));
            $blackList = $blacklistModel->fetchRow($blacklistModel->select()->where('id = ?', $fine['causes_id']));
            $citys = $zones->fetchRow($zones->select()->where('id='.$fine['zone']));

            if ( !empty( $zoneAddress['zone_address'] )) {
                $zoneAddress = $zones->getZoneAddress($fine['zone']);
                $this->view->zone_address = $zoneAddress;
                $this->view->fine = $fine;
                $this->view->fine['street'] = $zoneAddress['zone_address'];
            } else {
                $this->view->fine = $fine;
            }

            $cityDataArray = $city->getCity( $citys['city_id'] );

            $fine_date = new DateTime($fine['date']); //current date/time
            $payToHours = $zone->warning_pay_time;
            $fine_date->add(new DateInterval("PT".$payToHours."H"));
            $this->view->fineTime  =$fine_date->format('d.m.Y');
            $this->view->zone = $zone;
            $this->view->blackList = $blackList;
            $html = $this->view->render('fines/'.$citys['city_id'].'.phtml');

            if (strpos($options['name'], 'parkingee.unipark.lt') !== false && $citys['city_id'] != 3) {
                $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'ESTONIA', $default_font_size = 14, $default_font = '', $mgl = 0,
                    $mgr = 0, $mgt = 0, $mgb = 0, $mgh = 0, $mgf = 0, $orientation = 'L');

            }else
            {
                $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 12, $default_font = '', $mgl = 25,
                    $mgr = 10, $mgt = 20, $mgb = 0, $mgh = 9, $mgf = 9, $orientation = 'P');

            }
            
            $pdf->SetDisplayMode('fullpage');
            $pdf->writeHTML($html);
            if ( $cityDataArray && $cityDataArray['fineType'] == 1 ) {
                $tmpFileName = md5(time()) . '-' . $fine['id_public'] . '-' . $fine['plate'] . '.pdf.tmp';
                Core_Utils::serveFilePartial($pdf->Output(null, 'S'), $tmpFileName,
                    'notification-form' . '-' . $fine['id_public'] . '-' . $fine['plate'] . '.pdf');
            } else {
                $tmpFileName = md5(time()) . '-' . date('YmdHis',
                        strtotime($fine['date'])) . '-' . $fine['plate'] . '.pdf.tmp';
                Core_Utils::serveFilePartial($pdf->Output(null, 'S'), $tmpFileName,
                    'notification-form-' . '-' . date('YmdHis',
                        strtotime($fine['date'])) . '-' . $fine['plate'] . '.pdf');
            }
        }
    }

    public function sendFinesAction()
    {
        $model = new Model_Fines();

        $domain_model = new Model_Domain();
        $options = $domain_model->options();
        $this->view->billing = $options['billing'];
        if ($this->_request->isPost()) {
            $municipality = $this->_request->getParam('send_municipality');
            if ($municipality) {
                $fines = $this->_request->getParam('fines');
                $model->setdFinesNotificationsMunicipality($fines);
            }
            $mail = $this->_request->getParam('email');
            $fines = $this->_request->getParam('fines');
            $doc_type = $this->_request->getParam('doc_type');
            if ($mail && ! empty($fines)) {
                $model->setdFinesNotifications($mail, $fines, $doc_type);
            }
        }
        $where = [];
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $where[] = 'user_id= '.$id;
        $where[] = 'status_id = '.Model_Fines::STATUS_WAITING_FOR_FINE_GENERATION;
        $where[] = 'active = 1 ';

        $this->view->items = $model->getFine(null, $where);
    }

    /**
     * Import LITAS ESIS.
     */
    public function paymentsImportAction()
    {
        $model = new Model_FinesPayments();

        if ($this->_request->isPost()) {
            if ($_FILES['payments']) {
                $domain_model = new Model_Domain();
                $options = $domain_model->options();
                switch ($options['locale']) {
                    case 'lt':
                        $model->importPaymentsLT($_FILES);
                        break;
                    case 'lv':
                        $model->importPaymentsLV($_FILES);
                        break;
                    case 'ee':
                        $model->importPaymentsEE($_FILES);
                        break;
                }
            } elseif ($_POST['archive_no'] && $_POST['plate']) {
                $payment = $model->find($this->getParam('archive_no'))->current();
                if ($payment) {
                    $payment->plate = $this->getParam('plate');
                    $payment->save();
                    die();
                } else {
                    throw new Zend_Exception('No payment found');
                }
            } elseif ($_POST['archive_no'] && $_POST['delete_item']) {
                $payment = $model->find($this->getParam('archive_no'))->current();
                if ($payment) {
                    $adm = Core_Auth_Admin::getInstance()->getStorage()->read();

                    $payment->deleted = date('Y-m-d H:i:s');
                    $payment->deleted_by = $adm->adm_name.' '.$adm->adm_surname;
                    $payment->save();
                    die();
                } else {
                    throw new Zend_Exception('No payment found');
                }
            }
        }

        $select = $model->select()->from(['p' => $model->info('name')]);

        $select->where("plate = '' and deleted IS NULL and payment_type <> 'D'");
        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'time DESC',
            $select
        );

        $select = $model->select()->from(['p' => $model->info('name')]);
        $select->where("fine_id IS NULL and deleted IS NULL and payment_type <> 'D'");

        $paginator2 = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'time DESC',
            $select
        );
        $this->view->paginator = $paginator;
        $this->view->paginator2 = $paginator2;
    }

    public function paymentsHistoryAction()
    {
        $model = new Model_FinesPayments();
        $translates = json_decode(Core_Locale::translate('export_headers_payment_history'));
        if ($this->_request->isPost()) {
            if ($_POST['recover_item']) {
                $payment = $model->find($this->getParam('recover_item'))->current();
                if ($payment) {
                    $adm = Core_Auth_Admin::getInstance()->getStorage()->read();
                    $payment->deleted = null;
                    $payment->deleted_by = null;
                    $payment->save();
                } else {
                    throw new Zend_Exception('No payment found');
                }
            }
        }

        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        if (! $date_from) {
            $this->view->date_from = $date_from = date('Y-m-d', strtotime('-50 days'));
        }

        $this->view->only_deleted = $only_deleted = $this->_request->getParam('only_deleted');

        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $select = $model->select()->from(['p' => $model->info('name')]);
        $select->columns(new Zend_Db_Expr('FORMAT(p.amount_cnt / 100,2) as amount'));

        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $zones = new Model_UserCities();
        $select2 = $zones->select()->from(['z' => $zones->info('name')],
            ['z.city_id'])->where('active = 1 and user_id = ?', (int) $id);
        $city = new Model_City();
        $city = $city->fetchall('id in('.$select2.')');
        $cities = [];
        foreach ($city as $c) {
            $cities[] = $c['id'];
        }
        #if(in_array("1", $cities)) {

        if ($date_from) {
            $select->where("time >= '$date_from 00:00:00'");
        }

        if ($date_to) {
            $select->where("time <= '$date_to 23:59:59'");
        }

        if ($only_deleted) {
            $select->where('deleted is NOT NULL');
        }
        $this->view->filter = $filter = (string) trim($this->_request->getParam('filter'));
        if ($filter) {
            $select->where("
                payment_text LIKE '%$filter%' OR
                payer_name LIKE '%$filter%' OR
                plate LIKE '%$filter%' OR
                 deleted LIKE '%$filter%' OR
                deleted_by LIKE '%$filter%'

            ");
        }
        #}else{
        #    $select->where("time <= '2000-01-01 23:59:59'");
        #}
        $select->where("payment_type <> 'D'");

        $this->view->paginator_params = [
            'date_from' => $date_from,
            'date_to' => $date_to,
            'only_deleted' => $only_deleted,
            'filter' => $filter,
        ];

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'time DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;

        if ($this->_request->isPost('csv') || $this->_request->isPost('xls') || $this->_request->isPost('pdf')) {
            if ($date_from) {
                $select->where("time >= '$date_from 00:00:00'");
            }
            if ($date_to) {
                $select->where("time <= '$date_to 23:59:59'");
            }
            $data = $model->fetchAll($select);

            $finalData = [];
            $i = 1;
            $finalData[] = $translates->csv;
            foreach ($data as $row) {
                $finalData[] = [
                    utf8_decode($i++),
                    utf8_decode($row['time']),
                    utf8_decode($row['payment_text']),
                    utf8_decode($row['payer_name']),
                    utf8_decode($row['amount_cnt']),
                    utf8_decode($row['plate']),
                    utf8_decode($row['deleted']),
                    utf8_decode($row['deleted_by']),
                ];
            }
        }
        if ($_POST['xls-paymnet']) {
            ($date_from) ? $date_p = date('Y-m', strtotime($date_from)) : $date_p = date('Y-m');
            $db = Zend_Registry::get('db');

            $select = $db->select()
                ->from(['p' => 'box_fines_payments'], [
                    'plate' => 'p.plate',
                    'payer' => 'p.payer_name',
                    'fine_paid_date' => 'p.time',
                    'sum' => 'p.amount_cnt',
                ])
                ->joinLeft(['l' => 'box_fines'], 'l.id = p.fine_id', ['fine_date' => 'l.date'])
                ->joinLeft(['z' => 'box_zone'], 'z.id = l.zone', ['zone' => 'z.zone_code'])
                ->where("p.payment_type = 'C' and p.time like '".$date_p."%'");

            $data = $db->fetchAll($select);

            $finalData = [];
            $i = 1;
            //$finalData[] = $translates->csv;
            $finalData[] = array(
                Core_Locale::translate('xls_p_eil'),
                Core_Locale::translate('xls_p_auto'),
                Core_Locale::translate('xls_p_payer'),
                Core_Locale::translate('xls_p_data'),
                Core_Locale::translate('xls_p_fdata'),
                Core_Locale::translate('xls_p_zone'),
                Core_Locale::translate('xls_p_sum'),
                );

            foreach ($data as $row) {
                $finalData[] = [
                    $i++,
                    $row['plate'],
                    $row['payer'],
                    $row['fine_paid_date'],
                    $row['fine_date'],
                    $row['zone'],
                    $a[] = number_format(($row['sum'] / 100), 2, '.', ''),
                ];

                $finalDataSum = [
                    '',
                    '',
                    '',
                    '',
                    '',
                    Core_Locale::translate('xls_p_tsum'),
                    number_format((array_sum($a)), 2, '.', ''),
                ];
            }
        }

        //EXPORTINGS =========================================================================

        $this->view->table = $model->info('name');

        $is_csv = $this->_getParam('csv');

        //EXPORT TO CSV ======================================================================
        $model = new Model_FinesPayments();

        if ($this->_request->isPost() && $is_csv) {
            if ($date_from && $date_to) {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].$date_to.'.csv';
            } else {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].date('Y-m-d').'.csv';
            }
            $fp = fopen('php://output', 'w+');

            if ($date_from) {
                $select->where("time >= '$date_from 00:00:00'");
            }
            if ($date_to) {
                $select->where("time <= '$date_to 23:59:59'");
            }

            $data = $model->fetchAll($select);
            $i = 1;

            fputcsv($fp, $translates->csv);
            foreach ($data as $fields) {
                fputcsv($fp, [
                    $i++,
                    $fields['time'],
                    $fields['payment_text'],
                    $fields['payer_name'],
                    $fields['amount_cnt'],
                    $fields['plate'],
                    $fields['deleted'],
                    $fields['deleted_by'],
                ]);
            }

            fclose($fp);

            if ($file1 !== false) {
                header('Content-type: text/plain; charset=utf-8');
                header('Content-Disposition: attachment; filename="'.$file1.'"');
                exit;
            }
        }
        //EXPORT TO EXCEL =====================================================================

        $is_xls = $this->_getParam('xls');
        if ($this->_request->isPost() && $is_xls) {
            set_time_limit(0);
            if ($date_from && $date_to) {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].$date_to.'.xls';
            } else {
                $file1 = $translates->filename[0].$date_from.$translates->filename[1].date('Y-m-d').'.xls';
            }

            $filename = $_SERVER['DOCUMENT_ROOT'].'/tmp/excel-'.date('m-d-Y').'.xls';
            $handle = fopen('php://output', 'w+');
            $filename = realpath($filename);

            fwrite($handle, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));

            foreach ($finalData as $finalRow) {
                fputcsv($handle, $finalRow, "\t");
            }

            fclose($handle);

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel')
                ->setRawHeader('Content-Disposition: attachment; filename='.$file1)
                ->setRawHeader('Content-Transfer-Encoding: binary')
                ->setRawHeader('Expires: 0')
                ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->setRawHeader('Pragma: public')
                ->setRawHeader('Content-Length: '.filesize($filename))
                ->sendResponse();

            exit;
        }

        // EXPORT TO PDF ========================================================================

        //require $_SERVER['DOCUMENT_ROOT'].'/library/fpdf/mc_table.php';
        $is_pdf = $this->_getParam('pdf');

        if ($this->_request->isPost() && $is_pdf) {
            set_time_limit(0);

            $pdf = new PDF_MC_Table();
            $pdf->Open();
            $pdf->AddPage();

            $pdf->SetWidths([10, 20, 30, 20, 15, 20, 20, 30]);
            srand(microtime() * 1000000);

            $o = 1;
            foreach ($finalData as $row) {
                if ($o == 1) {
                    $pdf->SetFont('Arial', 'B', '8');
                } elseif ($o != 1) {
                    $pdf->SetFont('Arial', '', '7');
                }
                $pdf->Row($row);
                $o++;
            }

            $pdf->Output();
        }

        // To carry out the payment of fines exporting excel ==========================================

        $is_xls_payment = $this->_getParam('xls-paymnet');
        if ($this->_request->isPost() && $is_xls_payment) {
            (! $finalDataSum) ? $finalDataSum = [] : $finalDataSum = $finalDataSum;

            $file1 = Core_Locale::translate('xls_p_title').' '.date('Y-m', strtotime($date_from)).'.xls';

            $filename = $_SERVER['DOCUMENT_ROOT'].'/tmp/excelPayment'.date('m-d-Y').'.xls';
            $handle = fopen('php://output', 'w+');
            $filename = realpath($filename);

            fwrite($handle, $bom = (chr(0xEF).chr(0xBB).chr(0xBF)));
            foreach ($finalData as $finalRow) {
                fputcsv($handle, $finalRow, ';');
            }
            fputcsv($handle, $finalDataSum, ';');

            fclose($handle);

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel')
                ->setRawHeader('Content-Disposition: attachment; filename='.$file1)
                ->setRawHeader('Content-Transfer-Encoding: binary')
                ->setRawHeader('Expires: 0')
                ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->setRawHeader('Pragma: public')
                ->setRawHeader('Content-Length: '.filesize($filename))
                ->sendResponse();

            exit;
        }
    }

    public function passiveScanningAction()
    {
        $model = new Model_AllPlates();

        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        if (! $date_from) {
            $this->view->date_from = $date_from = date('Y-m-d', strtotime('-1 days'));
        }
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $this->view->filter = $filter = (string) trim($this->_request->getParam('filter'));
        $select = $model->getPlates();
        if ($date_from) {
            $select->where('time >= ?', $date_from.' 00:00:00');
        }
        if ($date_to) {
            $select->where('time <= ?', $date_to.' 23:59:59');
        }

        if ($filter) {
            $select->where("
                plate LIKE '%$filter%' OR
                gps LIKE '%$filter%' OR
                `box_all_plates`.`adm_device_id` LIKE '%$filter%' OR
                zone_code LIKE '%$filter%' OR
                time LIKE '%$filter%'

            ");
        }

        $this->view->paginator_params = [
            'date_from' => $date_from,
            'date_to' => $date_to,
            'filter' => $filter,
        ];
        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'time DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;
    }

    public function importXlsxAction () {
        $model = new Model_FinesPaymentInsertXlsx();
        $select = $model->getFailImport();
        $this->view->importMessageClass = "";
        $this->view->importMessageStyle = "display: none;";

        if ($this->_request->isPost())
        {
            if ($_FILES['payments'])
            {
                $result = $model->importPaymentsEEFromXLSX( $_FILES );
                switch ( $result ) :
                    case 0 :
                        $this->view->importMessage = Core_Locale::translate('import_xlsx_error');
                        $this->view->importMessageClass =  "alert-error";
                        break;
                    case 1 :
                        $this->view->importMessage = Core_Locale::translate('import_xlsx_success');
                        $this->view->importMessageClass =  "alert-success";
                        break;
                    case 2 :
                        $this->view->importMessage = Core_Locale::translate('import_xlsx_error_bad_format');
                        $this->view->importMessageClass =  "alert-error";
                        break;
                    default :
                        $this->view->importMessage = "";
                        $this->view->importMessageClass =  "";
                        break;
                        endswitch;
                $this->view->importMessageStyle = 'display: block;';
            }
        }

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'created DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;
    }

    private function generateFinePdf ( $idArray = array() ) {
        ini_set('memory_limit', '1024M');
        set_time_limit(0);

        $files = array();
        $i = 0;
        foreach ( $idArray as $item ) {
            $i++;
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);
            $id = $item;

            $model = new Model_Fines();

            if ($id) {
                $fine = $model->getFine($id);
                $zones = new Model_Zone();
                $blacklistModel = new Model_BlackListCauses();
                $city = new Model_City();

                $zone = $zones->fetchRow($zones->select()->where('id = ?', $fine['zone']));
                $blackList = $blacklistModel->fetchRow($blacklistModel->select()->where('id = ?',
                    $fine['causes_id']));
                $citys = $zones->fetchRow($zones->select()->where('id=' . $fine['zone']));
                $this->view->fine = $fine;

                $cityDataArray = $city->getCity($citys['city_id']);

                $fine_date = new DateTime($fine['date']); //current date/time
                $payToHours = $zone->warning_pay_time;
                $fine_date->add(new DateInterval("PT" . $payToHours . "H"));
                $this->view->fineTime = $fine_date->format('d.m.Y');
                $this->view->zone = $zone;
                $this->view->blackList = $blackList;
                $html = $this->view->render('fines/' . $citys['city_id'] . '.phtml');

                $domain_model = new Model_Domain();
                $options = $domain_model->options();

                if (strpos($options['name'], 'parkingee.unipark.lt') !== false && $citys['city_id'] != 3) {
                    $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'ESTONIA', $default_font_size = 14,
                        $default_font = '', $mgl = 0,
                        $mgr = 0, $mgt = 0, $mgb = 0, $mgh = 0, $mgf = 0, $orientation = 'L');

                } else {
                    $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 12,
                        $default_font = '', $mgl = 25,
                        $mgr = 10, $mgt = 20, $mgb = 0, $mgh = 9, $mgf = 9, $orientation = 'P');

                }

                $pdf->SetDisplayMode('fullpage');
                $pdf->writeHTML($html);
                if ($cityDataArray && $cityDataArray['fineType'] == 1) {
                    $tmpFileName = 'notification-form' . '-' . $fine['id_public'] . '-' . $fine['plate'] . '.pdf';
                    $pdf->Output( TMP_FILES_PATH."/".$tmpFileName, 'F');
                } else {
                    $tmpFileName = 'notification-form' . '-' . date('YmdHis',
                            strtotime($fine['date'])) . '-' . $fine['plate'] . '.pdf';
                    $pdf->Output( TMP_FILES_PATH."/".$tmpFileName, 'F');
                }

                $files[] = $tmpFileName;
            }
        }
        return $files;
    }

    private function removeTmpfiles ( $filesArray = array() ) {
        foreach ( $filesArray as $item ) {
            unlink( TMP_FILES_PATH."/".$item );
        }
        return true;
    }

    public function importOmnivaAction ()
    {
        $model = new Model_FinesOmnivaImport();
        $this->view->importMessageClass = "";
        $this->view->importMessageStyle = "display: none;";

        if ($this->_request->isPost())
        {
            if ( $_FILES )
            {
                $result = $model->importOmnivaXLS( $_FILES );
                switch ( $result ) :
                    case 0 :
                        $this->view->importMessage = Core_Locale::translate('import_xlsx_error');
                        $this->view->importMessageClass =  "alert-error";
                        break;
                    case 1 :
                        $this->view->importMessage = Core_Locale::translate('import_xlsx_success');
                        $this->view->importMessageClass =  "alert-success";
                        break;
                    case 2 :
                        $this->view->importMessage = Core_Locale::translate('import_xlsx_error_bad_format');
                        $this->view->importMessageClass =  "alert-error";
                        break;
                    default :
                        $this->view->importMessage = "";
                        $this->view->importMessageClass =  "";
                        break;
                endswitch;
                $this->view->importMessageStyle = 'display: block;';
            }
        }
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('Fines'),
            'module' => 'admin',
            'controller' => 'fines',
            'action' => 'index',
            'class' => 'awe-warning-sign',
            'a_class' => 'no-submenu',
            'params' => [
                'lang' => LANG,
            ],
            'pages' => [
                'fine_omniva' => [
                    'label' => Core_Locale::translate('Import Omniva xls'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'import-omniva',
                    'visible' => false,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'fine_send' => [
                    'label' => Core_Locale::translate('Send Fines'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'send-fines',
                    'visible' => false,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'fine_view' => [
                    'label' => Core_Locale::translate('Fine View'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'view',
                    'visible' => false,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'fines_list' => [
                    'label' => Core_Locale::translate('Fines list'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'index',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'payments_import' => [
                    'label' => Core_Locale::translate('Payment Import'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'payments-import',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'payments_history' => [
                    'label' => Core_Locale::translate('Payments History'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'payments-history',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'plate_check_log' => [
                    'label' => Core_Locale::translate('Plate check log'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'plate-check-log',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'passive_scanning' => [
                    'label' => Core_Locale::translate('Passive scanning log'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'passive-scanning',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'import_xlsx' => [
                    'label' => Core_Locale::translate('import-xlsx'),
                    'module' => 'admin',
                    'controller' => 'fines',
                    'action' => 'import-xlsx',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
            ],
        ];

        return $nav;
    }
}
