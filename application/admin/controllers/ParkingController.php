<?php

require_once APPLICATION_PATH.'/../library/PHPExcel/PHPExcel.php';

class Admin_ParkingController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{

    public function indexAction()
    {
        $adminGroup = new Model_AdminGroups();
        $operatorsGroupId = $adminGroup->getOperatorGroups();
        $translates = json_decode(Core_Locale::translate('export_headers_parking'));
        $model = new Model_Parking();
        $this->view->filter = $filter = trim($this->_request->getParam('filter'));

        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        if (!$date_from) {
            $this->view->date_from = $date_from = date('Y-m-d');
        }
        $this->view->zone = $zone = $this->getParam('zone') ?: [];
        $this->view->operator = $operator = $this->_request->getParam('operator');
        $this->view->zones_list = (new Model_Zone())->getUserInfo(user()->id);
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $this->view->city = $city = $this->getParam('city') ? $this->getParam('city') : null;

        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;

        if ($date_from) {
            $where [] = "date_start >= '$date_from 00:00:00'";
        }
        if ($date_to) {
            $where [] = "date_start <= '$date_to 23:59:59'";
        }

        $this->view->filter = $filter = trim($this->_request->getParam('filter'));
        if ($filter) {
            $where [] = ("
                plate LIKE '%$filter%' OR
                bar_code LIKE '%$filter%' OR
                phone LIKE '%$filter%'
            ");

        }
        if ($operator) {
            $where [] = ("operator_name = '$operator'");

        }

        $domain_model = new Model_Domain();
        $options = $domain_model->options();

        $ui = new Model_City();
        $cities = new Model_UserCities();
        $selectCities = $cities->select();
        $selectCities->where("active = 1 and user_id = ?", (int)$id);
        $visibleCities = $cities->fetchAll($selectCities);
        $citiesArray = array();

        foreach ($visibleCities as $oneCity) {
            $citiesArray [] = $oneCity['city_id'];
        }

        $userCities = (new Model_City())->userCities();
        $this->view->city_list = $userCities;
        $this->view->operators = json_decode($options['parking_sources']);
        $this->view->parking_update_buttons = $options['parking_update_buttons'];

        $zones_model = new Model_Zone();
        $selectZones = $zones_model->select();

        if ($city) {
            $selectZones->where("city_id in ('" . implode("','", $city) . "')");
        } else {
            $selectZones->where("city_id in ('" . implode("','", $citiesArray) . "')");
            $adm = new Model_Admins();
            $city_operators = $adm->fetchAll(' admin_groups_id in(' . implode(',',
                    $operatorsGroupId) . ') AND adm_active = 1 and id =0');
            $this->view->operators_list = $city_operators;
        }

        if ($zone){
           $selectZones->where("id in ('" . implode("','", $zone) . "')");
        }

        $visibleZones = $zones_model->fetchAll($selectZones);
        $zonesArray = array();
        foreach ($visibleZones as $zone) {
            $zonesArray [] = $zone['id'];
        }

        $domain = new Model_Domain();
        $cent = $domain->domainOptionsCents();
        $digits = $domain->domainOptionsDigits();
        $this->view->paginator_params = array(
            'operator' => $operator,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'filter' => $filter,
            'locale' => $options['locale']
        );

        $where [] = ("zone in ('" . implode("','", $zonesArray) . "')");

        $join[] = [
            "select" => [
                "zone_code",
                "city_id",
                "CONCAT(UPPER(LEFT(operator_name, 1)), SUBSTRING(operator_name, 2)) as operator",
                "FORMAT((price_cent / '$cent'),'$digits') as price"
            ],
            "as" => "box_zone",
            "table" => "box_zone",
            "on" => "box_parkings.zone = box_zone.id"
        ];
        $join[] = [
            "select" => "CityName as city",
            "as" => "box_city",
            "table" => "box_city",
            "on" => "box_city.id = box_zone.city_id"
        ];

        // var_dump("select city_id from box_user_cities where box_user_cities.user_id = $id and active = 1");exit;

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            $where,
            'date_start DESC',
            null,
            null,
            $join
        );

        $allData = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            '',
            $where,
            'date_start DESC',
            null,
            null,
            $join
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;
        $this->view->allData = $allData;

        if ($this->_request->isPost('csv') || $this->_request->isPost('xls') || $this->_request->isPost('pdf')) {

            if ($operator) {
                $where[] = "operator_name = '$operator'";
            }

            if ($this->_request->isPost('xls') || $this->_request->isPost('pdf')) {
                $data = $model->getCityZoneCsv($where);
                // $handle = fopen('php://output', 'w+');
                $finalData = array();
                $i = 1;
                $finalData[] = $translates->csv;

                foreach ($data as $row) {

                    $to_time = strtotime("".($row["date_end"])."");
                    $from_time = strtotime("".($row['date_start'])."");
                    $minutes = round(abs($to_time - $from_time) / 60);

                    $finalData[] = [
                        utf8_decode($i++),
                        utf8_decode($row['city']),
                        utf8_decode($row['zone_code']),
                        utf8_decode($row['operator']),
                        utf8_decode($row['plate']),
                        utf8_decode($row['bar_code']),
                        utf8_decode($row['phone']),
                        utf8_decode($row['duration_hours']),
                        utf8_decode($minutes),
                        utf8_decode($row['date_start']),
                        utf8_decode($row['date_end']),
                        utf8_decode($row['billing_id']),
                        utf8_decode($row['price']),
                    ];
                }
            }
        }
        //EXPORTINGS =========================================================================

        $this->view->table = $model->info('name');
        $is_csv = $this->_getParam('csv');

        //EXPORT TO CSV ======================================================================

        if ($this->_request->isPost() && $is_csv) {
            if ($date_from && $date_to) {
                $file1 = $translates->filename[0] . $date_from . $translates->filename[1] . $date_to . '.csv';
            } else {
                $file1 = $translates->filename[0] . $date_from . $translates->filename[1] . date('Y-m-d') . '.csv';
            }
            $fp = fopen('php://output', 'w+');


            //$data = $model->getCityZoneCsv($where);
            $i = 1;
            fputcsv($fp, $translates->csv);
            foreach ($data as $fields) {
                $minutes = round(abs(strtotime("".($fields["date_end"])."") - strtotime("".($fields['date_start'])."")) / 60);
                fputcsv($fp, array($i++, $fields['city'], $fields['zone_code'], $fields['operator'], $fields['plate'], $fields['bar_code'], $fields['phone'], $fields['duration_hours'], $minutes, $fields['date_start'], $fields['date_end'], $fields['billing_id'], $fields['price_cent']));
            }

            fclose($fp);

            if ($file1 !== false) {
                header('Content-type: text/plain');
                header('Content-Disposition: attachment; filename="' . $file1 . '"');
                exit;
            }
        }
        //EXPORT TO EXCEL =====================================================================

        $is_xls = $this->_getParam('xls');
        if ($this->_request->isPost() && $is_xls) {
            $filename = $_SERVER['DOCUMENT_ROOT'] . '/tmp/excel-' . date('m-d-Y') . '.xls';

            if ($date_from && $date_to) {
                $file1 = $translates->filename[0] . $date_from . $translates->filename[1] . $date_to . '.xls';
            } else {
                $file1 = $translates->filename[0] . $date_from . $translates->filename[1] . date('Y-m-d') . '.xls';
            }

            $filename = realpath($filename);

            $handle = fopen('php://output', 'w+');

            foreach ($finalData as $finalRow) {
                fputcsv($handle, $finalRow, "\t");
            }

            fclose($handle);

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $file1 = str_replace(" ","_",$file1);

            $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel')
                ->setRawHeader('Content-Disposition: attachment; filename=' . $file1)
                ->setRawHeader('Content-Transfer-Encoding: binary')
                ->setRawHeader('Expires: 0')
                ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->setRawHeader('Pragma: public')
                ->setRawHeader('Content-Length: ' . filesize($filename))
                ->sendResponse();

            exit;
        }

        //EXPORT TO ATASKAITA =====================================================================

        $is_atas = $this->_getParam('atas');

        if ($this->_request->isPost() && $is_atas) {
            ini_set('memory_limit', '1024M');
            set_time_limit(0);

            $where = [];
            if ($date_from) {
                $where[] = "date_start >= '$date_from 00:00:00'";
            }
            if ($date_to) {
                $where[] = "date_start <= '$date_to 23:59:59'";
            }
            if ($date_from && $date_to) {
                // $where[] = "date_start >= '$date_from 00:00:00' AND date_end <= '$date_to 23:59:59'";
                $file1 = $date_from . '_' . $date_to . $translates->atas->filename . '.xls';
            } else {
                $file1 = $date_from . '_' . date('Y-m-d') . $translates->atas->filename . '.xls';
            }

            //$where[] = "user_id = '$id'";
            //$where[] = "active = 1";
            if ($operator) {
                $where[] = "operator_name = '$operator'";
            }

            $model_zone = new Model_Zone();
            $user_info = $model_zone->getUserInfo($id);
            if ($city) {
                $city_ids = implode(',', $city);
                $where[] = "box_zone.city_id in($city_ids)";
            } else {
                $zoneArray = [];
                foreach ($user_info as $zone) {
                    $zoneArray[] = $zone['zone_id'];
                }
                $city_ids = implode(',', $zoneArray);
                $where[] = "box_zone.id in($city_ids)";
            }

            $data = $model->getCityZoneCsv($where);

            foreach ($data as $item) {
                $z_name[$item['zone_name']] = $item['zone_name'];
                $array[] = [
                    $item['zone_name'],
                    date_format(new DateTime($item['date_inserted']), 'Y-m-d'),
                    $item['price'],
                ];
            }

            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->SetCellValue('A1', $translates->atas->zone);
            $objPHPExcel->getActiveSheet()->SetCellValue('B1', $translates->atas->date);
            $objPHPExcel->getActiveSheet()->SetCellValue('C1', $translates->atas->amount);
            $objPHPExcel->getActiveSheet()->SetCellValue('D1', $translates->atas->other);

            if (empty($date_to)) {
                $date_to = date('Y-m-d');
            }

            $date_to = date('Y-m-d H:i:s', strtotime($date_to . ' +1 day'));

            // Dates
            $period = new DatePeriod(
                new DateTime($date_from),
                new DateInterval('P1D'),
                new DateTime($date_to)
            );
            foreach ($period as $date) {
                $dateArray[] = $date->format('Y-m-d');
            }

            $zoneRowCount = 2;
            $dateRowCount = 2;
            $sumRowCount = 2;

            $mergeFrom = 2;
            $mergeTo = 2;

            foreach ($z_name as $zone) {
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $mergeFrom,
                    $zone)->getStyle('A' . $mergeFrom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                foreach ($dateArray as $dateItem) {
                    $objPHPExcel->getActiveSheet()->SetCellValue('B' . $dateRowCount, $dateItem);
                    $a = 0;
                    foreach ($array as $key => $item) {
                        if ($item['0'] == $zone) {
                            if ($item['1'] == $dateItem) {
                                $a = $item['2'] + $a;

                                unset($array[$key]);
                            }
                        }
                    }

                    $objPHPExcel->getActiveSheet()->SetCellValue('C' . $sumRowCount, $a ? $a : 0);
                    $sumRowCount++;
                    $dateRowCount++;
                    $mergeTo++;
                }
                $objPHPExcel->getActiveSheet()->mergeCells('A' . $mergeFrom . ':A' . ($mergeTo - 1));
                $mergeFrom = $mergeTo;
                $zoneRowCount++;
            }

            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=' . $file1);
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            exit;
        }

        $is_fullatas = $this->_getParam('fullatas');

        if ($this->_request->isPost() && $is_fullatas) {
            set_time_limit(0);

            if (!$date_to) {
                $date_to = date('Y-m-d');
            }

            $file1 = $date_from . '_' . $date_to . $translates->atas->filename.'.xls';
            if ($operator) {
                $where[] = "operator_name = '$operator'";
            }
            if ($date_to) {
                $where [] ="date_end <= '$date_to 23:59:59'";
            }
            $data = $model->getCityZoneCsv($where);
            $model_city = new Model_City();


            if ($city) {
                $cities = $model_city->fetchAll("id in ('".implode("','",$city)."')");
            } else {
                $cities = $model_city->fetchAll("id in ('".implode("','",$citiesArray)."')");
            }

            $objPHPExcel = new PHPExcel();
            $i =0;

            $style = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                ),
                'font' => array(
                    'bold' => true
                )
            );
            $date_toConverted = date('Y-m-d H:i:s', strtotime($date_to . ' +1 day'));
            $period = new DatePeriod(
                new DateTime($date_from),
                new DateInterval('P1D'),
                new DateTime($date_toConverted)
            );

            foreach ($period as $date) {
                $dateArray[] = $date->format('Y-m-d');
            }

            foreach( $cities as $city )
            {

                $array = array();
                $z_name = array();
                $sheet =$objPHPExcel->createSheet($i++);

                $sheet->mergeCells('C9:D9');$sheet->mergeCells('E9:G9');
                $sheet->SetCellValue('B9', $translates->atas->zone);
                $sheet->SetCellValue('C9', $translates->atas->period);
                $sheet->SetCellValue('E9', $translates->atas->amount);
                $sheet->getColumnDimension('C')->setWidth(20);
                $timePediod = 10;

                $sheet->getStyle('B2')->getAlignment()->setWrapText(true);
                $sheet->getStyle('B6')->getAlignment()->setWrapText(true);
                $sheet->getDefaultStyle()->applyFromArray($style);
                $sheet->setTitle($city['CityName']);
                $sheet->getColumnDimension('B')->setWidth(20);
                $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4_PLUS_PAPER);
                $sheet->setShowGridlines(true);
                $sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 8);
                $objPHPExcel->setActiveSheetIndex($i);

                $sheet->mergeCells('B2:H4');
                $sheet->mergeCells('B6:H6');

                $sheet->SetCellValue('B2',$translates->atas->header.$translates->atas->from.' '.$date_from.' '.$translates->atas->to.' '.$date_to)->getStyle()->applyFromArray($style);
                $sheet->SetCellValue('B6','Mokėjimai aplikacija')->getStyle()->applyFromArray($style);

                //$date_to = date('Y-m-d H:i:s', strtotime($date_to . ' +1 day'));

                // Dates
                $sheet->SetCellValue('B48', $translates->atas->zone);
                $sheet->SetCellValue('C48', $translates->atas->date);
                $sheet->SetCellValue('E48', $translates->atas->amount);

                $dateRowCount = 49;
                $sumRowCount = 49;

                $mergeFrom = 49;
                $mergeTo = 49;

                foreach ($data as $item) {
                    if ($city['id'] === $item['city_id']){
                        $z_name[$item['zone_name']] = $item['zone_name'];
                        $array[] = array($item['zone_name'], date_format(new DateTime($item['date_inserted']), 'Y-m-d'), $item['price']);
                    }
                }

                if(isset($z_name)){
                    foreach ($z_name as $zone) {
                        $sheet->mergeCells('C'.($mergeFrom-1).':D'.($mergeFrom-1));
                        $sheet->mergeCells('E'.($mergeFrom-1).':G'.($mergeFrom-1));
                        $sheet->SetCellValue('B'.($mergeFrom-1), $translates->atas->zone);
                        $sheet->SetCellValue('C'.($mergeFrom-1), $translates->atas->date);
                        $sheet->SetCellValue('E'.($mergeFrom-1), $translates->atas->amount);
                        $sheet->SetCellValue('B' . $mergeFrom, $zone)->getStyle('B' . $mergeFrom)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                        foreach ($dateArray as $dateItem) {
                            $sheet->SetCellValue('C' . $dateRowCount, $dateItem);
                            $a = 0;
                            foreach ($array as $key => $item) {

                                if ($item['0'] == $zone) {

                                    if ($item['1'] == $dateItem) {
                                        $a = $item['2'] + $a;


                                        unset($array[$key]);
                                    }

                                }
                            }

                            $sheet->SetCellValue('E' . $sumRowCount, $a ? $a : 0);
                            $sheet->mergeCells('C'.$sumRowCount.':D'.$sumRowCount);
                            $sheet->mergeCells('E'.$sumRowCount.':G'.$sumRowCount);
                            $sumRowCount++;
                            $dateRowCount++;
                            $mergeTo++;
                        }
                        $sheet->setCellValue('E'.$sumRowCount,  '=SUM(E'.$mergeFrom.':E'.($mergeTo -1).')' );

                        $sheet->mergeCells('C'.$timePediod.':D'.$timePediod);$sheet->mergeCells('E'.$timePediod.':G'.$timePediod);
                        $sheet->setCellValue('E'.$timePediod,  '=SUM(E'.$mergeFrom.':E'.($mergeTo -1).')' );
                        $sheet->setCellValue('B'.$timePediod,  $zone);
                        $sheet->setCellValue('C'.$timePediod,  $date_from . ' - ' . $date_to );
                        $timePediod++;


                        $sheet->mergeCells('E'.$sumRowCount.':G'.$sumRowCount);
                        $sheet->setCellValue('B'.$sumRowCount,  'Viso:' );
                        $sheet->mergeCells('B' . $mergeFrom . ':B' . ($mergeTo - 1));
                        $mergeTo++;$sumRowCount++;$dateRowCount++;
                        $sheet->setBreak('B'.($mergeTo-1), PHPExcel_Worksheet::BREAK_ROW);
                        $mergeTo++;$sumRowCount++;$dateRowCount++;
                        $mergeFrom = $mergeTo;

                    }
                    if($timePediod > 10){
                        $sheet->mergeCells('C'.$timePediod.':D'.$timePediod);$sheet->mergeCells('E'.$timePediod.':G'.$timePediod);
                        $sheet->SetCellValue('B'.$timePediod, 'Viso: ');
                        $sheet->setCellValue('E'.$timePediod,  '=SUM(E10:E'.($timePediod-1).')' );
                        $timePediod++;
                        $sheet->mergeCells('C'.$timePediod.':E'.$timePediod);
                        $sheet->SetCellValue('C'.$timePediod, 'Viso surinkta pinigų suma EUR: ');
                        $sheet->SetCellValue('F'.$timePediod, '=SUM(E10:E'.($timePediod-2).')' );
                    }
                }



            }
            // Redirect output to a client’s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=' . $file1);
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

            $objWriter->save('php://output');
            exit;
        }

        // EXPORT TO PDF ========================================================================

        //require $_SERVER['DOCUMENT_ROOT'].'/library/fpdf/mc_table.php';

        $is_pdf = $this->_getParam('pdf');

        if ($this->_request->isPost() && $is_pdf) {
            set_time_limit(0);

            $pdf = new PDF_MC_Table();
            $pdf->Open();
            $pdf->AddPage();

            $pdf->SetWidths([10, 10, 12, 20, 20, 20, 20, 17, 17, 17, 17]);
            srand(microtime() * 1000000);

            $o = 1;
            foreach ($finalData as $row) {
                if ($o == 1) {
                    $pdf->SetFont('Arial', 'B', '8');
                } elseif ($o != 1) {
                    $pdf->SetFont('Arial', '', '7');
                }
                $pdf->Row($row);
                $o++;
            }

            $pdf->Output();
        }
    }

    public function paymentsLogAction()
    {
        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');

        if (!$date_from) {
            $this->view->date_from = $date_from = date('Y-m-d', strtotime('-2 days'));
        }

        $model = new Model_PaymentsLog();
        $select = $model->select();
        if ($date_from) {
            $select->where('date >= ?', $date_from . ' 00:00:00');
        }
        if ($date_to) {
            $select->where('date <= ?', $date_to . ' 23:59:59');
        }
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $zones = new Model_UserCities();
        $select2 = $zones->select()->from(['z' => $zones->info('name')],
            ['z.city_id'])->where('active = 1 and user_id = ?', (int) $id);
        $zone = new Model_Zone();
        $zone = $zone->fetchall('city_id in(' . $select2 . ')');
        $visible_zones = [];
        foreach ($zone as $c) {
            $visible_zones[] = $c['id'];
        }
        if (!empty($visible_zones)) {
            $select->where('zone in (' . implode(',', $visible_zones) . ')');
        } else {
            $select->where("zone = '0' ");
        }
        $items = $model->fetchAll($select);
        // $this->view->items = $items;
        // $paginator = $model->fetchAll($select);

        //  if($items->count()) foreach($items as $i){
        //       $i->request = $this->prettyPrint($i->request);
        //        $i->response = $this->prettyPrint($i->response);
        //    }

        $select->order('date DESC');
        $this->view->filter = $filter = (string)trim($this->_request->getParam('filter'));
        if ($filter) {
            $select->where("
               request LIKE '%$filter%' OR
                response LIKE '%$filter%'

            ");
        }
        $this->view->paginator_params = [
            'date_from' => $date_from,
            'date_to' => $date_to,
        ];

        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            null,
            'date DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('Parking List'),
            'module' => 'admin',
            'controller' => 'parking',
            'action' => 'index',
            'class' => 'awe-list',
            'a_class' => 'no-submenu',
            'params' => [
                'lang' => LANG,
            ],
            'pages' => [
                'parking_list' => [
                    'label' => Core_Locale::translate('Parking List'),
                    'module' => 'admin',
                    'controller' => 'parking',
                    'action' => 'index',
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'payments_log' => [
                    'label' => Core_Locale::translate('Payments Import Log'),
                    'module' => 'admin',
                    'controller' => 'parking',
                    'action' => 'payments-log',
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
            ],
        ];

        return $nav;
    }

    private function prettyPrint($json)
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = null;
        $json_length = strlen($json);

        for ($i = 0; $i < $json_length; $i++) {
            $char = $json[$i];
            $new_line_level = null;
            $post = '';
            if ($ends_line_level !== null) {
                $new_line_level = $ends_line_level;
                $ends_line_level = null;
            }
            if ($in_escape) {
                $in_escape = false;
            } elseif ($char === '"') {
                $in_quotes = !$in_quotes;
            } elseif (!$in_quotes) {
                switch ($char) {
                    case '}':
                    case ']':
                        $level--;
                        $ends_line_level = null;
                        $new_line_level = $level;
                        break;
                    case '{':
                    case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;
                    case ':':
                        $post = ' ';
                        break;
                    case ' ':
                    case "\t":
                    case "\n":
                    case "\r":
                        $char = '';
                        $ends_line_level = $new_line_level;
                        $new_line_level = null;
                        break;
                }
            } elseif ($char === '\\') {
                $in_escape = true;
            }
            if ($new_line_level !== null) {
                $result .= "\n" . str_repeat("\t", $new_line_level);
            }
            $result .= $char . $post;
        }

        return '<pre>' . $result . '</pre>';
    }
}

