<?php

class Admin_BlitzController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    public function indexAction()
    {
        $model = new Model_Blitz();
        $types = new Model_BlitzOrderType();
        $statuses = new Model_BlitzStatuses();

        $this->view->date_from = $date_from = $this->_request->getParam('date_from');
        $this->view->date_to = $date_to = $this->_request->getParam('date_to');
        $this->view->status = $status = $this->_request->getParam('status');
        $this->view->today = $today = (string) trim($this->_request->getParam('today'));

        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;

        $select = $model->select()->setIntegrityCheck(false)->from(['p' => $model->info('name')]);
        $select->joinLeft(
            $types->info('name').' as t',
            't.id = p.order_type',
            ['t.type']
        );
        $select->joinLeft(
            $statuses->info('name').' as s',
            's.id = p.status',
            ['s.name as status_name']
        );
        //
        $where = [];
        $select->order('p.id DESC');

        if ($today) {
            $select->where('date_to = ?', date('Y-m-d'));

            // echo date('YYY-MM-DD');
        } else {
            if ($date_from) {
                $select->where('date_from >= ?', $date_from.' 00:00:00');
            }
            if ($date_to) {
                $select->where('date_to <= ?', $date_to.' 23:59:59');
            }
            if (! $date_from) {
                $this->view->date_from = $date_from = date('Y-m-d', strtotime('-2 days'));
            }

            $this->view->filter = $filter = (string) trim($this->_request->getParam('filter'));
            if ($filter) {
                $select->where("
                person LIKE '%$filter%' OR
                company LIKE '%$filter%' OR
                p.name LIKE '%$filter%' OR
                surname LIKE '%$filter%' OR
                payment_type LIKE '%$filter%' OR
                flight_number LIKE '%$filter%' OR
                status LIKE '%$filter%' OR
                plate LIKE '%$filter%'
            ");
            }

            if ($status) {
                $select->where("s.name = '$status'");
            }

            $this->view->paginator_params = [
                'status' => $status,
                'date_from' => $date_from,
                'date_to' => $date_to,
                'filter' => $filter,
            ];
        }
        $paginator = Core_Paginator::create(
            $model,
            $this->view,
            $this->_getParam('page'),
            DEFAULT_PAGINATION_ITEMS,
            $where,
            'id DESC',
            $select
        );

        $this->view->page = $this->_getParam('page');
        $this->view->paginator = $paginator;

        //EXPORTINGS =========================================================================

        $where = [];

        if ($today) {
            $where[] = 'date_to = '.date('Y-m-d');
        } else {
            if ($date_from) {
                $where[] = "date_from >= '$date_from 00:00:00'";
            }
            if ($date_to) {
                $where[] = "date_to <= '$date_to 23:59:59'";
            }
            if ($status) {
                $where[] = "box_blitz_statuses.name = '$status'";
            }
        }

        $this->view->table = $model->info('name');

        $is_csv = $this->_getParam('csv');

        //EXPORT TO CSV ======================================================================

        if ($this->_request->isPost() && $is_csv) {
            if ($date_from && $date_to) {
                $file1 = 'Parkavimo registracijos '.$date_from.' iki '.$date_to.'.csv';
            } else {
                $file1 = 'Parkavimo registracijos '.$date_from.' iki '.date('Y-m-d').'.csv';
            }

            $fp = fopen('php://output', 'w+');

            $data = $model->getBlicExport($where);

            $i = 1;

            fputcsv($fp, [
                    utf8_decode('Eiles nr.'),
                    utf8_decode('Asmuo'),
                    utf8_decode('Pirkėjas'),
                    utf8_decode('Asmens / Įmonės kodas'),
                    utf8_decode('Apmokėjimo būdas'),
                    utf8_decode('Valst. Numeris'),
                    utf8_decode('Statusas'),
                    utf8_decode('Sutarties tipas'),
                    utf8_decode('Nuo'),
                    utf8_decode('Iki'),
                    utf8_decode('Suma su PVM'),
                ]
            );

            foreach ($data as $fields) {
                fputcsv($fp, [
                        $i++,
                        utf8_decode($fields['person']),
                        utf8_decode(! empty($fields['name_surname']) ? $fields['name_surname'] : $fields['company']),
                        utf8_decode(! empty($fields['person_code']) ? $fields['person_code'] : $fields['company_code']),
                        utf8_decode($fields['payment_type']),
                        utf8_decode($fields['plate']),
                        utf8_decode($fields['status_name']),
                        utf8_decode($fields['blic_type']),
                        utf8_decode($fields['date_from']),
                        utf8_decode($fields['date_to']),
                        utf8_decode($fields['amount_pvm']),
                    ]
                );
            }

            fclose($fp);

            if ($file1 !== false) {
                header('Content-type: text/plain');
                header('Content-Disposition: attachment; filename="'.$file1.'"');
                exit;
            }
        }
        //EXPORT TO EXCEL =====================================================================

        $is_xls = $this->_getParam('xls');
        if ($this->_request->isPost() && $is_xls) {
            set_time_limit(0);

            $filename = '/var/www/vhosts/'.$_SERVER['HTTP_HOST'].'/httpdocs/tmp/excel-'.date('m-d-Y').'.xls';

            if ($date_from && $date_to) {
                $file1 = 'Parkavimo registracijos '.$date_from.' iki '.$date_to.'.xls';
            } else {
                $file1 = 'Parkavimo registracijos'.$date_from.' iki '.date('Y-m-d').'.xls';
            }

            $filename = realpath($filename);

            $handle = fopen('php://output', 'w+');

            $data = $model->getBlicExport($where);
            // $handle = fopen('php://output', 'w+');
            $finalData = [];
            $i = 1;
            $finalData[] = [
                utf8_decode('Eiles nr.'),
                utf8_decode('Asmuo'),
                utf8_decode('Pirkėjas'),
                utf8_decode('Asmens / Įmonės kodas'),
                utf8_decode('Apmokėjimo būdas'),
                utf8_decode('Valst. Numeris'),
                utf8_decode('Statusas'),
                utf8_decode('Sutarties tipas'),
                utf8_decode('Nuo'),
                utf8_decode('Iki'),
                utf8_decode('Suma su PVM'),
            ];

            foreach ($data as $row) {
                $finalData[] = [
                    utf8_decode($i++),
                    utf8_decode($row['person']),
                    utf8_decode(! empty($row['name_surname']) ? $row['name_surname'] : $row['company']),
                    utf8_decode(! empty($row['person_code']) ? $row['person_code'] : $row['company_code']),
                    utf8_decode($row['payment_type']),
                    utf8_decode($row['plate']),
                    utf8_decode($row['status_name']),
                    utf8_decode($row['blic_type']),
                    utf8_decode($row['date_from']),
                    utf8_decode($row['date_to']),
                    utf8_decode($row['amount_pvm']),
                ];
            }

            foreach ($finalData as $finalRow) {
                fputcsv($handle, $finalRow, "\t");
            }

            fclose($handle);

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $this->getResponse()->setRawHeader('Content-Type: application/vnd.ms-excel')
                ->setRawHeader('Content-Disposition: attachment; filename='.$file1)
                ->setRawHeader('Content-Transfer-Encoding: binary')
                ->setRawHeader('Expires: 0')
                ->setRawHeader('Cache-Control: must-revalidate, post-check=0, pre-check=0')
                ->setRawHeader('Pragma: public')
                ->setRawHeader('Content-Length: '.filesize($filename))
                ->sendResponse();

            exit;
        }

        // EXPORT TO PDF ========================================================================

        //require $_SERVER['DOCUMENT_ROOT'].'/library/fpdf/mc_table.php';

        $is_pdf = $this->_getParam('pdf');

        if ($this->_request->isPost() && $is_pdf) {
            set_time_limit(0);

            $pdf = new PDF_MC_Table();
            $pdf->Open();
            $pdf->AddPage();

            $pdf->SetWidths([10, 10, 12, 20, 20, 20, 20, 17, 17, 17, 17]);
            srand(microtime() * 1000000);

            $data = $model->getBlicExport($where);

            $finalData = [];
            $i = 1;
            $finalData[] = [
                utf8_decode('Eiles nr.'),
                utf8_decode('Asmuo'),
                utf8_decode('Pirkėjas'),
                utf8_decode('Asmens / Įmonės kodas'),
                utf8_decode('Apmokėjimo būdas'),
                utf8_decode('Valst. Numeris'),
                utf8_decode('Statusas'),
                utf8_decode('Sutarties tipas'),
                utf8_decode('Nuo'),
                utf8_decode('Iki'),
                utf8_decode('Suma su PVM'),
            ];

            foreach ($data as $row) {
                $finalData[] = [
                    utf8_decode($i++),
                    utf8_decode($row['person']),
                    utf8_decode(! empty($row['name_surname']) ? $row['name_surname'] : $row['company']),
                    utf8_decode(! empty($row['person_code']) ? $row['person_code'] : $row['company_code']),
                    utf8_decode($row['payment_type']),
                    utf8_decode($row['plate']),
                    utf8_decode($row['status_name']),
                    utf8_decode($row['blic_type']),
                    utf8_decode($row['date_from']),
                    utf8_decode($row['date_to']),
                    utf8_decode($row['amount_pvm']),
                ];
            }

            $o = 1;
            foreach ($finalData as $row) {
                if ($o == 1) {
                    $pdf->SetFont('Arial', 'B', '8');
                } elseif ($o != 1) {
                    $pdf->SetFont('Arial', '', '7');
                }
                $pdf->Row($row);
                $o++;
            }

            $pdf->Output();
        }
    }

    public function viewAction()
    {
        $id = (int) $this->_request->getParam('id');
        // $status = (int)$this->_request->getParam('status');
        // $car = $this->_request->getParam('car');
        $model = new Model_Blitz();

        $this->view->blitz = $model->fetchrow('id = '.$id);
    }

    public function notificationPdfAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $this->_request->getParam('id');

        $model = new Model_Fines();

        // $zone = (int)$this->getParam('zone');
        //   $zones= new Model_WebserviceLogin();
        //    $select2 = $zones->select()->from(array('z' => $zones->info('name')),array('z.city_id'))->where( "zone_id = ".$city);

        if ($id) {
            $this->view->fine = $fine = $model->getFine($id);
            $zones = new Model_Fines();
            $city = new Model_zone();

            $zone = $zones->fetchRow($zones->select()->where('id = ?', $id));
            $citys = $city->fetchRow($city->select()->where('id='.$zone['zone']));

            //var_dump($this->view->fine);

            $html = $this->view->render('blitz/'.$citys['city_id'].'.phtml');
            //  echo $html;
            // die();

            $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 12, $default_font = '', $mgl = 25,
                $mgr = 10, $mgt = 20, $mgb = 0, $mgh = 9, $mgf = 9, $orientation = 'P');
            $pdf->SetDisplayMode('fullpage');
            $pdf->SetFooter('{PAGENO}/{nb}');
            $pdf->writeHTML($html);
            $tmpFileName = md5(time()).'-'.date('YmdHis',
                    strtotime($fine['date'])).'-'.$fine['plate'].'.pdf.tmp';
            Core_Utils::serveFilePartial($pdf->Output(null, 'S'), $tmpFileName,
                'notification-form'.'-'.date('YmdHis', strtotime($fine['date'])).'-'.$fine['plate'].'.pdf');
            die();
        }
    }

    public function blitzInsertAction()
    {
        $ids = (int) $this->_getParam('id');
        $model = new Model_Blitz();
        $log = new Model_BlitzLog();
        $form = new Admin_Form_Blitz(['id' => $ids]);

        if ($ids != 0) {
            $form->amount->setAttribs(['disable' => 'disable']);
            //$form->amount_pvm->setAttribs(array('disable' => 'disable'));
            $this->view->club_membership = '1';
        }
        if (! empty($_POST)) {
            $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
            $_POST['admin_id'] = $id;
            $datef = $_POST['date_from'];
            $datet = $_POST['date_to'];
            $date_a = new DateTime(".$datef.");
            $date_b = new DateTime(".$datet.");
            $interval = date_diff($date_b, $date_a);
            $_POST['day_amount'] = $interval->format('%d');

            $image_name = time().'_blitz';
            for ($i = 1; $i <= 6; $i++) {
                if ($_FILES['img_attach'.$i]['size'] > 0) {
                    move_uploaded_file($_FILES['img_attach'.$i]['tmp_name'],
                        APPLICATION_PATH.'/../uploads/blitz/file'.$image_name.'_'.$i.'.png');
                    $_POST['img_attach'.$i] = '/uploads/blitz/file'.$image_name.'_'.$i.'.png';
                }
            }

            if ($ids != 0) {
                $row = $model->getAdapter()->quoteInto('id = ?', $ids);
                $model->update($_POST, $row);
            } else {
                $contractNr = $model->getContractNumber($_POST['contract_nr']);
                if ( !$contractNr && !empty($_POST['contract_nr'])) {
                    $model->save($_POST);
                }
            }

            // Vars for mails
            $order_type = $_POST['order_type'];
            $status = $_POST['status'];
            $plate = $_POST['plate'];
            $date_to = $_POST['date_to'];
            $return_date = $_POST['date_to'];
            $email = $_POST['email'];

            if ($order_type == '2') {
                if ($status == '1') {
                    $message = $model->fetchRow("mail1_sent = 0 and status = '".$status."'", ' id ASC');
                    if (! empty($message)) {
                        $row2 = $model->getAdapter()->quoteInto('id = ?', $message['id']);
                        $model->update(['mail1_sent' => 1], $row2);
                        $mail = new Zend_Mail('UTF-8');
                        $mail->addTo('blic@unipark.lt'); // stova.tech@unipark.lt
                        $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
                        $mail->setSubject('uniPark BLIC+ '.$plate.' Naujas užsakymas');
                        $mail->setBodyHtml(''.$plate.' yra užsakyta BLIC paslauga. Prašome paimti automobilio raktus iš PACK&FLY. Nepamirškite nufotografuoti automobilio išorę iš visų pusių ir priekinį skydelį. Pastebėjus automobilio pažeidimus, tokias vietas nufotografuoti pakartotinai. Išsaugokite duomenis ir perstatykite automobilį į P1 aikštelę.<br /><br />
Raktelius ir sutartį grąžinti klientų aptarnavimo vadybininkui (-ei).<br /><br /><br />
Dėkojame, kad vairą sukate į uniPark<br /><br>
Tel.: +370 700 77877 (24/7)<br /><br>
blic@unipark.lt<br /><br>
www.unipark.lt');
                        $mail->send();
                    }
                } elseif ($status == '3') {
                    $message = $model->fetchRow("mail3_sent = 0 and status = '".$status."'", ' id ASC');
                    if (! empty($message)) {
                        $row2 = $model->getAdapter()->quoteInto('id = ?', $message['id']);
                        $model->update(['mail3_sent' => 1], $row2);
                        $mail = new Zend_Mail('UTF-8');
                        $mail->addTo('donatas@auto360.lt'); // donatas@auto360.lt
                        $mail->addCC('blic@unipark.lt'); // blic@unipark.lt
                        $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
                        $mail->setSubject('uniPark BLIC '.$plate.' paruoštas plovimui, atsiėmimas '.$date_to);
                        $mail->setBodyHtml('Automobilis, kurio numeris '.$plate.', yra paruoštas plovimui. Automobilį prašome nuplauti dieną prieš atsiėmimą. Paimkite raktus iš uniPark biuro.<br /><br />
Nuplaukite automobilį ir grąžinkite jį į aikštelę P1, raktus perduokite uniPark vadybininkui.<br /><br /><br />
Dėkojame, kad vairą sukate į uniPark<br /><br>
Tel.: +370 700 77877 (24/7)<br /><br>
blic@unipark.lt<br /><br>
www.unipark.lt');
                        $mail->send();
                    }
                } elseif ($status == '4') {
                    $message = $model->fetchRow("mail4_sent = 0 and status = '".$status."'", ' id ASC');
                    if (! empty($message)) {
                        $row2 = $model->getAdapter()->quoteInto('id = ?', $message['id']);
                        $model->update(['mail4_sent' => 1], $row2);
                        $mail = new Zend_Mail('UTF-8');
                        $mail->addTo('blic@unipark.lt'); // blic@unipark.lt
                        $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
                        $mail->setSubject('uniPark BLIC '.$plate.' nuplautas');
                        $mail->setBodyHtml('Automobilis '.$plate.' buvo nuplautas ir grąžintas į aikštelę P1. Automobilį perstatykite į aikštelę PC. Nepamirškite nufotografuoti automobilio išorę iš visų pusių ir priekinį skydelį. Pastebėjus automobilio pažeidimus, tokias vietas nufotografuoti pakartotinai. Automobilio raktus perduokite PACK&FLY iki '.$return_date.'.<br /><br />
Dėkojame, kad vairą sukate į uniPark<br /><br>
Tel.: +370 700 77877 (24/7)<br /><br>
blic@unipark.lt<br /><br>
www.unipark.lt');
                        $mail->send();
                    }
                } elseif ($status == '5') {
                    $message = $model->fetchRow("mail5_sent = 0 and status = '".$status."'", ' id ASC');
                    if (! empty($message)) {
                        $row2 = $model->getAdapter()->quoteInto('id = ?', $message['id']);
                        $model->update(['mail5_sent' => 1], $row2);
                        $mail = new Zend_Mail('UTF-8');
                        $mail->addTo($email); // $email
                        $mail->addCC('blic@unipark.lt'); // blic@unipark.lt
                        $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
                        $mail->setSubject('uniPark BLIC '.$plate.'');
                        $mail->setBodyHtml('Gerb. Kliente, <br /><br /> primename, kad Jūsų automobilis yra PC aikštelėje rezervuotoje BLIC vietoje. Nepamirškite automobilio raktus ir dokumentus paimti iš PACK&FLY biuro - ten pat, kur ir palikote (išvykimo terminale).
Dėkojame, kad vairą sukate į uniPark<br /><br>
Tel.: +370 700 77877 (24/7)<br /><br>
blic@unipark.lt<br /><br>
www.unipark.lt');
                        $mail->send();
                    }
                }
            } elseif ($order_type == '1') {
                if ($status == '1') {
                    $message = $model->fetchRow("mail1_sent = 0 and status = '".$status."'", ' id ASC');
                    if (! empty($message)) {
                        $row2 = $model->getAdapter()->quoteInto('id = ?', $message['id']);
                        $model->update(['mail1_sent' => 1], $row2);
                        $mail = new Zend_Mail('UTF-8');
                        $mail->addTo('blic@unipark.lt'); // stova.tech@unipark.lt
                        $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
                        $mail->setSubject('uniPark BLIC '.$plate.' Naujas užsakymas');
                        $mail->setBodyHtml(''.$plate.' yra užsakyta BLIC paslauga. Prašome paimti automobilio raktus iš PACK&FLY. Nepamirškite nufotografuoti automobilio išorę iš visų pusių ir priekinį skydelį. Pastebėjus automobilio pažeidimus, tokias vietas nufotografuoti pakartotinai. Išsaugokite duomenis ir perstatykite automobilį į P1 aikštelę.<br /><br />
Raktelius ir sutartį grąžinti klientų aptarnavimo vadybininkui (-ei).<br /><br /><br />
Dėkojame, kad vairą sukate į uniPark<br /><br>
Tel.: +370 700 77877 (24/7)<br /><br>
blic@unipark.lt<br /><br>
www.unipark.lt');
                        $mail->send();
                    }
                } elseif ($status == '2') {
                    $message = $model->fetchRow("mail2_sent = 0 and status = '".$status."'", ' id ASC');
                    if (! empty($message)) {
                        $row2 = $model->getAdapter()->quoteInto('id = ?', $message['id']);
                        $model->update(['mail2_sent' => 1], $row2);
                        $mail = new Zend_Mail('UTF-8');
                        $mail->addTo('blic@unipark.lt'); // blic@unipark.lt
                        $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
                        $mail->setSubject('uniPark BLIC '.$plate.' pastatytas aikštelėje P1');
                        $mail->setBodyHtml('Automobilis, kurio numeris '.$plate.', buvo pastatytas aikštelėje P1. Kliento atvykimo laikas: '.$return_date.'. Iki šios datos automobilis turi būti perstatytas į PC aikštelę. <br /><br />
Nepamirškite nufotografuoti automobilio išorę iš visų pusių ir priekinį skydelį. Pastebėjus automobilio pažeidimus, tokias vietas nufotografuoti pakartotinai. Automobilio raktus perduokite PACK&FLY.<br /><br />
Dėkojame, kad vairą sukate į uniPark<br /><br>
Tel.: +370 700 77877 (24/7)<br /><br>
blic@unipark.lt<br /><br>
www.unipark.lt');
                        $mail->send();
                    }
                } elseif ($status == '5') {
                    $message = $model->fetchRow("mail5_sent = 0 and status = '".$status."'", ' id ASC');
                    if (! empty($message)) {
                        $row2 = $model->getAdapter()->quoteInto('id = ?', $message['id']);
                        $model->update(['mail5_sent' => 1], $row2);
                        $mail = new Zend_Mail('UTF-8');
                        $mail->addTo($email); // $email
                        $mail->addCC('blic@unipark.lt'); // blic@unipark.lt
                        $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
                        $mail->setSubject('uniPark BLIC '.$plate.'');
                        $mail->setBodyHtml('Gerb. Kliente, <br /><br /> primename, kad Jūsų automobilis yra PC aikštelėje rezervuotoje BLIC vietoje. Nepamirškite automobilio raktus ir dokumentus paimti iš PACK&FLY biuro - ten pat, kur ir palikote (išvykimo terminale).
Dėkojame, kad vairą sukate į uniPark<br /><br>
Tel.: +370 700 77877 (24/7)<br /><br>
blic@unipark.lt<br /><br>
www.unipark.lt');
                        $mail->send();
                    }
                }
            }

            $_POST['user'] = $id;
            $log->save($_POST);
            $this->_redirect($this->view->baseUrl('blitz/'));
        }
        $row = $model->find((int) $ids)->current();
        if ($row) {
            $form->populate($row->toArray());
            $this->view->date_from = $date_from = $row['date_from'];
            $this->view->date_to = $date_to = $row['date_to'];
            $this->view->club_membership = $club_membership = $row['club_membership'];
        }
        $this->view->form = $form;
        $this->view->page = $this->_getParam('page');
    }

    public function blitzPdfAction()
    {
        $model = new Model_Blitz();

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $this->_request->getParam('id');

        if ($id) {
            $this->view->fine = $fine = $model->getBlitz($id);

            $html = $this->view->render('blitz/blitz-pdf.phtml');

            $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 8, $default_font = '', $mgl = 5,
                $mgr = 6, $mgt = 4, $mgb = 0, $mgh = 9, $mgf = 9, $orientation = 'P');
            $pdf->SetDisplayMode('fullpage');
            $pdf->writeHTML($html);
            $tmpFileName = 'test';//md5(time())."-".date('YmdHis',strtotime($fine['date']))."-".$fine['plate'].".pdf.tmp";
            Core_Utils::serveFilePartial($pdf->Output(null, 'S'), $tmpFileName, 'blitz.pdf');
            die();
        }
    }

    public function blitzSendAction()
    {
        $model = new Model_Blitz();

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $id = (int) $this->_request->getParam('id');

        if ($id) {
            $this->view->fine = $fine = $model->getBlitz($id);

            $html = $this->view->render('blitz/blitz-pdf.phtml');

            $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 8, $default_font = '', $mgl = 5,
                $mgr = 6, $mgt = 4, $mgb = 0, $mgh = 9, $mgf = 9, $orientation = 'P');
            $pdf->SetDisplayMode('fullpage');
            $pdf->writeHTML($html);
            $mail = new Zend_Mail('UTF-8');
            $mail->addTo($fine['email']);
            $mail->addCC('blic@unipark.lt');//blic@unipark.lt
            $mail->setFrom('parking@unipark.lt', 'Unipark Parking System');
            $mail->setSubject('subject');
            $mail->setBodyText('body');
            $mail->createAttachment(
                $pdf->Output('blitz-pdf.pdf', 'S'),
                'application/pdf',
                Zend_Mime::DISPOSITION_ATTACHMENT,
                Zend_Mime::ENCODING_BASE64,
                'blitz-pdf.pdf'
            );
            $mail->send();

            $blitz = $model->fetchRow("id = '$id'");
            $blitz->pdf_send = 1;
            $blitz->save();
        }
        $this->_redirect($this->view->baseUrl('blitz/'));
    }

    public static function getNavigation()
    {
        $nav = [
            'label' => Core_Locale::translate('Blic'),
            'module' => 'admin',
            'controller' => 'blitz',
            'action' => 'index',
            'class' => 'awe-list',
            'a_class' => 'no-submenu',
            'params' => [
                'lang' => LANG,
            ],
            'pages' => [
                'blitz_list' => [
                    'label' => Core_Locale::translate('Blic'),
                    'module' => 'admin',
                    'controller' => 'blitz',
                    'action' => 'index',
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
                'blitz_insert' => [
                    'label' => Core_Locale::translate('Add_blic_contract'),
                    'module' => 'admin',
                    'controller' => 'blitz',
                    'action' => 'blitz-insert',
                    'visible' => true,
                    'params' => [
                        'lang' => LANG,
                    ],
                ],
            ],
        ];

        return $nav;
    }
}
