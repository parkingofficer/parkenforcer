<?php

/**
 * Description of ShowController.
 *
 * @author Darius Matulionis
 */
class Admin_ShowController extends Zend_Controller_Action
{
    public function indexAction()
    {
        die();
    }

    public function imageAction()
    {
        $image = urldecode($this->_getParam('image'));

        $type = $this->_getParam('type');

        switch ($type) {
            case '80x80':
                $width = 80;
                $height = 80;
                break;
            case '40x40':
                $width = 40;
                $height = 40;
                break;
            default:
                $width = 300;
                $height = 200;
                break;
        }

        $thumb = Core_GdThumb_PhpThumbFactory::create(APPLICATION_PATH.'/..'.$image);
        $thumb->adaptiveResize($width, $height);
        $thumb->show();
        die();
    }
}
