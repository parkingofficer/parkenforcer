<?php

/**
 * @name ScriptsController
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2011-12-07, 16.14.31
 */
class Admin_ScriptsController extends Zend_Controller_Action
{
    public function importWhiteListAction()
    {
        $this->$Core_Api_Oxid;
    }

    public function indexAction()
    {
        $this->_redirect('/');
    }

    public function testDocFineAction()
    {
        $service = new Model_Fines();
        //$fine = $service->getFine(10015);

        $document = $service->getFineDoc(5383);

        header('Content-disposition: attachment; filename=protokolas.docx');
        echo file_get_contents($document->save());

        //dd($fine);

        die();
    }

    public function importAction()
    {
        /*
         SET FOREIGN_KEY_CHECKS=0;
TRUNCATE TABLE box_projects_log;
TRUNCATE TABLE box_projects_has_services;
TRUNCATE TABLE box_projects_has_partners;
TRUNCATE TABLE box_projects_has_methods;
TRUNCATE TABLE box_projects_has_managers;
TRUNCATE TABLE box_projects_has_field_of_expertise;
TRUNCATE TABLE box_projects_has_countries;
TRUNCATE TABLE box_projects_has_clients;

DELETE FROM `box_clients` WHERE image IS NULL OR image = '';

UPDATE `box_projects` SET
`languages_id` = NULL,
 `contract_types_id` = NULL,
 `managers_id` = NULL,
 `administrator_id` = NULL,
 `evrk_id` = NULL,
 `statuses_id` = NULL,
 `partners_id` = NULL,
 `contract_number` = '',
 `title_lt` = '',
 `title_en` = '',
 `short_title_lt` = '',
 `short_title_en` = '',
 `project_value_lt` = '',
 `project_value_eur` = '',
 `vpvi_proportion` = '',
 `ppmi_proportion` = '',
 `vpvi_staff_no` = '',
 `start_date` = '',
 `end_date` = '',
`geo_coverage_lt` = '',
 `geo_coverage_en` = '',
 `updated` = '';


TRUNCATE TABLE box_partners;
TRUNCATE TABLE box_services;
TRUNCATE TABLE box_statuses;
TRUNCATE TABLE box_methods;
TRUNCATE TABLE box_managers;
TRUNCATE TABLE box_field_of_expertise;
TRUNCATE TABLE box_evrk;
TRUNCATE TABLE box_countries;
TRUNCATE TABLE box_contract_types;

SET FOREIGN_KEY_CHECKS=1;
         */

        header('Content-Type: text/html; charset=utf-8');
        if ($this->_request->isPost()) {
            set_time_limit(0);
            ini_set('max_execution_time', 0);

            $file = $_FILES['file']['tmp_name'];
            $content = file_get_contents($file);
            $content = str_replace(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;',
                '', $content);
            $content = explode('###', $content);
            //unset($content[0]);
            $rows = [];
            foreach ($content as $r) {
                $r = explode(';', $r);
                $nr = trim($r[0]);
                switch (trim($r[1])) {
                    case 'lietuvių':
                        $lang = 'LT';
                        break;
                    case 'LT':
                        $lang = 'LT';
                        break;
                    case 'anglų':
                        $lang = 'EN';
                        break;
                    case 'EN':
                        $lang = 'EN';
                        break;
                }
                $rows[$nr][$lang] = $r;
            }

            /*
            Projekto Nr./ Project No.
            Kalba/ Language	Sutarties tipas/ Contract type
            Sutarties Nr./ Contract No.
            Projekto pavadinimas/ Project title
            Trumpasis projekto pavadinimas/ Short project title
            Georgafinė apimtis/ Geographical coverage
            Bendra projekto vertė (LT)/ Overall project value, LT
            Bendra projekto vertė (EUR)/ Overall project value, EUR
            Užsakovas/ Client
            Sutarties laikotarpis (nuoiki)/ Contract dates (start/end)
            Vadovaujantis partneris/Leading partner
            Projekto dalis kurią vykdė VPVI/ Proportion carried out by PPMI
            Projekto dalis kurią vykdė PPMI Group/ Proportion carried out by PPMI Group
            Kiti partneriai/ Other partners
            Projekto vadovas/ Project manager
            Projekto vadybininkas/ Project administrator
            Projekto darbuotojai/ Staff provided
            Veiklos sritis/ Field of expertise
            Paslaugos tipas/ Service type
            Metodai/ Methods
            Detalus projekto aprašymas/ Detailed description of project
            Galutinė ataskaita/ Final report
            Projekto būsena/ Project status
            */

            //var_dump($rows);
            die();

            $p_model = new Model_Projects();

            foreach ($rows as $nr => $r) {
                $p_model->getAdapter()->beginTransaction();
                try {
                    $project = $p_model->fetchRow("project_number = '$nr'");
                    if (! $project) {
                        $project = $p_model->createRow();
                    }

                    $project->project_number = $nr;
                    $project->contract_number = $r['EN'][3] ? $r['EN'][3] : $r['LT'][3];
                    $project->title_lt = $r['LT'][4];
                    $project->title_en = $r['EN'][4];
                    $project->short_title_lt = $r['LT'][5];
                    $project->short_title_en = $r['EN'][5];
                    $project->project_value_lt = $r['EN'][7] ? $r['EN'][7] : $r['LT'][7];
                    $project->project_value_eur = $r['EN'][8] ? $r['EN'][8] : $r['LT'][8];
                    $project->vpvi_proportion = $r['EN'][13] ? $r['EN'][13] : $r['LT'][13];
                    $project->ppmi_proportion = $r['EN'][14] ? $r['EN'][14] : $r['LT'][14];
                    $project->start_date = $r['EN'][10] ? $r['EN'][10] : $r['LT'][10];
                    $project->end_date = $r['EN'][11] ? $r['EN'][11] : $r['LT'][11];
                    $project->geo_coverage_lt = $r['LT'][6] ? $r['LT'][6] : $r['EN'][6];
                    $project->geo_coverage_en = $r['EN'][6] ? $r['EN'][6] : $r['LT'][6];

                    $project->languages_id = 3;
                    $project->visible = 1;
                    $project->contract_types_id = $this->getMulti(new Model_ContractTypes(), $r['LT'][2], $r['EN'][2]);
                    $project->managers_id = $this->getMulti(new Model_Managers(), $r['LT'][16], $r['EN'][16]);
                    $project->administrator_id = $this->getMulti(new Model_Managers(), $r['LT'][17], $r['EN'][17]);
                    $project->statuses_id = $this->getMulti(new Model_Statuses(), $r['LT'][24], $r['EN'][24]);
                    $project->partners_id = $this->getMulti(new Model_Partners(), $r['LT'][12], $r['EN'][12]);
                    $project->updated = date('Y-m-d H:i:s');
                    $project->save();

                    //Client
                    $this->saveMulti(new Model_ProjectsHasClients(),
                        $this->getMulti(new Model_Clients(), $r['LT'][9], $r['EN'][9]), 'clients_id', $project->id);

                    //Partners
                    $partners_lt = explode(';', $r['LT'][15]);
                    $partners_en = explode(';', $r['EN'][15]);
                    if ($partners_lt && ! empty($partners_lt)) {
                        foreach ($partners_lt as $k => $p_lt) {
                            $this->saveMulti(new Model_ProjectsHasPartners(),
                                $this->getMulti(new Model_Partners(), $p_lt, $partners_en[$k] ? $partners_en[$k] : ''),
                                'partners_id', $project->id);
                        }
                    }

                    //managers
                    $items_lt = explode(',', $r['LT'][18]);
                    $items_en = explode(',', $r['EN'][18]);
                    if ($items_lt && ! empty($items_lt)) {
                        foreach ($items_lt as $k => $i_lt) {
                            $this->saveMulti(new Model_ProjectsHasManagers(),
                                $this->getMulti(new Model_Managers(), $i_lt, $items_en[$k] ? $items_en[$k] : ''),
                                'managers_id', $project->id);
                        }
                    }

                    // Field of expertise
                    $items_lt = explode(';', $r['LT'][19]);
                    $items_en = explode(';', $r['EN'][19]);
                    if ($items_lt && ! empty($items_lt)) {
                        foreach ($items_lt as $k => $i_lt) {
                            $this->saveMulti(new Model_ProjectsHasFieldOfExpertise(),
                                $this->getMulti(new Model_FieldOfExpertise(), $i_lt,
                                    $items_en[$k] ? $items_en[$k] : ''), 'field_of_expertise_id', $project->id);
                        }
                    }

                    // Services
                    $items_lt = explode(';', $r['LT'][20]);
                    $items_en = explode(';', $r['EN'][20]);
                    if ($items_lt && ! empty($items_lt)) {
                        foreach ($items_lt as $k => $i_lt) {
                            $this->saveMulti(new Model_ProjectsHasServices(),
                                $this->getMulti(new Model_Services(), $i_lt, $items_en[$k] ? $items_en[$k] : ''),
                                'services_id', $project->id);
                        }
                    }

                    // Methods
                    $items_lt = explode(';', $r['LT'][21]);
                    $items_en = explode(';', $r['EN'][21]);
                    if ($items_lt && ! empty($items_lt)) {
                        foreach ($items_lt as $k => $i_lt) {
                            $this->saveMulti(new Model_ProjectsHasMethods(),
                                $this->getMulti(new Model_Methods(), $i_lt, $items_en[$k] ? $items_en[$k] : ''),
                                'methods_id', $project->id);
                        }
                    }

                    $p_model->getAdapter()->commit();
                } catch (Exception $e) {
                    $p_model->getAdapter()->rollBack();
                    var_dump($e->getMessage());
                    echo($e->getMessage());
                    var_dump($project);
                    die();
                }
            }

            //var_dump($rows);
            die('DONE');
        } else {
            echo '<form method="post" enctype="multipart/form-data">
				<input type="file" name="file"/>
				<input type="submit" />
		</form>';
        }
        die();
    }

    public function saveMulti($model, $id, $key, $project_id)
    {
        $has = $model->fetchRow("projects_id = $project_id AND $key = $id");
        if (! $has) {
            $model->save([
                'projects_id' => $project_id,
                $key => $id,
            ]);
        }
    }

    public function getMulti($model, $title_lt, $title_en)
    {
        $title_lt = trim(str_replace(["'", '"', '""', '"""'], '', trim($title_lt)));
        $title_en = trim(str_replace(["'", '"', '""', '"""'], '', trim($title_en)));

        $item = $model->fetchRow("title_lt LIKE '$title_lt' OR title_en LIKE '$title_en' OR title_lt LIKE '$title_en' OR title_en LIKE '$title_lt'");
        if (! $item) {
            $item = $model->save([
                'title_lt' => $title_lt,
                'title_en' => $title_en,
                'visible' => 1,
            ]);
        }

        return $item->id;
    }

    public function generateModelsAction()
    {
        $prefix = 'box_';
        $modelPath = APPLICATION_PATH.'/models/';
        $db = Zend_Registry::get('db');
        $a = $db->fetchCol('show tables');
        foreach ($a as $key => $tbl) {
            if (substr($tbl, 0, strlen($prefix)) == $prefix) {
                $tbl = substr($tbl, strlen($prefix));
            }
            $tbl = explode('_', $tbl);
            $file_name = '';
            foreach ($tbl as $k => $value) {
                $file_name .= ucfirst($value);
            }
            $file_name_noExtension = $file_name;
            $file_name .= '.php';
            if (! file_exists($modelPath.$file_name)) {
                var_dump($file_name);
                $primary = $db->fetchRow('show index from '.$a [$key]." where Key_name = 'PRIMARY' ");
                if (! $primary) {
                    $primary = '';
                } else {
                    $primary = $primary ['Column_name'];
                }
                $fileContent = [];
                $fileContent [] = '<?php';
                $fileContent [] = '/**';
                $fileContent [] = '* @category  Models';
                $fileContent [] = "* @name $file_name_noExtension";
                $fileContent [] = '* @copyright Darius Matulionis';
                $fileContent [] = '* @author Darius Matulionis <darius@matulionis.lt>';
                $fileContent [] = '* @since : '.date('Y-m-d').', '.date('H:i:s').'';
                $fileContent [] = '*/';
                $fileContent [] = "class Model_$file_name_noExtension extends Core_Db_Table";
                $fileContent [] = "\t".'protected $_primary = \''.$primary.'\';';
                $fileContent [] = "\t".'protected $_name = \''.$a [$key].'\';';
                $fileContent [] = '}';
                // echo join(PHP_EOL, $fileContent);
                $fh = fopen($modelPath.$file_name, 'w') or die("can't open file");
                fwrite($fh, implode(PHP_EOL, $fileContent));
                fclose($fh);
                // show index from baa_aircraft_types
                // where Key_name = 'PRIMARY'
                // };
            }
            $a [$key] = $file_name;
        }
        var_dump($modelPath);
        var_dump($a);
        var_dump($db);
        exit();
    }

    /*
     public function confClientAction(){
    $nav = $this->getNavigation();
    foreach ($nav['pages']['projects_configuration']['pages'] as $p){
    //file_put_contents(APPLICATION_PATH."/admin/views/scripts/projects/".$p['action'].".phtml", file_get_contents(APPLICATION_PATH."/admin/views/scripts/projects/conf-client.phtml"));

    $ac = explode("-", $p['action']);
    unset($ac[0]);
    $func = "conf";
    $tbl = "";
    foreach ($ac as $a){
    $func .= ucfirst($a);
    $tbl .= ucfirst($a);
    }
    $func .="Action";

    //var_dump($func);
    //var_dump($tbl);
    echo "<pre>";
    print_r( '
            public function '.$func.'(){
            $model = new Model_'.$tbl.'();
            $id = (int)$this->_request->getParam("id");
            $status = $this->registerStandartActions($model);

            if($this->_request->isPost()){
            $data = $this->_request->getParam("insert");
            if(!empty($data)){
            $model->save($data);
            $this->_redirect("/admin/projects/'.$p['action'].'");
            }
            }

            $this->view->items = $model->fetchAll(null,"title ASC");
            if($id){
            $this->view->item = $model->find($id)->current();
            }
            }
            ');
    }
    die();

    $model = new Model_Clients();
    $id = (int)$this->_request->getParam("id");
    $status = $this->registerStandartActions($model);
*/
}

