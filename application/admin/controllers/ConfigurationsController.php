<?php

class Admin_ConfigurationsController extends Core_Controller_Action_Admin implements Core_Controller_Navigation_Interface
{
    /**
     * ZONE LIST CONFIGS.
     */
    public function zonesInsertAction()
    {
        $id = (int) $this->_getParam('id');
        $model = new Model_Zone();

        $form = new Admin_Form_Zone();
        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $values = $form->getValues();
            if (empty($values['minutes_to_not_paid'])) {
                $values['minutes_to_not_paid'] = null;
            }
            if (empty($values['published_time'])) {
                $values['published_time'] = null;
            }
            if (empty($values['enforcement_time'])) {
                $values['enforcement_time'] = null;
            }
            $row = $model->save($values);
            $id = $row->id;

            //  $w_model->update(array('zone_id' => 0), 'zone_id = ' . $id);
            //  if (!empty($values['title'])) foreach ($values['title'] as $groups_id) {
            //     $w_model->update(array('city_id' => $id), 'id = ' . $groups_id);
            //  }
            /*-------------------------------------------------------------------------------*/
            $c_model = new  Model_ZonesWhitelists();

            $c_model->update(['active' => 0], 'zone_id = '.$id);
            if (! empty($values['title'])) {
                foreach ($values['title'] as $wlist_id) {
                    $line = $c_model->fetchRow($c_model->info('name').'.zone_id = '.$id.' AND '.$c_model->info('name').'.wg_list_id = '.$wlist_id);

                    if ($line) {
                        $c_model->update([
                            'zone_id' => $id,
                            'wg_list_id' => $wlist_id,
                            'active' => '1',
                        ], 'zone_id = '.$id.' AND wg_list_id = '.$wlist_id);
                    } else {
                        $c_model->save([
                            'zone_id' => $id,
                            'wg_list_id' => $wlist_id,
                            'active' => '1',
                        ]);
                    }
                }
            }

            Model_ZoneCauseRates::sync($id, $this->_request->getParam('fine_rates'));

            /*-------------------------------------------------------------------------------*/
            $this->_redirect($this->view->baseUrl('configurations/zones-list'));
        }

        $z_model = new Model_ZonesWhitelists();
        $row = $model->find((int) $id)->current();
        if ($row) {
            $wlg = $row->toArray();
            $wlg['title'] = $z_model->getPairs($z_model->fetchAll('zone_id = '.$id." AND active='1'"), 'wg_list_id',
                'wg_list_id');
            // $wlg['title'] = $z_model->getPairs($z_model->fetchAll('zone_id = ' . $id), 'primary_wg_id', 'primary_wg_id');
            $form->populate($wlg);

            $this->view->fine_rates = $z_model->getPairs((new Model_ZoneCauseRates)->fetchAll('zone_id = '.$id), 'cause_id', 'rate');
        }


        $this->view->form = $form;
    }

    public function zonesListAction()
    {
        $model = new Model_Zone();
        $this->registerStandartActions($model);
        $this->view->items = $model->getWlist();
    }
    /**
     * @throws Zend_Db_Table_Exception
     * @throws Zend_Form_Exception
     */

    /**
     * CITY LIST CONFIGS.
     */
    public function citiesInsertAction()
    {
        $id = (int) $this->_getParam('id');
        $model = new Model_City();
        $z_model = new Model_Zone();

        $form = new Admin_Form_City($id);
        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $values = $form->getValues();
            $city = $model->save($values);
            $id = $city->id;

            $z_model->update(['city_id' => 0], 'city_id = '.$id);
            if (! empty($values['zone_name'])) {
                foreach ($values['zone_name'] as $zone_id) {
                    $z_model->update(['city_id' => $id], 'id = '.$zone_id);
                }
            }

            $this->_redirect($this->view->baseUrl('configurations/cities-list'));
        }

        $row = $model->find((int) $id)->current();
        if ($row) {
            $city = $row->toArray();
            $city['zone_name'] = $z_model->getPairs($z_model->fetchAll('city_id = '.$id), 'id', 'id');
            $form->populate($city);
        }

        $this->view->form = $form;
    }

    public function citiesListAction()
    {
        $model = new Model_City();
        $this->registerStandartActions($model);
        $this->view->items = $model->getCities();
    }

    /**
     * WHITE LIST CONFIGS.
     */
    public function whiteListGroupInsertAction()
    {
        $id = (int) $this->_getParam('id');
        $model = new Model_WhiteListGroups();
        $form = new Admin_Form_WhiteListGroup();
        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $values = $form->getValues();
            $row = $model->save($values);
            $id = $row->id;
            $this->_redirect($this->view->baseUrl('configurations/white-list-groups-list'));
        }
        $row = $model->find((int) $id)->current();
        if ($row) {
            $form->populate($row->toArray());
        }
        $this->view->form = $form;
    }

    public function whiteListGroupsListAction()
    {
        $model = new Model_WhiteListGroups();
        //$this->registerStandartActions( $model );
        $delete = (int) $this->_getParam('delete');

        if ($delete) {
            $wl_model = new Model_WhiteList();
            $items = $wl_model->fetchAll('group_id = '.$delete);
            if ($items->count() == 0) {
                $model->delete('id = '.$delete);
            } else {
                die('Group is not empty!');
            }
        }
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $uc = new Model_UserCities();
        $zone = new Model_Zone;
        $zone_wlg = new Model_ZonesWhitelists();
        $wlg_model = new Model_WhiteListGroups();
        $select2 = $uc->select()->from(['z' => $uc->info('name')], ['z.city_id'])->where('active = 1 and user_id = ?',
            (int) $id);
        $select3 = $zone->select()->from(['z' => $zone->info('name')],
            ['z.id'])->where('city_id in ('.$select2.')');
        $select4 = $zone_wlg->select()->from(['z' => $zone_wlg->info('name')],
            ['z.wg_list_id'])->where('zone_id in ('.$select3.')');

        $this->view->items = $model->fetchAll('visible = 1 and id in('.$select4.')');
    }

    /**
     * BLACK LIST CONFIGS.
     */
    public function blackListCauseInsertAction()
    {
        $id = (int) $this->_getParam('id');
        $model = new Model_BlackListCauses();
        $form = new Admin_Form_BlackListCause();
        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $values = $form->getValues();
            $row = $model->save($values);
            $id = $row->id;
            $this->_redirect($this->view->baseUrl('configurations/black-list-causes-list'));
        }
        $row = $model->find((int) $id)->current();
        if ($row) {
            $form->populate($row->toArray());
        }
        $this->view->form = $form;
    }

    public function blackListCausesListAction()
    {
        $model = new Model_BlackListCauses();
        $this->registerStandartActions($model);
        $this->view->items = $model->fetchAll();
    }

    /**
     * FINES RATE CONFIGS.
     */
    public function fineRatesInsertAction()
    {
        $id = (int) $this->_getParam('id');
        $model = new Model_FineRates();

        $form = new Admin_Form_FineRates($id);
        if ($this->_request->isPost() && $form->isValid($_POST)) {
            $values = $form->getValues();
            $rate = $model->save($values);
            $id = $rate->id;

            $this->_redirect($this->view->baseUrl('configurations/fine-rates-list'));
        }

        if ($row = $model->find((int) $id)->current()) {
            $form->populate($row->toArray());
        }

        $this->view->form = $form;
    }

    public function fineRatesListAction()
    {
        $model = new Model_FineRates();
        $this->registerStandartActions($model);
        $this->view->items = $model->fetchAll();
    }


    public function translationsAction()
    {
        $this->view->lng = $lng = $this->_request->getParam('lng');
        $model = new Model_Translate();

        if ($this->_request->isPost()) {
            $id = (int) $this->_getParam('id');
            $val = $this->_getParam('val');
            if ($id && $val) {
                $model->update(['msgstring' => $val], 'id = '.$id);
                die();
            }
        }

        $this->view->items = $model->fetchAll("locale = '$lng'");
    }

    public static function getNavigation()
    {
        $config = Zend_Registry::get('cmsEnv');
        $menu = [];
        foreach ($config->languages as $lang) {
            $menu['translations_'.$lang] = [
                'label' => Core_Locale::translate('Translations '.strtoupper($lang)),
                'module' => 'admin',
                'controller' => 'configurations',
                'action' => 'translations',
                'params' => [
                    'lang' => LANG,
                    'lng' => $lang,
                ],
            ];
        }
        $menu2 = [
            'configurations_cities' => [
                'label' => Core_Locale::translate('Cities'),
                'module' => 'admin',
                'controller' => 'configurations',
                'action' => 'cities-list',
                'params' => [
                    'lang' => LANG,
                ],
                'pages' => [
                    'configurations_cities' => [
                        'label' => Core_Locale::translate('Add City'),
                        'module' => 'admin',
                        'controller' => 'configurations',
                        'action' => 'cities-insert',
                        'visible' => false,
                        'params' => [
                            'lang' => LANG,
                        ],
                    ],
                ],
            ],
            'configurations_white_list_groups' => [
                'label' => Core_Locale::translate('White List Groups'),
                'module' => 'admin',
                'controller' => 'configurations',
                'action' => 'white-list-groups-list',
                'params' => [
                    'lang' => LANG,
                ],
                'pages' => [
                    'configurations_cities' => [
                        'label' => Core_Locale::translate('Add White List Group'),
                        'module' => 'admin',
                        'controller' => 'configurations',
                        'action' => 'white-list-group-insert',
                        'visible' => false,
                        'params' => [
                            'lang' => LANG,
                        ],
                    ],
                ],
            ],
            'configurations_black_list_causes' => [
                'label' => Core_Locale::translate('Fine Causes List'),
                'module' => 'admin',
                'controller' => 'configurations',
                'action' => 'black-list-causes-list',
                'params' => [
                    'lang' => LANG,
                ],
                'pages' => [
                    'configurations_cities' => [
                        'label' => Core_Locale::translate('Add Fine Cause'),
                        'module' => 'admin',
                        'controller' => 'configurations',
                        'action' => 'black-list-cause-insert',
                        'visible' => false,
                        'params' => [
                            'lang' => LANG,
                        ],
                    ],
                ],
            ],
            /*
            'configurations_fine_status' => [
                'label' => Core_Locale::translate('Fine Statuses'),
                'module' => 'admin',
                'controller' => 'configurations',
                'action' => 'fine-status-list',
                'params' => [
                    'lang' => LANG,
                ],
                'pages' => [
                    'configurations_cities' => [
                        'label' => Core_Locale::translate('Add Fine Staus'),
                        'module' => 'admin',
                        'controller' => 'configurations',
                        'action' => 'fine-status-insert',
                        'visible' => false,
                        'params' => [
                            'lang' => LANG,
                        ],
                    ],
                ],
            ], */
            'configurations_zones' => [
                'label' => Core_Locale::translate('Zones'),
                'module' => 'admin',
                'controller' => 'configurations',
                'action' => 'zones-list',
                'params' => [
                    'lang' => LANG,
                ],
                'pages' => [
                    'configurations_cities' => [
                        'label' => Core_Locale::translate('Add Zone'),
                        'module' => 'admin',
                        'controller' => 'configurations',
                        'action' => 'zones-insert',
                        'visible' => false,
                        'params' => [
                            'lang' => LANG,
                        ],
                    ],
                ],
            ],
            'configurations_fine_rates' => [
                'label' => Core_Locale::translate('Fine rates list'),
                'module' => 'admin',
                'controller' => 'configurations',
                'action' => 'fine-rates-list',
                'params' => [
                    'lang' => LANG,
                ],
                'pages' => [
                    'configurations_fine_rate' => [
                        'label' => Core_Locale::translate('Add Fine Rate'),
                        'module' => 'admin',
                        'controller' => 'configurations',
                        'action' => 'fine-rates-insert',
                        'visible' => false,
                        'params' => [
                            'lang' => LANG,
                        ],
                    ],
                ],
            ],
        ];

        $full_menu = array_merge($menu, $menu2);
        $nav = [
            'label' => Core_Locale::translate('Configurations'),
            'module' => 'admin',
            'controller' => 'configurations',
            'action' => 'index',
            'class' => 'awe-wrench',
            'order' => 200,
            'pages' => $full_menu,
        ];

        return $nav;
    }
}
