<?php

/**
 * Description of Auth.
 *
 * @author Darius Matulionis
 */
class Admin_AuthController extends Zend_Controller_Action
{
    public function init()
    {
    }

    public function indexAction()
    {
        $this->_redirect('/admin/auth/login');
    }

    public function loginAction()
    {
        $this->_helper->layout()->disableLayout();
        $users = new Model_Admins();

        $form = new Admin_Form_Login();

        $this->view->form = $form;

        if ($this->getRequest()->isPost() && $form->isValid($_POST)) {
            $data = $form->getValues();

            $auth = Core_Auth_Admin::getInstance();

            $authAdapter = new Zend_Auth_Adapter_DbTable($users->getAdapter(), 'box_admins');

            $authAdapter->setIdentityColumn('adm_login')->setCredentialColumn('adm_password');

            $authAdapter->setIdentity($data ['adm_login'])->setCredential(sha1($data ['adm_password']));

            $result = $auth->authenticate($authAdapter);

            if ($result->isValid()) {
                if ($data['remember_me']) {
                    Zend_Session::rememberMe(3600000);
                }

                $storage = new Zend_Auth_Storage_Session();
                $userData = $authAdapter->getResultRowObject();

                $g_model = new Model_AdminGroups();
                $a2g_model = new Model_AdminsHasAdminGroups();

                $group = $g_model->find($userData->admin_groups_id)->current();
                $userData->group_title = $group->title;
                if (! $userData->adm_root) {
                    $userData->permisions = unserialize($group->permisions);
                    $groups = $a2g_model->fetchAll('admins_id = '.$userData->id);

                    if ($groups->count() > 0) {
                        foreach ($groups as $key => $value) {
                            $grp = $g_model->find($value->admin_groups_id)->current();
                            $userData->groups[$value->admin_groups_id] = $value->admin_groups_id;
                            $grp->permisions = unserialize($grp->permisions);
                            foreach ($grp->permisions as $perm => $value) {
                                if ((! isset($userData->permisions [$perm]) || $userData->permisions [$perm] == 0) && $value == 1) {
                                    $userData->permisions [$perm] = 1;
                                }
                            }
                        }
                    }

                    //$grp_model = new Model_AdminGroups();
                    //$grp = $grp_model->find($session->admin_groups_id)->current();
                    //if($grp){
                    //$session->permisions = unserialize($grp->permisions);
                    //}
                }
                $storage->write($userData);

                $user = $users->find(Core_Auth_Admin::getInstance()->getStorage()
                    ->read()->id)
                    ->current();
                $user->adm_last_login = date('Y-m-d H:i:s');
                $user->adm_last_ip = $_SERVER ['REMOTE_ADDR'];
                $user->save();
                $this->_redirect('/admin/'.LANG);
            } else {
                $this->view->errorMessage = 'Invalid username or password. Please try again.';
            }
        }
    }

    public function logoutAction()
    {
        $storage = new Zend_Auth_Storage_Session();
        $storage->clear();
        $this->_redirect('/admin/auth/login');
    }
}
