<?php

class Admin_Form_Zone extends Core_Form
{
    protected $checkbox_deco;

    public function __construct($id = null, $options = null)
    {
        parent::__construct();
        $this->setAttrib('class', 'form-horizontal');
        $this->removeDecorator('DtDdWrapper');
        $this->removeDecorator('HtmlTag');
        $this->addElementPrefixPath('Core_Validate', 'Core/Validate', 'validate');
        $this->addElement($this->createElement('hidden', 'id')->setDecorators([])->addDecorator('ViewHelper'));
        $this->addElement($this->createElement('checkbox',
            'visible')->setLabel('Visible')->setValue(1));

        /*  $time = array(
              '07:00:00'=> '07:00:00',
              '08:00:00'=> '08:00:00',
              '09:00:00'=> '09:00:00',
              '10:00:00'=> '10:00:00',
              '11:00:00'=> '11:00:00',
              '12:00:00'=> '12:00:00',
              '13:00:00'=> '13:00:00',
              '14:00:00'=> '14:00:00',
              '15:00:00'=> '15:00:00',
              '16:00:00'=> '16:00:00',
              '17:00:00'=> '17:00:00',
              '18:00:00'=> '18:00:00',
              '19:00:00'=> '19:00:00',
              '20:00:00'=> '20:00:00',
              '21:00:00'=> '21:00:00',
              '22:00:00'=> '22:00:00',);
        */
        $domain = new Model_Domain();
        $domain = $domain->fetchrow("name like '%".$_SERVER['HTTP_HOST']."%' ");
        if ($domain['billing'] == 1) {
            $this->addElement($this->createElement('text', 'billing_id')
                ->setLabel('billing_id')
                ->setRequired(true)
                ->addValidator('notEmpty', true));
        }
        $this->addElement($this->createElement('text', 'zone_name')
            ->setLabel('Zone_name')
            ->setRequired(true)
            ->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text', 'zone_code')
            ->setLabel('Zone_code')
            ->setRequired(true)
            ->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text', 'hour_price')
            ->setLabel('Hour_price')
            ->setRequired(true)
            ->addValidator('notEmpty', true));

        $this->addElement($this->createElement('select', 'work_time_begin', [
            'multiOptions' => [
                '00:00:00' => '00:00:00',
                '01:00:00' => '01:00:00',
                '02:00:00' => '02:00:00',
                '03:00:00' => '03:00:00',
                '04:00:00' => '04:00:00',
                '05:00:00' => '05:00:00',
                '06:00:00' => '06:00:00',
                '07:00:00' => '07:00:00',
                '08:00:00' => '08:00:00',
                '09:00:00' => '09:00:00',
                '10:00:00' => '10:00:00',
                '11:00:00' => '11:00:00',
                '12:00:00' => '12:00:00',
                '13:00:00' => '13:00:00',
                '14:00:00' => '14:00:00',
                '15:00:00' => '15:00:00',
                '16:00:00' => '16:00:00',
                '17:00:00' => '17:00:00',
                '18:00:00' => '18:00:00',
                '19:00:00' => '19:00:00',
                '20:00:00' => '20:00:00',
                '21:00:00' => '21:00:00',
                '22:00:00' => '22:00:00',
                '23:00:00' => '23:00:00',
                '23:59:59' => '23:59:59',
            ],
        ])
            ->setLabel('Work_time_begin')
            ->setRequired(true)
            ->addValidator('notEmpty', true));

        $this->addElement($this->createElement('select', 'work_time_end', [
            'multiOptions' => [
                '00:00:00' => '00:00:00',
                '01:00:00' => '01:00:00',
                '02:00:00' => '02:00:00',
                '03:00:00' => '03:00:00',
                '04:00:00' => '04:00:00',
                '05:00:00' => '05:00:00',
                '06:00:00' => '06:00:00',
                '07:00:00' => '07:00:00',
                '08:00:00' => '08:00:00',
                '09:00:00' => '09:00:00',
                '10:00:00' => '10:00:00',
                '11:00:00' => '11:00:00',
                '12:00:00' => '12:00:00',
                '13:00:00' => '13:00:00',
                '14:00:00' => '14:00:00',
                '15:00:00' => '15:00:00',
                '16:00:00' => '16:00:00',
                '17:00:00' => '17:00:00',
                '18:00:00' => '18:00:00',
                '19:00:00' => '19:00:00',
                '20:00:00' => '20:00:00',
                '21:00:00' => '21:00:00',
                '22:00:00' => '22:00:00',
                '23:00:00' => '23:00:00',
                '23:59:59' => '23:59:59',
            ],
        ])
            ->setLabel('Work_time_end')
            ->setRequired(true)
            ->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text', 'warning_pay_time')
            ->setLabel('Warning_pay_time')
            ->setRequired(true)
            ->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text', 'warning_price')
            ->setLabel('Warning_price')
            ->setRequired(true)
            ->addValidator('notEmpty', true));

        $w_model = new Model_WhiteListGroups();
        $w_select = $w_model->select();

        $this->addElement($this->createElement('Multiselect', 'title')
            ->setLabel('Avaible white list groups')
            ->setAttrib('style', 'width:100%')
            ->setMultiOptions($w_model->getPairs($w_model->fetchAll($w_select), 'id', 'title'))
            ->setRequired(false)
        );

        $this->addElement($this->createElement('text', 'tolerance_time')
            ->setLabel('Tolerance_time')
            ->setRequired()
            ->addValidator('notEmpty', true)
            );

        $this->addElement($this->createElement('text', 'minutes_to_not_paid')
            ->setLabel('minutes_to_not_paid')
            );

        $this->addElement($this->createElement('checkbox', 'pvm')
            ->setLabel('pvm')
            );

        $this->addElement($this->createElement('text', 'usms_zone')
            ->setLabel('usms_zone')
            );
        if (strpos($domain['name'], 'parkingee.unipark.lt') !== false) {
            $this->addElement($this->createElement('text', 'published_time')
                ->setLabel('published_time')
            );
            $this->addElement($this->createElement('text', 'enforcement_time')
                ->setLabel('enforcement_time')
            );
        }
        $this->addElement($this->createElement('text', 'zone_address')
            ->setLabel('zone_address')
        );
        $this->addSubmitButtons();
    }
}
