<?php

class Admin_Form_BlitzAgency extends Core_Form
{
    protected $checkbox_deco;

    public function __construct($id = null, $options = null)
    {
        parent::__construct();
        $this->setAttrib('class', 'form-horizontal');
        $this->removeDecorator('DtDdWrapper');
        $this->removeDecorator('HtmlTag');
        $this->addElementPrefixPath('Core_Validate', 'Core/Validate', 'validate');
        $this->addElement($this->createElement('hidden', 'id')->addDecorator('ViewHelper'));

        $model = new Model_Blitz();
        $select = $model->select();
        $select->where('id = ?', 0);
        $order_type_set = new Model_BlitzOrderTypeSet();

        if ($id) {
            $select->orWhere('id = ?', $id);
        }

        if ($id['id']) {
            $order = $model->getOrderType($id['id']);
            $order_type_id = $order['order_type'];
        } else {
            $order_type_id = '1';
        }

        $elementDecorators = [
            'ViewHelper',
            ['Label', ['requiredSuffix' => ' *']],
        ];

        $title = Core_Auth_Admin::getInstance()->getStorage()->read()->group_title;
        $model = new Model_AdminGroups();
        $hide = $model->hideBlitzinfo($title);

        $order_type = new Model_BlitzOrderType();
        $order_type_select = $order_type->select();

        $opts = $order_type_set->getStatusesByType($order_type_id);

        $w_model = new Model_BlitzStatuses();
        $w_select = $w_model->select();

        if ($hide['hide_blitzinfo'] == '1') {
            $this->addElement($this->createElement('select', 'order_type')
                ->setLabel('Užsakymo tipas')
                ->setMultiOptions($order_type->getPairs($order_type->fetchAll($order_type_select), 'id', 'type'))
                ->setRequired(true)
                ->setAttrib('disabled', 'disabled')
            );
            $this->addElement($this->createElement('Select', 'status')
                ->setLabel('Statusas')
                ->setMultiOptions($order_type_set->getPairs($opts, 'status_id', 'name'))
                ->setRequired(true)
            );
            $this->addElement($this->createElement('text',
                'invoice_nr')->setLabel('Sutarties nr.')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'plate')->setLabel('Valstybinis numeris')->setRequired(true)->addValidator('notEmpty',
                true)->setDecorators($elementDecorators)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'date_from')->setLabel(Core_Locale::translate('Data nuo'))->setRequired(true)->addValidator('notEmpty',
                true)->setDecorators($elementDecorators)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'date_to')->setLabel('Data iki')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'flight_number')->setLabel('Grįžimo skrydžio numeris')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'name')->setLabel('Vardas')->setRequired(false)->addValidator('notEmpty', true)->setAttrib('disabled',
                'disabled'));
            $this->addElement($this->createElement('text',
                'surname')->setLabel('Pavardė')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'phone')->setLabel('Telefono numeris')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'email')->setLabel('El. Paštas')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'birth_date')->setLabel('Gimimo data ')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'model')->setLabel('Modelis')->setRequired(false)->addValidator('notEmpty', true)->setAttrib('disabled',
                'disabled'));
            $this->addElement($this->createElement('text',
                'tech_doc_nr')->setLabel('Techninio paso Nr.')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'insurance_nr')->setLabel('Draudimo poliso Nr.')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'mileage_before')->setLabel('Rida atidavimo metu')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'mileage_after')->setLabel('Rida pasiėmimo metu')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'day_amount')->setLabel('Dienų skaičius')->setRequired(false)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('select', 'washing', [
                'multiOptions' => [
                    'vidus/išorė' => 'vidus/išorė',
                    'išorė' => 'išorė',
                ],
            ])->setLabel('Plovimas')
                ->setRequired(true)
                ->addValidator('notEmpty', true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'fast_track_nr')->setLabel('Greitosios patikros kuponų sk.')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'club_membership')->setLabel('Verslo klubo narystė')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'amount')->setLabel('Suma be PVM')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'amount_pvm')->setLabel('Suma su PVM')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'club_membership_nr')->setLabel('Verslo klubo narystės Nr.')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'company')->setLabel('Įmonė')->setRequired(false)->addValidator('notEmpty', true)->setAttrib('disabled',
                'disabled'));
            $this->addElement($this->createElement('text',
                'contract_nr')->setLabel('Sutarties numeris su UAB „Stova“')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'company_code')->setLabel('Įmonės kodas')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'pvm_code')->setLabel('PVM kodas')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'company_phone')->setLabel('Įmonės telefono Nr.')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'company_email')->setLabel('Įmonės El. paštas')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'company_address')->setLabel('Įmonės adresas')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('text',
                'address')->setLabel('Adresas')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
            $this->addElement($this->createElement('select', 'pvm_invoice', [
                'multiOptions' => [
                    'Taip' => 'Taip',
                    'Ne' => 'Ne',
                ],
            ])->setLabel('Įtraukti į PVM sąskaitą faktūrą')
                ->setRequired(true)
                ->addValidator('notEmpty', true)->setAttrib('disabled', 'disabled'));

            $Type_model = new Model_BlitzPaymentType();
            $PT_select = $Type_model->select();

            $this->addElement($this->createElement('Select', 'payment_type')
                ->setLabel('Apmokėjimo būdas')
                ->setMultiOptions($Type_model->getPairs($Type_model->fetchAll($PT_select), 'name', 'name'))
                ->setRequired(true)
                ->setAttrib('disabled', 'disabled')
            );
            $this->addElement($this->createElement('file',
                'img_attach')->setDestination(APPLICATION_PATH)->setLabel('Nuotrauka')->setRequired(false)->addValidator('notEmpty',
                true)->setAttrib('disabled', 'disabled'));
        } else {
            $this->addElement($this->createElement('select', 'order_type')
                ->setLabel('Užsakymo tipas')
                ->setMultiOptions($order_type->getPairs($order_type->fetchAll($order_type_select), 'id', 'type'))
                ->setRequired(true)
            );
            $this->addElement($this->createElement('Select', 'status')
                ->setLabel('Statusas')
                ->setMultiOptions($order_type_set->getPairs($opts, 'status_id', 'name'))
                ->setRequired(true)
            );
            $this->addElement($this->createElement('text',
                'invoice_nr')->setLabel('Sutarties nr.')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'plate')->setLabel('Valstybinis numeris')->setRequired(true)->addValidator('notEmpty',
                true)->setDecorators($elementDecorators));
            $this->addElement($this->createElement('text',
                'date_from')->setLabel(Core_Locale::translate('Data nuo'))->setRequired(true)->addValidator('notEmpty',
                true)->setDecorators($elementDecorators));
            $this->addElement($this->createElement('text',
                'date_to')->setLabel('Data iki')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'flight_number')->setLabel('Grįžimo skrydžio numeris')->setRequired(false)->addValidator('notEmpty',
                true));
            $this->addElement($this->createElement('text',
                'name')->setLabel('Vardas')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'surname')->setLabel('Pavardė')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'phone')->setLabel('Telefono numeris')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'email')->setLabel('El. Paštas')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'birth_date')->setLabel('Gimimo data ')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'model')->setLabel('Modelis')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'tech_doc_nr')->setLabel('Techninio paso Nr.')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'insurance_nr')->setLabel('Draudimo poliso Nr.')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'mileage_before')->setLabel('Rida atidavimo metu')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'mileage_after')->setLabel('Rida pasiėmimo metu')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'day_amount')->setLabel('Dienų skaičius')->setRequired(false));
            $this->addElement($this->createElement('select', 'washing', [
                'multiOptions' => [
                    'vidus/išorė' => 'vidus/išorė',
                    'išorė' => 'išorė',
                ],
            ])->setLabel('Plovimas')
                ->setRequired(true)
                ->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'fast_track_nr')->setLabel('Greitosios patikros kuponų sk.')->setRequired(false)->addValidator('notEmpty',
                true));
            $this->addElement($this->createElement('text',
                'club_membership')->setLabel('Verslo klubo narystė')->setRequired(false)->addValidator('notEmpty',
                true));
            $this->addElement($this->createElement('text',
                'amount')->setLabel('Suma be PVM')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'amount_pvm')->setLabel('Suma su PVM')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'club_membership_nr')->setLabel('Verslo klubo narystės Nr.')->setRequired(false)->addValidator('notEmpty',
                true));
            $this->addElement($this->createElement('text',
                'company')->setLabel('Įmonė')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'contract_nr')->setLabel('Sutarties numeris su UAB „Stova“')->setRequired(false)->addValidator('notEmpty',
                true));
            $this->addElement($this->createElement('text',
                'company_code')->setLabel('Įmonės kodas')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'pvm_code')->setLabel('PVM kodas')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'company_phone')->setLabel('Įmonės telefono Nr.')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'company_email')->setLabel('Įmonės El. paštas')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'company_address')->setLabel('Įmonės adresas')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('text',
                'address')->setLabel('Adresas')->setRequired(false)->addValidator('notEmpty', true));
            $this->addElement($this->createElement('select', 'pvm_invoice', [
                'multiOptions' => [
                    'Taip' => 'Taip',
                    'Ne' => 'Ne',
                ],
            ])->setLabel('Įtraukti į PVM sąskaitą faktūrą')
                ->setRequired(true)
                ->addValidator('notEmpty', true));

            $Type_model = new Model_BlitzPaymentType();
            $PT_select = $Type_model->select();

            $this->addElement($this->createElement('Select', 'payment_type')
                ->setLabel('Apmokėjimo būdas')
                ->setMultiOptions($Type_model->getPairs($Type_model->fetchAll($PT_select), 'name', 'name'))
                ->setRequired(true)
            );
            //$this->addElement( $this->createElement('file', 'img_attach')->setLabel('Nuotrauka')->setRequired(false)->addValidator('notEmpty', true) );
        }
    }
}
