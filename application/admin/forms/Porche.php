<?php

class Admin_Form_Porche extends Core_Form
{
    protected $checkbox_deco;

    public function __construct($id = null, $options = null)
    {
        parent::__construct();
        $this->setAttrib('class', 'form-horizontal');
        $this->removeDecorator('DtDdWrapper');
        $this->removeDecorator('HtmlTag');
        $this->addElementPrefixPath('Core_Validate', 'Core/Validate', 'validate');
        $this->addElement($this->createElement('hidden', 'id')->addDecorator('ViewHelper'));

        $model = new Model_Porche();
        $statusesDb = new Model_PorcheStatuses();
        $servicesDb = new Model_PorcheServices();
        $serviceStatusDb = new Model_PorcheServiceStatuses();
        $orderServices = new Model_PorcheOrderServices();
        $paymentTypeStatuses = new Model_PorchePaymentTypeStatuses();

        $status_select = $statusesDb->select();
        $PT_select = $paymentTypeStatuses->select();

        $elementDecorators = [
            'ViewHelper',
            ['Label', ['requiredSuffix' => ' *']],
        ];

        if ($id['id']) {
            $row = $model->fetchRow('id = '. $id['id']);
            $servicesArray = json_decode($row['services']);

            foreach ($servicesArray as $s){

                $serviceSelect = $servicesDb->fetchRow('id = '. $s->service_id);
                $serviceName = $serviceSelect['name'];
                $serviceStatusesArray = $serviceStatusDb->fetchAll('service_id = '. $s->service_id);
                $activeStatus = $orderServices->fetchRow('porche_id = '. $id['id'].' and service_id ='.$s->service_id.' and count_id ='.$s->count_id);
                $serviceStatus = $activeStatus['status_id'];

                $this->addElement($this->createElement('Select', 'service_status_'.$s->service_id.'_'.$s->count_id)
                    ->setLabel($serviceName)
                    ->setValue($serviceStatus)
                    ->setMultiOptions($serviceStatusDb->getPairs($serviceStatusesArray, 'id', 'status'))
                    ->setRequired(true)
                );
            }
        }


        $this->addElement($this->createElement('Select', 'status')
            ->setLabel('Statusas')
            ->setMultiOptions($statusesDb->getPairs($statusesDb->fetchAll($status_select), 'id', 'name'))
            ->setRequired(true)
        );

        $this->addElement($this->createElement('Select', 'payment_type')
            ->setLabel('Apmokėjimo būdas')
            ->setMultiOptions($paymentTypeStatuses->getPairs($paymentTypeStatuses->fetchAll($PT_select), 'id', 'name'))
            ->setRequired(true)
        );

        $this->addElement($this->createElement('text',
            'plate')->setLabel('Valstybinis numeris')->setRequired(true)->addValidator('notEmpty',
            true)->setDecorators($elementDecorators));

        $this->addElement($this->createElement('text',
            'date_from')->setLabel(Core_Locale::translate('Data nuo'))->setRequired(true)->addValidator('notEmpty',
            true)->setDecorators($elementDecorators));

        $this->addElement($this->createElement('text',
            'date_to')->setLabel('Data iki')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'flight_number')->setLabel('Grįžimo skrydžio numeris')->setRequired(false)->addValidator('notEmpty',
            true));

        $this->addElement($this->createElement('text',
            'name')->setLabel('Vardas Pavardė')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'phone')->setLabel('Telefono numeris')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'email')->setLabel('El. Paštas')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'birth_date')->setLabel('Gimimo data ')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'tech_doc_nr')->setLabel('Techninio paso Nr.')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'insurance_nr')->setLabel('Draudimo poliso Nr.')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'contract_nr')->setLabel('Sutarties numeris su UAB „Stova“')->setRequired(false)->addValidator('notEmpty',
            true));

        $this->addElement($this->createElement('text',
            'address')->setLabel('Adresas')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'birth_date')->setLabel('Gimimo data')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'mileage_before')->setLabel('Rida atidavimo metu')->setRequired(false)->addValidator('notEmpty', true));

        $this->addElement($this->createElement('text',
            'mileage_after')->setLabel('Rida pasiėmimo metu')->setRequired(false)->addValidator('notEmpty', true));


        //$this->addElement( $this->createElement('file', 'img_attach')->setLabel('Nuotrauka')->setRequired(false)->addValidator('notEmpty', true) );
    }
}
