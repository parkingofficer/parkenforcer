<?php

class Admin_Form_BlackList extends Core_Form
{
    protected $checkbox_deco;

    public function __construct($options = null)
    {
        parent::__construct();
        $this->setAttrib('class', 'form-horizontal');
        $this->removeDecorator('DtDdWrapper');
        $this->removeDecorator('HtmlTag');
        $this->addElementPrefixPath('Core_Validate', 'Core/Validate', 'validate');
        $this->addElement($this->createElement('hidden', 'id')->setDecorators([])->addDecorator('ViewHelper'));
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $zones = new Model_UserCities();
        $zone = new Model_Zone();
        $select2 = $zones->select()->from(['z' => $zones->info('name')], ['z.city_id'])->where('user_id = ?', (int) $id);
        $select3 = $zone->select()->from(['z' => $zone->info('name')],
            ['z.id'])->where('city_id in ('.$select2.')');

        $c_model = new Model_BlackListCauses();
        $pairs = $c_model->getPairs($c_model->fetchAll('visible = 1'), 'id', 'title');
        $this->addElement($this->createElement('select',
            'cause_id')->setLabel('Cause')->setMultioptions($pairs)->setRequired(true)->addValidator('notEmpty',
            true)->setDecorators($this->controlGroupDeco));
        $this->addElement($this->createElement('text',
            'plate_no')->setLabel('Plate')->setRequired(true)->addValidator('notEmpty',
            true)->setDecorators($this->controlGroupDeco));
        $c_model = new Model_Zone();
        $this->addElement($this->createElement('select',
            'zone_code')->setLabel('Zone')->setMultiOptions($c_model->getPairs($c_model->fetchAll('id in('.$select3.')'),
            'id', 'zone_code'))->setRequired(false)->addValidator('notEmpty',
            false)->setDecorators($this->controlGroupDeco));

        $time_validarot = new Zend_Validate_Date();
        $time_validarot->setFormat('Y-m-d H:i:s');
        $this->addElement($this->createElement('text',
            'time_checked')->setLabel('Time checked')->setValue(date('Y-m-d H:i:s'))->setRequired(true)->addValidator($time_validarot)->addValidator('notEmpty',
            true)->setDecorators($this->controlGroupDeco));
        //$this->addElement( $this->createElement( 'text', 'lat' )->setLabel( 'Latitude' )->setRequired( true )->addValidator('float')->addValidator( 'notEmpty', true )->setDecorators($this->controlGroupDeco) );
        //$this->addElement( $this->createElement( 'text', 'lon' )->setLabel( 'Longitude' )->setRequired( true )->addValidator('float')->addValidator( 'notEmpty', true )->setDecorators($this->controlGroupDeco) );
        $this->addSubmitButtons();
    }
}
//
