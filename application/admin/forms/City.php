<?php

class Admin_Form_City extends Core_Form
{
    protected $checkbox_deco;

    public function __construct($id = null, $options = null)
    {
        parent::__construct();
        $this->setAttrib('class', 'form-horizontal');
        $this->removeDecorator('DtDdWrapper');
        $this->removeDecorator('HtmlTag');
        $this->addElementPrefixPath('Core_Validate', 'Core/Validate', 'validate');
        $this->addElement($this->createElement('hidden', 'id')->setDecorators([])->addDecorator('ViewHelper'));
        $this->addElement($this->createElement('checkbox',
            'visible')->setLabel('Visible')->setValue(1)->setDecorators($this->controlGroupDeco));
        $this->addElement($this->createElement('text',
            'CityName')->setLabel('City Name')->setRequired(true)->addValidator('notEmpty',
            true)->setDecorators($this->controlGroupDeco));

        // $model2 = new Model_Zone ();
        //  $row = $model2->fetchRow($model2->select()->where('Taken = ?', 0));
        // Change the value of one or more columns
        //  $row-> Taken = '1' ;
        // UPDATE the row in the database with new values

        $model = new Model_Zone();
        $select = $model->select();
        $select->where('city_id = ?', 0);
        if ($id) {
            $select->orWhere('city_id = ?', $id);
        }

        $this->addElement($this->createElement('Multiselect', 'zone_name')
            ->setLabel('Avaible Zones')
            ->setMultiOptions($model->getPairs($model->fetchAll($select), 'id', 'zone_name'))
            ->setRequired(false));

        $this->addSubmitButtons();
    }
}
