<?php

/**
 * Admin LogIn Form
 *
 * @author Darius Matulionis
 */
class Admin_Form_Login extends Zend_Form
{
    public function init()
    {
        $viewHelper = [['ViewHelper', 'Error']];

        $this->addElement($this->createElement('text',
            'adm_login')->setDecorators($viewHelper)->setLabel('El. pašto adresas')->setRequired(true)->setValidators(['EmailAddress']));
        $this->addElement($this->createElement('password',
            'adm_password')->setDecorators($viewHelper)->setLabel('Slaptažodis')->setRequired(true));

        $this->addElement($this->createElement('checkbox',
            'remember_me')->setDecorators($viewHelper)->setLabel('Prisiminti mane'));


        $this->addElement($this->createElement('submit', 'Prisijungti')->removeDecorator('DtDdWrapper'));
    }
}
