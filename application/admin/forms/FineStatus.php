<?php

class Admin_Form_FineStatus extends Core_Form
{
    protected $checkbox_deco;
    public function __construct($options = null)
    {
        parent::__construct();
        $this->setAttrib('class', 'form-horizontal');
        $this->removeDecorator('DtDdWrapper');
        $this->removeDecorator('HtmlTag');
        $this->addElementPrefixPath('Core_Validate', 'Core/Validate', 'validate');
        $this->addElement($this->createElement('hidden', 'id')->setDecorators([])->addDecorator('ViewHelper'));
        $this->addElement($this->createElement('checkbox',
            'visible')->setLabel('Visible')->setValue(1)->setDecorators($this->controlGroupDeco));
        $this->addElement($this->createElement('text',
            'title')->setLabel('Title')->setRequired(true)->addValidator('notEmpty',
            true)->setDecorators($this->controlGroupDeco));
        $this->addSubmitButtons();
    }
}
