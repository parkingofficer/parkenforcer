<?php

/**
 * @name AdminGroup
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2011-12-09, 15.16.45
 */
class Admin_Form_AdminGroup extends Core_Form
{
    protected $checkbox_deco;

    public function __construct($options = null)
    {
        parent::__construct();
        $this->addElementPrefixPath('Core_Validate', 'Core/Validate', 'validate');
        $this->addElement($this->createElement('hidden', 'id')
            ->setDecorators([])
            ->addDecorator('ViewHelper'));
        $this->addElement($this->createElement('checkbox', 'visible')
            ->setLabel('Visible')
            ->setValue(1));
        $this->addElement($this->createElement('text', 'title')
            ->setLabel('Title')
            ->setRequired(true)
            ->addValidator('notEmpty', true));
        $this->checkbox_deco = [
            [
                'ViewHelper',
            ],
            'Description',
            'HtmlTag',
            'Label',
        ];

        $c_model = new Model_City();

        $cityArray = [];
        array_push($cityArray, '');
        $cityArray += $c_model->getPairs($c_model->fetchAll(), 'id', 'CityName');

        $this->addElement($this->createElement('select', 'city')
            ->setLabel('Visible Cities')
            ->setMultiOptions($cityArray)
            ->setRequired(false)
            ->addValidator('notEmpty', false)->setDecorators($this->controlGroupDeco));

        $this->addElement($this->createElement('select', 'role')
            ->setLabel('Role')
            ->setMultiOptions([
                '0' => '',
                'admin' => 'Administrator',
                'operator' => 'Operatorius',
            ])
            ->setRequired(false)
            ->addValidator('notEmpty', false)
            ->setDecorators($this->controlGroupDeco));

        $nav = new Core_Navigation_Aggregator(Zend_Controller_Front::getInstance());
        $domain = new Model_Domain();
        $domain_options = $domain->options();
        //if module not in array do not show in admin options
        foreach ($nav->getControllers() as $controller => $value) {
            if (! empty($domain_options['modules'])) {
                $modules = json_decode($domain_options['modules']);
                if (! in_array($controller, $modules)) {
                    continue;
                }
            } else {
                continue;
            }

            $prefix = substr($controller, 0, 6);
            $controller = substr($controller, 6);
            if ($prefix == 'Admin_' && $controller != 'Ajax' && $controller != 'Cron' && $controller != 'Index' && $controller != 'Auth' && $controller != 'Error') {
                if (in_array('Core_Controller_Navigation_Interface',
                    class_implements('Admin_'.ucfirst($controller).'Controller'))) {
                    $controllerName = 'Admin_'.ucfirst($controller).'Controller';
                    $navig = call_user_func($controllerName.'::getNavigation');

                    $controller = str_replace('-', '_', strtolower($controller));

                    $sub = new Zend_Form_SubForm();
                    $sub->setDescription(ucfirst($controller));
                    $sub->setLegend(ucfirst($navig['label']));
                    $this->addSubForm($sub, $controller);

                    $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($navig),
                        RecursiveIteratorIterator::SELF_FIRST);
                    foreach ($iterator as $page) {
                        if (is_array($page) && isset($page['action']) && ! isset($page['pages'])) {
                            $sub->addElement($this->createElement('checkbox',
                                str_replace('-', '_', $page ['controller']).'_'.str_replace('-', '_',
                                    $page ['action']))
                                ->setLabel($page['label'])
                                ->setDecorators($this->flTagDeco)
                                ->setBelongsTo('permisions'));
                        }
                    }
                    $this->addClear();
                }
            }
        }
        if (! empty($domain_options['modules'])) {
            $modules = json_decode($domain_options['modules']);

            if (in_array('Admin_Blitz_status_only', $modules)) {
                $dashDiv = new Zend_Form_SubForm();
                $dashDiv->setDescription('Blitz only status');
                $dashDiv->setLegend('Blitz only status');
                $this->addSubForm($dashDiv, 'blitz only status');

                $dashDiv->addElement($this->createElement('checkbox', 'hide_blitzinfo')
                    ->setLabel('Change only status in blitz')
                    ->setDecorators($this->flTagDeco)
                    ->setBelongsTo('hide_blitzinfo')
                );
            }
            if (in_array('Admin_Parking_Show_Buttons', $modules) && ! empty($domain_options['parking_update_buttons'])) {
                $importDiv = new Zend_Form_SubForm();
                $importDiv->setDescription('Operatoriu importas');
                $importDiv->setLegend('Operatoriu importas');
                $this->addSubForm($importDiv, 'operatoriu importas');

                $importDiv->addElement($this->createElement('checkbox', 'hide_operators')
                    ->setLabel('Hide operators imports')
                    ->setDecorators($this->flTagDeco)
                    ->setBelongsTo('hide_operators')
                );
            }
            if (in_array('Admin_Dashboard', $modules)) {
                $dashDiv = new Zend_Form_SubForm();
                $dashDiv->setDescription('Dashboard');
                $dashDiv->setLegend('Dashboard');
                $this->addSubForm($dashDiv, 'dashboard');

                $dashDiv->addElement($this->createElement('checkbox', 'hide_dashboard')
                    ->setLabel('Hide dashboard')
                    ->setDecorators($this->flTagDeco)
                    ->setBelongsTo('hide_dashboard')
                );
            }
            if (in_array('Admin_Parking_App', $modules)) {
                $dashDiv = new Zend_Form_SubForm();
                $dashDiv->setDescription('App registrations');
                $dashDiv->setLegend('App registrations');
                $this->addSubForm($dashDiv, 'app registrations');

                $dashDiv->addElement($this->createElement('checkbox', 'hide_appreg')
                    ->setLabel('Hide app registrations')
                    ->setDecorators($this->flTagDeco)
                    ->setBelongsTo('hide_appreg')
                );
            }
        }
        $this->addSubmitButtons();
    }
}
