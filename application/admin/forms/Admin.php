<?php

/**
 * @name AdminGroup
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2011-12-09, 15.16.45
 */
class Admin_Form_Admin extends Core_Form
{
    public function __construct($options = null)
    {
        parent::__construct();
        $this->addElementPrefixPath('Core_Form_Validate', 'Core/Form/Validate', 'validate');
        $this->setAttrib('class', 'form-horizontal');
        $this->removeDecorator('DtDdWrapper');
        $this->removeDecorator('HtmlTag');

        $this->addElement($this->createElement('hidden', 'id')
            ->setDecorators([])
            ->addDecorator('ViewHelper'));
        $this->addElement($this->createElement('checkbox', 'adm_active')
            ->setLabel('Can Log In')
            ->setValue(1)->setDecorators($this->controlGroupDeco));
        $this->addElement($this->createElement('text', 'adm_name')
            ->setLabel('Name')
            ->setRequired(true)
            ->addValidator('notEmpty', true)->setDecorators($this->controlGroupDeco));
        $this->addElement($this->createElement('text', 'adm_surname')
            ->setLabel('Surname')
            ->setRequired(true)
            ->addValidator('notEmpty', true)->setDecorators($this->controlGroupDeco));
        $this->addElement($this->createElement('text', 'adm_login')
            ->setLabel('Log in')
            ->setRequired(true)
            ->addValidator('notEmpty', true)
            ->addValidator('EmailAddress', true)
            ->setDecorators($this->controlGroupDeco)
        );

        $this->addElement($this->createElement('text', 'adm_device_id')
            ->setLabel('Device ID')
            ->setRequired(true)
            ->addValidator('notEmpty', true)->setDecorators($this->controlGroupDeco));

        $password = '';
        if (! $options ['users_id']) {
            //$password = Core_Utils::generatePassword ( 6, 4 );
            //$this->getView ()->gen_password = $password;
        }
        $pass = $this->createElement('password', 'adm_password')
            ->setLabel('Password')
            ->setDecorators($this->controlGroupDeco)
            ->addValidator('PasswordConfirmation', null, []);

        if (! $options ['id']) {
            $pass
                //->setDescription ( "Generated password: " . $password )
                ->setRenderPassword(true)
                ->setValue($password)
                ->setRequired(true)
                ->addValidator('notEmpty', true);
        }
        $this->addElement($pass);

        $pass_conf = $this->createElement('password', 'password_confirm')
            ->setLabel('Repeat password')->setDecorators($this->controlGroupDeco);
        if (! $options ['id']) {
            $pass_conf->setRenderPassword(true)
                ->setValue($password)
                ->setRequired(true)
                ->addValidator('notEmpty', true);
        }
        $this->addElement($pass_conf);

        $model = new Model_AdminGroups();
        $c_model = new Model_City();

        $adm_group = Core_Auth_Admin::getInstance()->getStorage()->read()->admin_groups_id;
        $group = $model->fetchRow('id ='.$adm_group);

        if ($group['city'] && $group['city'] != 0) {
            $groupsByCity = $model->fetchAll('city ='.$group['city'], 'title ASC');
            $citysByCity = $c_model->fetchAll('id ='.$group['city']);
        } else {
            $groupsByCity = $model->fetchAll(null, 'title ASC');
            $citysByCity = $c_model->fetchAll();
        }

        $this->addElement($this->createElement('select', 'admin_groups_id')
            ->setLabel('Group')
            ->setMultiOptions($model->getPairs($groupsByCity, 'id', 'title'))
            ->setRequired(true)
            ->addValidator('notEmpty', true)->setDecorators($this->controlGroupDeco));

        $this->addElement($this->createElement('Multiselect', 'CityName')
            ->setLabel('Visible Cities')
            ->setMultiOptions($c_model->getPairs($citysByCity, 'id', 'CityName'))
            ->setRequired(false)
            ->addValidator('notEmpty', false)->setDecorators($this->controlGroupDeco));

        $this->addElement($this->createElement('text',
            'email')->setLabel('E-mail')->setDecorators($this->controlGroupDeco));

        $checkbox_deco = [
            [
                'ViewHelper',
            ],
            'Description',
            [
                'HtmlTag',
                [
                    'tag' => 'dd',
                ],
            ],
            [
                'Label',
                [
                    'tag' => 'dt',
                    'class' => 'checkbox',
                ],
            ],
        ];
        $s_frm = new Zend_Form_SubForm();
        foreach ($model->getPairs($groupsByCity, 'id', 'title') as $key => $value) {
            if ($key) {
                $s_frm->addElement($this->createElement('checkbox', (string) $key)
                    ->setLabel((string) $value)
                    ->setDecorators($checkbox_deco));
            }
        }
        $s_frm->setDescription('Other groups');
        $s_frm->setDecorators([
            [
                'Description',
                [
                    'tag' => 'legend',
                ],
            ],
            'FormElements',
            [
                'Fieldset',
            ],
        ]);
        $this->addSubForm($s_frm, 'groups');

        $this->addSubmitButtons();
    }
}
