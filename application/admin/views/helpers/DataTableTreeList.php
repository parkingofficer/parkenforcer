<?php

/**
 * @name dataTableTreeList
 */
class Zend_View_Helper_DataTableTreeList
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function dataTableTreeList(
        $list = null,
        $params = null,
        $params_title = null,
        $links = null,
        $options = ['ShowTableSearch' => true, 'ShowTableActions' => true]
    ) {
        return $this->view->partial('_partial/dataTablesTree-list.phtml', [
            'list' => $list,
            'params' => $params,
            'params_title' => $params_title,
            'links' => $links,
            'options' => $options,
        ]);
    }
}
