<?php

/**
 * Description of CmsList.
 *
 * @author Darius Matulionis
 */
class Zend_View_Helper_CmsList
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function cmsList($list = null, $params = null, $links = null, $checkboxes = null)
    {
        if (empty($list) || empty($params)) {
            return;
        }

        return $this->view->partial('_partial/simple-list.phtml',
            ['list' => $list, 'params' => $params, 'links' => $links, 'checkboxes' => $checkboxes]);
    }
}
