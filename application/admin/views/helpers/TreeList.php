<?php

/**
 * Description of TreeList.
 *
 * @author Darius Matulionis
 */
class Zend_View_Helper_TreeList
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function treeList($list = null, $links = null)
    {
        if (empty($list)) {
            return;
        }

        return $this->view->partial('_partial/tree-list.phtml', ['tree' => $list, 'links' => $links]);
    }
}
