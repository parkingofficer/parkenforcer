<?php

/**
 * @name dataTableList
 */
class Zend_View_Helper_DataTableList
{
    public $view;

    public function setView(Zend_View_Interface $view)
    {
        $this->view = $view;
    }

    public function dataTableList(
        $list = null,
        $params = null,
        $params_title = null,
        $links = null,
        $options = ['ShowTableSearch' => true, 'ShowTableActions' => true]
    ) {
        return $this->view->partial('_partial/dataTables-list.phtml', [
            'list' => $list,
            'params' => $params,
            'params_title' => $params_title,
            'links' => $links,
            'options' => $options,
        ]);
    }
}
