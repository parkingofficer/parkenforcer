<?php

class Zend_View_Helper_BaseUrl
{
    public function baseUrl($link = '', $no_lang = false)
    {
        if ($no_lang) {
            return 'http://'.$_SERVER['HTTP_HOST'].'/'.$link;
        }

        return 'http://'.$_SERVER['HTTP_HOST'].'/admin/'.LANG.'/'.$link;
    }
}
