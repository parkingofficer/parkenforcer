<?php

class Admin_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initAutoloadModuleAdmin()
    {
        $autoloader = new Zend_Application_Module_Autoloader([
            'namespace' => 'Admin',
            'basePath' => APPLICATION_PATH.'/admin',
        ]);

        return $autoloader;
    }

    /**
     * Cache Init.
     */
    protected function _initCache()
    {
        $front = $this->getResource('frontController');
        $resourses = $this->bootstrap('frontController')->getApplication()->getOption('resources');
        new Core_Cache_Adapter($resourses['cachemanager']['database']);
    }

    protected function _initRegisterPlugins()
    {
        $this->bootstrap('frontController');
        $front = $this->getResource('frontController');

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');

        $view = $viewRenderer->view;

        ZendX_JQuery_View_Helper_JQuery::enableNoConflictMode();
        $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
        $view->addHelperPath('ZendX/JQuery/View/Helper/JQuery', 'ZendX_JQuery_View_Helper_JQuery');

        $view->addHelperPath('Core/Form/Element', 'Core_Form_Element');
        $view->addHelperPath('Core/View/Helper', 'Core_View_Helper');

        /*
         * check if admin is logged in plugin. If not, redirect to login form
         */
        $front->registerPlugin(new Core_Controller_Plugin_CheckAuth());

        /*
         * Translations
         */
        $front->registerPlugin(new Admin_Plugin_Translate());

        /*
         * ACL
         */
        $front->registerPlugin(new Admin_Plugin_Acl());

        /*
         * assign menu to a view
         */
        if (! Zend_Registry::isRegistered('navigation_container')) {
            Zend_Registry::set('navigation_container', []);
        }
        $front->registerPlugin(new Admin_Plugin_Menu());

        /*
         * priskirsim specifik initus
         */
        $front->registerPlugin(new Admin_Plugin_Init());
    }
}
