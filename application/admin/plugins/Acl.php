<?php

/**
 * @name Acl
 * @copyright Baltic Aviation Academy
 * @author Darius Matulionis <d.matulionis@balticaa.com>
 * @since : 2011-05-26, 13.03.51
 */
class Admin_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if (Core_Auth_Admin::getInstance()->hasIdentity()) {
            $group_id = Zend_Auth::getInstance()->getStorage()->read()->admin_groups_id;
            $acl = new Zend_Acl();

            // $acl->addRole(new Zend_Acl_Role(1));
            Zend_Registry::set('acl', $acl);
            //var_dump(Zend_Auth::getInstance()->getStorage()->read()->permisions);
            if (! Zend_Auth::getInstance()->getStorage()->read()->adm_root) {
                $nav = new Core_Navigation_Aggregator(Zend_Controller_Front::getInstance());
                foreach ($nav->getControllers() as $controller => $value) {
                    $prefix = substr($controller, 0, 6);
                    $controller = substr($controller, 6);
                    if ($prefix == 'Admin_' && $controller != 'Ajax' && $controller != 'Cron' && $controller != 'Index' && $controller != 'Auth' && $controller != 'Error') {
                        $controller = strtolower($controller);
                        if (in_array('Core_Controller_Navigation_Interface',
                            class_implements('Admin_'.ucfirst($controller).'Controller'))) {
                            $controllerName = 'Admin_'.ucfirst($controller).'Controller';
                            $navig = call_user_func($controllerName.'::getNavigation');

                            $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($navig),
                                RecursiveIteratorIterator::SELF_FIRST);
                            foreach ($iterator as $page) {
                                if (is_array($page) && isset($page['action']) && ! isset($page['pages'])) {
                                    //var_dump($page ['controller'] . '_' . str_replace("-", "_", $page ['action']) );
                                    $resource = new Zend_Acl_Resource(str_replace('-', '_',
                                            $page ['controller']).'_'.str_replace('-', '_', $page ['action']));
                                    if (! $acl->has($resource)) {
                                        $acl->addResource($resource);
                                    }
                                }
                            }

                            /*
                            if (isset( $navig ['label'] ))
                                $pgs [$controller] = $navig;
                            elseif (isset( $navig [0] )) {
                                foreach ( $navig as $k => $p ) {
                                    $pgs [$controller . "_split" . $k] = $p;
                                }
                            }
                            if($pgs['users_split0']){

                                //var_dump($pgs);
                                //var_dump($controller);
                                //die();
                            }
                            foreach ( $pgs as $cntrl => $p ) {
                                foreach ( $p ['pages'] as $key => $page ) {
                                    if (isset( $page ['sub_actions'] ) && !empty( $page ['sub_actions'] )) {
                                        foreach ( $page ['sub_actions'] as $k => $sub_action ) {
                                            $acl->addResource( new Zend_Acl_Resource( $controller . '_' . str_replace( "-", "_", $page ['action'] ) . '_' . $sub_action ) );
                                        }
                                    }
                                    if (isset( $page ['sub_pages_actions'] ) && !empty( $page ['sub_pages_actions'] )) {
                                        foreach ( $page ['sub_pages_actions'] as $k => $sub_page_actions ) {
                                            $sub_action = "";
                                            if (!empty( $sub_page_actions ['params'] )) {
                                                foreach ( $sub_page_actions ['params'] as $param => $param_value ) {
                                                    $sub_action .= "_" . $param . "_" . $param_value;
                                                }
                                            }
                                            // var_dump($sub_page_actions);die();
                                            $acl->addResource( new Zend_Acl_Resource( $sub_page_actions ['controller'] . '_' . $sub_page_actions ['action'] . $sub_action ) );
                                        }
                                    }
                                }
                            }*/
                        }
                        /*
                        foreach ( $value as $k => $action ) {
                            if ('Action' == substr( $action, -6 )) {
                                $action = substr( $action, 0, strlen( $action ) - 6 );
                                $action = strtolower( implode( "_", explode( ",", preg_replace( "/([A-Z])/", ',\\1', $action ) ) ) );
                                 //var_dump($controller . '_' . $action);
                                if ($action != "register_insert" && $action != "register_list" && !strstr( $action, "registerTreeActions" ))
                                    $acl->addResource( new Zend_Acl_Resource( $controller . '_' . $action ) );
                            }
                        }*/
                    }
                }

                // var_dump(Zend_Auth::getInstance()->getStorage()->read()->permisions);
                $acl->addRole(new Zend_Acl_Role($group_id));
                if (Zend_Auth::getInstance()->getStorage()->read()->permisions && ! empty(Zend_Auth::getInstance()->getStorage()->read()->permisions)) {
                    foreach (Zend_Auth::getInstance()->getStorage()->read()->permisions as $p_name => $value) {
                        if ($value) {
                            try {
                                $acl->allow($group_id, $p_name);
                            } catch (Exception $exc) {
                                //var_dump( $p_name );
                                //var_dump( $exc->getTraceAsString() );
                            }
                        } else {
                            try {
                                $acl->deny($group_id, $p_name);
                            } catch (Exception $exc) {
                                //var_dump( $p_name );
                                //var_dump( $exc->getTraceAsString() );
                            }
                        }
                    }
                }
                $controller = strtolower($request->getParam('controller'));
                $controller = str_replace('-', '_', ($controller));
                $action = strtolower(str_replace('-', '_', $request->getParam('action')));
                $params = $request->getParams();
                unset($params ['controller']);
                unset($params ['module']);
                unset($params ['action']);
                $sub_actions = [
                    'delete' => '1',
                    'insert' => '1',
                    'view' => '1',
                    'move' => '1',
                    'visible' => '1',
                ];
                $sub_action = '';
                if (! empty($params)) {
                    foreach ($params as $key => $value) {
                        if (isset($sub_actions [$key]) && is_string($key)) {
                            $sub_action = "_$key";
                        } elseif (is_string($key) && is_string($value)) {
                            $sub_action = '_'.$key.'_'.$value;
                        }
                    }
                }

                //die();
                $allowed = false;
                if (in_array($controller.'_'.$action, $acl->getResources())) {
                    if ($acl->isAllowed($group_id, $controller.'_'.$action)) {
                        $allowed = true;
                    }
                } else {
                    $allowed = true;
                }

                //var_dump($allowed);
                //var_dump($controller . '_' . $action);
                //var_dump($acl->getResources());

                // var_dump($group_id);
                if ($group_id == 1) {
                    $allowed = true;
                }
                if (! $allowed) {
                    header('Location: /admin/error/not-allowed');
                    exit();
                }
                Zend_Registry::set('acl', $acl);
            }
        }
    }
}
