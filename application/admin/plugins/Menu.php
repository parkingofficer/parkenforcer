<?php

/**
 * Description of Menu.
 *
 * @author Darius Matulionis
 */
class Admin_Plugin_Menu extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() == 'admin') {
            $front = Zend_Controller_Front::getInstance();

            $agg = new Core_Navigation_Aggregator($front, 'Admin');
            $nav = $agg->getNavigation();

            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
            $viewRenderer->view->navigation($nav);

            $model = new Model_Admins();
            $db = $model->getAdapter();
            $db->query("SET NAMES 'utf8' ");
            Zend_Registry::set('db', $db);
        }
    }
}
