<?php

/**
 * Description of Init.
 *
 * @author Darius Matulionis
 */
class Admin_Plugin_Init extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() == 'admin') {
            //$articlesModel = new Model_DbTable_Articles(); // hack for Zend_Registry::get('db');

            $front = Zend_Controller_Front::getInstance();
            $front->registerPlugin(new Zend_Controller_Plugin_ErrorHandler([
                'module' => 'admin',
                'controller' => 'error',
                'action' => 'error',
            ]));

            $this->_initViewHelpers();

            $session = new Zend_Session_Namespace('Default');

            if ($request->getParam('editor_lang')) {
                $session->editor_lang = $request->getParam('editor_lang');
            }
            if (! isset($session->editor_lang)) {
                $session->editor_lang = DEFAULT_LANG;
            }

            Zend_Registry::set('editor_lang', $session->editor_lang);
        }
    }

    protected function _initViewHelpers()
    {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('layout')->setLayoutPath(APPLICATION_PATH.'/admin/views/scripts');
        $view = $layout->getView();

        $view->addHelperPath('Core/Form/Element', 'Core_Form_Element');
        $view->addHelperPath('Core/View/Helper', 'Core_View_Helper');

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
    }
}
