<?php

class ErrorController extends Zend_Controller_Action
{
    public function notAllowedAction()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function errorAction()
    {
        $this->_helper->layout()->disableLayout();
        $errors = $this->_getParam('error_handler');
        if (! $errors) {
            $this->view->message = 'You have reached the error page';

            return;
        }
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE :
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                $this->view->errorType = 'notice';
                $this->_helper->viewRenderer('401');
                break;
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER :
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                $this->view->errorType = 'notice';
                $this->_helper->viewRenderer('402');
                break;
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION :
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                $this->view->errorType = 'notice';
                $this->_helper->viewRenderer('403');
                break;
            default :
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                $this->view->errorType = 'critical';
                $this->_helper->viewRenderer('500');
                break;
        }
        // Log exception, if logger available
        $log = $this->getLog();
        if ($log) {
            $server = Zend_Debug::dump($_SERVER, 'SERVER', false);
            $log->log($this->view->message, $priority,
                $errors->exception->getMessage().'<br/>'.'<br/>'.$errors->exception.'<br/>'.'<br/><pre>'.$server.'</pre>');
        }
        // conditionally display exceptions
        // if ($this->getInvokeArg('displayExceptions') == true) {
        $this->view->exception = $errors->exception;
        // }
        $this->view->request = $errors->request;
        $this->view->showErrors = true;
        if (APPLICATION_ENV == 'production' && Zend_Auth::getInstance()->getStorage()->read()->adm_root != 1) {
            if ($this->view->errorType == 'critical') {
                if ($this->_request->getParam('controller') != 'index' && $this->_request->getParam('action') != 'image') {
                    $mail = new Core_Mail_Mailer();
                    $mail->addTo('darius@matulionis.lt', 'Darius Matulionis');
                    $mail->setSubject('Application Error On EMP Kataliz');
                    $mail->addHeader('X-Priority', 1);
                    $mail->addHeader('X-MSMail-Priority', 'High');
                    $mail->addHeader('Importance', 'High');
                    $this->view->isMail = true;
                    $mail->setBodyHtml($this->view->render('error/error-messages.phtml'));
                    $this->view->isMail = false;
                    $mail->sendMail();
                }
            }
            $this->view->showErrors = false;
        }
    }

    public function getLog()
    {
        return false;
    }
}
