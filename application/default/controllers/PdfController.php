<?php

class PdfController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout()->disableLayout();
    }

    public function printAction($model)
    {
        $params = $this->_request->getParams();
        $cols_width = explode("\t", $params['table_cols_width']);
        $table = explode(PHP_EOL, $params['table_data']);

        $table_header = $table[0];
        unset($table[0]);

        $table_header = explode("\t", $table_header);

        if (! empty($table_header) && count($table_header) == count($cols_width)) {
            ini_set('memory_limit', '512M');
            $this->view->table_header = $table_header;
            $this->view->cols_width = $cols_width;
            $this->view->table = $table;
            $html = $this->view->render('pdf/print.phtml');
            //die($html);
            $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 0, $default_font = '', $mgl = 10,
                $mgr = 10, $mgt = 20, $mgb = 20, $mgh = 9, $mgf = 9, $orientation = 'L');
            $pdf->SetDisplayMode('fullpage');
            $pdf->writeHTML($html);
            $pdf->title = $params['title'];
            //$pdf->Output($params['title'].'.pdf',"I");
            $tmpFileName = md5(time()).'-export.pdf.tmp';
            Core_Utils::serveFilePartial($pdf->Output(null, 'S'), $tmpFileName, $params['title'].'-export.pdf');
            die();
        } else {
            die();
        }

        //var_dump($cols_width,$table_header, $table,$params);
        die();
    }
}
