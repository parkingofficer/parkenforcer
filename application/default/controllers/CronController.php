<?php

class CronController extends Zend_Controller_Action
{
    //1208078
    //26433015

    private static $_post = [];

    public function init()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function deleteTempDirsContentAction()
    {
        array_map('unlink', glob(TMP_FILES_PATH . '/*'));
    }

    public function paymentsImportToBillingAction()
    {
        set_time_limit(0);
        $api = new Core_Api_Oxid();

        $model = new Model_Parking();
        $items = $model->fetchAll('billing_id IS NULL', null, 200);
        if ($items->count()) {
            foreach ($items as $i) {
                $api->addTransaction($i);
            }
        }
    }

    public function finesPaymentsImportToBillingAction()
    {
        set_time_limit(0);
        $api = new Core_Api_Oxid();

        $model = new Model_FinesPayments();
        $items = $model->fetchAll("billing_id is null AND deleted is null AND payment_type <> 'D'", null, 100);
        if ($items->count()) {
            foreach ($items as $i) {
                $api->addTransaction2($i);
            }
        }
    }

    public function finesSendAction()
    {
        /**
         * @var Zend_Db_Adapter_Abstract
         */
        $db = Zend_Registry::get('db');
        $config = $db->getConfig();
        $config['type'] = 'pdo_mysql';
        $options = [
            'name' => 'finesNotifications',
            'driverOptions' => $config,
        ];

        try {
            $db->beginTransaction();

            $queue = new Zend_Queue('Db', $options);
            $messages = $queue->receive(5);

//            error_reporting('E_ALL');
//
//            ini_set('display_errors', 1);
//            ini_set('display_startup_errors', 1);

            $modelDomain = new Model_Domain();
            if ($messages->count() > 0) {
                foreach ($messages as $msg) {
                    $data = unserialize($msg->body);
                    $mail = new Zend_Mail('UTF-8');
                    $mail->addTo($data['to']);
                    $response = $modelDomain->options();
                    $mail->setFrom($response['fines_sent_from'], 'Parking System');
                    $mail->setSubject($data['subject']);
                    $mail->setBodyText($data['body']);

                    $pdf = new Core_Pdf_FromHtml($mode = '', $format = 'A4', $default_font_size = 0, $default_font = '',
                        $mgl = 25, $mgr = 10, $mgt = 20, $mgb = 0, $mgh = 9, $mgf = 9, $orientation = 'P');
                    $pdf->SetDisplayMode('fullpage');
                    $pdf->SetFooter('{PAGENO}/{nb}');

                    $model = new Model_Fines();
                    $finee = $data['fine'];
                    $id = $finee['id'];

                    if ($id) {
                        $this->view->fine = $fine = $model->getFine($id);
                        $zones = new Model_Fines();
                        $city = new Model_Zone();
                        $zone = $zones->fetchRow($zones->select()->where('id = ?', $id));
                        $citys = $city->fetchRow($city->select()->where('id =' . $zone['zone']));

                        if (isset($data['doc_type']) && $data['doc_type'] == 'docx') {
                            $service = new Model_Fines();
                            $doc = $service->getFineDoc($data['fine']['id']);
                            $mail->createAttachment(
                                file_get_contents($doc->save()),
                                'application/actet-stream',
                                Zend_Mime::DISPOSITION_ATTACHMENT,
                                Zend_Mime::ENCODING_BASE64,
                                'fine-notification.docx'
                            );
                        } else {
                            $this->view->fine = $data['fine'];
                            $html = $this->view->render('fines/' . $citys['city_id'] . '.phtml');
                            $pdf->writeHTML($html);

                            $mail->createAttachment(
                                $pdf->Output(null, 'S'),
                                'application/pdf',
                                Zend_Mime::DISPOSITION_ATTACHMENT,
                                Zend_Mime::ENCODING_BASE64,
                                'fine-notification.pdf'
                            );
                        }
                        $mail->send();
                        $queue->deleteMessage($msg);
                    }
                }
            }
            $db->commit();
        } catch (Exception $e) {
            var_dump($e);
            $db->rollBack();
            die('error');
        }
        die('Notifications is sent');
    }

    public function ftpAction()
    {
        $ftp_log = new Model_FtpLog();
        $ftp_server = '92.61.39.125';
        $ftp_user_name = 'scancar';
        $ftp_user_pass = 'scansiauliai1';

        // set up connection
        $conn_id = ftp_connect($ftp_server);

        // login with username and password
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

        // check connection
        if ((!$conn_id) || (!$login_result)) {
            echo 'FTP connection has failed!';
            echo "Attempted to connect to $ftp_server for user $ftp_user_name";
            exit;
        } else {
            //    echo "Connected to $ftp_server, for user $ftp_user_name \n";

            $iterator = new DirectoryIterator('ftp://scancar:scansiauliai1@92.61.39.125/Epsum/*.xml');

            $i = 0;
            while ($iterator->valid()) {
                $file1 = $iterator->current();
                $file_obj = $iterator->key() . ' => ' . $file_name = $file1->getFilename() . "\n";
                $url1 = ('ftp://scancar:scansiauliai1@92.61.39.125/Epsum/' . $file1);
                $url2 = ('ftp://scancar:scansiauliai1@92.61.39.125/Epsum/Archive/' . $file1);
                $file_c = file_get_contents($url1);

                // Objects
                $feed = file_get_contents($url1);
                $items = simplexml_load_string($feed);
                $ftp_log->ScanCar($items);

                //==========================================================================================================

                $ftp_log->save([
                    'file_name' => $file_name,
                    'file_content' => $file_c,
                ]);
                rename($url1, $url2);

                $iterator->next();
                $i++;
                if ($i >= 20) {
                    break;
                }
            }
        }
    }

    public function updateBankPaymentsAction()
    {
        $model = new Model_FinesPayments();
        $f_model = new Model_Fines();
        $payments = $model->fetchAll("time >= '" . date('Y-m-d H:i:s',
                strtotime('-30 days')) . "' AND fine_id IS NULL ");
        if ($payments->count()) {
            foreach ($payments as $payment) {
                $fine = $f_model->fetchRow(
                    "(plate = '$payment->plate' OR id_public = '$payment->plate') AND
                    status_id != " . Model_Fines::STATUS_FINE_PAYED . ' AND
                    bank_payment_no IS NULL'
                );
                if ($fine) {
                    $f_model->associatePayment(
                        $payment->archive_no,
                        $fine->id
                    );

                    $payment->fine_id = $fine->id;
                    $payment->save();
                }
            }
        }
    }

    public function deleteBlackListAction()
    {
        $model = new Model_BlackList();
        $model->update(
            [
                'delete' => 1,
                'delete_time' => date('Y-m-d H:i:s'),
                'deleted_by' => 'Parking System',
            ],
            "time_checked <= '" . date('Y-m-d 00:00:00') . "' AND `delete` = 0 AND deleted_by IS NULL"
        );
    }

    public function changeFinesStatusesAction()
    {
        if (!$this->checkExcludedDates(date('Y-m-d'))) {
            $model = new Model_Fines();
            $z_model = new Model_Zone();
            $zones = $z_model->fetchAll();
            if ($zones->count()) {
                foreach ($zones as $z) {
                    $select = $model->select();
                    $select->where('status_id = ?', Model_Fines::STATUS_NOTICE);
                    $select->where(new Zend_Db_Expr('DATE_ADD(date,INTERVAL ' . $z['warning_pay_time'] . " HOUR ) < '" . date('Y-m-d h:i:s') . "'"));
                    $select->where('zone = ' . $z['id']);
                    $fines = $model->fetchAll($select);
                    if ($fines->count()) {
                        foreach ($fines as $f2) {
                            $f2->status_id = Model_Fines::STATUS_WAITING_FOR_FINE_GENERATION;
                            $f2->save();
                            $domain_model = new Model_Domain();
                            $options = $domain_model->options();
                            if ($options['name'] == 'parking.unipark.lt') {
                                $billingQueue = new Model_BillingQueue();
                                $billingQueue->save([
                                    'type' => Model_BillingQueue::UPDATE_FINE,
                                    'system_id' => $f2['id'],
                                    'status' => 0,
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }

    public function getBiteParkingAction()
    {
        $cookies = TMP_FILES_PATH . DIRECTORY_SEPARATOR . 'bite.cookies.txt';
        $html = null;
        if (file_exists($cookies) && DateTime::createFromFormat('YmdHis',
                date('YmdHis'))->diff(DateTime::createFromFormat('YmdHis',
                date('YmdHis', filectime($cookies))))->m < 1) {
            $html = $this->connect('http://parking.bite.lt/parkings.php?so=desc&sf=msg_received&show_recent=Rodyti+%F0ios+dienos+parkavimus',
                null, $cookies);
            //echo $ret;
        } else {
            if (file_exists($cookies)) {
                unlink($cookies);
            }
            $this->connect('http://parking.bite.lt/index.php', [
                'username' => 'spark',
                'password' => 'sp630',
                'login' => 'Prisijungti',
            ], $cookies);

            $html = $this->connect('http://parking.bite.lt/parkings.php?so=desc&sf=msg_received&show_recent=Rodyti+%F0ios+dienos+parkavimus',
                null, $cookies);
            //echo $ret;
            //die('asdad');
        }

        if ($html) {
            libxml_use_internal_errors(true);
            $dom = new DomDocument;
            $dom->loadHTML($html);

            $tables = $dom->getElementsByTagName('table');
            $model = new Model_Parking();
            $zone_model = new Model_Zone();

            foreach ($tables as $table) {
                if ($table->hasAttributes()) {
                    foreach ($table->attributes as $a) {
                        if ($a->name == 'bgcolor' && $a->value == '#F5F5F5') {
                            $table = $table->getElementsByTagName('table')->item(3)->getElementsByTagName('table')->item(0);
                            //$tbody = $table->getElementsByTagName('tbody');//->getElementsByTagName('tr')->item(1);
                            //var_dump($table->childNodes);
                            foreach ($table->childNodes as $k => $tr) {
                                if ($tr->nodeName == 'tr' && $k != 0) {
                                    $data = [];
                                    foreach ($tr->childNodes as $td) {
                                        if ($td->nodeName == 'td' && $td->nodeValue) {
                                            array_push($data, $td->nodeValue);
                                        }
                                    }
                                    if ((int)$data[0]) {
                                        $date_end = $data[1] . ' ' . $data[5] . ':00';
                                        if ($data[4] > $data[5]) {
                                            $date_end = date('Y-m-d',
                                                    strtotime('+1 day', strtotime($data[1]))) . ' ' . $data[5] . ':00';
                                        }

                                        $model->save([
                                            'id' => 'bite_' . $data[0],
                                            'phone' => null,
                                            'operator' => 3,
                                            'operator_name' => 'bite',
                                            'plate' => $data[3],
                                            'bar_code' => null,
                                            'duration_hours' => ($data[4] && $data[5]) ? DateTime::createFromFormat('H:i',
                                                $data[4])->diff(DateTime::createFromFormat('H:i', $data[5]))->h : 1,
                                            'count' => 1,
                                            'price_cent' => $data[6] * 100,
                                            'city' => 0,
                                            'zone' => 1,
                                            'reminder' => $data[7] == '-' ? 0 : 1,
                                            'date_inserted' => date('Y-m-d H:i:s'),
                                            'date_start' => $data[1] . ' ' . $data[4] . ':00',
                                            'date_end' => $date_end,
                                        ]);
                                        $this->BillingSendNewTransaction(["id" => 'bite_' . $data[0]]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        die('BITE DB UPDATED');
    }

    public function getBiteTrakaiParkingAction()
    {
        $cookies = TMP_FILES_PATH . DIRECTORY_SEPARATOR . 'biteTrakai.cookies.txt';
        $html = null;
        if (file_exists($cookies) && DateTime::createFromFormat('YmdHis',
                date('YmdHis'))->diff(DateTime::createFromFormat('YmdHis',
                date('YmdHis', filectime($cookies))))->m < 1) {
            $html = $this->connect('http://parking.bite.lt/parkings.php?so=desc&sf=msg_received&show_recent=Rodyti+%F0ios+dienos+parkavimus',
                null, $cookies);
            //echo $ret;
        } else {
            if (file_exists($cookies)) {
                unlink($cookies);
            }
            $this->connect('http://parking.bite.lt/index.php', [
                'username' => 'vytas.t',
                'password' => 'vpt821',
                'login' => 'Prisijungti',
            ], $cookies);

            $html = $this->connect('http://parking.bite.lt/parkings.php?so=desc&sf=msg_received&show_recent=Rodyti+%F0ios+dienos+parkavimus',
                null, $cookies);
            //echo $ret;
            //die('asdad');
        }

        if ($html) {
            libxml_use_internal_errors(true);
            $dom = new DomDocument;
            $dom->loadHTML($html);

            $tables = $dom->getElementsByTagName('table');
            $model = new Model_Parking();
            $zone_model = new Model_Zone();

            foreach ($tables as $table) {
                if ($table->hasAttributes()) {
                    foreach ($table->attributes as $a) {
                        if ($a->name == 'bgcolor' && $a->value == '#F5F5F5') {
                            $table = $table->getElementsByTagName('table')->item(3)->getElementsByTagName('table')->item(0);
                            //$tbody = $table->getElementsByTagName('tbody');//->getElementsByTagName('tr')->item(1);
                            //var_dump($table->childNodes);
                            foreach ($table->childNodes as $k => $tr) {
                                if ($tr->nodeName == 'tr' && $k != 0) {
                                    $data = [];
                                    foreach ($tr->childNodes as $td) {
                                        if ($td->nodeName == 'td' && $td->nodeValue) {
                                            array_push($data, $td->nodeValue);
                                        }
                                    }
                                    if ((int)$data[0]) {
                                        $date_end = $data[1] . ' ' . $data[5] . ':00';
                                        if ($data[4] > $data[5]) {
                                            $date_end = date('Y-m-d',
                                                    strtotime('+1 day', strtotime($data[1]))) . ' ' . $data[5] . ':00';
                                        }
                                        $zone = $zone_model->fetchRow($zone_model->select()->where("zone_code = 'T" . $data[2] . "'"));
                                        if (empty($zone)) {
                                            $zone = $data[2];
                                        } else {
                                            $zone = $zone['id'];
                                        }
                                        $model->save([
                                            'id' => 'bite_' . $data[0],
                                            'phone' => null,
                                            'operator' => 3,
                                            'operator_name' => 'bite',
                                            'plate' => $data[3],
                                            'bar_code' => null,
                                            'duration_hours' => ($data[4] && $data[5]) ? DateTime::createFromFormat('H:i',
                                                $data[4])->diff(DateTime::createFromFormat('H:i', $data[5]))->h : 1,
                                            'count' => 1,
                                            'price_cent' => $data[6] * 100,
                                            'city' => 0,
                                            'zone' => $zone,
                                            'reminder' => $data[7] == '-' ? 0 : 1,
                                            'date_inserted' => date('Y-m-d H:i:s'),
                                            'date_start' => $data[1] . ' ' . $data[4] . ':00',
                                            'date_end' => $date_end,
                                        ]);
                                        $this->BillingSendNewTransaction(["id" => 'bite_' . $data[0]]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        die('BITE TRAKAI DB UPDATED');
    }

    public function getTele2ParkingAction()
    {
        $cookies = TMP_FILES_PATH . DIRECTORY_SEPARATOR . 'tele2.cookies.txt';
        $html = null;
        if (file_exists($cookies) && DateTime::createFromFormat('YmdHis',
                date('YmdHis'))->diff(DateTime::createFromFormat('YmdHis',
                date('YmdHis', filectime($cookies))))->m < 10) {
            // Cookies exist, point to a secured page?
            $html = $this->connect('https://parking.tele2.lt/node?f_f[submit]=Filtruoti&f_f[city]=&f_f[zone]=&f_f[tic_id]=&f_f[car]=&f_f[phone]=&f_f[d_nuo]=' . date('Y-m-d') . '&f_f[d_iki]=' . date('Y-m-d') . '&f_f[price]=#doco-istorija',
                null, $cookies);
        } else {
            if (file_exists($cookies)) {
                unlink($cookies);
            }
            $content = $this->connect('https://parking.tele2.lt/node', null, $cookies);
            $content = explode('name="form_build_id" id="', $content);
            $content = $content[1];
            $content = explode('"', $content);
            $form_key = $content[0];

            // Cookies don't exist post to a login page and redirect to a secured page?
            $this->connect('https://parking.tele2.lt/node?destination=node', [
                'name' => 'Unipark_admin',
                'pass' => 'Qwerty.123',
                'op' => 'Prisijungti',
                'form_build_id' => $form_key,
                'form_id' => 'user_login_block',
            ], $cookies);

            $html = $this->connect('https://parking.tele2.lt/node?f_f[submit]=Filtruoti&f_f[city]=&f_f[zone]=&f_f[tic_id]=&f_f[car]=&f_f[phone]=&f_f[d_nuo]=' . date('Y-m-d') . '&f_f[d_iki]=' . date('Y-m-d') . '&f_f[price]=#doco-istorija',
                null, $cookies);
        }

        if ($html) {
            libxml_use_internal_errors(true);
            $dom = new DomDocument;
            $dom->loadHTML($html);
            $container = $dom->getElementById('archive_list');

            $model = new Model_Parking();
            $zone_model = new Model_Zone();
            foreach ($container->childNodes as $k => $tr) {
                if ($tr->nodeName == 'tr' && $k != 0) {
                    $data = [];
                    foreach ($tr->childNodes as $td) {
                        if ($td->nodeName == 'td' && $td->nodeValue) {
                            array_push($data, $td->nodeValue);
                        }
                    }

                    $date_end = $data[3] . ' ' . $data[7];
                    if ($data[6] > $data[7]) {
                        $date_end = date('Y-m-d', strtotime('+1 day', strtotime($data[3]))) . ' ' . $data[7];
                    }

                    if ($data[0] == 'PT') {
                        $zone = $zone_model->fetchRow($zone_model->select()->where("zone_code = '" . $data[0]{1} . $data[1] . "'"));
                        if (empty($zone)) {
                            $zone = $data[1];
                        } else {
                            $zone = $zone['id'];
                        }
                    } elseif ($data[0] == 'PS') {
                        $zone = 1;
                    } else {
                        continue;
                    }

                    $model->save([
                        'id' => 'tele2_' . $data[2],
                        'phone' => $data[5],
                        'operator' => 2,
                        'operator_name' => 'tele2',
                        'plate' => $data[4],
                        'bar_code' => null,
                        'duration_hours' => DateTime::createFromFormat('H:i:s',
                            $data[6])->diff(DateTime::createFromFormat('H:i:s', $data[7]))->h,
                        'count' => 1,
                        'price_cent' => $data[8] * 100,
                        'city' => 1,
                        'zone' => $zone,
                        'reminder' => $data[9] == 'TAIP' ? 1 : 0,
                        'date_inserted' => date('Y-m-d H:i:s'),
                        'date_start' => $data[3] . ' ' . $data[6],
                        'date_end' => $date_end,
                    ]);
                    $this->BillingSendNewTransaction(["id" => 'tele2_' . $data[2]]);
                }
            }
        }

        die('TELE2 DB UPDATED');
    }

    public function getOmnitelParkingAction()
    {
        $json = file_get_contents('https://sms_park_4:QpAVsGsMAB@austeja.omnitel.net/smsp/parking/4/Scripts/list.asp');
        if ($json) {
            $omnitel = new Model_Omnitel();

            $json = json_decode($json);
            $omnitel->save(['json' => print_r($json, true)]);
            if (!empty($json)) {
                $model = new Model_Parking();
                foreach ($json as $p) {
                    $model->save([
                        'id' => $p->ID,
                        'phone' => $p->PHONE,
                        'operator' => $p->OPERATOR,
                        'operator_name' => 'omnitel',
                        'plate' => $p->CAR,
                        'bar_code' => $p->BAR_CODE,
                        'duration_hours' => $p->DURATION,
                        'count' => $p->COUNT,
                        'price_cent' => $p->PRICE,
                        'city' => $p->CITY,
                        'zone' => 1,//$p->ZONE,
                        'reminder' => $p->REMINDER,
                        'date_inserted' => DateTime::createFromFormat('YmdHis',
                            $p->DATE_INSERTED)->format('Y-m-d H:i:s'),
                        'date_start' => DateTime::createFromFormat('YmdHis', $p->DATE_START)->format('Y-m-d H:i:s'),
                        'date_end' => DateTime::createFromFormat('YmdHis', $p->DATE_END)->format('Y-m-d H:i:s'),
                    ]);
                    $this->BillingSendNewTransaction(["id" => $p->ID]);
                }
            }
        }
        die('OMINTEL DB UPDATED');
    }

    public function getBillingParkingAction()
    {
        $db = new PDO('mysql:host=213.159.63.98;dbname=unipark_mparking;port=3306', 'billing_remote',
            'Wa$ItRem0te0rN0t');
        $from = strtotime(date('Y-m-d 00:00:00', strtotime('-1 days')));
        $to = strtotime(date('Y-m-d 23:59:59'));
        // $select = $db->query("SELECT *
        //                       FROM mt_parking_transaction
        //                       LEFT JOIN mt_operator ON mt_parking_transaction.transaction_mobile_operator=mt_operator.operator_id
        //                       WHERE transaction_create_time BETWEEN ".$from." AND ".$to."
        //                       "
        // );
        $select = $db->query("SELECT * FROM mt_parking_transaction
LEFT JOIN mt_operator ON mt_parking_transaction.transaction_mobile_operator=mt_operator.operator_id
WHERE transaction_create_time > '1492473599' and operator_id = 3");
        //1491004801 = 01
        //1492300800 = 16
        //1492473599 = 17
        $data = $select->fetchAll(PDO::FETCH_ASSOC);

        $model = new Model_Parking();
        $zones = new Model_Zone();

        foreach ($data as $d) {
            $zoneId = $zones->getZoneID($d['transaction_zone_id']);
            $cityName = $zones->getCity($d['transaction_zone_id']);
            $createTime = $d['transaction_create_time'];
            $startTime = $d['transaction_start_pay_time'];
            if ($d['transaction_end_pay_time'] > $d['transaction_next_pay_time']) {
                $autoEndTime = $d['transaction_end_pay_time'];
            }
            if ($d['transaction_end_pay_time'] < $d['transaction_next_pay_time']) {
                $autoEndTime = $d['transaction_next_pay_time'];
            }
            $breakTime = $d['transaction_break_time'];
            $formatCreateTime = date('Y-m-d H:i:s', $createTime);
            $formatStartTime = date('Y-m-d H:i:s', $startTime);
            $formatEndTime = date('Y-m-d H:i:s', $autoEndTime);
            $formatBreakTime = date('Y-m-d H:i:s', $breakTime);

            $start = min($formatCreateTime, $formatStartTime);

            if ($breakTime == 0) {
                $end = $formatEndTime;
            } else {
                $end = $formatBreakTime;
            }
            try {
                $model->save([
                    'id' => 'MP_' . $d['transaction_id'],
                    'billing_id' => 'MP_BILL_' . $d['transaction_id'],
                    'phone' => $d['transaction_phone_number'],
                    'operator' => 5,
                    'operator_name' => 'mparking',
                    'plate' => $d['transaction_auto_number'],
                    'bar_code' => null,
                    'duration_hours' => DateTime::createFromFormat('Y-m-d H:i:s',
                        $formatStartTime)->diff(DateTime::createFromFormat('Y-m-d H:i:s', $end))->h,
                    'count' => 1,
                    'price_cent' => $d['transaction_payment_cost_sum'],
                    'city' => $cityName['id'],
                    'zone' => $zoneId['id'],
                    'reminder' => 0,
                    'date_inserted' => $formatCreateTime,
                    'date_start' => $start,
                    'date_end' => $end,
                ]);
            } catch (Exception $e) {
                continue;
            }
        }

        die('DB UPDATED');
    }

    // Delete dublicated transactions in Billing
    public function deleteBillDubTransactionsAction()
    {
        $date = date('Y-m-d');
        $dbhost = '216.159.63.98';
        $dbuser = 'rem_del_billing';
        $dbpass = 'lFWCvED11x7qpuk8';
        $dbname = 'billing_unipark';
        $conn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

        if (mysqli_connect_errno()) {
            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
        }

        $sql = "SELECT id
                FROM `transaction`
                WHERE drivein_at LIKE '" . $date . "%'
                GROUP BY drivein_at, driveout_at, parking_amount, license_plate_number
                HAVING COUNT(id) > 1";

        $result = mysqli_query($conn, $sql);

        $all = mysqli_fetch_all($result, MYSQLI_ASSOC);

        foreach ($all as $d) {
            $delete = 'DELETE FROM `transaction` WHERE id=' . $d['id'] . '';
            $result = mysqli_query($conn, $delete);
            if (false === $result) {
                printf("error: %s\n", mysqli_error($conn));
            } else {
                echo 'done.';
            }
        }

        die('OK');
    }

    // Get Palanga parkings from m-parking and update it with more time
    public function getPalangaParkingsAction()
    {
        $tomorrowMidnight = mktime(0, 0, 0, date('n'), date('j') + 1);
        $updateTime = mktime(0, 0, 0, date('n'), date('j') + 2);

        $db = new PDO('mysql:host=216.159.63.98;dbname=unipark_mparking;charset=utf8mb4', 'rem_del_billing',
            'lFWCvED11x7qpuk8');
        $select = $db->query('SELECT *
                              FROM mt_parking_transaction
                              WHERE transaction_zone_id in(132,133,134)
                              AND DATE(FROM_UNIXTIME(transaction_create_time)) = CURDATE()
                              AND transaction_auto_stop_time =' . $tomorrowMidnight . '
                              AND transaction_end_pay_time =' . $tomorrowMidnight . '
                              AND transaction_break_time =0'
        );

        $data = $select->fetchAll(PDO::FETCH_ASSOC);

        // SMS variables
        $smsSendTime = mktime(0, 8, 0, date('n'), date('j') + 1);
        $client = 247;
        $sender = rawurlencode('uniPark');
        $msg = "Mokamo stovėjimo paslauga buvo automatiškai pratęsta. Paslaugai sustabdyti uniPark programėlėje slinkite svirtį 'Apmokėti'.";
        $secret = 'UniPark902355r';

        foreach ($data as $d) {
            $count = $db->exec("UPDATE mt_parking_transaction
                                SET transaction_auto_stop_time='$updateTime', transaction_end_pay_time='$updateTime'
                                WHERE transaction_id =" . $d['transaction_id'] . ''
            );

            print_r($count);

            // SMS sending
            $hash = sha1($d['transaction_phone_number'] . $msg . $secret . $client);
            $request = $this->connect('http://api.smsmarketing.lt/legacy_api.php?action=send&time=' . $smsSendTime . '&client=' . $client . '&sender=' . $sender . '&rcpt=' . $d['transaction_phone_number'] . '&msg=' . $msg . '&hash=' . $hash . '',
                null, null);

            print_r($request);
        }

        die('OK');
    }

    // Ajax
    public function getBillingCompanyInfoAction()
    {
        if (isset($_POST['data'])) {
            $code = $_POST['data'];
        }
        $db = new PDO('mysql:host=216.159.63.98;dbname=billing_unipark;charset=utf8mb4', 'billing_remote',
            'Wa$ItRem0te0rN0t');
        $select = $db->query('SELECT c.name, c.vat_code, c.phone, c.email, c.address, cc.number
                              FROM client as c
                              LEFT JOIN client_contract as cc
                              ON c.id = cc.client_id
                              WHERE c.code = ' . $code . "
                              AND c.type <> 'individual'
                              "
        );
        $data = $select->fetchAll(PDO::FETCH_ASSOC);

        die(json_encode($data));
    }

    // Ajax
    public function getBillingPersonInfoAction()
    {
        if (isset($_POST['data'])) {
            $code = $_POST['data'];
        }
        $db = new PDO('mysql:host=216.159.63.98;dbname=billing_unipark;charset=utf8mb4', 'billing_remote',
            'Wa$ItRem0te0rN0t');
        $select = $db->query('SELECT c.name, c.phone, c.email, c.address, c.birthday
                              FROM client as c
                              WHERE c.code = ' . $code . "
                              AND c.type = 'individual'
                              "
        );
        $data = $select->fetchAll(PDO::FETCH_ASSOC);

        die(json_encode($data));
    }

    // Ajax
    public function getBlitzStatusesAction()
    {
        $order_type_set = new Model_BlitzOrderTypeSet();
        if (isset($_POST['data'])) {
            $id = $_POST['data'];
            $data = $order_type_set->getById($id);

            die(json_encode($data));
        }

        return false;
    }

    public function blitzAction()
    {
        $model = new Model_Blitz();
        $data = $model->fetchall("mail_sent <> '2' and date_to <= '" . date('Y-m-d', strtotime('+3 days')) . "'");
        if (!empty($data)) {
            foreach ($data as $order) {
                $datef = $order['date_to'];
                $datet = date('Y-m-d');
                $date_b = new DateTime(".$datef.");
                $date_a = new DateTime(".$datet.");
                $interval = date_diff($date_b, $date_a);
                $day = $interval->format('%d');
                $row2 = $model->getAdapter()->quoteInto('id = ?', $order['id']);
                $model->update(['mail_sent' => 2], $row2);

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                if ($day == '1') {
                    $d = 'diena';
                } elseif ($day > 1) {
                    $d = 'dienos';
                }
            }
        }
    }

    public function blitzConfirmAction()
    {
        $model = new Model_Blitz();
        $data = $model->fetchall("in_progress = '1'  ");
        if (!empty($data)) {
            foreach ($data as $order) {
                echo $order['id'];
                $row2 = $model->getAdapter()->quoteInto('id = ?', $order['id']);
                $model->update(['in_progress' => 2], $row2);

                $headers = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";

                //begin of HTML message
                $message = "<html>


        <p>Gerbiamas, $order->name $order->surname,<br>
<br>
Norime Jus informuoti, kad esate užsisakęs uniPark „Blitz“ paslaugą nuo $order->date_from iki $order->date_to.<br><br>
Automobilis Jūsų lauks uniPark centrinėje aikštelėje (PC, mėlyna zona), raktus ir dokumentus prašome atsiimti uniPark (Litcargus) Klientų aptarnavimo skyriuje, esančiame Atvykimo terminale prie C vartų.<br><br>
Prašome iš anksto pranešti, jeigu keistųsi Atvykimo skrydžio laikas.<br><br><br>

Linkime turiningos kelionės,<br>
uniPark
</p> <br>
  </body>
</html>";

                $body = trim('<html> <table style="width:650px;border-bottom: 4px solid #007abb;font-size:13px;line-height:140%;font-family: Trebuchet MS" cellpadding="0" cellspacing="0">
    <tr>
        <td style="width: 650px;height: 25px; font-size: 0" colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td style="width:20px">&nbsp;</td>
        <td style="width: 610px">
            <table style="width: 100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="width: 250px">
                        <a href="http://www.unipark.lt"><img src="https://unipark.lt/static/main/images/mail/logo-bord.png" alt="#"/>
                    </td>
                    <td style="width: 165px;font-family: Trebuchet MS;font-size: 11px; color: #5d5d5d; line-height: 15px">
                UAB „STOVA”<br/>Ozo g. 10A, LT-08200 Vilnius                    </td>
                    <td style="width: 20px; text-align: center">
                        <img src="https://unipark.lt/static/main/images/mail/seperator.gif" alt="#"/>
                    </td>
                    <td style="width: 205px;font-family: Trebuchet MS;font-size: 11px; color: #5d5d5d; line-height: 15px">
                Pagalbos tel.: +370 700 77877 (24/7)<br/>El.paštas:  <a href=info@unipark.lt></a>                        info@unipark.lt                    <td>
                </tr>
            </table>
        </td>
        <td style="width:20px">&nbsp;</td>
    </tr>
    <tr>
        <td style="width: 650px;height: 30px; font-size: 0px" colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td  colspan="3">
        </td>
    </tr>


    <tr>
        <td style="width: 650px;height: 25px; font-size: 0px" colspan="3">&nbsp;</td>
    </tr>

            <tr>
            <td style="width: 100%" colspan="3">
              <p>Gerbiamas, ' . $order->name . ' ' . $order->surname . ',<br>
<br>
Norime Jus informuoti, kad esate užsisakęs uniPark „Blitz“ paslaugą nuo  ' . $order->date_from . ' iki ' . $order->date_to . '.<br><br>
Automobilis Jūsų lauks uniPark centrinėje aikštelėje (PC, mėlyna zona), raktus ir dokumentus prašome atsiimti uniPark (Litcargus) Klientų aptarnavimo skyriuje, esančiame Atvykimo terminale prie C vartų.<br><br>
Prašome iš anksto pranešti, jeigu keistųsi Atvykimo skrydžio laikas.<br><br><br>

Linkime turiningos kelionės,<br>
uniPark
</p>

                        <tr>

        </tr>

    </table>
        </html>');
                //$message =
                //end of message
                mail($order['email'], 'uniPark paslaugos informacija ', $body, $headers);
            }
        }
    }

    /**
     * checks if there is unsent fines to billing server, marks as readable, calls api where req/resp will logged,
     * and then deletes row. It is made because of faster reading of unsent fines changes.
     */
    public function billingExportDataAction()
    {
        $billingQueue = new Model_BillingQueue();
        $fines = new Model_Fines();
        $transactions = new Model_Parking();
        $unsentArray = $billingQueue->fetchAll("status = 0 and try < 3", 'id ASC');
        $api = new Core_Api_Oxid();
        foreach ($unsentArray as $row) {

            if ($row['status'] == 0 && $row['type'] && $row['try'] < 3) {

                if ($row['type'] === Model_BillingQueue::CREATE_FINE || $row['type'] === Model_BillingQueue::UPDATE_FINE) {
                    $billingQueue->update(['status' => 1], 'id = ' . $row['id']);
                    $data = $fines->fetchRow(['id =' . $row['system_id']]);
                } elseif ($row['type'] === Model_BillingQueue::CREATE_TRANSACTION) {
                    $billingQueue->update(['status' => 1], 'id = ' . $row['id']);
                    $data = $transactions->fetchRow(["id ='" . $row['system_id'] . "'"]);
                    if($data['billing_id'] !== null){
                        $billingQueue->delete(['id = ' . $row['id']]);
                        continue;
                    }
                }

                $fail = $api->sendToBilling($data, $row['type']);

                if (!$fail) {
                    $billingQueue->delete(['id = ' . $row['id']]);
                } else {
                    $billingQueue->update(['status' => 0, 'try' => $row['try'] + 1], 'id = ' . $row['id']);
                    if ($row['try'] + 1 === 3) {
                        mail('robert@itgirnos.lt', 'PE export data of ' . $row['type'] . ' to billing failed 3rd time ',
                            json_encode($fail));
                        //mail('info@unipark.lt', 'PE export data to billing failed 3rd time ', $row['type'], $data);
                    }
                }

            }
        }
        return 'completed';
    }

    private function checkExcludedDates($date)
    {
        $dates = [
            date('Y-01-01'),//Sausio 1 - Nauji metai
            date('Y-02-16'),//Vasario 16 - Lietuvos Valstybės atkūrimo diena
            date('Y-03-11'),//Kovo 11 - Lietuvos Nepriklausomybės atkūrimo diena
            date('Y-05-01'),//Gegužės 1 - Tarptautinė darbininkų diena
            date('Y-06-24'),//Birželio 24 - Joninės
            date('Y-07-06'),//Liepos 6 - Valstybės (Lietuvos karaliaus Mindaugo karūnavimo) diena
            date('Y-08-15'),//Rugpjūčio 15 - Žolinė, Šv. Mergelės Marijos dangun ėmimo šventė
            date('Y-11-01'),//Lapkričio 1 - Visų šventųjų diena
            date('Y-12-24'),//Gruodžio 24 - Šv. Kūčios
            date('Y-12-25'),//Gruodžio 25 - Kalėdos
            date('Y-12-26'),//Gruodžio 26 - Kalėdos (antra diena)
            '2015-04-05',// - Velykos
            '2015-04-06',// - Velykos (antra diena)
            '2016-03-27',// - Velykos
            '2016-03-28',// - Velykos (antra diena)
            '2017-04-16',// - Velykos
            '2017-04-17',// - Velykos (antra diena)
            '2018-04-01',// - Velykos
            '2018-04-02',// - Velykos (antra diena)
            '2019-04-21',// - Velykos
            '2019-04-22',// - Velykos (antra diena)
            '2020-04-12',// - Velykos
            '2020-04-13',// - Velykos (antra diena)
        ];

        return in_array($date, $dates);
    }

    private function connect($url, $post = '', $cookies)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT,
            'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.100 Safari/534.30');
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 0);
        if ($post) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $cookies);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $cookies);
        $result = curl_exec($curl);
        curl_close($curl);

        return $result;
    }

    private function request($method, $path, array $data = [], array $uri = [])
    {
        $api_url = 'http://216.159.63.98/webservice/public';
        $api_key = '1d14f7ae80dfd1e5d17c8bfe10549ea2e9b95da3';
        $uri['key'] = $api_key;
        $url = $api_url . trim($path) . '?' . http_build_query($uri);
        $context = stream_context_create([
            'http' => [
                'method' => $method,
                'header' => "Content-Type: application/json\r\nAccept: application/json\r\n",
                'follow_location' => true,
                'content' => json_encode($data),
            ],
        ]);

        $fp = fopen($url, 'r', null, $context);
        $result = stream_get_contents($fp);
        fclose($fp);

        return $result;
    }

    private function BillingSendNewTransaction($data)
    {

        $billingQueue = new Model_BillingQueue();
        $billingQueue->save([
            'type' => Model_BillingQueue::CREATE_TRANSACTION,
            'system_id' => $data['id'],
            'status' => 0,
        ]);
    }

    public function setEstoniaTeliaKeyAction() {
        try {
            $client = new SoapClient('https://web.emt.ee:9112/?wsdl', array("soap_version" => SOAP_1_1, "trace" => 1));
            $params = array(
                "username" => "CPNEW",
                "password" => "Tere1234",
            );

            $response = $client->__soapCall("Login", [$params]);
            $session = $array = json_decode(json_encode($response), True);
            $keyModel = new Model_EstoniaTeliaKey();
            $keyModel->setKey( $session['session_id'] );
            echo "Key generate!";
            die();
        } catch (SoapFault $e) {
            die($e);
        }
    }

    public function sendContradictionEmailAction()
    {
        $model = new Model_Contradiction();
        try{
            $dataArray = $model->getAllNotSendContradiction();
            foreach ( $dataArray as $data ){
                $model->sendMail( $data ['fine_id'] );
            }

        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    public function changeEstoniaFinePublishedStatusAction ()
    {
        $model = new Model_Fines();
        $result = $model->changeFineStatus( $model::STATUS_PUBLISHED );

        print $result ? "OK" : 'False';
    }

    public function changeEstoniaFineInformedStatusAction ()
    {
        $model = new Model_Fines();
        $result = $model->changeFineStatus( $model::STATUS_PERSON_INFORMED );

        print $result ? "OK" : 'False';
    }
}
