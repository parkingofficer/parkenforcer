<?php

class Api2_IndexController extends Core_Controller_Action_Api
{
    /**
     * @dec For testing
     * @url http://{:host}/api2/{:key}
     *
     * @param key Your API key
     *
     * @return [{"success":"true|false","version":"version_number"}]
     */
    public function indexAction()
    {
    }
}
