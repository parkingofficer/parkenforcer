<?php
/**
 * @name Api_DocsController
 * @author darius.matulionis
 * @since : 2013.07.09, 17:22:07
 */
require_once 'UserController.php';
require_once 'BlackListController.php';
require_once 'WhiteListController.php';
require_once 'FinesController.php';

class Api2_DocsController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->formTitle = 'API DOCS';
    }

    public function indexAction()
    {
        $this->_request->setParam('disable_context', true);

        $controllers = [
            'User' => new Api2_UserController($this->_request, $this->_response),
            'BlackList' => new Api2_BlackListController($this->_request, $this->_response),
            'WhiteList' => new Api2_WhiteListController($this->_request, $this->_response),
            'Fines' => new Api2_FinesController($this->_request, $this->_response),
        ];

        $docs = [];
        foreach ($controllers as $resource => $controller) {
            foreach (get_class_methods($controller) as $action) {
                if (strstr($action, 'Action') != false) {
                    $r = new Zend_Reflection_Method($controller, $action);
                    $docblock = $r->getDocblock();
                    $data = [];

                    foreach ($docblock->getTags() as $tag) {
                        if ($tag->getName() == 'dec') {
                            $data['dec'] = $docblock->getTag('dec')->getDescription();
                        }
                        if ($tag->getName() == 'action') {
                            $data['action'] = $docblock->getTag('action')->getDescription();
                        }
                        if ($tag->getName() == 'url') {
                            $data['url'] = str_replace(
                                ['http://{:host}/', '{:key}'],
                                [$this->view->baseUrl(), $this->_request->getParam('key')],
                                $tag->getDescription()
                            );
                            $data['url_no_key'] = str_replace(
                                ['http://{:host}/'],
                                [$this->view->baseUrl()],
                                $tag->getDescription()
                            );
                        }

                        if ($tag->getName() == 'param' && $tag->getVariableName() == '$return_success') {
                            //$data['return_success'] = Core_Utils::json_format($tag->getDescription());
                            $data['return_success'] = $this->processJsonPrams($tag->getDescription());
                        }

                        if ($tag->getName() == 'param' && $tag->getVariableName() == '$return_error') {
                            //$data['return_success'] = Core_Utils::json_format($tag->getDescription());
                            $data['return_error'] = $this->processJsonPrams($tag->getDescription());
                        }

                        if ($tag->getName() == 'param' && $tag->getVariableName() == '$_POST') {
                            $data['post'] = $this->processPostPrams($tag->getDescription());
                        }
                    }
                    //$filter = new Zend_Reflection_Docblock_Tag_Param($tagDocblockLine);
                    //var_dump($docblock->getTag("param"));
                    //var_dump($docblock);
                    //var_dump($data);

                    $docs[$resource][$action] = $data;
                }
            }
        }
        //var_dump($docs);
        $this->view->docs = $docs;
    }

    private function processJsonPrams($string)
    {
        $data = '';
        preg_match_all('/\((.*?)\)/si', $string, $matches);

        $param_types = ['(int)', '(string)', '(double)', '(array)', '(boolean)', '(file)', '(date)'];
        if (! empty($matches[0])) {
            foreach (array_unique($matches[0]) as $v) {
                if (in_array($v, $param_types)) {
                    $string = str_replace($v, '<span class="green">'.$v.'</span>', $string);
                }
            }
        }
        $string = explode(';', $string);

        foreach ($string as $k => $line) {
            $line = trim($line);
            if (! empty($line)) {
                //$data[] = $line;
                $line = '<span class="string">'.str_replace([': <span', ' # '],
                        [':</span></span> <span', '<span class="comment"> '], $line).'</span>';
            }
            $line = str_replace([': ', '<span class="comment">'],
                [': <span class="bold blue">', '</span><span class="comment">'], $line);
            $line = str_replace('\\t', '<span class="pl20px">&nbsp;</span>', $line);
            $data[] = $line;
        }

        return $data;
    }

    private function processPostPrams($string)
    {
        $data = '';
        preg_match_all('/\((.*?)\)/si', $string, $matches);

        $param_types = ['(int)', '(string)', '(double)', '(array)', '(boolean)', '(file)', '(date)'];
        if (! empty($matches[0])) {
            foreach (array_unique($matches[0]) as $v) {
                if (in_array($v, $param_types)) {
                    $string = str_replace($v, '<span class="green">'.$v.'</span>', $string);
                }
            }
        }
        $string = explode(';', $string);

        foreach ($string as $k => $line) {
            $line = trim($line);
            if (! empty($line)) {
                $data[] = '<span class="string">'.str_replace([': <span', ' # '],
                        [':</span> <span', '<span class="comment"> '], $line).'</span>';
            }
        }

        return $data;
    }
}
