<?php

/*
 * @name Api_BlackListController
 * @author darius.matulionis
 * @since : 2013.07.09, 15:34:11
 */

use \AppBundle\Services\FinesService; 

class Api2_FinesController extends Core_Controller_Action_Api
{
    public function init()
    {
        parent::init();
        $this->checkSession();
    }

    /**
     * @dec For testing
     * @url http://{:host}/api/fines/{:key}
     * @action post
     * @param string $key Your API key
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
        $this->view->success = true;
        $this->view->msg = 'Test success';
    }

    /**
     * @dec Check Plate
     * @url http://{:host}/api/fines/check-plate/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     *        plate: (string) # Plate number;
     *      lat: (double) # (optional) GPS LAT Format (xx.xxxxxxx);
     *      lon: (double) # (optional) GPS LON Format (yy.yyyyyyy);
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (boolean) paid:  true/false # Is paid for parking;
     *        (string) zone:  zone # Zone letter;
     *        (date) paid_from:  datetime # Paid from (format Y-m-d H:i:s);
     *        (date) paid_to:  datetime # Paid till (format Y-m-d H:i:s);
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function checkPlateAction()
    {
        $log = new Model_PlateCheckLog();
        if ($this->authenticated) {
            $gps = null;
            $lat = null;
            $lon = null;

            if ((double)$this->_request->getParam('lat') && (double)$this->_request->getParam('lon')) {
                $lat = (double)$this->_request->getParam('lat');
                $lon = (double)$this->_request->getParam('lon');
                $gps = $lat . ', ' . $lon;
            }

            $log_id = $log->save(array(
                'adm_device_id' => $this->_request->getParam('adm_device_id'),
                'admin_id' => $this->authenticated_admin_id,
                'plate' => $this->_request->getParam('plate'),
                'time' => date('Y-m-d H:i:s'),
                'gps' => $gps,
                'request' => json_encode($this->_request->getParams()),
                'zone' => $this->_request->getParam('zone')
            ));
            $plate = $this->_request->getParam('plate');
            $zone = $this->_request->getParam('zone');

            $z_model = new Model_Zone();
            $zcs_model = new Model_ZoneCauseRates();
            //--------------------------------------------------------------------------------------//

            $zona = $z_model->fetchRow($z_model->select()->where(' id = ?', (int)$zone));
            $zoneId = $zona['id'];

            if (!$plate) {
                $this->view->success = false;
                $this->view->msg = 'No plate number';

            } else {

                if (date('H:i:s') >= $zona['work_time_begin'] and date('H:i:s') <= $zona['work_time_end']) {

                    $model = new Model_Parking();
                    $select2 = $model->select();
                    $select2->where('plate = ?', $plate);
                    $select2->where('zone = ?', $zone);
                    $select2->where('date_start >= ?', date('Y-m-d 00:00:00', strtotime('-7 days')));
                    $select2->where("date_end >= '".date("Y-m-d H:i:s", strtotime( "-".($zona["minutes_to_not_paid"])." minutes" ))."' OR date_end IS NULL");
                    $select2->order('(date_end is NULL) DESC');
                    $select2->order('date_end DESC');
                    $select2->limit(1);
                    $payment2 = $model->fetchRow($select2);

                    if ($payment2) {
                        $now = time();
                        $payment_start = strtotime($payment2->date_start);
                        $payment_end = strtotime($payment2->date_end);
                        if (($payment_end >= $now || $payment_end == null) && $payment2->zone = $zone) {
                            $this->view->success = true;
                            $this->view->paid = true;
                            $this->view->paid_from = $payment2->date_start;
                            if ($payment2->date_end == null) {
                                $end = 'initiate stop';
                            } else {
                                $end = $payment2->date_end;
                            }
                            $this->view->paid_to = $end;
                            $this->view->zone = $payment2->zone;
                            $this->view->msg = "Paid";

                            $b_model = new Model_BlackList();
                            $select = $b_model->select();
                            $select->where('plate_no = ?', $plate);
                            $select->where('`delete` = 0');
                            $select->where('zone = ?', $zone);
                            $plate_in_black_list = $b_model->fetchRow($select);
                            if ($plate_in_black_list) {
                                $plate_in_black_list->delete = 1;
                                $plate_in_black_list->deleted_by = $this->authenticated_admin;
                                $plate_in_black_list->delete_time = date('Y-m-d H:i:s');
                                $plate_in_black_list->save();
                                $this->view->msg = 'Plate deleted from black list. Payment was found';
                            }
                        } elseif (!$this->checkWhiteList($plate, $zone)) {
                            $integrationParking = (new Model_Parking())->checkEstoniaTeliaParking($plate, $zone);
                            if($integrationParking){
                                $this->view->paid_to = $integrationParking['to'];
                                $this->view->paid_from = $integrationParking['from'];
                                $this->view->zone = $integrationParking['zone'];
                                $this->view->msg = "Paid";
                                $this->view->paid = true;

                            }else{
                            $f_model = new Model_Fines();
                            $select = $f_model->select();
                            $select->where('plate = ?', $plate);
                            $select->where('zone = ?', $zone);
                            $select->where('date >= ?', date('Y-m-d 00:00:00'));
                            $select->where('date <= ?', date('Y-m-d 23:59:59'));
                            $fine = $f_model->fetchRow($select);
                            if ($fine) {
                                $this->view->success = true;
                                $this->view->paid = false;
                                $this->view->fine_is_created = true;
                                $this->view->msg = 'Fine is already created';
                            } else {
                                $b_model = new Model_BlackList();
                                $select = $b_model->select();
                                $select->where('plate_no = ?', $plate);
                                $select->where('`delete` = 0');
                                $select->where('zone = ?', $zone);
                                $plate_in_black_list = $b_model->fetchRow($select);
                                $repark = new DateTime($payment2['date_end']);

                                if (!empty($zona['minutes_to_not_paid'])) {

                                    $repark->add(new DateInterval('PT' . $zona['minutes_to_not_paid'] . 'M'));
                                    if ($plate_in_black_list) {
                                        if ($plate_in_black_list['time_checked'] < date('Y-m-d H:i:s',
                                                strtotime('- ' . $zona['tolerance_time'] . ' minutes'))) // jei 17:30 < 17:30-00:10
                                        {

                                            $this->view->success = true;
                                            $this->view->paid = false;
                                            $this->view->add_fine = true;
                                            $this->view->status_id = '1';
                                            $this->view->request_time = date('Y-m-d H:i:s');
                                            $this->view->plate_in_balck_list = true;
                                            $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                                            $this->view->cause_id = $plate_in_black_list->cause_id;
                                            $this->view->msg = 'Generating fine';
                                        } else {
                                            $this->view->success = true;
                                            $this->view->paid = false;
                                            $this->view->plate_in_balck_list = true;
                                            $this->view->cause_id = $plate_in_black_list->cause_id;
                                            $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                                            $this->view->msg = 'Plate is in black list';
                                        }
                                    } elseif (date('Y-m-d H:i:s', $now) >= $repark->format('Y-m-d H:i:s')) {
                                        $b_model->save(array(
                                            'zone' => $zone,
                                            'cause_id' => 1,
                                            'plate_no' => $plate,
                                            'time_checked' => date('Y-m-d H:i:s'),
                                            'gps' => $gps,
                                            'lat' => $lat,
                                            'lon' => $lon,
                                        ));
                                        $this->view->success = true;
                                        $this->view->paid = false;
                                        $this->view->added_to_balck_list = true;
                                        $this->view->msg = 'Parking 2nd time. Adding to black list';
                                    } else {

                                        $b_model->save(array(
                                            'zone' => $zone,
                                            'cause_id' => 1,
                                            'plate_no' => $plate,
                                            'time_checked' => date('Y-m-d H:i:s'),
                                            'gps' => $gps,
                                            'lat' => $lat,
                                            'lon' => $lon,
                                        ));


                                        $this->view->success = true;
                                        $this->view->paid = false;
                                        $this->view->added_to_balck_list = true;
                                        $this->view->add_fine = true;
                                        $this->view->paid_from = $payment2->date_start;
                                        $this->view->paid_to = $payment2->date_end;
                                        $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                                        $this->view->request_time = date('Y-m-d H:i:s');
                                        $this->view->plate_in_balck_list = true;
                                        $this->view->cause_id = '1';
                                        $this->view->status_id = '1';

                                        $this->view->msg = 'Time ended. Generating fine';
                                    }
                                }
                            }
                        }
                        }
                    } elseif (!$this->checkWhiteList($plate, $zone)) {
                        $integrationParking = (new Model_Parking())->checkEstoniaTeliaParking($plate, $zone);
                        if ($integrationParking) {
                            $this->view->paid_to = $integrationParking['to'];
                            $this->view->paid_from = $integrationParking['from'];
                            $this->view->zone = $integrationParking['zone'];
                            $this->view->msg = "Paid";
                            $this->view->paid = true;
                        } else {
                            $b_model = new Model_BlackList();
                            $select = $b_model->select();
                            $select->where('plate_no = ?', $plate);
                            $select->where('`delete` = 0');
                            $select->where('zone = ?', $zone);
                            $plate_in_black_list = $b_model->fetchRow($select);

                            if ($plate_in_black_list) {
                                if ($plate_in_black_list['time_checked'] < date('Y-m-d H:i:s',
                                        strtotime('- ' . $zona['tolerance_time'] . ' minutes'))) // jei 17:30 < 17:30-00:10
                                {
                                    $zoneCauseRate = $zcs_model->fetchRow($zcs_model->select()->where(" zone_id = '$zoneId' and cause_id = '2' "));
                                    $this->view->success = true;
                                    $this->view->paid = false;
                                    $this->view->add_fine = true;
                                    $this->view->amount = $zoneCauseRate['rate'];
                                    $this->view->status_id = '1';
                                    $this->view->request_time = date('Y-m-d H:i:s');
                                    $this->view->plate_in_balck_list = true;
                                    $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                                    $this->view->cause_id = $plate_in_black_list->cause_id;
                                    $this->view->msg = 'Generating fine';
                                } else {
                                    $this->view->success = true;
                                    $this->view->paid = false;
                                    $this->view->plate_in_balck_list = true;
                                    $this->view->cause_id = $plate_in_black_list->cause_id;
                                    $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                                    $this->view->msg = 'Plate is in black list';
                                }
                            } else {
                                $f_model = new Model_Fines();
                                $select = $f_model->select();
                                $select->where('plate = ?', $plate);
                                $select->where('zone = ?', $zone);
                                $select->where('date >= ?', date('Y-m-d 00:00:00'));
                                $select->where('date <= ?', date('Y-m-d 23:59:59'));
                                $fine = $f_model->fetchRow($select);
                                if ($fine) {
                                    $this->view->success = true;
                                    $this->view->paid = false;
                                    $this->view->fine_is_created = true;
                                    $this->view->msg = 'Fine is already created';
                                } else {
                                    $b_model->save(array(
                                        'zone' => $zone,
                                        'cause_id' => 2,
                                        'plate_no' => $plate,
                                        'time_checked' => date('Y-m-d H:i:s'),
                                        'gps' => $gps,
                                        'lat' => $lat,
                                        'lon' => $lon,
                                    ));

                                    $this->view->success = true;
                                    $this->view->paid = false;
                                    $this->view->added_to_balck_list = true;
                                    $this->view->msg = 'No plate number found in database. Adding to black list';
                                }

                                if ($zona['tolerance_time'] == 0) {
                                    $zoneCauseRate = $zcs_model->fetchRow($zcs_model->select()->where(" zone_id = '$zoneId' and cause_id = '2'"));

                                    $f_model = new Model_Fines();
                                    $select = $f_model->select();
                                    $select->where('plate = ?', $plate);
                                    $select->where('zone = ?', $zone);
                                    $select->where('date >= ?', date('Y-m-d 00:00:00'));
                                    $select->where('date <= ?', date('Y-m-d 23:59:59'));
                                    $fine = $f_model->fetchRow($select);

                                    if ($fine) {
                                        $this->view->success = true;
                                        $this->view->paid = false;
                                        $this->view->fine_is_created = true;
                                        $this->view->msg = 'Fine is already created';
                                    } else {
                                        $this->view->success = true;
                                        $this->view->paid = false;
                                        $this->view->add_fine = true;
                                        $this->view->amount = $zoneCauseRate['rate'];
                                        $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                                        $this->view->request_time = date('Y-m-d H:i:s');
                                        $this->view->plate_in_balck_list = true;
                                        $this->view->cause_id = '2';
                                        $this->view->status_id = '1';
                                        $this->view->msg = 'Generating fine';
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $this->view->success = true;
                    $this->view->added_to_balck_list = false;
                    $this->view->msg = 'Zone work time ended';
                }
            }
            //$log->update(array('response'=>json_encode($this->view)),'id = '.$log_id);
            $log_id->response = json_encode($this->view);
            $log_id->save();
        }
    }

    /**
     * @dec Get Fine Causes
     * @url http://{:host}/api/fines/get-fine-causes/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (array) fine_causes:  [;
     *          \t {;
     *          \t \t (int) id : fine_causes_id # Fine Causes ID;
     *          \t \t (string) title : cause_title # Fine Cause;
     *          \t };
     *      ] # List fine Causes;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function getFineCausesAction()
    {
        if ($this->authenticated) {
            $model = new Model_BlackListCauses();
            $this->view->fine_causes = $model->fetchAll('visible = 1')->toArray();
            $this->view->success = true;
        }
    }

    /**
     * @dec Get Fine Statuses
     * @url http://{:host}/api/fines/get-fine-statuses/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (array) fine_statuses:  [;
     *          \t {;
     *          \t \t (int) id : fine_status_id # Fine Causes ID;
     *          \t \t (string) title : status_title # Fine Status;
     *          \t };
     *      ] # List fine Causes;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function getFineStatusesAction()
    {
        if ($this->authenticated) {
            $this->view->fine_statuses = Model_Fines::statuses()->map(function ($status, $key) {
                return [
                    'id' => (int) $key,
                    'title' => $status
                ];
            })->toArray();
            $this->view->success = true;
        }
    }

    /**
     * @dec Insert Fine
     * @url http://{:host}/api2/fines/insert/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     *        causes_id: (int) # Fine Cause ID. Get list from http://{:host}/api2/fines/get-fine-causes/{:key};
     *        status_id: (int) # Fine Status ID. Get list from http://{:host}/api2/fines/get-fine-statuses/{:key};
     *        plate: (string) # Plane number;
     *        car: (string) # Car model and manufacture;
     *        date: (string) # Fine date. Format:(Y-m-d H:i:s);
     *        first_photo_date: (string) # First photo date. Format:(Y-m-d H:i:s);
     *        start: (string) # Start date time. Format:(Y-m-d H:i:s);
     *        end: (string) # End date time. Format:(Y-m-d H:i:s);
     *        lat: (double) # GPS LAT Format (xx.xxxxxxx);
     *        lon: (double) # GPS LON Format (yy.yyyyyyy);
     *        street: (string) # Street;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (int) fine_id: fine_id # Fine ID;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function insertAction()
    {
        if ($this->authenticated) {
            $model = new Model_Fines();
            $p_model = new Model_Parking();

            try {
                $model->getAdapter()->beginTransaction();
                $data = $this->_request->getPost();
                $photo_files = [
                    'first_photo',
                    'plate_photo',
                    'front_photo',
                    'back_photo',
                    'left_side_photo',
                    'right_side_photo',
                ];
                $can_be_empty = array_merge([
                    'id',
                    'gps',
                    'car',
                    'bank_payment_no',
                    'notification_sent_time',
                    'notification_sent_to',
                    'zone',
                    'date',
                    'change_status_date',
                    'start',
                    'end',
                    'admin_id',
                    'municipality_id',
                    'id_public',
                    'amount',
                    'default_amount',
                    'rate',
                    'paid',
                    'first_photo_date',
                    'import_xlsx',
                    'import_xlsx_file_name',
                    'name',
                    'surname',
                    'u_email',
                    'u_phone_number',
                    'u_comment',
                    'billing_id'
                ], $photo_files);
                $is_valid_post = true;
                $not_asigned_fields = null;
                foreach ($model->fetchNew() as $k => $v) {
                    if (! in_array($k, $can_be_empty)) {
                        if (! isset($data[$k]) || ! $data[$k]) {
                            $is_valid_post = false;
                            $not_asigned_fields[$k] = '';
                        }
                    }
                }

                $this->view->not_asigned_fields = $not_asigned_fields;

                if ($is_valid_post) {
                    if ($data['causes_id'] == 1) {
                        $select2 = $p_model->select();
                        $select2->where('plate = ?', $data['plate']);
                        $select2->where('zone = ?', $data['zone']);
                        $select2->where('date_start >= ?', date('Y-m-d 00:00:00', strtotime('-7 days')));
                        $select2->order('(date_end is NULL) DESC');
                        $select2->order('date_end DESC');
                        $select2->limit(1);
                        $payment2 = $model->fetchRow($select2);

                        if ($payment2) {
                            $data['start'] = $payment2->date_start;
                            $data['end'] = $payment2->date_end;
                        }
                    }

                    $b_model = new Model_BlackList();
                    $select = $b_model->select();
                    $select->where('plate_no = ?', $data['plate']);
                    $select->where('zone = ?', $data['zone']);
                    $select->where('`delete` = 0');
                    $plate_in_black_list = $b_model->fetchRow($select);
                    if ($plate_in_black_list) {
                        $plate_in_black_list->delete = 1;
                        $plate_in_black_list->deleted_by = $this->authenticated_admin;
                        $plate_in_black_list->delete_time = date('Y-m-d H:i:s');
                        $plate_in_black_list->save();
                        if ($data['causes_id'] != 1) {
                            $data['start'] = $plate_in_black_list->time_checked;
                            $data['end'] = date('Y-m-d H:i:s');
                        }
                        $this->view->msg = 'Plate deleted from black list and new fine inserted ';
                    }
                    $zone = $this->_request->getParam('zone');
                    $Model_Zone = new Model_Zone();
                    $zoneObject = $Model_Zone->fetchRow('id = '.$zone);
                    $model_city = new Model_City();
                    $cityObject = $model_city->fetchRow( 'id = '.$zoneObject->city_id.'' );
                    $fine_date = new DateTime($data['date']); //current date/time
                    $fine_date->add(new DateInterval("PT{$zoneObject['warning_pay_time']}H"));

                    $Model_BlackListCauses = new Model_BlackListCauses();
                    $blackListCause = $Model_BlackListCauses->fetchRow('id = '.$data['causes_id']);

                    $cause = $data['causes_id'] ;
                    $data['date'] = date('Y-m-d H:i:s');
                    $data['adm_device_id'] = $this->_request->getParam('adm_device_id');
                    $data['admin_id'] = $this->authenticated_admin_id;
                    $data['gps'] = $data['lat'].', '.$data['lon'];
                    $data['plate'] = strtoupper(str_replace(' ', '', $data['plate']));
                    $data['zone'] = $zone;
                    $data['id_public'] = Model_Fines::uniqueFineNumber( $cityObject );
                    $zcs_model = new Model_ZoneCauseRates();
                    $zoneCauseRate = $zcs_model->fetchRow($zcs_model->select()->where(" zone_id = '$zone' and cause_id = '$cause' "));
                    $data['amount'] = $zoneCauseRate['rate'];
                    $data['default_amount'] = $zoneCauseRate['rate'];
                    $data['zone_name'] = $zoneObject['zone_name'];
                    $data['due_date'] = $fine_date->format('d.m.Y');
                    $data['operator_name'] = $this->authenticated_admin;
                    $data['cause_name'] = $blackListCause['title'];

                    $fine = $model->save($data);

                    // $zone = $this->_request->getParam('zone');

                    $this->view->fine = $data;
                    $this->view->fine_id = $fine->id;
                    $this->view->time_added_to_black_list = $plate_in_black_list->time_checked;
                    $domain_model = new Model_Domain();
                    $options = $domain_model->options();
                    if ($options['name'] == 'parking.unipark.lt' || $options['name'] == 'parkingee.unipark.lt' || $options['name'] == 'stageparking.unipark.lt') {
                        $billingQueue = new Model_BillingQueue();
                        $billingQueue->save([
                            'type' => Model_BillingQueue::CREATE_FINE,
                            'system_id' => $fine['id'],
                            'status' => 0,
                        ]);
                    }
                    $model->getAdapter()->commit();

                    //$api = new Core_Api_Oxid();
                    //$api->createFine($fine);
                } else {
                    $model->getAdapter()->rollBack();
                    $this->view->success = false;
                    $this->view->msg = 'Not all required fields are posted';
                }
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                $this->view->msg = $e->getMessage();
            }
        }
    }

    /**
     * @dec Upload Fine Photo
     * @url http://{:host}/api2/fines/insert-all-plates/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     *        plate: (varchar) # plate;
     *        gps: (varchar) # gps;
     *
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function insertAllPlatesAction()
    {
        $model = new Model_AllPlates();
        $this->view->success = true;
        $data = json_decode(file_get_contents('php://input'), true);
        $admin = new Model_Admins();
        $access = $admin->fetchrow("adm_device_id = '".$data['adm_device_id']."'");
        if (! empty($access)) {
            if (! empty($data['zone'])) {
                foreach ($data['plates'] as $d) {
                    $model->save([
                        'plate' => $d['plate'],
                        'adm_device_id' => $data['adm_device_id'],
                        'gps' => $d['gps'],
                        'zone' => $data['zone'],
                    ]);
                }
            } else {
                $this->view->success = false;
                $this->view->reason = 'no zone id';
            }
        } else {
            $this->view->success = false;
            $this->view->reason = 'wrong device id';
        }
        //$this->view->msg =  $data["plates"];
    }

    /**
     * @dec Upload Fine Photo
     * @url http://{:host}/api2/fines/upload-fine-photo/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     *        fine_id: (int) # Fine ID;
     *        photo: (file) # Photo Format (JPG/PNG);
     *        photo_name: (string) # Which photo is uploading (first_photo/plate_photo/front_photo,back_photo/left_side_photo/right_side_photo);
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *      (string) photo_url: photo_url # Photo url;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function uploadFinePhotoAction()
    {
        //  if($this->authenticated){
        $model = new Model_Fines();
        $model->getAdapter()->beginTransaction();

        $photo_files = [
            'first_photo',
            'plate_photo',
            'front_photo',
            'back_photo',
            'left_side_photo',
            'right_side_photo',
        ];
        $data = $this->_request->getPost();

        if (! isset($data['fine_id']) || empty($data['fine_id'])) {
            $this->view->success = false;
            $this->view->msg = 'No fine ID';
        } elseif (! isset($data['photo_name']) || empty($data['photo_name']) || ! in_array($data['photo_name'],
                $photo_files)) {
            $this->view->success = false;
            $this->view->msg = 'No photo name';
        } elseif (count($_FILES) != 1) {
            $this->view->success = false;
            $this->view->msg = 'One photo file should be uploaded';
        } else {
            $fine = $model->find((int)$data['fine_id'])->current();
            if (! $fine) {
                $this->view->success = false;
                $this->view->msg = 'No Fine found';
            } else {
                //$this->view->file = $_FILES;
                $fv = new Zend_Validate_File_MimeType(['image/jpeg', 'image/png']);
                if (! isset($_FILES['photo']) || empty($_FILES['photo']['tmp_name']) || $fv->isValid($_FILES['photo']['type'])) {
                    $this->view->success = false;
                    $this->view->msg = 'Bad file';
                } else {
                    try {
                        $upload = new Core_Utils_Upload();
                        $upload->uploadFile(FINES_PHOTOS_PATH.'/'.$data['fine_id'].'/', 'md5', 10, $_FILES);
                        if (! $upload->_files['photo']) {
                            $this->view->success = false;
                            $this->view->msg = 'File upload Error';
                        } else {
                            $fine->{$data['photo_name']} = $upload->_files['photo'];
                            $fine->save();
                            $model->getAdapter()->commit();
                            $this->view->success = true;
                            $this->view->msg = 'File upload success';
                        }
                    } catch (Exception $e) {
                        $model->getAdapter()->rollBack();
                        $this->view->success = false;
                        $this->view->msg = $e->getMessage();
                    }
                }
            }
        }
    }

    /**
     * @dec Get Zones and rates
     * @url http://{:host}/api/fines/get-user-zones/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (array) fine_statuses:  [;
     *          \t {;
     *          \t \t (int) id : fine_status_id # Fine Causes ID;
     *          \t \t (string) title : status_title # Fine Status;
     *          \t };
     *      ] # List fine Causes;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function getUserZonesAction()
    {
        if ($this->authenticated) {
            $z_model= new Model_Zone();
            $zones = $z_model->getUserInfo($this->authenticated_admin_id);
            $this->view->success = true;
            $this->view->zones = [$zones];
            return;

        }
    }

    /**
     * @dec Get Zones and rates
     * @url http://{:host}/api/fines/get-zone-cause-rate/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     *        zone: (string) # zone Id;
     *        cause: (string) # Const;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (array) fine_statuses:  [;
     *          \t {;
     *          \t \t (int) id : fine_status_id # Fine Causes ID;
     *          \t \t (string) title : status_title # Fine Status;
     *          \t };
     *      ] # List fine Causes;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function getZoneCauseRateAction()
    {
        if ($this->authenticated) {
            $z_model= new Model_Zone();
            $zcs_model = new Model_ZoneCauseRates();
            $zones = $z_model->getUserInfo($this->authenticated_admin_id);

            $fine = [];

            if($this->_request->getParam('zone') && $this->_request->getParam('cause')){
                $this->view->success = true;

                foreach ($zones as $single_zone){
                    $availableZones[] = $single_zone['id'];
                }

                $zone = $this->_request->getParam('zone');
                if(in_array($zone, $availableZones)){
                    $cause = $this->_request->getParam('cause');
                    $zoneCauseRate = $zcs_model->fetchRow($zcs_model->select()->where(" zone_id = '$zone' and cause_id = '$cause' "));
                }

                $fine['amount'] = $zoneCauseRate['rate'];

            }else{
                $this->view->success = false;
            }
            $this->view->fine = $fine;
            return;

        }
    }
    protected function checkWhiteList($plate, $zone)
    {

        $isInWhiteList = false;
        $model = new Model_WhiteList();
        $white_list = $model->checkWhiteList($plate, $zone);
        $time = date('Y-m-d H:i:s');
        //Die(var_dump($white_list));
        if ($white_list
            && $time <= $white_list->date_to
            && $time >= $white_list->date_from)
        {
            $this->view->success = $isInWhiteList = true;
            $this->view->paid = true;
            $this->view->paid_from = $white_list->date_from;
            $this->view->paid_to = $white_list->date_to;
            $this->view->white_list_group = $white_list->group;
            $this->view->address = $white_list->address;
            $this->view->msg = 'Plate is in white list';
        }

        return $isInWhiteList;
    }

}
