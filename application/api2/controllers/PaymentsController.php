<?php

/**
 * @name Api_PaymentsController
 * @author darius.matulionis
 */
class Api2_PaymentsController extends Core_Controller_Action_Api
{
    public function init()
    {
        parent::init();
        $api = $this->getParam('SystemApi');
        $model = new Model_Admins();
        if ($api) {
            $select = $model->select();
            $select->where('adm_device_id = ?', $api);
            $admin = $model->fetchRow($select);
            if ($admin) {
                $this->authenticated = true;
            } else {
                $this->authenticated = false;
                $this->view->success = false;
                $this->view->msg = 'API Key Not Found';
            }
        } else {
            $this->authenticated = false;
            $this->view->success = 'false';
            $this->view->msg = 'No API Key';
        }
    }

    /**
     * @dec For testing
     * @url http://{:host}/api/payments/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
    }

    public function paymentInsertAction()
    {
        if ($this->authenticated) {
            $api = $this->getParam('SystemApi');
            $model = new Model_Admins();
            if ($api) {
                $select = $model->select();
                $select->where('adm_device_id = ?', $api);
                $admin = $model->fetchRow($select);
                if ($api == 'aaa') {
                    $zone = 'Unipark';
                    $city = 'Šiauliai';
                }

                $pl_model = new Model_PaymentsLog();
                $p_log = $pl_model->save([
                    'date' => date('Y-m-d H:i:s'),
                    'request' => json_encode($this->_request->getParams()),
                ]);

                $this->view->success = false;
                $SystemName = $this->getParam('SystemName');
                if (! $SystemName) {
                    $this->view->msg = 'No System Name';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }
                $UniqueID = $this->getParam('UniqueID');
                if (! $UniqueID) {
                    $this->view->msg = 'No Unique Payment ID';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $dateValidate = new Zend_Validate_Date();

                $StartTime = $this->getParam('StartTime');
                if (! $StartTime) {
                    $this->view->msg = 'No Start Time';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }
                if (! $dateValidate->setFormat('Y-m-d H:i:s')->isValid($StartTime)) {
                    $this->view->msg = 'Start Time is not valid. Format (Y-m-d H:i:s)';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $EndTime = $this->getParam('EndTime');
                if (! $EndTime) {
                    $this->view->msg = 'No End Time';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                if (! $dateValidate->setFormat('Y-m-d H:i:s')->isValid($EndTime)) {
                    $this->view->msg = 'End Time is not valid. Format (Y-m-d H:i:s)';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $LicencePlate = $this->getParam('LicencePlate');
                if (! $LicencePlate) {
                    $this->view->msg = 'No License plate';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $Amount = (int) $this->getParam('Amount');
                if (! $Amount) {
                    $this->view->msg = 'No payment amount';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $LocationLat = (double) $this->getParam('LocationLat');
                if (! $LocationLat) {
                    $this->view->msg = 'No Latitude. Format (xx.xxxxxx)';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $LocationLon = (double) $this->getParam('LocationLon');
                if (! $LocationLon) {
                    $this->view->msg = 'No Longitude. Format (xx.xxxxxx)';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $MachineSerialNumber = (double) $this->getParam('MachineSerialNumber');
                if (! $MachineSerialNumber) {
                    $this->view->msg = 'No Machine Serial Number';
                    $p_log->response = json_encode($this->view->msg);
                    $p_log->save();

                    return;
                }

                $FreeTimeInMinutes = (int) $this->getParam('FreeTimeInMinutes');

                $model = new Model_Parking();
                $model->getAdapter()->beginTransaction();
                $LicencePlate = str_replace('-', '', $LicencePlate);
                $LicencePlate = str_replace(' ', '', $LicencePlate);
                try {
                    $data = [
                        'id' => $UniqueID,
                        'phone' => null,
                        'operator' => 4,
                        'operator_name' => strtolower($SystemName),
                        'plate' => $LicencePlate,
                        'bar_code' => $MachineSerialNumber,
                        'duration_hours' => DateTime::createFromFormat('Y-m-d H:i:s',
                            $StartTime)->diff(DateTime::createFromFormat('Y-m-d H:i:s', $EndTime))->h,
                        'count' => 1,
                        'price_cent' => $Amount,
                        'city' => $city,
                        'zone' => $zone,
                        'reminder' => 0,
                        'date_inserted' => date('Y-m-d H:i:s'),
                        'date_start' => $StartTime,
                        'date_end' => $EndTime,
                        'free_time_in_minutes' => $FreeTimeInMinutes ? $FreeTimeInMinutes : null
                    ];

                    $model->save($data);
                    $model->getAdapter()->commit();

                    $this->view->success = true;

                    $p_log->response = json_encode($data);
                    $p_log->save();
                } catch (Exception $e) {
                    $model->getAdapter()->rollBack();
                    $this->view->success = false;
                    $this->view->msg = $e->getMessage();

                    $p_log->response = json_encode($e->getMessage());
                    $p_log->save();
                }
            }
        }
    }
}
