<?php

/**
 * @name Api_WhiteListController
 * @author darius.matulionis
 * @since : 2013.07.09, 15:34:11
 */
class Api2_WhiteListController extends Core_Controller_Action_Api
{
    public function init()
    {
        parent::init();
        $this->checkSession();
    }

    /**
     * @dec For testing
     * @url http://{:host}/api/white-list/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function indexAction()
    {
    }

    /**
     * @dec White list group add
     * @url http://{:host}/api/white-list/add/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *      id: (int) # null for insert or id for update;
     *      white_list_group_id: (int) # White List Group ID;
     *      company: (string) # Company Title;
     *      plate: (string) # Plate;
     *      date_from: (string) # Date From (format Y-m-d);
     *      date_to: (string) # Date To (format Y-m-d);
     *      contract_date: (string) # Contract Date (format Y-m-d);
     *      contract_sum: (double) # Contract Sum;
     *      address: (string) # Address;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (int) white_list_id:  white_list_id # White List ID;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function addAction()
    {
        $data = [
            'company' => $this->getParam('company'),
            'plate' => $this->getParam('plate'),
            'date_from' => $this->getParam('date_from'),
            'date_to' => $this->getParam('date_to'),
            'contract_date' => $this->getParam('contract_date'),
            'contract_sum' => $this->getParam('contract_sum'),
            'address' => $this->getParam('address'),
        ];
        $has_empty_fields = false;
        foreach ($data as $v) {
            if (empty($v)) {
                $has_empty_fields = true;
            }
        }

        $date_valid = new Zend_Validate_Date();

        if (! $has_empty_fields) {
            $id = (int) $this->getParam('id') ? (int) $this->getParam('id') : null;
            $white_list_group_id = (int) $this->getParam('white_list_group_id') ? (int) $this->getParam('white_list_group_id') : null;
            if (! $white_list_group_id) {
                $this->view->success = false;
                $this->view->msg = 'No White List Group';
            } else {
                $model = new Model_WhiteListGroups();
                $grp = $model->find($white_list_group_id)->current();
                if (! $grp) {
                    $api = new Core_Api_Oxid();
                    $contract_type = $api->getContractType($white_list_group_id);
                    if ($contract_type->id && $contract_type->name) {
                        $grp = $model->save([
                            'id' => $contract_type->id,
                            'title' => $contract_type->name,
                        ]);
                        $white_list_group_id = $grp->id;
                    }
                }
                if ($grp) {
                    if ($date_valid->isValid($data['date_from']) && $date_valid->isValid($data['date_to']) && $date_valid->isValid($data['contract_date'])) {
                        $model = new Model_WhiteList();
                        $model->getAdapter()->beginTransaction();
                        try {
                            $data['id'] = $id;
                            $data['group_id'] = $white_list_group_id;
                            $row = $model->save($data);

                            $model->getAdapter()->commit();
                            $this->view->success = true;
                            $this->view->msg = 'White list item inserted';
                            $this->view->white_list_id = $row->id;
                        } catch (Exception $e) {
                            $model->getAdapter()->rollBack();
                            $this->view->success = false;
                            $this->view->msg = $e->getMessage();
                        }
                    } else {
                        $this->view->success = false;
                        $this->view->msg = 'Some of dates is not valid';
                    }
                } else {
                    $this->view->success = false;
                    $this->view->msg = 'Was unable to create new white list group';
                }
            }
        } else {
            $this->view->success = false;
            $this->view->msg = 'No all required fields specified';
        }
    }

    /**
     * @dec Delete from white list
     * @url http://{:host}/api/white-list/delete/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *      id: (int) # white list id;
     * @param json $return_success
     *      (string) msg: White item deleted;
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function deleteAction()
    {
        $id = (int) $this->getParam('id');
        if ($id) {
            $model = new Model_WhiteList();
            $model->getAdapter()->beginTransaction();
            try {
                $item = $model->find($id)->current();
                if ($item) {
                    $item->delete();
                    $model->getAdapter()->commit();
                    $this->view->success = true;
                    $this->view->msg = 'White item deleted';
                } else {
                    $this->view->success = false;
                    $this->view->msg = 'No White list item was found';
                }
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                $this->view->msg = $e->getMessage();
            }
        } else {
            $this->view->success = false;
            $this->view->msg = 'No White list ID';
        }
    }

    /**
     * @dec Truncate white list
     * @url http://{:host}/api/white-list/truncate/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json $return_success
     *      (string) msg: White list truncated;
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function truncateAction()
    {
        $model = new Model_WhiteList();
        $model->getAdapter()->beginTransaction();
        try {
            $model->getAdapter()->query('TRUNCATE TABLE '.$model->info('name'));
            $model->getAdapter()->commit();
            $this->view->success = true;
            $this->view->msg = 'White list truncated';
        } catch (Exception $e) {
            $model->getAdapter()->rollBack();
            $this->view->success = false;
            $this->view->msg = $e->getMessage();
        }
    }

    /**
     * @dec White list group add
     * @url http://{:host}/api/white-list/group-add/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *      id: (int) # null for insert or id for update;
     *      title: (string) # White list title;
     * @param json $return_success
     *      (string) msg: White list group inserted;
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (int) white_list_group_id:  white_list_group_id # White List Group ID;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function groupAddAction()
    {
        $id = (int) $this->getParam('id') ? (int) $this->getParam('id') : null;
        $title = $this->getParam('title');
        if ($title) {
            $model = new Model_WhiteListGroups();
            $model->getAdapter()->beginTransaction();
            try {
                $row = $model->save([
                    'id' => $id,
                    'title' => $title,
                ]);
                $model->getAdapter()->commit();
                $this->view->success = true;
                $this->view->msg = 'White list group inserted';
                $this->view->white_list_group_id = $row->id;
            } catch (Exception $e) {
                $model->getAdapter()->rollBack();
                $this->view->success = false;
                $this->view->msg = $e->getMessage();
            }
        } else {
            $this->view->success = false;
            $this->view->msg = 'No group title';
        }
    }

    /**
     * @dec Truncate white list groups
     * @url http://{:host}/api/white-list/truncate-groups/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param json $return_success
     *      (string) msg: White list group truncated;
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     */
    public function truncateGroupsAction()
    {
        $model = new Model_WhiteListGroups();
        $model->getAdapter()->beginTransaction();
        try {
            $model->getAdapter()->query('TRUNCATE TABLE '.$model->info('name'));
            $model->getAdapter()->commit();
            $this->view->success = true;
            $this->view->msg = 'White list groups truncated';
        } catch (Exception $e) {
            $model->getAdapter()->rollBack();
            $this->view->success = false;
            $this->view->msg = $e->getMessage();
        }
    }

    /**
     * @dec Add new user
     * @url http://{:host}/api/white-list/get-list/{:key}
     * @action post
     *
     * @param string $key Your API key
     * @param array $_POST
     *        adm_device_id: (string) # Device ID;
     * @param json $return_success
     *        (boolean) success: true # Is request success;
     *        (double) version:  api_version_number # API version number;
     *        (array) white_list:  [;
     *          \t {;
     *          \t \t (string) plate : plate_no # White List Plate No / ID;
     *          \t \t (int) group_id : group_id # White List Group ID;
     *          \t \t (string) company : company_title # Company/Person;
     *          \t \t (string) date_from : paidFrom # Paid From Date. Format (YYYY-MM-DD);
     *          \t \t (string) date_to : paidTill # Paid Till Date. Format (YYYY-MM-DD);
     *          \t \t (string) contract_date : contract_date # Contract signed Date. Format (YYYY-MM-DD);
     *          \t \t (double) contract_sum : contract_sum # Contract sum Format (yy.yyyyyyy);
     *          \t \t (string) address : address # Address;
     *          \t \t (string) group : group # White list group;
     *          \t };
     *      ] # List of black;
     * @param json $return_error
     *        (boolean) success: false # Is request success;
     *        (boolean) has_session: false # Is device authenticated;
     *        (double) version: api_version_number # API version number;
     *        (string) msg: error_message # Error message;
     * (double) has_session: false # Has user session for this device ID;
     */
    public function getListAction()
    {
        if ($this->authenticated) {
            $zone = $this->_request->getParam('zone');

            $model = new Model_WhiteList();

            // $select2 = $zones->select()->from(array('z' => $zones->info('name')),array('z.primary_wg_id'))->where( "zone_id in(".$zone.")");
            //  $select2= $zones->fetchRow($zones->select()->where( 'zone_id in('.$select2.')'));
            $where = [];
            $where[] = ('zone_id = '.$zone);
            $this->view->white_list = $model->getWhiteList($where);
        }
    }
}
