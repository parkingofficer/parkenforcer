<?php

/**
 * Description of Admins.
 *
 * @author Darius Matulionis
 */
class Model_AllPlates extends Core_Db_Table
{
    protected $_name = 'box_all_plates';

    protected $_primary = 'id';

    public function getPlates()
    {
        $zone = new Model_Zone();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zone->info('name'),
            $this->_name.'.zone = '.$zone->info('name').'.id',
            $zone->info('name').'.zone_code as zc'
        );

        $admin = new Model_Admins();
        $select->joinLeft(
            $admin->info('name'),
            $this->_name.'.adm_device_id = '.$admin->info('name').'.adm_device_id',
            'CONCAT('.$admin->info('name').'.adm_name," ",'.$admin->info('name').'.adm_surname) as admin'
        );

        return $select;
    }
}
