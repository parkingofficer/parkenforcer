<?php

/**
 * @category  Models
 * @name PlateCheckLog
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2014-02-11, 16:10:32
 */
class Model_PlateCheckLog extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_plate_check_log';

    public function getPlateCheckLogMessages()
    {
        return [
            'Plate is in white list' => 0,
            'Paid' => 0,
            'Plate deleted from black list. Payment was found' => 0,
            'Time ended. Adding to black list' => 0,
            'Plate is in black list' => 0,
            'Fine is already created' => 0,
            'No plate number found in database. Adding to black list' => 0,
            'Zone work time ended' => 0,
            'Generating fine' => 0,
            //'' => null,
        ];
    }

    public function getAdminsPairs()
    {
        $select = $this->select()->setIntegrityCheck(false);
        $select->from(['p' => $this->_name], ['adm_device_id']);
        $select->distinct(true);
        $keys = $this->getAdapter()->fetchCol($select);
        $pairs = ['' => ''];
        $admin = new Model_Admins();
        if (! empty($keys)) {
            $select = $admin->select()->setIntegrityCheck(false);
            $select->from(['a' => $admin->info('name')], [
                'a.adm_device_id',
                'CONCAT(a.adm_name," ",a.adm_surname) as admin',
            ])->where("adm_device_id IN ('".implode("','", $keys)."')");
            $pairs = $this->getPairs($admin->fetchAll($select), 'adm_device_id', 'admin', true);
        }

        return $pairs;
    }

    /**
     * Get plate check select base.
     *
     * @return \Zend_Db_Select
     * @throws \Zend_Db_Table_Exception
     */
    public function getPlateCheckSelect()
    {
        $admin = new Model_Admins();

        $domain_model = new Model_Domain();
        $options = $domain_model->options();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        if ($options['apps_username'] == true) {
            $select->joinLeft(
                $admin->info('name'),
                $this->_name.'.admin_id = '.$admin->info('name').'.id',
                'CONCAT('.$admin->info('name').'.adm_name," ",'.$admin->info('name').'.adm_surname) as admin'
            );
        } else {
            $select->joinLeft(
                $admin->info('name'),
                $this->_name.'.adm_device_id = '.$admin->info('name').'.adm_device_id',
                'CONCAT('.$admin->info('name').'.adm_name," ",'.$admin->info('name').'.adm_surname) as admin'
            );
        }

        $zones_model = new Model_Zone();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name').'.id = '.$this->_name.'.zone',
            [$zones_model->info('name').'.zone_code', $zones_model->info('name').'.city_id']
        );

        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name').'.id = '.$zones_model->info('name').'.city_id',
            $city_model->info('name').'.CityName as city'
        );

        $user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
            $user_id->info('name').'.user_id'
        );

        return $select;
    }

    /* public function getPlateChecks($where = array(),$message = null){

         $select = $this->getPlateCheckSelect();

         $zones_model = new Model_Zone();
         $user_id = new Model_UserCities();
         $select->joinLeft(
             $user_id->info('name'),
             $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
             $user_id->info('name').'.user_id'
         );

         $select->order("time ASC");

         if(!empty($where)) foreach($where as $w) $select->where($w);

         $items = $this->fetchAll($select);
         $return = array();
         if($items->count()) foreach($items as $i){
             if($i['response']){
                 $item = array_merge($i->toArray(),(array)json_decode($i['response']));
                 //var_dump($item);

             }else{
                 $item = $i->toArray();
             }

             $item['success'] = $item['success'] ? 'TRUE' : 'FALSE';

             if(!$message){
                 $return['items'][] = $item;
             }elseif($message && isset($item['msg']) && $item['msg'] == $message){
                 $return['items'][] = $item;
             }
         }
         //var_dump($return);
         //die();

         return $return;

     }*/
    public function getPlateChecks($where = [], $message = null)
    {
        $adm_model = new Model_Admins();
        $select = $this
            ->select()
            ->from(['pc' => $this->_name])
            ->setIntegrityCheck(false);

        $domain_model = new Model_Domain();
        $options = $domain_model->options();
        if ($options['apps_username'] == true) {
            $select->joinLeft($adm_model->info('name').' as adm', 'adm.adm_device_id = pc.adm_device_id',
                ['adm.*', 'adm.id as adm_id', 'CONCAT(adm.adm_name, " ", adm.adm_surname) as admin']);
        } else {
            $select->joinLeft($adm_model->info('name').' as adm', 'adm.adm_device_id = pc.adm_device_id',
                ['adm.*', 'adm.id as adm_id', 'CONCAT(adm.adm_name, " ", adm.adm_surname) as admin']);
        }
        $zones_model = new Model_Zone();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name').'.id = pc.zone',
            $zones_model->info('name').'.zone_code'
        );

        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name').'.id = '.$zones_model->info('name').'.city_id',
            $city_model->info('name').'.CityName as city'
        );
        $select->order('pc.time ASC');

        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        $items = $this->fetchAll($select);
        $return = [];
        if ($items->count()) {
            foreach ($items as $i) {
                if ($i['response']) {
                    $item = array_merge($i->toArray(), (array) json_decode($i['response']));
                    //var_dump($item);
                } else {
                    $item = $i->toArray();
                }

                $item['success'] = $item['success'] ? 'TRUE' : 'FALSE';

                if (! $message) {
                    $return['items'][] = $item;
                } elseif ($message && isset($item['msg']) && $item['msg'] == $message) {
                    $return['items'][] = $item;
                }
            }
        }
        //var_dump($return);
        //die();

        return $return;
    }

    public function getPlateChecksCsv($where)
    {
        $admin = new Model_Admins();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $domain_model = new Model_Domain();
        $options = $domain_model->options();
        if ($options['apps_username'] == true) {
            $select->joinLeft(
                $admin->info('name'),
                $admin->info('name').'.id = '.$this->_name.'.admin_id',
                'CONCAT('.$admin->info('name').'.adm_name," ",'.$admin->info('name').'.adm_surname) as admin'
            );
        } else {
            $select->joinLeft(
                $admin->info('name'),
                $admin->info('name').'.adm_device_id = '.$this->_name.'.adm_device_id',
                'CONCAT('.$admin->info('name').'.adm_name," ",'.$admin->info('name').'.adm_surname) as admin'
            );
        }
        $zones_model = new Model_Zone();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name').'.id = '.$this->_name.'.zone',
            [$zones_model->info('name').'.zone_code', $zones_model->info('name').'.city_id']
        );

        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name').'.id = '.$zones_model->info('name').'.city_id',
            $city_model->info('name').'.CityName as city'
        );
        /*
                    $user_id = new Model_UserCities();
                    $select->joinLeft(
                        $user_id->info('name'),
                        $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
                        $user_id->info('name').'.user_id'
                    );
        */
        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchall($select);
    }

    public function getPlateChecks2($where = [], $message = null)
    {
        $select = $this->getPlateChecksCsv($where);

        $zones_model = new Model_Zone();
        $user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
            $user_id->info('name').'.user_id'
        );

        $select->order('time ASC');

        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        $items = $this->fetchAll($select);
        $return = [];
        if ($items->count()) {
            foreach ($items as $i) {
                if ($i['response']) {
                    $item = array_merge($i->toArray(), (array) json_decode($i['response']));
                    //var_dump($item);
                } else {
                    $item = $i->toArray();
                }

                $item['success'] = $item['success'] ? 'TRUE' : 'FALSE';

                if (! $message) {
                    $return['items'][] = $item;
                } elseif ($message && isset($item['msg']) && $item['msg'] == $message) {
                    $return['items'][] = $item;
                }
            }
        }
        //var_dump($return);
        // die();

        return $this->fetchall($return);
    }
}
