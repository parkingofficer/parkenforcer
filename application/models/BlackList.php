<?php

/**
 * @category  Models
 * @name BlackList
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-11-27, 10:36:07
 */
class Model_BlackList extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_black_list';

    public function getBlackList($show_deleted = false, $where = [])
    {
        $blc_model = new Model_BlackListCauses();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $blc_model->info('name'),
            $blc_model->info('name').'.id = '.$this->_name.'.cause_id',
            $blc_model->info('name').'.title as cause'
        );

        $zones_model = new Model_Zone();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name').'.id = '.$this->_name.'.zone',
            [$zones_model->info('name').'.zone_code', $zones_model->info('name').'.tolerance_time']
        );

        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name').'.id = '.$zones_model->info('name').'.city_id',
            $city_model->info('name').'.CityName as city'
        );

        if (! $show_deleted) {
            $select->where($this->_name.'.delete = ?', 0);
        }

        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        $select->order('time_checked DESC');
        $plates = $this->fetchall($select);
        $black_list = [];
        foreach ($plates as $plate) {
            $time = $plate['time_checked'];
            $t_time = $plate['tolerance_time'];
            $tolerance_time_end = date('Y-m-d H:i:s', strtotime("$time + $t_time mins"));
            $current = date('Y-m-d H:i:s', strtotime('-1 min'));
            //$diff = date("Y-m-d H:i:s",$current) - date("Y-m-d H:i:s",$tolerance_time_end);
            $date_a = new DateTime(".$tolerance_time_end.");
            $date_b = new DateTime(".$current.");
            if ($date_b <= $date_a) {
                $interval_diff = date_diff($date_b, $date_a);

                $interval = $interval_diff->days * 24 * 60;
                $interval += $interval_diff->h * 60;
                $interval += $interval_diff->i;
            } else {
                $interval = 0;
            }
            // $interval = date_diff($date_b,$date_a);

            //echo $interval->format('%h:%i:%s');
            $black_list [] = [
                'id' => $plate['id'],
                'cause_id' => $plate['cause_id'],
                'plate_no' => $plate['plate_no'],
                'time_checked' => $plate['time_checked'],
                'zone' => $plate['zone'],
                'tolerance_time_end' => $interval,
                'lat' => $plate['lat'],
                'lon' => $plate['lon'],
            ];
        }

        return $black_list;
    }

    public function getBlackListIndex()
    {
        $blc_model = new Model_BlackListCauses();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $blc_model->info('name'),
            $blc_model->info('name').'.id = '.$this->_name.'.cause_id',
            $blc_model->info('name').'.title as cause'
        );

        $zones_model = new Model_Zone();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name').'.id = '.$this->_name.'.zone',
            [$zones_model->info('name').'.zone_code', $zones_model->info('name').'.city_id']
        );

        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name').'.id = '.$zones_model->info('name').'.city_id',
            $city_model->info('name').'.CityName as city'
        );

        $user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
            $user_id->info('name').'.user_id'
        );

        return $select;
    }

    public function getBlackListCsv($where)
    {
        $blc_model = new Model_BlackListCauses();

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $blc_model->info('name'),
            $blc_model->info('name').'.id = '.$this->_name.'.cause_id',
            $blc_model->info('name').'.title as cause'
        );

        $zones_model = new Model_Zone();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name').'.id = '.$this->_name.'.zone',
            [$zones_model->info('name').'.zone_code', $zones_model->info('name').'.city_id']
        );

        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name').'.id = '.$zones_model->info('name').'.city_id',
            $city_model->info('name').'.CityName as city'
        );

        /*$user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
            $user_id->info('name').'.user_id'
        );*/
        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchall($select);
    }
}
