<?php

class Model_FineLog extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_fines_log';

    public static function event($fine_id, $message , $authenticatedAdmin = "")
    {
        $user = ($authenticatedAdmin) ? $authenticatedAdmin : user()->adm_name. ' '. user()->adm_surname;

        (new static)->save([
            'fine_id' => $fine_id,
            'who' => $user,
            'message' => $message
        ]);
    }
}
