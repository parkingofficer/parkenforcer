<?php

/**
 * Porche orders.
 *
 * @author Jarek Skuder
 */

use \SendGrid\Mail;

class Model_Porche extends Core_Db_Table
{
    protected $_name = 'box_porche';
    protected $_primary = 'id';
    protected $baseUrl = 'http://sierra.unipark.lt/api/infomessages';
    protected $key = '73FA240A-9ED1-3348-CAB6-D8CFBA9DD88D';
    protected $Bearer = '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';

    public function getlist()
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        return $select;
    }

    public function getMaxid()
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name, [new Zend_Db_Expr('max(id) as maxId')]);

        return $this->getAdapter()->fetchAll($select);
    }

    public function getOrderType($id)
    {
        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->where($this->_name.'.id ='.$id);

        return $this->getAdapter()->fetchRow($select);
    }

    public function getPorche($id = null)
    {
        $admin = new Model_Admins();

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $admin->info('name'),
            $this->_name.'.admin_id = '.$admin->info('name').'.id',
            'CONCAT('.$admin->info('name').'.adm_name," ",'.$admin->info('name').'.adm_surname) as admin'
        );

        if ($id) {
            $select->where($this->_name.'.id = ?', $id);

            return $this->getAdapter()->fetchRow($select);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getPorcheExport($where)
    {
        $blic_status = new Model_PorcheStatuses();

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $blic_status->info('name'),
            $blic_status->info('name').'.id = '.$this->_name.'.status',
            $blic_status->info('name').'.name as status_name'
        );
        $select->columns([
            new Zend_Db_Expr('CONCAT('.$this->_name.'.name," ",'.$this->_name.'.surname) as name_surname'),
        ]);

        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchAll($select);
    }

    public function sendSMS ( $porscheOrderId, $serviceType, $serviceId = null, $orderStatusId = null )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)->where('id =?', $porscheOrderId);
        $porscheData = $this->fetchRow( $select )->toArray();
        $phone = $porscheData['phone'];

        if ( !$phone ) return false;

        if (substr($phone, 0, 1) === '+') {
            $phone = substr($phone, 1);
        }

        $translate = new Model_Translate();
        $locale = ( $porscheData['language'] && $porscheData['language'] == 'lt' ) ? 'lt' : 'en';
        $PSName = ( $serviceId ) ? 'PSName_'.$serviceId : 'POName_'.$orderStatusId;
        $serviceName = $translate->getTranslateByLocale( $locale, $PSName );

        $data = array(
            "msisdn" => $phone,
            "plate" => $porscheData['plate'],
            "type" => $serviceType,
            'serviceName' => $serviceName,
            "project" => 'porche',
            "locale" => $locale
        );
        $data_string = json_encode( $data );

        try {
            //open connection
            $ch = curl_init();

            // Check if initialization had gone wrong*
            if ($ch === false) {
                throw new Exception('failed to initialize');
            }

            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $this->baseUrl);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'X-AUTH-TOKEN: '.$this->key.''));

            $content = curl_exec($ch);

            // Check the return value of curl_exec(), too
            if ($content === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            // Close curl handle
            curl_close($ch);
        } catch(Exception $e) {
            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);
            return false;
        }
        return true;
    }

    public function sendMail ( $porscheOrderId, $serviceType, $serviceId = null, $orderStatusId = null, $paymentType = null ) {

        if ( isset( $serviceType->sendBilling ) && $serviceType->sendBilling == 0 && $paymentType == 3 ) return false;

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)->where('id =?', $porscheOrderId);
        $porscheData = $this->fetchRow( $select )->toArray();
        $clientEmail = ( isset( $serviceType->to->mail ) && $serviceType->to->mail ) ? $serviceType->to->mail : $porscheData['email'];

        $PorscheServicesModel = new Model_PorcheServices();

        $translate = new Model_Translate();
        $locale = ( $porscheData['language'] && $porscheData['language'] == 'lt' ) ? 'lt' : 'en';
        $PSName = ( $serviceId ) ? 'PSName_'.$serviceId : 'POName_'.$orderStatusId;
        $serviceName = $translate->getTranslateByLocale( $locale, $PSName );

        $SubstitutionName = ( $porscheData['name'] ) ? $porscheData['name'] : "Unipark";
        $SubstitutionEmail = ( $porscheData['email'] ) ? $porscheData['email'] : "info@unipark.lt";
        $SubstitutionPhone = ( $porscheData['phone'] ) ? $porscheData['phone'] : "+37070077877";

        $subjectText = ( ( isset( $serviceType->to->mail ) && $serviceType->to->mail ) &&
            ( isset( $serviceType->to->subject ) && $serviceType->to->subject )
        ) ? "uniPark PORSCHE ".$porscheData['plate']." naujas užsakymas, grįžimas - ".date('Y-m-d H:i:s', strtotime($porscheData['date_to'])) :
            "uniPark PORSCHE ".$porscheData['plate']." paruoštas, grįžimas - ".date('Y-m-d H:i:s', strtotime($porscheData['date_to']));
        $subject = ( isset( $serviceType->to->mail ) && $serviceType->to->mail ) ? $subjectText : "Sending with SendGrid is Fun";

        $email = new Mail\Mail();
        $email->setFrom( $serviceType->from->email, $serviceType->from->name );
        $email->setSubject( $subject );
        $email->addTo( $clientEmail );

        $email->addContent("text/plain", "And easy to do anywhere, even with PHP");
        $email->addContent(
            "text/html", "And easy to do anywhere, even with PHP"
        );

        $email->addSubstitution("-time_created-", date('Y-m-d', strtotime($porscheData['inserted'])));
        $email->addSubstitution("-people-", $SubstitutionName);
        $email->addSubstitution("-name-", $SubstitutionName);
        $email->addSubstitution("-plate-", $porscheData['plate']);
        $email->addSubstitution("-emailas-", $SubstitutionEmail);
        $email->addSubstitution("-phone-", $SubstitutionPhone);
        $email->addSubstitution("-services-", $serviceName);
        $email->addSubstitution("-servicesList-", $PorscheServicesModel->getServicesNames( self::getServicesIdToSendMail( $porscheData['services'] ) ) );

        $email->setTemplateId( $serviceType->templateId->$locale );

        $sendgrid = new \SendGrid( $this->Bearer );

        try {
            $sendgrid->send($email);
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
            return false;
        }
        return true;
    }

    public function getDataStatusesToBilling ( $orderId, $postStatusId = null )
    {
        $result = [];
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)->where('id =?', $orderId);
        $row = $this->fetchRow( $select );
        if ( $row )
        {
            $row = $row->toArray();
            $dbPS = new Model_PorcheStatuses();

            $statusId = ( $postStatusId ) ? $postStatusId : $row['status'];

            $result = [
                "OrderId" => $row['id'],
                "OrderStatus"=> $dbPS->getServiceBillingId( $statusId )
            ];
        }

        return $result;
    }

    public function getServicesIdToSendMail ( $json ) {
        $serviceId = "";
        $serviceArray = json_decode( $json, true);

        $i = 1;
        foreach ( $serviceArray as $items )
        {
            $separator = ( count ( $serviceArray ) == $i ) ? "" : ", ";
            $serviceId .= $items['service_id'].$separator;
            $i++;
        }
        return $serviceId;
    }
}
