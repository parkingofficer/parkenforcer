<?php

/**
 * @category  Models
 * @name Translate
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-11-25, 10:22:27
 */
class Model_Translate extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_translate';

    public function getListByLocale($locale)
    {
        $cache = Zend_Registry::get('cache');
        $cacheId = "translations_$locale";
        if (! ($data = $cache->load($cacheId))) {
            $data = $this->getListByLocaleNoCache($locale);
            $cache->save($data);
        }

        return $data;
    }

    public function getListByLocaleNoCache($locale)
    {
        $data = $this->fetchAll("locale = '$locale' && is_translated = 1");
        if ($data->count()) {
            $data = $data->toArray();
        } else {
            $data = [];
        }

        return $data;
    }

    public function getTranslateByLocale ( $locale, $msgid )
    {
        $data = $this->fetchAll("locale = '$locale' && is_translated = 1 && msgid = '$msgid'");
        if ($data->count()) {
            $data = $data->toArray();
        } else {
            $data = [];
        }

        return $data[0]['msgstring'];
    }
}
