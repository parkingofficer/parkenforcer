<?php

/**
 * @category  Models
 * @name Fines
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-12-03, 13:31:03
 */
class Model_Fines extends Core_Db_Table
{
    use \App\Models\Traits\InteractsWithFineStatus;

    const STATUS_NOTICE = 1;

    const STATUS_FINE_PAYED = 2;

    const STATUS_WAITING_FOR_FINE_GENERATION = 3;

    const STATUS_PERSON_INFORMED = 4;

    const STATUS_FINE_GENERATED = 5;

    const STATUS_PERSON_NOTICED = 6;

    const STATUS_FINE_DISMISSED = 7;

    const STATUS_TIME_ELAPSED = 8;

    const STATUS_SENT = 9;

    const STATUS_NOT_PUBLISHED = 10;

    const STATUS_PUBLISHED = 11;

    const STATUS_ENFORCEMENT = 12;

    /**
     * @var string
     */
    protected $_primary = 'id';

    /**
     * @var string
     */
    protected $_name = 'box_fines';

    /**
     * @var integer
     */
    private static $offset = 0; // possibility 7-3-1 method to set offset

    /**
     * @param null $id
     * @param array $where
     *
     * @return array|mixed
     */
    public function getFine($id = null, $where = [])
    {
        $select = \App\Models\Filters\FineFilters::baseBuilder();

        $user_cities = \Model_UserCities::table();
        $select->joinLeft(
            $user_cities,
            \Model_Zone::table().'.city_id = '.$user_cities.'.city_id ',
            $user_cities.'.user_id'
        );

        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        if ($id) {
            $select->where($this->_name.'.id = ?', $id);

            return $this->getAdapter()->fetchRow($select);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    /**
     * @param $where
     *
     * @return \Zend_Db_Table_Rowset_Abstract
     */
    public function getFines($where)
    {
        $select = \App\Models\Filters\FineFilters::baseBuilder();

        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchAll($select);
    }

    /**
     * @param $fine_id
     * @param $car
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function changeCar($fine_id, $car)
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found');
        }
        $old_value = $fine->car;
        $fine->car = $car;
        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf('Fine car set to: <strong>"%s"</strong>, was <em>"%s"</em>', $car, $old_value)
        );
        $this->sendUpdateToBilling($fine);

        return $fine;
    }

    /**
     * @param $fine_id
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function markAsGenerated($fine_id)
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found');
        }

        $old_status = self::statuses($fine->status_id);

        $fine->status_id = static::STATUS_FINE_GENERATED;
        $fine->notification_sent_time = date('Y-m-d H:i:s');
        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf('Fine status set to: <strong>"%s"</strong>, was <em>"%s"</em>', self::statuses(static::STATUS_FINE_GENERATED), $old_status)
        );
        $this->sendUpdateToBilling($fine);

        return $fine;
    }

    /**
     * @param $fine_id
     * @param $amount
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function changeAmount($fine_id, $amount)
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found');
        }

        $old_value = $fine->amount;

        $fine->amount = $amount;
        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf('Fine amount set to: <strong>"%s" EUR</strong>, was <em>"%s" EUR</em>', $amount, $old_value)
        );
        $this->sendUpdateToBilling($fine);

        return $fine;
    }

    /**
     * @param $fine_id
     * @param $rate
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function changeRate($fine_id, $rate)
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found');
        }

        $old_value = $fine->rate;

        $fine->rate = $rate;
        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf('Fine rate set to: <strong>"%s"</strong>, was <em>"%s"</em>', $rate, $old_value)
        );
        $this->sendUpdateToBilling($fine);

        return $fine;
    }

    public function changeStatus($fine_id, $status)
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found');
        }

        $old_value = $fine->status_id;

        $fine->status_id = $status;
        $fine->change_status_date = date('Y-m-d H:i:s');
        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf(
                'Fine status set to: <strong>"%s"</strong>, was <em>"%s"</em>',
                self::statuses($status),
                self::statuses($old_value)
            )
        );
        $this->sendUpdateToBilling($fine);

        return $fine;
    }

    /**
     * @param $fine
     *
     * @return bool
     */
    public static function allowDismiss($fine)
    {
        return in_array($fine['status_id'], [
            static::STATUS_NOTICE,
            static::STATUS_WAITING_FOR_FINE_GENERATION,
            static::STATUS_PERSON_INFORMED,
            static::STATUS_FINE_GENERATED,
            static::STATUS_PERSON_NOTICED,
        ]);
    }

    /**
     * @param $fine_id
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function dismiss($fine_id)
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found');
        }

        $old_status = self::statuses($fine->status_id);

        $fine->status_id = self::STATUS_FINE_DISMISSED;
        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf(
                'Fine status set to: <strong>"%s"</strong>, was <em>"%s"</em>',
                self::statuses(static::STATUS_FINE_DISMISSED), $old_status
            )
        );
        $this->sendUpdateToBilling($fine);

        return $fine;
    }

    /**
     * @param $archive_no
     * @param $fine_id
     * @param $plate
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     * @throws \Zend_Exception
     */
    public function associatePayment($archive_no, $fine_id, $plate = null)
    {
        $fine = $this->find($fine_id)->current();
        $fine_payment = (new Model_FinesPayments)->find($archive_no)->current();

        if (!$fine || !$fine_payment) {
            throw new \Exception('Fine not found');
        }

        if ($fine_payment && $fine) {
            if ($plate && $fine->plate == $plate) {
                $fine_payment->plate = $plate;
            }
            $fine_payment->fine_id = $fine->id;
            $fine_payment->save();

            $old_paid = $fine->paid;
            $old_status = self::statuses($fine->status_id);

            $fine->paid = $fine->paid + ($fine_payment->amount_cnt / 100);
            $fine->bank_payment_no = $archive_no;

            if ($fine->paid >= static::finalFineAmount($fine->amount, $fine->rate)) {
                $fine->status_id = Model_Fines::STATUS_FINE_PAYED;

                Model_FineLog::event(
                    $fine_id,
                    sprintf('Fine status set to: <strong>"%s"</strong>, was <em>"%s"</em>', self::statuses(static::STATUS_FINE_PAYED), $old_status)
                );
            }

            $fine->save();

            Model_FineLog::event(
                $fine_id,
                sprintf(
                    'Payment <strong>"%s"</strong> of <em>%s EUR</em>, was associate with fine. Total fine paid value was <em>%s EUR</em>, now: <em>%s EUR</em>',
                    $fine_payment->archive_no, ($fine_payment->amount_cnt / 100), $old_paid, $fine->paid
                )
            );
            $this->sendUpdateToBilling($fine);
            return $fine;

        } else {
            throw new Zend_Exception('No data found');
        }
    }

    /**
     * @param $fine_id
     * @param $date
     * @param $amount
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function addManualPayment($fine_id, $date, $amount , $authenticatedAdmin = "")
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found');
        }

        $payment = (new Model_FinesPayments())->save([
            'archive_no' => 'MAN_'.date('Ymd', strtotime($date)).str_random(10),
            'fine_id' => $fine->id,
            'plate' => $fine->plate,
            'time' => $date,
            'amount_cnt' => $amount * 100,
            'payment_type' => 'M',
            'currency' => 'EUR',
            'client_code' => 'Manual',
            'payment_text' => 'Manual',
            'payer_bank_code' => 'Manual',
            'payer_bank' => 'Manual',
            'payer_account' => 'Manual',
            'payer_name' => 'Manual',
            'payer_code' => '1',
            'parent_archive_no' => null
        ]);

        $old_paid = $fine->paid;
        $fine->paid = $fine->paid + $amount;

        $log_status_changed = false;
        $old_status = null;
        if ($fine->paid >= self::finalFineAmount($fine->amount, $fine->rate)) {
            $log_status_changed = true;
            $old_status = self::statuses($fine->status_id);
            $fine->status_id = self::STATUS_FINE_PAYED;
        }

        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf(
                'Manual payment <strong>"%s"</strong> of <em>%s EUR</em>, was added. Total fine paid value was <em>%s EUR</em>, now: <em>%s EUR</em>',
                $payment->archive_no, $amount, $old_paid, $fine->paid
            ),
            $authenticatedAdmin
        );

        if ($log_status_changed) {
            Model_FineLog::event(
                $fine_id,
                sprintf('Fine status set to: <strong>"%s"</strong>, was <em>"%s"</em>', self::statuses(static::STATUS_FINE_PAYED), $old_status),
                $authenticatedAdmin
            );
        }
        $this->sendUpdateToBilling($fine);
        return $payment;
    }

    /**
     * @param $email
     * @param $fines
     * @param string $doc_type
     *
     * @throws \Zend_Exception
     */
    public function setdFinesNotifications($email, $fines, $doc_type = 'pdf')
    {
        $db = Zend_Registry::get('db')->getConfig();
        $db['type'] = 'pdo_mysql';
        $options = [
            'name' => 'finesNotifications',
            'driverOptions' => $db,
        ];
        // Create an DB queue
        $queue = new Zend_Queue('Db', $options);

        foreach ($fines as $fine_id) {
            $this->getAdapter()->beginTransaction();
            try {
                $fine = $this->getFine($fine_id);
                $city = new Model_City();
                $zone = new Model_Zone();
                $zone = $zone->fetchrow('id ='.$fine['zone']);
                $city = $city->fetchrow('id = '.$zone['city_id']);
                //$select2 = $uc->select()->from(array('z' => $uc->info('name')), array('z.city_id'))->where("active = 1 and user_id = ?", $id);
                $data = [
                    'to' => $email,
                    'subject' => $fine['plate'].' UniPark. Fiksuoto administracinio teisės pažeidimo pranešimas '.date('Y-m-d'),
                    'fine' => $fine,
                    'doc_type' => $doc_type,
                    'body' => 'Vadovaujantis Vietinės rinkliavos už naudojimąsi '.$city['FineCityName'].' miesto  savivaldybės tarybos nustatytomis vietomis automobiliams statyti nuostatų, patvirtintų '.$city['FineCityName'].' miesto savivaldybės tarybos, fiksuotas nuostatuose numatytos vietinės rinkliavos mokėjimo tvarkos pažeidimas. Pažeidimą fiksavo ir pažeidimo faktą liudija '.$city['company'].' darbuotojas '.$fine['admin'],
                ];

                $queue->send(serialize($data));

                $this->update([
                    'status_id' => self::STATUS_FINE_GENERATED,
                    'notification_sent_time' => date('Y-m-d H:i:s'),
                    'notification_sent_to' => $email,
                ], 'id = '.$fine['id']);

                $this->getAdapter()->commit();

                Model_FineLog::event(
                    $fine_id,
                    sprintf(
                        'Notification was send to <strong>"%s"</strong>',
                        $email
                    )
                );

            } catch (Exception $e) {
                $this->getAdapter()->rollBack();
            }
        }
    }

    /**
     * @param $fines
     *
     * @return string
     */
    public function setdFinesNotificationsMunicipality($fines)
    {
        foreach ($fines as $fine_id) {
            try {
                $fine = $this->getFine($fine_id);
                $city = new Model_City();
                $zone = new Model_Zone();
                $admin = new Model_Admins();
                $zone = $zone->fetchrow('id ='.$fine['zone']);
                $city = $city->fetchrow('id = '.$zone['city_id']);
                $admin = $admin->fetchrow('id = '.$fine['admin_id']);
                //$select2 = $uc->select()->from(array('z' => $uc->info('name')), array('z.city_id'))->where("active = 1 and user_id = ?", $id);

                if ($fine['causes_id'] == 1) {
                    $subtype = 1135;
                } else {
                    $subtype = 1136;
                }
                $address = explode('. ', $fine['street']);
                $data = [
                    'date' => $fine['date'],
                    'violation_type_id' => '2044',
                    'violationSubTypeId' => $subtype,
                    'savivaldybe' => $city['FineCityName'],
                    'miestas' => $city['CityName'],
                    'gatve' => $address[0].'.',
                    'namas' => $address[1],
                    'plate_number' => $fine['plate'],
                    'ParkingZoneId' => $zone['municipality_id'],
                    'Comment' => $admin['adm_name'].' '.$admin['adm_surname'],
                    'location' => $fine['gps'],
                ];
                $url = 'http://ext.siauliai.lt:4080/DVPS_API/api/Violation/';
                $context = stream_context_create([
                    'http' => [
                        'method' => 'POST',
                        'header' => ["Content-Type: application/json\r\nAccept: application/json\r\nAuthorization: Basic c3RvdmE6c3RvdmExMjM=\r\n"],
                        'follow_location' => true,
                        'content' => json_encode($data),
                    ],
                ]);
                $fp = fopen($url.'Post', 'r', null, $context);
                $result = stream_get_contents($fp);
                fclose($fp);
                $response = json_decode($result);
                $violationId = intval($response->ViolationId);
                if ($violationId) {
                    $imgPath = 'http://'.$_SERVER['HTTP_HOST'].'/files/fines/'.$fine['id'].'/';
                    if (! empty($fine['first_photo'])) {
                        $image = $imgPath.$fine['first_photo'];
                        $this->sendImages($fine, $url, $violationId, $image);
                    }
                    if (! empty($fine['plate_photo'])) {
                        $image = $imgPath.$fine['plate_photo'];
                        $this->sendImages($fine, $url, $violationId, $image);
                    }
                    if (! empty($fine['front_photo'])) {
                        $image = $imgPath.$fine['front_photo'];
                        $this->sendImages($fine, $url, $violationId, $image);
                    }
                    if (! empty($fine['back_photo'])) {
                        $image = $imgPath.$fine['back_photo'];
                        $this->sendImages($fine, $url, $violationId, $image);
                    }
                    if (! empty($fine['left_side_photo'])) {
                        $image = $imgPath.$fine['left_side_photo'];
                        $this->sendImages($fine, $url, $violationId, $image);
                    }
                    if (! empty($fine['right_side_photo'])) {
                        $image = $imgPath.$fine['right_side_photo'];
                        $this->sendImages($fine, $url, $violationId, $image);
                    }

                    $this->update([
                        'municipality_id' => $violationId,
                        'status_id' => self::STATUS_FINE_GENERATED,
                        'notification_sent_time' => date('Y-m-d H:i:s'),
                        'notification_sent_to' => 'municipality',
                    ], 'id = '.$fine['id']);

                    Model_FineLog::event(
                        $fine_id,
                        sprintf(
                            'Fine was reported to Municipality'
                        )
                    );
                }
            } catch (Exception $e) {
                die(print_r($e));
            }
        }

        return 'sent';
    }

    /**
     * @param $fine_id
     * @param null $fine
     *
     * @return \PhpOffice\PhpWord\TemplateProcessor
     */
    public function getFineDoc($fine_id, $fine = null)
    {
        if (! $fine) {
            $service = new self();
            $fine = $service->getFine($fine_id);
        }
        $zones = new self();
        $city = new Model_Zone();
        $zone = $zones->fetchRow($zones->select()->where('id = ?', $fine_id));
        $citys = $city->fetchRow($city->select()->where('id='.$zone['zone']));

        $document = new \PhpOffice\PhpWord\TemplateProcessor(APPLICATION_PATH.'/views/forms-tpl/'.$citys['city_id'].'.docx');

        $document->setValue('fine_date', $fine['date']);
        $document->setValue('fine_street', $fine['street']);
        $document->setValue('plate', $fine['plate']);
        $document->setValue('car', $fine['car']);
        $document->setValue('admin', $fine['admin']);
        $document->setValue('fine_d', date('Y-m-d', strtotime($fine['date'])));
        $document->setValue('fine_time', date('H:i', strtotime($fine['date'])));

        $nesumokejo = 'Vietinės rinkliavos nesumokėjimas';
        $baigesi = 'Pasibaigęs apmokėtas automobilio stovėjimo laikas';

        $p_37_1 = '37.1. nesumokėta rinkliava už transporto priemonės stovėjimą mokamoje automobilių stovėjimo vietoje;';
        $p_37_2 = '37.2. pasibaigęs apmokėtas stovėjimo laikas;';

        if ($fine['causes_id'] == 2) {
            $document->setValue('p_37_1', "<w:r><w:rPr><w:u w:val=\"single\"/></w:rPr><w:t>$p_37_1</w:t></w:r>");
            $document->setValue('p_37_2', $p_37_2);
            $document->setValue('nesumokejo',
                "<w:r><w:rPr><w:u w:val=\"single\"/></w:rPr><w:t>$nesumokejo</w:t></w:r>");
            $document->setValue('baigesi', $baigesi);
        }
        if ($fine['causes_id'] == 1) {
            $document->setValue('p_37_1', $p_37_1);
            $document->setValue('p_37_2', "<w:r><w:rPr><w:u w:val=\"single\"/></w:rPr><w:t>$p_37_2</w:t></w:r>");
            $document->setValue('nesumokejo', $nesumokejo);
            $document->setValue('baigesi', "<w:r><w:rPr><w:u w:val=\"single\"/></w:rPr><w:t>$baigesi</w:t></w:r>");
        }

        //File 1
        $img = realpath(FINES_PHOTOS_PATH.'/'.$fine['id'].'/'.$fine['first_photo']);
        if ($fine['first_photo'] && file_exists($img)) {
            $document->zipClass->deleteName('word/media/image1.jpg');
            $document->zipClass->addFile($img, 'word/media/image1.jpg');
        }

        //File 2
        $img = realpath(FINES_PHOTOS_PATH.'/'.$fine['id'].'/'.$fine['plate_photo']);
        if ($fine['plate_photo'] && file_exists($img)) {
            $document->zipClass->deleteName('word/media/image2.jpg');
            $document->zipClass->addFile($img, 'word/media/image2.jpg');
        }

        //File 3
        $img = realpath(FINES_PHOTOS_PATH.'/'.$fine['id'].'/'.$fine['front_photo']);
        if ($fine['front_photo'] && file_exists($img)) {
            $document->zipClass->deleteName('word/media/image3.jpg');
            $document->zipClass->addFile($img, 'word/media/image3.jpg');
        }

        //File 4
        $img = realpath(FINES_PHOTOS_PATH.'/'.$fine['id'].'/'.$fine['back_photo']);
        if ($fine['back_photo'] && file_exists($img)) {
            $document->zipClass->deleteName('word/media/image4.jpg');
            $document->zipClass->addFile($img, 'word/media/image4.jpg');
        }

        //File 5
        $img = realpath(FINES_PHOTOS_PATH.'/'.$fine['id'].'/'.$fine['left_side_photo']);
        if ($fine['left_side_photo'] && file_exists($img)) {
            $document->zipClass->deleteName('word/media/image5.jpg');
            $document->zipClass->addFile($img, 'word/media/image5.jpg');
        }

        //File 6
        $img = realpath(FINES_PHOTOS_PATH.'/'.$fine['id'].'/'.$fine['right_side_photo']);
        if ($fine['right_side_photo'] && file_exists($img)) {
            $document->zipClass->deleteName('word/media/image6.jpg');
            $document->zipClass->addFile($img, 'word/media/image6.jpg');
        }

        return $document;
    }

    /**
     * @return array
     */
    public function getZonesListAction()
    {
        $zwlg_model = new Model_Zone();
        $zones = $this->fetchAll();
        $items = [];

        if ($zones->count()) {
            foreach ($zones as $z) {
                $wlgroup = $z->toArray();

                //Get Whitelists
                $whitelistgroups = $zwlg_model->fetchAll('id = '.$z->zone);
                if ($whitelistgroups->count()) {
                    $wlgroup['zones'] = implode('<br/>', array_map(function ($z) {
                        return $z['zone_code'];
                    },
                        $whitelistgroups->toArray()));
                }
                // var_dump($whitelistgroups);

                $items[] = $wlgroup;
            }
        }

        return $items;
    }

    /**
     * @param $id
     * @param array $array
     *
     * @return bool
     */
    public function setNewFinesStatus($id, $array = [])
    {
        $this->getAdapter()->beginTransaction();
        try {
            $this->update($array, 'id = '.$id);
            $this->getAdapter()->commit();
        } catch (Exception $e) {
            $this->getAdapter()->rollBack();

            return false;
        }
    }

    /**
     * @param $fine
     * @param $url
     * @param $violationId
     * @param $image
     *
     * @return bool|string
     */
    private function sendImages($fine, $url, $violationId, $image)
    {
        $fileHandle = fopen($image, 'rb');
        $fileContents = stream_get_contents($fileHandle);
        fclose($fileHandle);
        $params = [
            'http' => [
                'method' => 'POST',
                'header' => "Content-Type: multipart/form-data\r\nAuthorization: Basic c3RvdmE6c3RvdmExMjM=\r\n",
                'content' => $fileContents,
            ],
        ];

        $ctx = stream_context_create($params);
        $fp = fopen($url.'PostViolationImage?violationId='.$violationId, 'rb', false, $ctx);

        $response = stream_get_contents($fp);

        return $response;
    }

    /**
     * @param int $length
     * @param array $cityObject
     *
     * @return string
     */
    public static function uniqueFineNumber( $cityObject = array(), $length = 6)
    {
        if ( $cityObject['fineType'] == 1) {
            $countNumber = new \Model_Fines();

            $fineFrontNumber = date('Y');
            $fineBackNumber =$countNumber->countFineFromCity( $cityObject['id'] );
            $fineNumber = $fineFrontNumber.$fineBackNumber;
            $method731Number = self::method731( $fineNumber );
            $string = $fineNumber.$method731Number;
        } elseif ( $cityObject['fineType'] == 2 ) {
            $fines = new \Model_Fines();

            $select = $fines->select()->from('box_fines', array(new Zend_Db_Expr("MAX(id) AS maxID")));
            $result = $fines->fetchRow($select);
            $fineMaxId = $result['maxID'];

            $fineNumber = $fineMaxId + 100001;
            $method731Number = self::method731( $fineNumber );
            $string = $fineNumber.$method731Number;
        } else {
            $model = (new static());
            $stringRandom = substr(str_shuffle("123456789abcdefghijklmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ"), 0, $length);
            //$string = strtoupper(str_random($length));
            $string = strtoupper($stringRandom);
            $exists = $model->fetchRow(
                $model->select()->where('id_public = ?', $string)
            );

            if ($exists) {
                while ($exists) {
                    //$string = strtoupper(str_random($length));
                    $string = strtoupper($stringRandom);
                    $exists = $model->fetchRow(
                        $model->select()->where('id_public = ?', $string)
                    );
                }
            }
        }

        return $string;
    }

    /**
     * @param $fine
     * @param $rate
     *
     * @return string
     */
    public static function finalFineAmount($fine, $rate)
    {
        if ($rate == 0) {
            return $fine;
        }

        return number_format(
            $fine - $fine * ($rate / 100),
            2
        );
    }

    protected function sendUpdateToBilling($fine)
    {
        $domain_model = new Model_Domain();
        $options = $domain_model->options();
        if ($options['name'] == 'parking.unipark.lt' || $options['name'] == 'parkingee.unipark.lt' || $options['name'] == 'stageparking.unipark.lt') {
            $billingFinesQueue = new Model_BillingQueue();
            $billingFinesQueue->save([
                'type' => Model_BillingQueue::UPDATE_FINE,
                'system_id' => $fine['id'],
                'status' => 0,
            ]);
        }
    }

    /**
     * @param int $cityObjectId
     *
     * @return integer
     */

    public function countFineFromCity ( $cityObjectId )
    {
        $Model_Zone = new Model_Zone();
        $zoneObject = $Model_Zone->fetchAll('city_id = '.$cityObjectId);

        $zoneArray = [];
        foreach($zoneObject as $city)
        {
            $zoneArray [] = $city['id'];
        }

        $checkQuery = $this->select()->from($this->_name, array("count" => "COUNT(id)"))
            ->where( "date > ".date('Y')."-01-01" )
            ->where( "zone in ('".implode("','",$zoneArray). "')" );
        $countFine = $this->fetchRow($checkQuery);
        $count = str_pad(($countFine['count']+1),5,"0",STR_PAD_LEFT);

        return $count;
    }

    /**
     * @param int $docnum
     *
     * @return integer
     */

    public static function method731 ( $docnum )
    {

        $id = $docnum + self::$offset;
        $weights = [7, 3, 1];
        $sum = 0;
        foreach (array_reverse(str_split($id)) AS $index => $digit) {
            $weightIndex = $index % 3;
            $sum += $digit * $weights[$weightIndex];
        }
        $checkDigit = 9 - (($sum - 1) % 10);
        return $checkDigit;
    }


    public function getCityZoneCsv($where)
    {
        $domain = new Model_Domain();
        $cent = $domain->domainOptionsCents();
        $digits = $domain->domainOptionsDigits();
        $zones_model = new Model_Zone();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.id = ' . $this->_name . '.zone',
            [
                $zones_model->info('name') . '.zone_code',
                $zones_model->info('name') . '.city_id',
                $zones_model->info('name') . '.zone_name'
            ]
        );
        $select->columns([
            //new Zend_Db_Expr('CONCAT(UPPER(LEFT(operator_name, 1)), SUBSTRING(operator_name, 2)) as operator'),
            new Zend_Db_Expr('FORMAT((amount),' . $digits . ') as fine'),
            new Zend_Db_Expr('FORMAT((paid),' . $digits . ') as paid'),
        ]);
        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.city_id',
            $city_model->info('name') . '.CityName as city'
        );
        /*$user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
            $user_id->info('name').'.user_id'
        );*/

        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchAll($select);
    }


    /**
     * @param $fine_id
     * @param $date
     * @param $amount
     *
     * @return \Zend_Db_Table_Row_Abstract
     * @throws \Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function addXlsxImportPayment( $fine_id, $date, $amount, $fileName)
    {
        $fine = $this->find($fine_id)->current();

        if (!$fine) {
            throw new \Exception('Fine not found - '.$fine_id);
        }

        $dateOrg = new DateTime( $date );
        $payment = (new Model_FinesPayments())->save([
            'archive_no' => 'XLSX_'.$dateOrg->format('Ymd' ).str_random(10),
            'fine_id' => $fine->id,
            'plate' => $fine->plate,
            'time' => $dateOrg->format('Y-m-d H:i:s' ),
            'amount_cnt' => $amount * 100,
            'payment_type' => 'X',
            'currency' => 'EUR',
            'client_code' => 'XLSX Import',
            'payment_text' => 'XLSX Import',
            'payer_bank_code' => 'XLSX Import',
            'payer_bank' => 'XLSX Import',
            'payer_account' => 'XLSX Import',
            'payer_name' => 'XLSX Import',
            'payer_code' => '2',
            'parent_archive_no' => null,
            'document_no' => '',
            'operation_type' => Core_Locale::translate('operation_type_xlsx_paid'),
            'send_to_billing' => 0,
            'payment_code' => ''
        ]);

        $old_paid = $fine->paid;
        $fine->paid = $fine->paid + $amount;
        $fine->import_xlsx = date( 'Y-m-d H:i:s' );
        $fine->import_xlsx_file_name = $fileName;

        $log_status_changed = false;
        $old_status = null;
        if ($fine->paid >= self::finalFineAmount($fine->amount, $fine->rate)) {
            $log_status_changed = true;
            $old_status = self::statuses($fine->status_id);
            $fine->status_id = self::STATUS_FINE_PAYED;
        }

        $fine->save();

        Model_FineLog::event(
            $fine_id,
            sprintf(
                'Manual payment <strong>"%s"</strong> of <em>%s EUR</em>, was added. Total fine paid value was <em>%s EUR</em>, now: <em>%s EUR</em>',
                $payment->archive_no, $amount, $old_paid, $fine->paid
            )
        );

        if ($log_status_changed) {
            Model_FineLog::event(
                $fine_id,
                sprintf('Fine status set to: <strong>"%s"</strong>, was <em>"%s"</em>', self::statuses(static::STATUS_FINE_PAYED), $old_status)
            );
        }
        $this->sendUpdateToBilling($fine);
        return $payment;
    }

    public function changeFineStatus ( $status )
    {
        switch ( $status )
        {
            case self::STATUS_PUBLISHED :
                $tolerance_field = 'published_time';
                $change_status = self::STATUS_PERSON_INFORMED;
                break;
            case self::STATUS_PERSON_INFORMED :
                $tolerance_field = 'enforcement_time';
                $change_status = self::STATUS_ENFORCEMENT;
                break;
            default:
                $tolerance_field = '';
                $change_status = '';
                break;
        }

        if ( $tolerance_field && $change_status ) {
            $model = new Model_Fines();
            $z_model = new Model_Zone();
            $zones = $z_model->fetchAll();
            if ($zones->count()) {
                $result = [];
                foreach ($zones as $z) {
                    if ($z[$tolerance_field]) {
                        $select = $model->select();
                        $select->where('status_id = ?', $status);
                        $select->where(new Zend_Db_Expr('DATE_ADD(change_status_date,INTERVAL ' . $z[$tolerance_field] . " HOUR ) <= '" . date('Y-m-d H:i:s') . "'"));
                        $select->where('zone = ' . $z['id']);
                        $fines = $model->fetchAll($select);
                        if ($fines->count()) {
                            foreach ($fines as $f2) {
                                $f2->status_id = $change_status;
                                $f2->change_status_date = date('Y-m-d H:i:s');
                                $f2->save();
                                $result[] = $f2->id;

                                Model_FineLog::event(
                                    $f2->id,
                                    sprintf('Fine status set to: <strong>"%s"</strong>, was <em>"%s"</em>', self::statuses( $change_status ), self::statuses ( $status ))
                                );
                            }
                        }
                    }
                } return $result;
            } return false;
        } else return false;
    }

    public function changeArrayFineStatus ( $fine_array, $status )
    {
        foreach ( $fine_array as $fineId )
        {
            try {
            $this->changeStatus( $fineId, $status );
            } catch (\Exception $e) {
                continue;
            }
        }
    }
}
