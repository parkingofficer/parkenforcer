<?php

/**
 * @category  Models
 * @name City
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-11-25, 10:22:27
 */
class Model_City extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_city';

    /**
     * @return \Zend_Db_Table_Rowset_Abstract
     * @throws \Zend_Auth_Storage_Exception
     * @throws \Zend_Db_Table_Exception
     */
    public function userCities()
    {
        $citiesArray = [];
        $id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;

        if ($id) {
            $user_cities = new Model_UserCities();
            $select2 = $user_cities->select()
                ->from(['z' => $user_cities->info('name')], ['z.city_id'])
                ->where('active = 1 and user_id = ?', (int)$id);
            $citiesArray = $this->fetchAll(' id in (' . $select2 . ')');
        }

        return $citiesArray;
    }

    public function getCities()
    {
        $z_model = new Model_Zone();
        $cities = $this->fetchAll();
        $items = [];
        if ($cities->count()) {
            foreach ($cities as $c) {
                $city = $c->toArray();
                //Get Zones
                $zones = $z_model->fetchAll('city_id = '.$c->id); //
                if ($zones->count()) {
                    $city['zones'] = implode('<br/>', array_map(function ($z) {
                        return $z['zone_name'];
                    },
                        $zones->toArray()));
                }

                $items[] = $city;
            }
        }

        return $items;
    }

    public function getCity ( $id ) {
        try {
            $city = $this->fetchRow('id = '.$id)->toArray();
            return $city;
        } catch ( \Exception $e ) {
            return false;
        }

    }
}
