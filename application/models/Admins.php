<?php

/**
 * Description of Admins.
 *
 * @author Darius Matulionis
 */
class Model_Admins extends Core_Db_Table
{
    protected $_name = 'box_admins';

    protected $_primary = 'id';

    public static function isRoot()
    {
        if (user()) {

            $has_root_group = (new static())->getAdapter()->query("
              SELECT COUNT(*) FROM box_admins_has_admin_groups WHERE admins_id = ? AND admin_groups_id = ?
            ", [user()->id, 1])->fetchColumn();

            return user()->adm_root || user()->admin_groups_id == 1 || $has_root_group;
        }

        return false;
    }

    public function getCityZone()
    {
        $userCities = new Model_UserCities();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $userCities->info('name'),
            $userCities->info('name').'.user_id = '.$this->_name.'.id'.' AND '.$userCities->info('name').'.active = 1',
            $userCities->info('name').'.city_id'
        );
        $cities = new Model_City();
        $select->joinLeft(
            $cities->info('name'),
            $cities->info('name').'.id = '.$userCities->info('name').'.city_id',
            $cities->info('name').'.CityName'
        );

        $admins = $this->getAdapter()->fetchAll($select);

        $items = [];

        foreach ($admins as $admin) {
            $cities = '';
            if (isset($items[$admin['id']])) {
                $cities = $items[$admin['id']]['CityName'];
            }
            $admin['CityName'] = $admin['CityName'].'<br/>'.$cities;

            $items[$admin['id']] = $admin;
        }

        return $items;
    }

    public function getUsersByGroup($group)
    {
        $userCities = new Model_UserCities();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $userCities->info('name'),
            $userCities->info('name').'.user_id = '.$this->_name.'.id'.' AND '.$userCities->info('name').'.active = 1',
            $userCities->info('name').'.city_id'
        );
        $cities = new Model_City();
        $select->joinLeft(
            $cities->info('name'),
            $cities->info('name').'.id = '.$userCities->info('name').'.city_id',
            $cities->info('name').'.CityName'
        );

        $select->where($this->_name.'.admin_groups_id IN(?)', $group);

        $admins = $this->getAdapter()->fetchAll($select);

        $items = [];

        foreach ($admins as $admin) {
            $cities = '';
            if (isset($items[$admin['id']])) {
                $cities = $items[$admin['id']]['CityName'];
            }
            $admin['CityName'] = $admin['CityName'].'<br/>'.$cities;

            $items[$admin['id']] = $admin;
        }

        return $items;
    }

    public function getUserById ( $id )
    {
        $userArray = $this->fetchRow( $this->_name.'.id = '. $id );
        if ( !empty ( $userArray ) ) {
            return $userArray->toArray();
        } else {
            return array();
        }

    }
}
