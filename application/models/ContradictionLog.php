<?php
/**
 * Created by PhpStorm.
 * User: viktoras
 * Date: 19.4.18
 * Time: 10.08
 */

class Model_ContradictionLog extends Core_Db_Table
{
    protected $_primary = 'id';
    protected $_name = 'box_contradiction_log';

    public static function event($contra_id, $message)
    {
        (new static)->save([
            'contra_id' => $contra_id,
            'who' => user()->adm_name. ' '. user()->adm_surname,
            'message' => $message
        ]);
    }
}
