<?php

/**
 * Created by PhpStorm.
 * User: jarek
 * Date: 03/02/2015
 * Time: 12:20.
 */
class Model_FtpLog extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_ftp_log';

    protected $baseUrl = 'http://parking.unipark.lt/api2/fines/check-plate/';

    protected $key = 'e99794d45ed2f8a8943ef830cbefa24b';

    public function ScanCar($items)
    {
        $client = new Zend_Rest_Client('http://'.$_SERVER['HTTP_HOST'].'/');
        $url = '/api2/fines/check-plate/e99794d45ed2f8a8943ef830cbefa24b';
        $errors = [];

        //print_r((string)$items->identifier);
        $post_data = [
            'adm_device_id' => 'V0003_20150204T070211A',
            'plate' => ((string) $items->vrm),
            'zone' => '1',
            'lat' => ((string) $items->offenceLocation->Latitude),
            'lon' => ((string) $items->offenceLocation->Longitude),
            //  'web_service'  => 'Zend Rest Call',
            //  'zend_tutorial'   => 'ZEND_REST_CLIENT tutorial',
            //'selection' => 'POST Rest Service sample'
        ];

        // POST CALL

        try {
            // send request to the REST service
            // view a post method
            $result = ($client->restPost($url, $post_data));
        } catch (Zend_Rest_Client_Exception $e) {
            // catch Rest Client Exception
            $errors[] = '['.$e->getCode().']:'.$e->getMessage();
        } catch (Exception $e) {
            // catch general exception if any occurs
            // like DB
            $errors[] = '['.$e->getCode().']:'.$e->getMessage();
        }
        if ($errors) {
            // print out errors
            print_r($errors);
        } else {
            // print out the response body
            print_r($result->getBody());
        }
    }
}
