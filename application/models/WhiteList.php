<?php

/**
 * @category  Models
 * @name WhiteList
 * @copyright Darius Matulionis
 * @author    Darius Matulionis <darius@matulionis.lt>
 * @since     : 2013-11-26, 15:11:22
 */
class Model_WhiteList extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_white_list';

    public function getWhiteList($where = null)
    {
        $wlg_model = new Model_WhiteListGroups();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        $select->joinLeft(
            $wlg_model->info('name') . ' as wlg',
            'wlg.id = ' . $this->_name . '.group_id',
            ['wlg.title as group']
        );

        $zones_model = new Model_ZonesWhitelists();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.wg_list_id = ' . $this->_name . '.group_id',
            $zones_model->info('name') . '.zone_id'
        );

        $city_model = new Model_Zone();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.zone_id',
            $city_model->info('name') . '.city_id'
        );

        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        $select->order('group_id ASC ');
        $select->order('date_to DESC ');

        return $this->getAdapter()->fetchAll($select);
    }

    public function getWhiteListQuery($where = null, $id = null)
    {
        $wlg_model = new Model_WhiteListGroups();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        $select->joinLeft(
            $wlg_model->info('name') . ' as wlg',
            'wlg.id = ' . $this->_name . '.group_id',
            ['wlg.title as group']
        );

        $zones_model = new Model_ZonesWhitelists();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.wg_list_id = ' . $this->_name . '.group_id',
            $zones_model->info('name') . '.zone_id'
        );

        $city_model = new Model_Zone();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.zone_id',
            [$city_model->info('name') . '.city_id', $city_model->info('name') . '.zone_name']
        );
        if ($id) {
            $user_id = new Model_UserCities();
            $select->joinLeft(
                $user_id->info('name'),
                $city_model->info('name') . '.city_id = ' . $user_id->info('name') . '.city_id and user_id=' . $id . ' and ' . $user_id->info('name') . '.active = 1',
                $user_id->info('name') . '.user_id'
            );
            $select->where('user_id=' . $id);
        }
        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        $select->order('group_id ASC ');
        $select->order('date_to DESC ');

        return $select;
    }

    public function getWhiteIndex($id)
    {
        $wlg_model = new Model_WhiteListGroups();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        $select->joinLeft(
            $wlg_model->info('name') . ' as wlg',
            'wlg.id = ' . $this->_name . '.group_id',
            ['wlg.title as group']
        );

        $zones_model = new Model_ZonesWhitelists();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.wg_list_id = ' . $this->_name . '.group_id',
            $zones_model->info('name') . '.zone_id'
        );

        $city_model = new Model_Zone();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.zone_id',
            $city_model->info('name') . '.city_id'
        );

        $user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $city_model->info('name') . '.city_id = ' . $user_id->info('name') . '.city_id and user_id=' . $id,
            $user_id->info('name') . '.user_id'
        );

        $select->where('user_id=' . $id);
        $select->where($user_id->info('name') . ".active= '1'");
        $select->where($zones_model->info('name') . ".active= '1'");
        $select->order($this->_name . '.id DESC');
        $select->group(['plate', 'group_id']);

        return $select;
    }

    public function WhiteListCSV($where)
    {
        $wlg_model = new Model_WhiteListGroups();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        $select->joinLeft(
            $wlg_model->info('name') . ' as wlg',
            'wlg.id = ' . $this->_name . '.group_id',
            ['wlg.title as group']
        );

        $zones_model = new Model_ZonesWhitelists();
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.wg_list_id = ' . $this->_name . '.group_id',
            $zones_model->info('name') . '.zone_id'
        );

        $city_model = new Model_Zone();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.zone_id',
            $city_model->info('name') . '.city_id'
        );

        /*$user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name').'.city_id = '.$city_model->info('name').'.city_id',
            $user_id->info('name').'.user_id'
        );*/
        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchall($select);
    }

    public function checkWhiteList($plate, $zone)
    {
        $wlg_model = new Model_WhiteListGroups();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $wlg_model->info('name') . ' as wlg',
            'wlg.id = ' . $this->_name . '.group_id',
            ['wlg.title as group']
        );

        $zones = new Model_ZonesWhitelists();
        $wlist = $zones->select()->from(['z' => $zones->info('name')],
            ['z.wg_list_id'])->where('zone_id in(' . $zone . ") AND active ='1'");

        if ($wlist == null) {
            $select->where('contract_sum >1000000');
        } else {
            $select->where('group_id in( ' . $wlist . ')');
            $select->where("date_to >'" . date('Y-m-d H:i:s') . "'");
            $select->where("date_from <= '" . date('Y-m-d H:i:s') . "'");
            $select->where('plate = ?', $plate);
        }
        $select->order(['group_id ASC', 'date_to DESC']);
        $select->limit(1);

        return $this->fetchRow($select);
    }
}
