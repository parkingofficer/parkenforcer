<?php

/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 2014-12-10
 * Time: 12:18.
 */
class select
{
    /**
     * @param null $requirement_id
     * @param null $role_id
     * @param null $training_type_id
     *
     * @return Zend_Db_Table_Rowset_Abstract
     * @throws Zend_Db_Select_Exception
     * @throws Zend_Db_Table_Exception
     */
    public function getRequirements($requirement_id = null, $role_id = null, $training_type_id = null)
    {
        $rr_model = new Models_DbTable_InstructorRequirements();
        $r_model = new Models_DbTable_InstructorRoles();
        $tt_model = new Models_DbTable_TrainingTypes();

        $select = $rr_model->select()->from(['rr' => $rr_model->info('name')])->setIntegrityCheck(false);
        $select->columns([
            'rr.*',
            'rr.id as rq_id',
            new Zend_Db_Expr('IF(rr.has_continuation = 0,"NO","YES") AS has_continuation_str'),
            new Zend_Db_Expr('IF(rr.general_for_role = 0,"NO","YES") AS general_for_role_str'),
        ]);
        $select->joinLeft(['r' => $r_model->info('name')], 'r.id = rr.role_id', 'r.role');
        $select->joinLeft(['tt' => $tt_model->info('name')], 'tt.id = r.training_type_id', 'tt.title as training_type');
        $select->joinLeft(['ttt' => $tt_model->info('name')], 'ttt.id = rr.training_type_id',
            'ttt.title as only_for_training_type');
        $select->order(['only_for_training_type ASC', 'training_type ASC', 'role ASC', 'requirement ASC']);

        if ($requirement_id) {
            $select->where('rr.id = ?', $requirement_id);
        }
        if ($role_id) {
            $select->where('r.id = ?', $role_id);
            $select->where('rr.general_for_role = ?', 0);
        }
        if ($training_type_id) {
            $select->where('rr.training_type_id = ?', $training_type_id);
        }

        $select->where('rr.visible = 1');

        return $r_model->fetchAll($select);
    }
}
