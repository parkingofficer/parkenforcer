<?php

/**
 * Description of Admins.
 *
 * @author Robert
 */
class Model_SendgridLog extends Core_Db_Table
{
    protected $_name = 'box_sendgrid_log';

    protected $_primary = 'id';
}
