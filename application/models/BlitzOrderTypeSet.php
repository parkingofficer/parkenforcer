<?php

/**
 * Created by PhpStorm.
 * User: ITGIRNOS
 * Date: 5/4/2016
 * Time: 3:33 PM.
 */
class Model_BlitzOrderTypeSet extends Core_Db_Table
{
    protected $_name = 'box_blitz_order_type_set';

    protected $_primary = 'id';

    public function getById($id)
    {
        $blitz_statuses = new Model_BlitzStatuses();

        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->joinLeft(
            $blitz_statuses->info('name').' as bs',
            'bs.id = '.$this->_name.'.status_id',
            ['bs.name', $this->_name.'.status_id']
        );
        $select->where($this->_name.'.order_type_id ='.$id);

        return $this->getAdapter()->fetchAll($select);
    }

    public function getStatusesByType($id)
    {
        $statuses = new Model_BlitzStatuses();
        $types = new Model_BlitzOrderType();

        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->joinLeft(
            $statuses->info('name').' as stat',
            'stat.id = '.$this->_name.'.status_id',
            ['stat.name']
        );
        $select->joinLeft(
            $types->info('name').' as type',
            'type.id = '.$this->_name.'.order_type_id',
            [$this->_name.'.status_id']
        );
        $select->where($this->_name.'.order_type_id ='.$id);

        return $this->getAdapter()->fetchAll($select);
    }

    public function getAllById($id)
    {
        $blitz_statuses = new Model_BlitzStatuses();

        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name, [$this->_name.'.status_id', 'bs.name']);
        $select->joinLeft(
            $blitz_statuses->info('name').' as bs',
            'bs.id = '.$this->_name.'.status_id',
            ['bs.name', $this->_name.'.status_id']
        );
        $select->where($this->_name.'.order_type_id ='.$id);

        return $this->fetchAll($select);
    }

    public function getlist()
    {
        return $this->fetchall();
    }
}
