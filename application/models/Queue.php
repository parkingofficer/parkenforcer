<?php

/**
 * @category  Models
 * @name Queue
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2014-07-07, 15:50:12
 */
class Model_Queue extends Core_Db_Table
{
    protected $_primary = 'queue_id';

    protected $_name = 'queue';
}
