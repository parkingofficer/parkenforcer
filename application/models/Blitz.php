<?php

/**
 * Description of Admins.
 *
 * @author Darius Matulionis
 */
class Model_Blitz extends Core_Db_Table
{
    protected $_name = 'box_blitz';

    protected $_primary = 'id';

    public function getlist()
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        return $select;
    }

    public function getMaxid()
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name, [new Zend_Db_Expr('max(id) as maxId')]);

        return $this->getAdapter()->fetchAll($select);
    }

    public function getOrderType($id)
    {
        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->where($this->_name.'.id ='.$id);

        return $this->getAdapter()->fetchRow($select);
    }

    public function getBlitz($id = null)
    {
        $admin = new Model_Admins();

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $admin->info('name'),
            $this->_name.'.admin_id = '.$admin->info('name').'.id',
            'CONCAT('.$admin->info('name').'.adm_name," ",'.$admin->info('name').'.adm_surname) as admin'
        );

        if ($id) {
            $select->where($this->_name.'.id = ?', $id);

            return $this->getAdapter()->fetchRow($select);
        }

        return $this->getAdapter()->fetchAll($select);
    }

    public function getBlicExport($where)
    {
        $blic_status = new Model_BlitzStatuses();
        $blic_type = new Model_BlitzOrderType();

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $blic_status->info('name'),
            $blic_status->info('name').'.id = '.$this->_name.'.status',
            $blic_status->info('name').'.name as status_name'
        );
        $select->joinLeft(
            $blic_type->info('name'),
            $blic_type->info('name').'.id = '.$this->_name.'.order_type',
            $blic_type->info('name').'.type as blic_type'
        );
        $select->columns([
            new Zend_Db_Expr('CONCAT('.$this->_name.'.name," ",'.$this->_name.'.surname) as name_surname'),
        ]);

        if (! empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchAll($select);
    }

    public function getContractNumber( $contractNr )
    {
        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->where($this->_name.'.contract_nr ='.$contractNr);

        try  {
            $result = $this->getAdapter()->fetchRow($select);
            return $result;
        } catch ( \Exception $e){
            return false;
        }

    }
}
