<?php
/**
 * Created by PhpStorm.
 * User: viktoras
 * Date: 19.4.18
 * Time: 18.19
 */

class Model_SendGridLog extends core_db_table
{

    protected $_name = 'box_sendgrid_log';
    protected $_primary = 'id';

    public static function event(
        $controller,
        $service,
        $record_id,
        $action,
        $response
    )
    {
        (new static)->save([
            'controller' => $controller,
            'service' => $service,
            'record_id' => $record_id,
            'action' => $action,
            'response' => $response,
        ]);
    }

}