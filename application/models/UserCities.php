<?php

/**
 * @category  Models
 * @name UserCities
 * @copyright Robert Lechovič
 * @author Robert Lechovič <robert@itgirnos.lt>
 * @since : 2014-11-28, 09:36:27
 */
class Model_UserCities extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_user_cities';
}
