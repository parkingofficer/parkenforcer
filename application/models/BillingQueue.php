<?php

/**
 * Created by PhpStorm.
 * User: Robert
 * Date: 2018-06-15
 * Time: 09:34
 */

class Model_BillingQueue extends core_db_table
{
    protected $_name = 'billing_queue';

    protected $_primary = 'id';

    public const CREATE_FINE = 'createFine';
    public const UPDATE_FINE = 'updateFine';
    public const CREATE_TRANSACTION = 'createTransaction';
    public const PORSCHE_STATUS_CHANGE = 'additionalServiceStatusChange';

}