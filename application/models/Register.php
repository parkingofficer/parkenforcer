<?php

class Model_Register extends Core_Db_Table{

    protected $_primary = 'id';

    protected $_name = 'box_app_register';

    public function getAppReg(){

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        $select->order($this->_name.'.id DESC');

        return $select;
    }
}
