<?php

/**
 * @category  Models
 * @name PaymentsLog
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2014-07-07, 15:50:12
 */
class Model_PaymentsLog extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_payments_log';
}
