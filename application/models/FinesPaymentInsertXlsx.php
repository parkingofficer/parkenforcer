<?php
/**
 * @category  Models
 * @name FinesPaymentInsertXlsx
 * Created by PhpStorm.
 * User: viktoras
 * Date: 19.2.20
 * Time: 16.36
 */

class Model_FinesPaymentInsertXlsx extends Core_Db_Table
{
    protected $_primary = 'id';
    protected $_name = 'box_import_xlsx';

    public function importPaymentsEEFromXLSX ( $file ) {

        $f_model = new Model_Fines();
        $u_model = new Model_Admins();
        $IL_model = new Model_FinesPaymentInsertXlsxLog();
        $fileName = $file['payments']['tmp_name'];
        $uploadFileName = $file['payments']['name'];

        // User info
        $user_id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $userDataArray = $u_model->getUserById( $user_id );
        $nameSurname = $userDataArray['adm_name']." ".$userDataArray['adm_surname'];

        try {
            $inputFileType = PHPExcel_IOFactory::identify($fileName);
            if (!in_array($inputFileType, ["Excel2007", "Excel5", "OOCalc", "Excel2003XML"])) {
                return 2;
            }
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($fileName);
        } catch ( \Exception $e) {
            return 2;
        }

        // Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        // Loop through each row of the worksheet in turn
        $xlsxData = [];
        if ( !$this->checkFileData ( $file, $user_id, $nameSurname ) ) {
            for ($row = 2; $row <= $highestRow; $row++) {
                //  Read a row of data into an array
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    null,
                    true,
                    false);
                // Insert row data array into database
                $exelDataArray = $rowData[0];

                $where = [];
                $where[] = "id_public = '$exelDataArray[0]'";
                $where[] = "status_id != 2";
                $id_public = $f_model->getFines($where);

                $fineArray = $id_public->toArray();
                $fineArray = $fineArray[0];

                $insertArray = [
                    'fine_id' => $fineArray['id'],
                    'fine_id_public' => $exelDataArray[0],
                    'amount_cnt' => ($exelDataArray[2]*100),
                    'user_id' => $user_id,
                    'user' => $nameSurname,
                    'xlsx_file_name' => $uploadFileName
                ];

                $xlsxData [] = $rowData[0];

                $tempDate  = strtotime(  PHPExcel_Style_NumberFormat::toFormattedString($exelDataArray[1],'D/M/Y' ));
                $date = date('Y-m-d', $tempDate) ;

                if ($fineArray['id']) {
                    try {
                        $f_model->addXlsxImportPayment($fineArray['id'], $date, $exelDataArray[2], $uploadFileName);
                        $insertArray['action'] = 'Insert';
                        $this->save($insertArray);
                    } catch (\Exception $e) {
                        $insertArray['action'] = 'Error';
                        $insertArray['cause_of_error'] = $e->getMessage();
                        $this->save($insertArray);
                    }
                } else {
                    $insertArray['action'] = 'Error';
                    $insertArray['cause_of_error'] = 'Error - No find fine';
                    try {
                        $this->save($insertArray);
                    } catch (\Exception $e){}

                }
            }
            $IL_model->save([
                'user_id' => $user_id,
                'user' => $nameSurname,
                'xlsx_file_name' => $uploadFileName,
                'import_source' => json_encode( $xlsxData )
            ]);

            return true;
        } else { return false; }
    }

    static private function parseExlelDataArray ( $array = array() ) {

        $dataArray = [];
        foreach ( $array as $item )
        {
            if ( $item ) {
                $dataArray [] = $item;
            }
        }

        return $dataArray;
    }

    private function checkFileData ( $file, $user_id, $nameSurname ) {
        $IL_model = new Model_FinesPaymentInsertXlsxLog();

        $uploadFileName = $file['payments']['name'];
        $fileName = $file['payments']['tmp_name'];
        try {
            $inputFileType = PHPExcel_IOFactory::identify($fileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($fileName);
        } catch ( \Exception $e ) {
            return true;
        }

        // Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $errorNumber = [];
        $countColumnNumber = [];
        $parseColumn = [];
        for ($row = 2; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                null,
                true,
                false);

            $parseColumnNumber = $this->parseExlelDataArray ( $rowData[0] );
            $parseColumn [] = $this->parseExlelDataArray ( $rowData[0] );
            $rowA = $rowData[0][0];
            $rowB = $rowData[0][1];
            $rowC = $rowData[0][2];
            if ( is_numeric($rowA) || strlen($rowA) === 6 || empty($rowA) ) { $errorNumber [] = 0; }
            else { $errorNumber [] = 1; }

            if ( count($parseColumnNumber) === 3 || (empty($rowA) && empty($rowB) && empty($rowC) ) ) { $countColumnNumber [] = 1; }
            else { $countColumnNumber [] = 0; }

        }

        $errorFormat = ( !in_array( 0, $countColumnNumber) && !in_array( 1, $errorNumber) ) ? false : true;

        if ( $errorFormat ){
            $IL_model->save([
                'user_id' => $user_id,
                'user' => $nameSurname,
                'xlsx_file_name' => $uploadFileName." fail",
                'import_source' => json_encode( $parseColumn )
            ]);
        }

        return $errorFormat;
    }

    public function getFailImport () {

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->where( 'cause_of_error IS NOT NULL' );
        $select->order( 'id ASC' );

        return $select;
    }
}