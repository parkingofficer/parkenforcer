<?php

/**
 * Description of Admins.
 *
 * @author Darius Matulionis
 */
class Model_BlitzLog extends Core_Db_Table
{
    protected $_name = 'box_blitz_log';

    protected $_primary = 'id';

    public function getlist()
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        return $select;
    }
}
