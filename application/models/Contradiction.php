<?php
/**
 * Created by PhpStorm.
 * User: viktoras
 * Date: 19.4.17
 * Time: 10.22
 */

use \SendGrid\Mail;

class Model_Contradiction extends core_db_table
{
    protected $_name = 'box_contradiction';
    protected $_primary = 'id';

    protected $statusNew;
    protected $statusAccepted;
    protected $statusRejected;
    protected $service;
    protected $Bearer;

    public function changeStatus($id, $status)
    {
        $contradiction = $this->find($id)->current();

        if (!$contradiction) {
            throw new \Exception('Contradiction not found');
        }

        $old_value = $contradiction->contra_status;
        $user_id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;

        $contradiction->user_id = $user_id;
        $contradiction->contra_status = $status;
        $contradiction->change_date = date('Y-m-d H:i:s');
        $contradiction->save();

        Model_ContradictionLog::event(
            $id,
            sprintf(
                'Contradiction status set to: <strong>"%s"</strong>, was <em>"%s"</em>',
                Admin_ContradictionController::statuses($status),
                Admin_ContradictionController::statuses($old_value)
            )
        );
        return $contradiction;
    }

    public function addSendMailStatus($id)
    {
        $contradiction = $this->find($id)->current();

        if (!$contradiction) {
            throw new \Exception('Contradiction not found');
        }

        $contradiction->send_status = 1;
        $contradiction->save();
    }

    public function getContradictionFineId($id)
    {
        $contradiction = $this->find($id)->current();

        if (!$contradiction) {
            throw new \Exception('Contradiction not found');
        }

        return $contradiction->fine_id;
    }

    public function getContradictionFileName($id)
    {
        $contradiction = $this->find($id)->current();

        if (!$contradiction) {
            throw new \Exception('Contradiction not found');
        }

        return $contradiction->file_name;
    }

    public function getContradictionStatus($id)
    {
        $contradiction = $this->find($id)->current();

        if (!$contradiction) {
            throw new \Exception('Contradiction not found');
        }

        return $contradiction->contra_status;
    }

    public function getAllNotSendContradiction()
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $keys = $config->getOption('contradiction');
        $status = $this->statusNew = $keys['status_new'];

        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->where( '('.$this->_name.'.send_status = 0 OR '.$this->_name.'.send_status IS NULL )' );
        $select->where( $this->_name.'.file_name IS NOT NULL ' );
        $select->where( $this->_name.'.contra_status != '.$status );
        $select->where( $this->_name.'.`deleted_at` IS NULL' );

        return $this->getAdapter()->fetchAll($select);
    }

    public function sendMail ( $fineId ) {

        if ( !isset( $fineId ) ) exit();

        $dataContradiction = $this->select( ['file_name', 'file_type', 'contra_status']);
        $dataContradiction->where( 'fine_id =?', $fineId );
        $selectContradiction = $this->fetchRow( $dataContradiction );

        if ( $selectContradiction ) {
            $dataContradictionArray = $selectContradiction->toArray();
        } else {
            Model_SendGridLog::event(
                'Contradiction',
                'contra',
                $fineId,
                'False',
                json_encode( array( 'message' => 'Prieštaravimas, kurio baudos id - '.$fineId.' sistemoje nerastas' ))
            );
            return false;
        }

        $model_fine = new Model_Fines();
        $dataFine = $model_fine->select(['name', 'surname', 'u_email', 'id_public', 'zone']);
        $dataFine->where( 'id =?', $fineId );
        $selectFine = $model_fine->fetchRow($dataFine);

        if ( $selectFine ) {
            $dataFineArray = $selectFine->toArray();
        } else { Model_SendGridLog::event(
            'Contradiction',
            'contra',
            $dataContradictionArray['id'],
            'False',
            json_encode( array( 'message' => 'Bauda, kurios id - '.$fineId.' sistemoje nerastas' ))
        );
            return false;
        }

        $Model_Zone = new Model_Zone();
        $zoneObject = $Model_Zone->fetchRow('id = '.$dataFineArray['zone']);
        $model_city = new Model_City();
        $cityObject = $model_city->fetchRow( 'id = '.$zoneObject->city_id.'' );

        switch ($cityObject['fineType']){
            case 1 :
                $this->service = 'contra_narva';
                break;
            default:
                $this->service = 'contra';
                break;
        }

        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $keySendGrid = $config->getOption('sendgrid');
        $this->Bearer = $keySendGrid['bearer'];

        $model_sendGrid = new Model_SendGrid();
        $dataSendGrid = $model_sendGrid->select();
        $dataSendGrid->where('service =?', $this->service);
        $selectSendGrid = $model_sendGrid->fetchRow($dataSendGrid);

        if ($selectSendGrid) {
            $dataArray = $selectSendGrid->toArray();
            $mailTypeArray = json_decode( $dataArray['mailType'], true );
        }

        $locale = 'en';
        $file = FINES_PHOTOS_PATH . '/' . $fineId . '/'.$dataContradictionArray['file_name'];
        $content_file = base64_encode ( file_get_contents ( $file ) );

        $subject = "Sending with SendGrid is Fun";

        $email = new Mail\Mail();
        $email->setFrom( $mailTypeArray['from']['email'], $mailTypeArray['from']['name'] );
        $email->setSubject( $subject );
        $email->addTo( $dataFineArray['u_email'] );

        $email->addContent("text/plain", "And easy to do anywhere, even with PHP");
        $email->addContent(
            "text/html", "And easy to do anywhere, even with PHP"
        );

        $email->addSubstitution("-time_created-", date('Y-m-d', strtotime("")));
        $email->addSubstitution("-name-", $dataFineArray['name'] );
        $email->addSubstitution("-surname-", $dataFineArray['surname'] );
        $email->addSubstitution("-fineno-", $dataFineArray['id_public'] );
        $email->addSubstitution("-contradictionstatus-", Core_Locale::translate( Admin_ContradictionController::statuses( $dataContradictionArray['contra_status'] )) );

        $email->addAttachment(
            $content_file,
            $dataContradictionArray['file_type'],
            $dataContradictionArray['file_name'],
            "attachment",
            $dataContradictionArray['file_name']
        );

        $email->setTemplateId( $mailTypeArray['templateId'][$locale] );

        $sendgrid = new \SendGrid( $this->Bearer );

        try {
            //$sendgrid->send($email);

            $response = $sendgrid->send($email);
            //print $response->statusCode() . "\n";
            //print_r($response->headers());
            //print $response->body() . "\n";

            (new Model_Contradiction())->addSendMailStatus( $dataContradictionArray['id'] );
            Model_SendGridLog::event(
                'Contradiction',
                $this->service,
                $dataContradictionArray['id'],
                'Success',
                json_encode( array ( $response->statusCode(), 'emai' => $email ) )
            );
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
            Model_SendGridLog::event(
                'Contradiction',
                $this->service,
                $dataContradictionArray['id'],
                'False',
                json_encode( array( 'message' => $e->getMessage() ))
            );
            return false;
        }
        return true;
    }
}