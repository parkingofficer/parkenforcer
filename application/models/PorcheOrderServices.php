<?php

/**
 * Porche order services.
 *
 * @author Jarek Skuder
 */
class Model_PorcheOrderServices extends Core_Db_Table
{
    protected $_name = 'box_porche_order_services';

    protected $_primary = 'id';

    function getServiceStatus ( $pId, $sId )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'porche_id = ?', $pId )
            ->where( 'service_id = ?', $sId );

        $data = $this->fetchRow( $select );
        $status = ( isset( $data ) ) ? $data->toArray() : null;

        return $status['status_id'];
    }

    function getAllStatusDataToBilling ( $orderId, $sendData = array () )
    {
        $dbPSS = new Model_PorcheServiceStatuses();
        $dbDataArray = array();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'porche_id = ?', $orderId );
        $data = $this->fetchAll( $select );

        if ( isset( $data ) ){
            foreach ( $data->toArray() as $item )
            {
               if ( !in_array($item['service_id'],$sendData ) ) {
                   array_push($dbDataArray, [
                       "Id" => $item['service_id'],
                       "Status" => $dbPSS->getBillingId($item['status_id'], $item['service_id'])
                   ]);
               }
            }

        } else { $dbDataArray = []; }

        return $dbDataArray;
    }

}
