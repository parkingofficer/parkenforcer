<?php

/**
 * Porche log.
 *
 * @author Jarek Skuder
 */
class Model_PorcheLog extends Core_Db_Table
{
    protected $_name = 'box_porche_log';

    protected $_primary = 'id';

    public function getlist()
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);

        return $select;
    }
}
