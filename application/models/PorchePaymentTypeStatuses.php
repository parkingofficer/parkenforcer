<?php

/**
 * Porche payment type statuses.
 *
 * @author Jarek Skuder
 */
class Model_PorchePaymentTypeStatuses extends Core_Db_Table
{
    protected $_name = 'box_porche_payment_type_statuses';

    protected $_primary = 'id';

    public function getlist()
    {
        return $this->fetchall();
    }
}
