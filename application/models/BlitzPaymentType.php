<?php

/**
 * Description of Admins.
 *
 * @author Robert Lechovič
 */
class Model_BlitzPaymentType extends Core_Db_Table
{
    protected $_name = 'box_blitz_payment_type';

    protected $_primary = 'id';

    public function getlist()
    {
        return $this->fetchall();
    }
}
