<?php

/**
 * @category  Models
 * @name AdminsHasAdminGroups
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-03-19, 17:10:01
 */
class Model_AdminsHasAdminGroups extends Core_Db_Table
{
    protected $_primary = 'admins_id';

    protected $_name = 'box_admins_has_admin_groups';
}
