<?php

/**
 * Porche statuses.
 *
 * @author Jarek Skuder
 */
class Model_PorcheStatuses extends Core_Db_Table
{
    protected $_name = 'box_porche_statuses';

    protected $_primary = 'id';

    public function getlist()
    {
        return $this->fetchall();
    }

    function getServiceSmsType ( $sId )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'id = ?', $sId );
        $data = $this->fetchRow( $select );

        if ( isset($data) )
        {
            $data = $data->toArray();
            $smsType = $data['smsType'];
        } else { $smsType = null; }

        return $smsType;
    }

    function getServiceMailType ( $sId )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'id = ?', $sId );
        $data = $this->fetchRow( $select );

        if ( isset($data) ) {
            $data->toArray();
            $mailType = ($data['mailType']) ? json_decode($data['mailType']) : null;
            $mailType = (isset($mailType) && $mailType->enable == 1) ? $mailType : null;
        } else { $mailType = null; }

        return $mailType;
    }

    function getServiceBillingId ( $sId )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'id = ?', $sId );
        $data = $this->fetchRow( $select );

        if ( isset($data) )
        {
            $data = $data->toArray();
            $billingId = $data['billing_id'];
        } else { $billingId = null; }

        return $billingId;
    }
}
