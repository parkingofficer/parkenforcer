<?php

/**
 * @category  Models
 * @name BlackListCauses
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-11-27, 10:50:07
 */
class Model_BlackListCauses extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_black_list_causes';
}
