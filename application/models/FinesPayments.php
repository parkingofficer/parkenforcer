<?php

/**
 * @category  Models
 * @name FinesPayments
 * @copyright Jarek Skuder
 * @author    Jarek Skuder <jarek@itgirnos.lt>
 */
class Model_FinesPayments extends Core_Db_Table
{
    protected $_primary = 'archive_no';

    protected $_name = 'box_fines_payments';

    public function importPaymentsLT($file)
    {
        $f_model = new Model_Fines();
        $content = file_get_contents($file['payments']['tmp_name']);

        if (!empty($content)) {
            $regexp = '/[a-zA-Z]{3}(.?){1}[0-9]{3}/i';
            $regexpPublicId = '/[a-zA-Z0-9]{6}/';

            // Remove the XML namespace
            $content = str_replace('<ns2:', '<', $content);
            $content = str_replace('</ns2:', '</', $content);

            $xml = simplexml_load_string($content);

            $xml = $xml->BkToCstmrStmt->Stmt;
            foreach ($xml as $x) {
                foreach ($x->Ntry as $con) {
                    $amount = $con->Amt;
                    $amountCnt = floatval($amount);
                    $amountCnt = $amountCnt * 100;
                    $archiveNr = $con->AcctSvcrRef;
                    $is_payment = $this->find($archiveNr)->current();

                    if (!$is_payment) {
                        $type = $con->CdtDbtInd;
                        $time = $con->BookgDt->DtTm;
                        if ($type == 'CRDT') {
                            foreach ($con->NtryDtls as $dtl) {
                                foreach ($dtl->TxDtls as $txDtl) {
                                    $payment_text = $txDtl->RmtInf;
                                    $plateNr = $payment_text->Strd->CdtrRefInf->Ref;
                                    $currency = $amount['Ccy'];
                                    $client_code = $txDtl->RltdPties->Dbtr->Id->PrvtId->Othr->Id;
                                    if ($txDtl->RltdAgts->CdtrAgt) {
                                        $payer_bank_code = $txDtl->RltdAgts->CdtrAgt->FinInstnId->BIC;
                                        $payer_bank = $txDtl->RltdAgts->CdtrAgt->FinInstnId->Nm;
                                    }
                                    if ($txDtl->RltdAgts->DbtrAgt) {
                                        $payer_bank_code = $txDtl->RltdAgts->DbtrAgt->FinInstnId->BIC;
                                        $payer_bank = $txDtl->RltdAgts->DbtrAgt->FinInstnId->Nm;
                                    }
                                    $payer_acc = $txDtl->RltdPties->DbtrAcct->Id->IBAN;
                                    $payer_name = $txDtl->RltdPties->Dbtr->Nm;
                                    $time = str_replace('T', ' ', $time);
                                    $time = str_replace('.000', '', $time);
                                    $archiveNr = str_replace('', '', $archiveNr);
                                    $plateNr = str_replace('', '', $plateNr);
                                    $amount = str_replace('', '', $amount);
                                    $type = 'C';
                                    $currency = str_replace('', '', $currency);
                                    $client_code = str_replace('', '', $client_code);
                                    $payer_bank_code = str_replace('', '', $payer_bank_code);
                                    $payer_bank = str_replace('', '', $payer_bank);
                                    $payer_acc = str_replace('', '', $payer_acc);
                                    $payer_name = str_replace('', '', $payer_name);
                                    $json = json_encode($payment_text);

                                    $array = json_decode($json, true);

                                    $concated_text = str_replace([' ', '-'], '', $array['Ustrd']);
                                    preg_match($regexp, $concated_text, $plate);

                                    if ($plateNr) {
                                        if (strlen($plateNr) == 6) {
                                            $carPlate = $plateNr;
                                        } else {
                                            $carPlate = null;
                                        }
                                        $payment_text = $plateNr;
                                    } elseif (is_array($plate)) {
                                        $payment_text = reset($payment_text);
                                        $carPlate = $plate[0];
                                        if (strlen($carPlate) !== 6) {
                                            preg_match($regexpPublicId, trim($array['Ustrd']), $carPlate);
                                            if (is_array($carPlate)) {
                                                $carPlate = $carPlate[0];
                                            }
                                        }
                                    }
                                    $arr = [];
                                    $arr['archive_no'] = $archiveNr;
                                    $arr['plate'] = $carPlate;
                                    $arr['time'] = $time;
                                    $arr['amount_cnt'] = $amountCnt;
                                    $arr['payment_type'] = $type;
                                    $arr['currency'] = $currency;
                                    $arr['client_code'] = $client_code;
                                    $arr['payment_text'] = $payment_text;
                                    $arr['payer_bank_code'] = $payer_bank_code;
                                    $arr['payer_bank'] = $payer_bank;
                                    $arr['payer_account'] = $payer_acc;
                                    $arr['payer_name'] = $payer_name;
                                    $arr['payer_code'] = 0;
                                    $arr['operation_type'] = '';
                                    $arr['document_no'] = '';
                                    $arr['payment_code'] = '';
                                    if (!$arr['plate']) {
                                        $arr['plate'] = '';
                                    } else {
                                        $arr['plate'] = strtoupper(str_replace([' ', '-'], '', $arr['plate']));
                                    }

                                    $json = json_encode($arr);
                                    $array = json_decode($json, true);
                                    $payment = $this->save($array, 'archive_no');

                                    if ($payment->plate) {
                                        $fine = $f_model->fetchRow("
                                            (plate = '$payment->plate' OR id_public = '$payment->plate') AND
                                            status_id != " . Model_Fines::STATUS_FINE_PAYED . ' AND
                                            bank_payment_no IS NULL
                                        ');
                                        if ($fine) {
                                            $f_model->associatePayment(
                                                $payment->archive_no,
                                                $fine->id
                                            );

                                            $payment->fine_id = $fine->id;
                                            $payment->save();
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $d_archiveNr = $archiveNr . '_d';
                        $is_d_payment = $this->find($d_archiveNr)->current();
                        if (!$is_d_payment) {
                            $type = $con->CdtDbtInd;
                            $time = $con->BookgDt->DtTm;

                            if ($type == 'DBIT') {
                                foreach ($con->NtryDtls as $dtl) {
                                    foreach ($dtl->TxDtls as $txDtl) {
                                        $currency = $amount['Ccy'];
                                        $client_code = $txDtl->RltdPties->Dbtr->Id->PrvtId->Othr->Id;
                                        $payment_text = $txDtl->RmtInf;
                                        if ($txDtl->RltdAgts->CdtrAgt) {
                                            $payer_bank_code = $txDtl->RltdAgts->CdtrAgt->FinInstnId->BIC;
                                            $payer_bank = $txDtl->RltdAgts->CdtrAgt->FinInstnId->Nm;
                                        }
                                        if ($txDtl->RltdAgts->DbtrAgt) {
                                            $payer_bank_code = $txDtl->RltdAgts->DbtrAgt->FinInstnId->BIC;
                                            $payer_bank = $txDtl->RltdAgts->DbtrAgt->FinInstnId->Nm;
                                        }
                                        $payer_acc = $txDtl->RltdPties->DbtrAcct->Id->IBAN;
                                        $payer_name = $txDtl->RltdPties->Dbtr->Nm;
                                        $time = str_replace('T', ' ', $time);
                                        $time = str_replace('.000', '', $time);
                                        $amount = str_replace('', '', $amount);
                                        $type = 'D';
                                        $currency = str_replace('', '', $currency);
                                        $client_code = str_replace('', '', $client_code);
                                        $payer_bank_code = str_replace('', '', $payer_bank_code);
                                        $payer_bank = str_replace('', '', $payer_bank);
                                        $payer_acc = str_replace('', '', $payer_acc);
                                        $payer_name = str_replace('', '', $payer_name);

                                        $json = json_encode($payment_text);
                                        $array = json_decode($json, true);
                                        preg_match($regexp, trim($array['Ustrd']), $plate);
                                        $archiveNr = str_replace('', '', $archiveNr);
                                        $arr = [];
                                        $json = json_encode($payment_text);
                                        $arr['archive_no'] = $d_archiveNr;
                                        $arr['plate'] = '';
                                        $arr['time'] = $time;
                                        $arr['amount_cnt'] = $amountCnt;
                                        $arr['payment_type'] = $type;
                                        $arr['currency'] = $currency;
                                        $arr['client_code'] = $client_code;
                                        $arr['payment_text'] = reset($payment_text);
                                        $arr['payer_bank_code'] = $payer_bank_code;
                                        $arr['payer_bank'] = $payer_bank;
                                        $arr['payer_account'] = $payer_acc;
                                        $arr['payer_name'] = $payer_name;
                                        $arr['payer_code'] = 0;
                                        $arr['parent_archive_no'] = $archiveNr;
                                        $arr['operation_type'] = '';
                                        $arr['document_no'] = '';
                                        $arr['payment_code'] = '';

                                        $json = json_encode($arr);
                                        $array = json_decode($json, true);
                                        $this->save($array, 'archive_no');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function importPaymentsLV($file)
    {
        $f_model = new Model_Fines();
        $content = file_get_contents($file['payments']['tmp_name']);

        if (!empty($content)) {
            // Remove the XML namespace
            $content = str_replace('<ns2:', '<', $content);
            $content = str_replace('</ns2:', '</', $content);

            $xml = simplexml_load_string($content);

            $xml = $xml->BkToCstmrAcctRpt->Rpt;

            foreach ($xml as $x) {
                foreach ($x->Ntry as $con) {
                    $amount = $con->Amt;
                    $amountCnt = floatval($amount);
                    $amountCnt = $amountCnt * 100;
                    $archiveNr = $con->AcctSvcrRef;
                    $payer_bank = $con->BkTxCd->Prtry->Issr;
                    $is_payment = $this->find($archiveNr)->current();

                    if (!$is_payment) {
                        $type = $con->CdtDbtInd;
                        $time = $con->BookgDt->Dt;

                        if ($type == 'CRDT') {
                            foreach ($con->NtryDtls as $dtl) {
                                foreach ($dtl->TxDtls as $txDtl) {
                                    $is_onlyPlate = $txDtl->RmtInf;
                                    //$plateNr = $is_onlyPlate->Strd->CdtrRefInf->Ref;
                                    $currency = $amount['Ccy'];
                                    $client_code = $txDtl->RltdPties->Dbtr->Id->PrvtId->Othr->Id;
                                    $payment_text = $txDtl->RmtInf;

                                    //$string = preg_match('/[^A-Za-z\-]{3}[0-9]{3}$/',$long_string);

                                    $payer_acc = $txDtl->RltdPties->DbtrAcct->Id->IBAN;
                                    $payer_name = $txDtl->RltdPties->Dbtr->Nm;
                                    $payer_code = $txDtl->RltdPties->Cdtr->Id->OrgId->Othr->Id;
                                    $time = str_replace('T', ' ', $time);
                                    $time = str_replace('.000', '', $time);
                                    $archiveNr = str_replace('', '', $archiveNr);
                                    $amount = str_replace('', '', $amount);
                                    $type = str_replace('', '', $type);
                                    $currency = str_replace('', '', $currency);
                                    $client_code = str_replace('', '', $client_code);
                                    $payer_bank_code = str_replace('', '', null);
                                    $payer_bank = str_replace('', '', $payer_bank);
                                    $payer_acc = str_replace('', '', $payer_acc);
                                    $payer_name = str_replace('', '', $payer_name);
                                    $payer_code = str_replace('', '', $payer_code);

                                    $json = json_encode($payment_text);
                                    $array = json_decode($json, true);
                                    /*
                                    $regexp = "/[a-zA-Z]{2}(.?){1}[0-9]{4}/i";
                                    $plate = null;
                                    if( preg_match($regexp, trim($array['Ustrd']), $plate))
                                    {
                                    }
                                    else{
                                        $regexp2 = "/[a-zA-Z]{2}(.?){1}[0-9]{3}/i";
                                        preg_match($regexp2, trim($array['Ustrd']), $plate);
                                    }
                                    */
                                    $plate1 = $array['Ustrd'];
                                    $regexp = '/[a-zA-Z]{2}(.?){1}[0-9]{4}/i';
                                    $regexp2 = '/[a-zA-Z]{2}(.?){1}[0-9]{3}/i';
                                    $regexp3 = '/[a-zA-Z]{2}(.?){1}[0-9]{5,9}/i';

                                    $plate = null;
                                    $plate2 = null;
                                    $plate3 = null;

                                    $perg1 = preg_match($regexp, trim($plate1), $plate);
                                    $perg2 = preg_match($regexp2, trim($plate1), $plate2);
                                    $perg3 = preg_match($regexp3, trim($plate1), $plate3);

                                    if ($perg1) {
                                        $string = $plate[0];
                                        $needles = ['nr', 'NR'];

                                        foreach ($needles as $str) {
                                            $pos = strpos($string, $str);
                                            if ($pos !== false) {
                                                $result[] = 1;
                                            } else {
                                                $result[] = '';
                                            }
                                        }

                                        if ($result[0] !== false) {
                                            $arrFrom = ['Nr', 'NR', 'nr', ' ', '.'];
                                            if ($plate3) {
                                                $string = str_replace($arrFrom, '', $plate3[0]);
                                                $string1 = str_replace($string, '', $plate1);
                                                $string = strlen($string);
                                                ($string > 4) ? $plate_r = '' : $plate_r = $plate[0];
                                                if (!$plate_r) {
                                                    if (preg_match($regexp, trim($string1), $plate)) {
                                                    } elseif (preg_match($regexp2, trim($string1), $plate)) {
                                                    }
                                                    $plate_r = $plate[0];
                                                }
                                            } else {
                                                $plate_r = $plate[0];
                                            }
                                        } else {
                                            $plate_r = $plate[0];
                                        }
                                    } else {
                                        $plate_r = $plate2[0];
                                    }

                                    $arr = [];
                                    $arr['archive_no'] = $archiveNr;
                                    //$arr['plate'] = $plate[0];
                                    $arr['plate'] = $plate_r;
                                    $arr['time'] = $time;
                                    $arr['amount_cnt'] = $amountCnt;
                                    $arr['payment_type'] = $type;
                                    $arr['currency'] = $currency;
                                    $arr['client_code'] = $client_code;
                                    $arr['payment_text'] = reset($payment_text);
                                    $arr['payer_bank_code'] = $payer_bank_code;
                                    $arr['payer_bank'] = $payer_bank;
                                    $arr['payer_account'] = $payer_acc;
                                    $arr['payer_name'] = $payer_name;
                                    $arr['payer_code'] = $payer_code;
                                    if (!$arr['plate']) {
                                        $arr['plate'] = '';
                                    } else {
                                        $arr['plate'] = strtoupper(str_replace([' ', '-'], '', $arr['plate']));
                                    }
                                    $json = json_encode($arr);
                                    $array = json_decode($json, true);

                                    $payment = $this->save($array, 'archive_no');

                                    if ($payment->plate) {
                                        $fine = $f_model->fetchRow(
                                            "plate = '$payment->plate' AND
                                            status_id != " . Model_Fines::STATUS_FINE_PAYED . ' AND
                                            bank_payment_no IS NULL'
                                        );
                                        if ($fine) {
                                            //var_dump($fine->toArray());
                                            $fine->bank_payment_no = $payment->plate;
                                            $fine->status_id = Model_Fines::STATUS_FINE_PAYED;
                                            $fine->save();

                                            $payment->fine_id = $fine->id;
                                            $payment->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public function importPaymentsEE($file)
    {
        $f_model = new Model_Fines();
        $content = file_get_contents($file['payments']['tmp_name']);

        if (!empty($content)) {
            // Remove the XML namespace
            $content = str_replace('<ns2:', '<', $content);
            $content = str_replace('</ns2:', '</', $content);

            $xml = simplexml_load_string($content);

            $xml = $xml->BkToCstmrStmt->Stmt;

            foreach ($xml as $x) {
                foreach ($x->Ntry as $con) {
                    $amount = $con->Amt;
                    $amountCnt = floatval($amount);
                    $amountCnt = $amountCnt * 100;
                    $archiveNr = $con->AcctSvcrRef;
                    $payer_bank = $con->BkTxCd->Prtry->Issr;
                    $is_payment = $this->find($archiveNr)->current();

                    if (!$is_payment) {
                        $type = $con->CdtDbtInd;
                        $time = $con->BookgDt->Dt;

                        if ($type == 'CRDT') {
                            foreach ($con->NtryDtls as $dtl) {
                                foreach ($dtl->TxDtls as $txDtl) {
                                    $is_onlyPlate = $txDtl->RmtInf;
                                    //$plateNr = $is_onlyPlate->Strd->CdtrRefInf->Ref;
                                    $currency = $amount['Ccy'];
                                    $client_code = $txDtl->RltdPties->Dbtr->Id->PrvtId->Othr->Id;
                                    $payment_text = $txDtl->RmtInf->Strd;
                                    //$string = preg_match('/[^A-Za-z\-]{3}[0-9]{3}$/',$long_string);

                                    $id_public = json_decode(json_encode($txDtl->RmtInf->Strd->CdtrRefInf->Ref), true);
                                    $payer_acc = $txDtl->RltdPties->DbtrAcct->Id->IBAN;
                                    $payer_name = $txDtl->RltdPties->Dbtr->Nm;
                                    $payer_code = $txDtl->RltdPties->Cdtr->Id->OrgId->Othr->Id;
                                    $time = str_replace('T', ' ', $time);
                                    $time = str_replace('.000', '', $time);
                                    $archiveNr = str_replace('', '', $archiveNr);
                                    $amount = str_replace('', '', $amount);
                                    $type = str_replace('', '', $type);
                                    $currency = str_replace('', '', $currency);
                                    $client_code = str_replace('', '', $client_code);
                                    $payer_bank_code = str_replace('', '', null);
                                    $payer_bank = str_replace('', '', $payer_bank);
                                    $payer_acc = str_replace('', '', $payer_acc);
                                    $payer_name = str_replace('', '', $payer_name);
                                    $payer_code = str_replace('', '', $payer_code);
                                    $json = json_encode($payment_text);
                                    $array = json_decode($json, true);
                                    /*
                                    $regexp = "/[a-zA-Z]{2}(.?){1}[0-9]{4}/i";
                                    $plate = null;
                                    if( preg_match($regexp, trim($array['Ustrd']), $plate))
                                    {
                                    }
                                    else{
                                        $regexp2 = "/[a-zA-Z]{2}(.?){1}[0-9]{3}/i";
                                        preg_match($regexp2, trim($array['Ustrd']), $plate);
                                    }
                                    */
                                    $plate1 = $array['Ustrd'];
                                    $regexp = '/[0-9]{3}(.?){1}[a-zA-Z]{3}/i';

                                    $plate = null;

                                    $perg1 = preg_match($regexp, trim($plate1), $plate);

                                    $plate_r = '';
                                    if ($perg1) {
                                        $string = $plate[0];
                                        $needles = ['nr', 'NR'];

                                        foreach ($needles as $str) {
                                            $pos = strpos($string, $str);
                                            if ($pos !== false) {
                                                $result[] = 1;
                                            } else {
                                                $result[] = '';
                                            }
                                        }

                                        $plate_r = $plate[0];
                                    }

                                    $arr = [];
                                    $arr['archive_no'] = $archiveNr;
                                    //$arr['plate'] = $plate[0];
                                    $arr['id_public'] = $id_public[0];
                                    $arr['plate'] = $plate_r;
                                    $arr['time'] = $time;
                                    $arr['amount_cnt'] = $amountCnt;
                                    $arr['payment_type'] = 'C';
                                    $arr['currency'] = $currency;
                                    $arr['client_code'] = $client_code;
                                    $arr['payment_text'] = $payment_text ? reset($payment_text) : '';
                                    $arr['payer_bank_code'] = $payer_bank_code;
                                    $arr['payer_bank'] = $payer_bank;
                                    $arr['payer_account'] = $payer_acc;
                                    $arr['payer_name'] = $payer_name;
                                    $arr['payer_code'] = $payer_code ? $payer_code : 0;
                                    $arr['operation_type'] = '';
                                    $arr['document_no'] = '';
                                    $arr['payment_code'] = '';
                                    if (!$arr['plate']) {
                                        $arr['plate'] = '';
                                    } else {
                                        $arr['plate'] = strtoupper(str_replace([' ', '-'], '', $arr['plate']));
                                    }

                                    $json = json_encode($arr);
                                    $array = json_decode($json, true);

                                    $payment = $this->save($array, 'archive_no');

                                    if ($payment->id_public) {
                                        $fine = $f_model->fetchRow(
                                            //"plate = '$payment->plate' AND
                                            "id_public = '$payment->id_public' AND
                                            status_id != " . Model_Fines::STATUS_FINE_PAYED . ' AND
                                            bank_payment_no IS NULL'
                                        );
                                        if ($fine) {
                                            $f_model->associatePayment(
                                                $payment->archive_no,
                                                $fine->id
                                            );
                                            //var_dump($fine->toArray());
                                            //$fine->bank_payment_no = $payment->plate;
                                            //$fine->status_id = Model_Fines::STATUS_FINE_PAYED;
                                            //$fine->paid = $amount;
                                            //$fine->save();

                                            $payment->fine_id = $fine->id;
                                            $payment->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function getHectonicPayment($array = [])
    {
        $this->getAdapter()->beginTransaction();
        try {
            $this->save($array, 'archive_no');
            $this->getAdapter()->commit();
        } catch (Exception $e) {
            $this->getAdapter()->rollBack();

            return false;
        }
    }
}
