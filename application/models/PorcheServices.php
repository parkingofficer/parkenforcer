<?php

/**
 * Porche services.
 *
 * @author Jarek Skuder
 */
class Model_PorcheServices extends Core_Db_Table
{
    protected $_name = 'box_porche_services';

    protected $_primary = 'id';

    function getServicesNames ( $idArray = "" )
    {
        $servicesNames = "";

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( ' id in ('.$idArray.')' );
        $data = $this->fetchAll( $select );

        $i = 1;
        foreach ( $data->toArray() as $item )
        {
            $separator = ( count($data->toArray()) == $i ) ? "" : ", ";
            $servicesNames .= $item['name'].$separator;
            $i++;
        }

        return $servicesNames;
    }
}
