<?php

namespace App\Models\Filters;

class FineFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [
        'date_from', 'date_to', 'keyword', 'city', 'zone', 'status', 'operator'
    ];

    public function apply($builder = null)
    {
        parent::apply();

        if (!$this->request->getParam('date_from')) {
            $this->date_from(date('Y-m-d', strtotime('-2 days')));
        }

        return $this->builder;
    }

    /**
     * @return \Zend_Db_Table_Select
     * @throws \Zend_Auth_Storage_Exception
     * @throws \Zend_Db_Table_Exception
     */
    public static function baseBuilder()
    {
        $fines_model = new \Model_Fines();
        $fines_table = $fines_model::table();

        $blc_model = \Model_BlackListCauses::table();
        $adm_model = \Model_Admins::table();
        $zones_model = new \Model_Zone;
        $zones_table = $zones_model::table();

        $domain_model = new \Model_Domain();
        $options = $domain_model->options();

        $city_model = new \Model_City();
        $userCities = collect($city_model->userCities())->implode('id', ',');

        $select = (new \Zend_Db_Table_Select($fines_model))
            ->setIntegrityCheck(false)
            ->from($fines_table);

        if ($userCities) {
            $available_zones = collect($zones_model->fetchAll(
                (new \Zend_Db_Table_Select(new \Model_Zone))->where("city_id IN ({$userCities})")
            ))->implode('id', ',');
            $select->where("zone IN ({$available_zones})");
        }

        $select->joinLeft(
            $blc_model,
            $fines_table.'.causes_id = '.$blc_model.'.id',
            $blc_model.'.title as cause'
        );

        if ($options['apps_username'] == true) {
            $select->joinLeft(
                $adm_model,
                $fines_table . '.admin_id = ' . $adm_model . '.id',
                'CONCAT(' . $adm_model . '.adm_name," ",' . $adm_model . '.adm_surname) as admin'
            );
        }else{
            $select->joinLeft(
                $adm_model,
                $fines_table . '.adm_device_id = ' . $adm_model . '.adm_device_id',
                'CONCAT(' . $adm_model . '.adm_name," ",' . $adm_model . '.adm_surname) as admin'
            );
        }

        $select->joinLeft(
            $zones_table,
            "{$fines_table}.zone = {$zones_table}.id",
            [$zones_table.'.zone_code', $zones_table.'.city_id']
        );

        $ct = \Model_City::table();
        $select->joinLeft(
            $ct,
            $zones_table.'.city_id = '.$ct.'.id',
            $ct.'.CityName as city'
        );

        $select->order($fines_table.'.date DESC');

        return $select;
    }

    /**
     * Filter the query by a given date from.
     *
     * @param  string $date_from
     *
     * @return \Zend_Db_Select
     */
    protected function date_from($date_from)
    {
        return $this->builder->where(
            \Model_Fines::table() . '.date >= ?', $date_from.' 00:00:00'
        );
    }

    /**
     * Filter the query by a given date to.
     *
     * @param  string $date_to
     *
     * @return \Zend_Db_Select
     */
    protected function date_to($date_to)
    {
        return $this->builder->where(
            \Model_Fines::table() . '.date <= ?', $date_to.' 23:59:59'
        );
    }

    /**
     * @param $statuses
     *
     * @return \Zend_Db_Select
     */
    protected function status($statuses)
    {
        return $this->builder->where(
            \Model_Fines::table().'.status_id in ('.implode(',', $statuses).')'
        );
    }

    /**
     * @param $operators
     *
     * @return \Zend_Db_Select
     */
    protected function operator($operators)
    {
        return $this->builder->where(
            \Model_Admins::table().'.id in ('.implode(',', $operators).')'
        );
    }

    /**
     * Filter the query by a given keyword.
     *
     * @param  string $keyword
     *
     * @return \Zend_Db_Select
     */
    protected function keyword($keyword)
    {
        $keyword = (string) trim($keyword);

        return $this->builder->where("
            id_public LIKE '%$keyword%' OR
            zone_code LIKE '%$keyword%' OR
            plate LIKE '%$keyword%' OR
            street LIKE '%$keyword%' OR
            adm_name LIKE '%$keyword%' OR
            adm_surname LIKE '%$keyword%'
        ");
    }

    /**
     * @param $cities
     *
     * @return \Zend_Db_Select
     */
    protected function city($cities)
    {
        return $this->builder->where(
            \Model_City::table().'.id in ('.implode(',', $cities).')'
        );
    }

    /**
     * @param $zones
     *
     * @return \Zend_Db_Select
     */
    protected function zone($zones)
    {
        return $this->builder->where(
            \Model_Zone::table().'.id in ('.implode(',', $zones).')'
        );
    }
}