<?php

namespace App\Models\Filters;

class ContradictionFilters extends Filters
{
    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [
        'date_from', 'date_to', 'keyword', 'city', 'zone', 'status'
    ];

    public function apply($builder = null)
    {
        parent::apply();
/*
        if (!$this->request->getParam('date_from')) {
            $this->date_from(date('Y-m-d', strtotime('-2 days')));
        }*/

        return $this->builder;
    }

    /**
     * @return \Zend_Db_Table_Select
     * @throws \Zend_Auth_Storage_Exception
     * @throws \Zend_Db_Table_Exception
     */
    public static function baseBuilder()
    {
        $fines_model = new \Model_Fines();
        $fines_table = $fines_model::table();

        $model_contradiction = new \Model_Contradiction();
        $contradiction_table = $model_contradiction::table();

        $zones_model = new \Model_Zone;
        $zones_table = $zones_model::table();

        $city_model = new \Model_City();
        $city_table = $city_model::table();
        $userCities = collect($city_model->userCities())->implode('id', ',');

        $select = (new \Zend_Db_Table_Select($model_contradiction))
            ->setIntegrityCheck(false)
            ->from($contradiction_table);

        if ($userCities) {
            $available_zones = collect($zones_model->fetchAll(
                (new \Zend_Db_Table_Select(new \Model_Zone))->where("city_id IN ({$userCities})")
            ))->implode('id', ',');
            $select->where("zone IN ({$available_zones})");
        }

        $select->joinLeft(
            $fines_table,
            "{$fines_table}.id = {$contradiction_table}.fine_id and box_fines.u_comment IS NOT NULL",
            [$fines_table.'.plate', $fines_table.'.u_comment', $fines_table.'.id_public as fineId']
        );

        $select->joinLeft(
            $zones_table,
            "{$zones_table}.id = {$fines_table}.zone",
            [$zones_table.'.zone_code', $zones_table.'.city_id']
        );

        $select->joinLeft(
            $city_table,
            "{$city_table}.id = {$zones_table}.city_id",
            [$city_table.'.CityName as city']
        );

        $select->order($contradiction_table.'.add_date DESC');

        return $select;
    }

    /**
     * @param $statuses
     *
     * @return \Zend_Db_Select
     */
    protected function status($statuses)
    {
        return $this->builder->where(
            \Model_Contradiction::table().'.contra_status in ('.implode(',', $statuses).')'
        );
    }

    /**
     * Filter the query by a given keyword.
     *
     * @param  string $keyword
     *
     * @return \Zend_Db_Select
     */
    protected function keyword($keyword)
    {
        $keyword = (string) trim($keyword);

        return $this->builder->where("
            plate LIKE '%$keyword%' OR
            CityName LIKE '%$keyword%' OR
            id_public LIKE '%$keyword%'
        ");
    }

    /**
     * @param $cities
     *
     * @return \Zend_Db_Select
     */
    protected function city($cities)
    {
        return $this->builder->where(
            \Model_City::table().'.id in ('.implode(',', $cities).')'
        );
    }

    /**
     * @param $zones
     *
     * @return \Zend_Db_Select
     */
    protected function zone($zones)
    {
        return $this->builder->where(
            \Model_Zone::table().'.id in ('.implode(',', $zones).')'
        );
    }

    /**
     * Filter the query by a given date from.
     *
     * @param  string $date_from
     *
     * @return \Zend_Db_Select
     */
    protected function date_from($date_from)
    {
        return $this->builder->where(
            \Model_Contradiction::table() . '.add_date >= ?', $date_from.' 00:00:00'
        );
    }

    /**
     * Filter the query by a given date to.
     *
     * @param  string $date_to
     *
     * @return \Zend_Db_Select
     */
    protected function date_to($date_to)
    {
        return $this->builder->where(
            \Model_Contradiction::table() . '.add_date <= ?', $date_to.' 23:59:59'
        );
    }
}