<?php

namespace App\Models\Filters;

abstract class Filters
{
    /**
     * @var \Zend_Controller_Request_Abstract
     */
    protected $request;

    /**
     * The select builder.
     *
     * @var \Zend_Db_Table_Select
     */
    protected $builder;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [];

    /**
     * Create a new Filter instance.
     *
     * @param \Zend_Controller_Request_Abstract $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Apply the filters.
     *
     * @param  \Zend_Db_Table_Select $builder
     * @return \Zend_Db_Table_Select
     */
    public function apply($builder = null)
    {
        $this->builder = $builder ? $builder : static::baseBuilder();

        foreach ($this->getFilters() as $filter) {
            if (method_exists($this, $filter)) {
                $value = $value = $this->request->getParam($filter);

                if (is_array($value)) {
                    $value = array_filter($value);
                }

                if ($value) {
                    $this->$filter($value);
                }
            }
        }

        return $this->builder;
    }

    /**
     * @return \Zend_Db_Table_Select
     */
    public static function baseBuilder()
    {
        return new \Zend_Db_Table_Select();
    }

    /**
     * Fetch all relevant filters from the request.
     *
     * @return array
     */
    public function getFilters()
    {
        return array_intersect(
            $this->filters ,
            array_keys($this->request->getParams())
        );
    }
}
