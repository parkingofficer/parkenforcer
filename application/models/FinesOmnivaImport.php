<?php
/**
 * Created by PhpStorm.
 * User: viktoras
 * Date: 19.4.25
 * Time: 17.05
 */

class Model_FinesOmnivaImport extends Core_Db_Table
{
    protected $_primary = 'id';
    protected $_name = 'box_import_omniva';

    public function importOmnivaXLS ( $file )
    {
        $f_model = new Model_Fines();
        $u_model = new Model_Admins();
        $IL_model = new Model_FinesOmnivaImportLog();
        $fileName = $file['payments']['tmp_name'];
        $uploadFileName = $file['payments']['name'];

        // User info
        $user_id = Core_Auth_Admin::getInstance()->getStorage()->read()->id;
        $userDataArray = $u_model->getUserById( $user_id );
        $nameSurname = $userDataArray['adm_name']." ".$userDataArray['adm_surname'];

        try {
            $inputFileType = PHPExcel_IOFactory::identify($fileName);
            if (!in_array($inputFileType, ["Excel2007", "Excel5", "OOCalc", "Excel2003XML"])) {
                return 2;
            }
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($fileName);
        } catch ( \Exception $e) {
            return 2;
        }

        // Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        // Loop through each row of the worksheet in turn
        $xlsxData = [];

        for ($row = 2; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                null,
                true,
                false);
            // Insert row data array into database
            $exelDataArray = $rowData[0];

            $fineId = $exelDataArray[0];
            $fineExelStatus = md5($exelDataArray[2]);
            $xlsxData [] = $rowData[0];

            if ( !empty($fineId) ) {

                switch ($fineExelStatus) {
                    case '35bcb7bd650f87005cc6263d65233b60':
                        $fineStatus = $f_model::STATUS_PERSON_INFORMED;
                        break;
                    case 'b3132bb19e4c88f6846e90a1cb7e1289':
                        $fineStatus = $f_model::STATUS_NOT_PUBLISHED;
                        break;
                    default:
                        $fineStatus = '';
                        break;
                }

                $where = [];
                $where[] = "id_public = '$fineId'";
                $where[] = "status_id = '".$f_model::STATUS_SENT."'";
                $id_public = $f_model->getFines($where);

                $fineArray = $id_public->toArray();
                $fineArray = $fineArray[0];
                $oldFineStatus = ( !empty( $fineArray['status_id'] ) ) ? $fineArray['status_id'] : 0;

                $insertArray = [
                    'fine_id' => $fineArray['id'],
                    'fine_id_public' => $fineId,
                    'old_status' => $oldFineStatus,
                    'new_status' => $fineStatus,
                    'user_id' => $user_id,
                    'user' => $nameSurname,
                    'xlsx_file_name' => $uploadFileName
                ];

                if ($fineArray['id'] && !empty($fineStatus)) {
                    try {
                        $f_model->changeStatus($fineArray['id'], $fineStatus);
                        $insertArray['action'] = 'Insert';
                        $this->save($insertArray);
                    } catch (\Exception $e) {
                        $insertArray['action'] = 'Error';
                        $insertArray['cause_of_error'] = $e->getMessage();
                        $this->save($insertArray);
                    }
                } else {
                    $insertArray['action'] = 'Error';
                    $insertArray['cause_of_error'] = 'Error - No find fine or Fine status is null';
                    try {
                        $this->save($insertArray);
                    } catch (\Exception $e) {
                    }
                }
            } else {
                $insertArray = [];
                $insertArray['fine_id_public'] = 'Not set';
                $insertArray['user_id'] = $user_id;
                $insertArray['xlsx_file_name'] = $uploadFileName;
                $insertArray['action'] = 'Error';
                $insertArray['cause_of_error'] = 'Error - No all require fields';
                $this->save($insertArray);
            }
        }

        $IL_model->save([
            'user_id' => $user_id,
            'user' => $nameSurname,
            'xlsx_file_name' => $uploadFileName,
            'import_source' => json_encode( $xlsxData )
        ]);

        return true;
    }
}