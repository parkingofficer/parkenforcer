<?php
/**
 * Created by PhpStorm.
 * User: viktoras
 * Date: 19.4.26
 * Time: 08.15
 */

class Model_FinesOmnivaImportLog extends Core_Db_Table
{
    protected $_primary = 'id';
    protected $_name = 'box_import_omniva_log';
}