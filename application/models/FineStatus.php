<?php

/**
 * @category  Models
 * @name FineStatus
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-12-03, 13:31:03
 */
class Model_FineStatus extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_fine_status';
}
