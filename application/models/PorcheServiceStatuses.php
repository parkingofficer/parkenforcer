<?php

/**
 * Porche service statuses.
 *
 * @author Jarek Skuder
 */
class Model_PorcheServiceStatuses extends Core_Db_Table
{
    protected $_name = 'box_porche_service_statuses';

    protected $_primary = 'id';

    function getServiceSmsType ( $sStatusId, $sId )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'id = ?', $sStatusId )
            ->where('service_id = ?', $sId);
        $data = $this->fetchRow( $select );

        if ( isset($data) )
        {
            $data = $data->toArray();
            $smsType = $data['smsType'];
        } else { $smsType = null; }

        return $smsType;
    }

    function getServiceMailType ( $sStatusId, $sId )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'id = ?', $sStatusId )
            ->where('service_id = ?', $sId);
        $data = $this->fetchRow( $select );

        if ( isset($data) )
        {
            $data->toArray();
            $mailType = ( $data['mailType'] ) ? json_decode( $data['mailType'] ) : null;
            $mailType = ( isset($mailType) && $mailType->enable == 1 ) ?  $mailType : null;
        } else { $mailType = null; }

        return $mailType;
    }

    function getPrimaryServiceStatus ( $serviceId ) {

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'master = ?', 1 )
            ->where( 'service_id = ?', $serviceId );
        $data = $this->fetchRow( $select );

        if ( isset($data) )
        {
            $data = $data->toArray();
            $statusId = $data['id'];
        } else { $statusId = null; }

        return $statusId;
    }

    function getBillingId ( $sStatusId, $sId )
    {
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name)
            ->where( 'id = ?', $sStatusId )
            ->where('service_id = ?', $sId);
        $data = $this->fetchRow( $select );

        if ( isset($data) )
        {
            $data = $data->toArray();
            $billingId = $data['billing_id'];
        } else { $billingId = null; }

        return $billingId;
    }
}
