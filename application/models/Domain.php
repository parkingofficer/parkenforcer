<?php

/**
 * Description of Admins.
 *
 * @author Darius Matulionis
 */
class Model_Domain extends Core_Db_Table
{
    protected $_name = 'box_domain';

    protected $_primary = 'id';

    public function domainOptionsDigits()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $options = $this->fetchRow("name like '%$domain%'");
        $digits = $options['digits_after_price'];

        return $digits;
    }

    public function domainOptionsCents()
    {
        $domain = $_SERVER['HTTP_HOST'];

        $options = $this->fetchRow("name like '%$domain%'");
        if (! empty($options['parking_price_is_cent'])) {
            $cent = 100;
        } else {
            $cent = 1;
        }

        return $cent;
    }

    public function options()
    {
        $domain = $_SERVER['HTTP_HOST'];
        $domain_model = new self();
        $options = $domain_model->fetchRow("name like '%$domain%'");

        return $options;
    }
}
