<?php

/**
 * Description of Admins.
 *
 * @author Darius Matulionis
 */
class Model_AdminGroups extends Core_Db_Table
{
    protected $_name = 'box_admin_groups';

    public function hideOperators($title)
    {
        $select = $this->select()->setIntegrityCheck(false)
            ->from($this->_name, $this->_name.'.hide_operators')
            ->where("title = '$title'");

        $data = $this->getAdapter()->fetchRow($select);

        return $data;
    }

    public function hideBlitzinfo($title)
    {
        $select = $this->select()->setIntegrityCheck(false)
            ->from($this->_name, $this->_name.'.hide_blitzinfo')
            ->where("title = '$title'");

        $data = $this->getAdapter()->fetchRow($select);

        return $data;
    }

    public function hidePorcheinfo($title)
    {
        $select = $this->select()->setIntegrityCheck(false)
            ->from($this->_name, $this->_name.'.hide_porche')
            ->where("title = '$title'");

        $data = $this->getAdapter()->fetchRow($select);

        return $data;
    }

    public function hideDashboard($title)
    {
        $select = $this->select()->setIntegrityCheck(false)
            ->from($this->_name, $this->_name.'.hide_dashboard')
            ->where("title = '$title'");

        $data = $this->getAdapter()->fetchRow($select);

        return $data;
    }

    public function hideAppreg($title)
    {
        $select = $this->select()->setIntegrityCheck(false)
            ->from($this->_name, $this->_name.'.hide_appreg')
            ->where("title = '$title'");

        $data = $this->getAdapter()->fetchRow($select);

        return $data;
    }

    public function getPermissions($title)
    {
        $select = $this->select()->setIntegrityCheck(false)
            ->from($this->_name, $this->_name.'.permisions')
            ->where("title = '$title'");

        $data = $this->getAdapter()->fetchRow($select);

        return unserialize($data['permisions']);
    }

    public function getOperatorGroups()
    {
        $options = $this->fetchAll("role in ('admin','operator')");
        $groups = [];
        foreach ($options as $admin) {
            $groups[] = $admin['id'];
        }

        return $groups;
    }

    /**
     * @return \Zend_Db_Table_Rowset_Abstract
     */
    public function getUserOperators($userCities)
    {
        $citiesArray = [];
        foreach($userCities as $city)
        {
            $citiesArray [] = $city['id'];
        }
        $user_cities = new Model_UserCities();
        $select2 = $user_cities->select()->distinct()
            ->from(['z' => $user_cities->info('name')], ['z.user_id'])
            ->where("z.active = 1 and z.city_id in ('".implode("','",$citiesArray). "'");

        //select active admins who are added for given cities
        return (new Model_Admins())
            ->fetchAll(
                'admin_groups_id in('.implode(',', $this->getOperatorGroups()).') AND
                adm_active = 1) and id in ('.$select2.')'
            );

    }
}
