<?php

class Model_ZoneCauseRates extends Core_Db_Table
{
    protected $_primary = 'zone_id';

    protected $_name = 'box_zone_causes_rates';

    public static function sync($zone_id, $rates)
    {
        $model = (new static);
        $model->getAdapter()->beginTransaction();
        try {
            (new static)->delete('zone_id = '.$zone_id);

            foreach ($rates as $rate) {
                $model->insert($rate);
            }

            $model->getAdapter()->commit();
        } catch (\Exception $e) {
            $model->getAdapter()->rollBack();
            throw new \Exception('Please try again. '.$e->getMessage());
        }
    }
}
