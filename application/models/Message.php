<?php

/**
 * @category  Models
 * @name Message
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2014-07-07, 15:50:12
 */
class Model_Message extends Core_Db_Table
{
    protected $_primary = 'message_id';

    protected $_name = 'message';
}
