<?php

/**
 * Description of Admins.
 *
 * @author Robert Lechovič
 */
class Model_BlitzStatuses extends Core_Db_Table
{
    protected $_name = 'box_blitz_statuses';

    protected $_primary = 'id';

    public function getlist()
    {
        return $this->fetchall();
    }
}
