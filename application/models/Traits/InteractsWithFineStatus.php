<?php

namespace App\Models\Traits;

trait InteractsWithFineStatus
{
    /*
    0 - Įspėjimas (rašomas kaip ir dabar)
    2 - Apmokėtas (vietoje susimokėjo per 24 val) (kai apmokamas įspėjimas)
    3 - Neapmokėtas (vietoje Neišsiųsta bauda) (kaip ir dabar)
    4 - Informuotas - iš bilingo sistemos (integracijos būdu) gauname info apie statuso pakeitimą     (klientui buvo siųstas priminimas, kad bauda nebuvo apmokėta (sms arba e-mailu))
    5 - Bauda (kai paspaudžiamas mygtukas suformuoti baudas)
    6 - Įspėtas - iš bilingo sistemos (integracijos būdu) gauname info apie statuso pakeitimą     (išsiųstas perspėjimas, kad bauda bus perduota išieškotojams (sms arba e-mailu))
    7 - Anuliuota bauda (kai adminas anuliuoja baudą    )
    8 -
    9 - Išsiųstas bauda (Savivaldybės darbuotojas rankiniu būdu pakeičia baudos statusą (EE funkcionalumas) )
    10 - Informacija apie skirtą baudą nėra paviešinta (EE funkcionalumas, statusas pasikeičia kai įkeliamas Omniva excel‘is)
    11 - Publikuota išrašyta bauda (Savivaldybės darbuotojas rankiniu būdu pakeičia baudos statusą, EE funkcionalumas )
    12 - išrašyta bauda perduota išieškotojams (EE funkcionalumas)
    */

    /**
     * @return array|null
     */
    public function statusLabel()
    {
        return self::statuses($this->status_id);
    }

    /**
     * @param null $key
     *
     * @return \Illuminate\Support\Collection|null
     */
    public static function statuses($key = null)
    {
        $domain_model = new \Model_Domain();
        $options = $domain_model->options();

        if (strpos($options['name'], 'parkingee.unipark.lt') !== false) {
            $statuses = [
                self::STATUS_NOTICE => \Core_Locale::translate('Notification'), //'Bauda',
                self::STATUS_FINE_PAYED => \Core_Locale::translate('Paid'), //'Apmokėtas',
                self::STATUS_SENT => \Core_Locale::translate('Sent'), //'Išsiųstas bauda',
                self::STATUS_PERSON_INFORMED => \Core_Locale::translate('Informed'), //'Informuotas',
                self::STATUS_NOT_PUBLISHED => \Core_Locale::translate('Not Published'), //'Nubausojo duomenys nepaviešinti',
                self::STATUS_PUBLISHED => \Core_Locale::translate('Published'), //'Nubausojo duomenys nepaviešinti',
                self::STATUS_ENFORCEMENT => \Core_Locale::translate('Enforcement'), //'Nubausojo duomenys nepaviešinti',
                self::STATUS_FINE_DISMISSED => \Core_Locale::translate('Dismissed'), //'Anuliuota bauda',
                self::STATUS_FINE_GENERATED => \Core_Locale::translate('Generated'), //'Bauda',
            ];
        } else {
            $statuses = [
                self::STATUS_NOTICE => \Core_Locale::translate('Notified'), //'Įspėjimas',
                self::STATUS_FINE_PAYED => \Core_Locale::translate('Paid'), //'Apmokėtas',
                self::STATUS_WAITING_FOR_FINE_GENERATION => \Core_Locale::translate('Waiting for generation'), //'Neapmokėtas',
                self::STATUS_PERSON_INFORMED => \Core_Locale::translate('Informed'), //'Informuotas',
                self::STATUS_FINE_GENERATED => \Core_Locale::translate('Generated'), //'Bauda',
                self::STATUS_PERSON_NOTICED => \Core_Locale::translate('Noticed'), //'Įspėtas',
                self::STATUS_FINE_DISMISSED => \Core_Locale::translate('Dismissed'), //'Anuliuota bauda',
            ];
        }

        if ($key) {
            return array_key_exists($key, $statuses) ? $statuses[$key] : null;
        }

        return collect($statuses);
    }
}
