<?php

/**
 * @category  Models
 * @name City
 * @copyright Robert Lechovič
 * @author Robert Lechovič <robert@itgirnos.lt>
 * @since : 2013-11-25, 10:22:27
 */
class Model_Zone extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_zone';

    public function getWlist()
    {
        $userCities = new Model_ZonesWhitelists();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $userCities->info('name'),
            $userCities->info('name').'.zone_id = '.$this->_name.'.id'.' AND '.$userCities->info('name').'.active = 1',
            $userCities->info('name').'.wg_list_id'
        );
        $cities = new Model_WhiteListGroups();
        $select->joinLeft(
            $cities->info('name'),
            $cities->info('name').'.id = '.$userCities->info('name').'.wg_list_id',
            $cities->info('name').'.title'
        );

        $wlg = $this->getAdapter()->fetchAll($select);

        $items = [];

        foreach ($wlg as $w) {
            $cities = '';
            if (isset($items[$w['id']])) {
                $cities = $items[$w['id']]['title'];
            }
            $w['title'] = $w['title'].'<br/>'.$cities;

            $items[$w['id']] = $w;
        }

        return $items;
    }

    public function getUserInfo($id)
    {
        $user_city_model = new Model_UserCities();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $user_city_model->info('name').' as uc',
            'uc.city_id = '.$this->_name.'.city_id',
            ['uc.user_id', $this->_name.'.id as zone_id']
        );
        $select->where('uc.active = 1');
        $select->where('uc.user_id ='.$id);

        // $select->order('id DESC');

        return $this->getAdapter()->fetchAll($select);
    }

    public function getZoneID($billingID)
    {
        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->where($this->_name.'.billing_id ='.$billingID);

        return $this->getAdapter()->fetchRow($select);
    }

    public function getCity($billingID)
    {
        $cityModel = new Model_City();
        $select = $this
            ->select()
            ->setIntegrityCheck(false)
            ->from($this->_name);
        $select->joinLeft(
            $cityModel->info('name'),
            $this->_name.'.city_id='.$cityModel->info('name').'.id',
            $cityModel->info('name').'.id'
        );
        $select->where('billing_id ='.$billingID);

        return $this->getAdapter()->fetchRow($select);
    }

    public function getWPrice($zone_id)
    {
        $select = $this
            ->select()
            ->from($this->_name);
        $select->where($this->_name.'.id = '.$zone_id);

        return $this->getAdapter()->fetchRow($select);
    }

    public function getZoneAddress ( $zoneId )
    {
        $select = $this
            ->select('zone_address')
            ->from($this->_name);
        $select->where($this->_name.'.id = '.$zoneId);

        return $this->getAdapter()->fetchRow($select);
    }
}
