<?php

/**
 * @category  Models
 * @name Parking
 * @copyright Darius Matulionis
 * @author    Darius Matulionis <darius@matulionis.lt>
 */
class Model_Parking extends Core_Db_Table
{
    protected $_primary = 'id';
    protected $_name = 'box_parkings';

    public function getCityZone($where = [])
    {
        $domain = new Model_Domain();
        $cent = $domain->domainOptionsCents();
        $digits = $domain->domainOptionsDigits();
        $zones_model = new Model_Zone();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.id = ' . $this->_name . '.zone',
            [$zones_model->info('name') . '.zone_code', $zones_model->info('name') . '.city_id']
        );
        $select->columns([
            new Zend_Db_Expr('CONCAT(UPPER(LEFT(operator_name, 1)), SUBSTRING(operator_name, 2)) as operator'),
            new Zend_Db_Expr('FORMAT((price_cent / ' . $cent . '),' . $digits . ') as price'),
        ]);
        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.city_id',
            $city_model->info('name') . '.CityName as city'
        );

        $user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name') . '.city_id = ' . $zones_model->info('name') . '.city_id',
            $user_id->info('name') . '.user_id'
        );
        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        $select->order('date_start DESC ');

        return $this->getAdapter()->fetchAll($select);
    }

    public function getCityZone2()
    {
        $domain = new Model_Domain();
        $cent = $domain->domainOptionsCents();
        $digits = $domain->domainOptionsDigits();
        $zones_model = new Model_Zone();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.id = ' . $this->_name . '.zone',
            [$zones_model->info('name') . '.zone_code', $zones_model->info('name') . '.city_id']
        );
        $select->columns([
            new Zend_Db_Expr('CONCAT(UPPER(LEFT(operator_name, 1)), SUBSTRING(operator_name, 2)) as operator'),
            new Zend_Db_Expr('FORMAT((price_cent / ' . $cent . '),' . $digits . ') as price'),
        ]);
        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.city_id',
            $city_model->info('name') . '.CityName as city'
        );

        $user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name') . '.city_id = ' . $zones_model->info('name') . '.city_id and active = 1',
            $user_id->info('name') . '.user_id'
        );

        return $select;
    }

    public function getCityZoneCsv($where)
    {
        $domain = new Model_Domain();
        $cent = $domain->domainOptionsCents();
        $digits = $domain->domainOptionsDigits();
        $zones_model = new Model_Zone();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.id = ' . $this->_name . '.zone',
            [
                $zones_model->info('name') . '.zone_code',
                $zones_model->info('name') . '.city_id',
                $zones_model->info('name') . '.zone_name'
            ]
        );
        $select->columns([
            new Zend_Db_Expr('CONCAT(UPPER(LEFT(operator_name, 1)), SUBSTRING(operator_name, 2)) as operator'),
            new Zend_Db_Expr('FORMAT((price_cent / ' . $cent . '),' . $digits . ') as price'),
        ]);
        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.city_id',
            $city_model->info('name') . '.CityName as city'
        );
        /*$user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name').'.city_id = '.$zones_model->info('name').'.city_id',
            $user_id->info('name').'.user_id'
        );*/

        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchAll($select);
    }

    public function getCityZoneAtaskaita($where)
    {
        $cent = $this->domainOptionsCents();
        $digits = $this->domainOptionsDigits();
        $zones_model = new Model_Zone();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.id = ' . $this->_name . '.zone',
            [
                $zones_model->info('name') . '.zone_code',
                $zones_model->info('name') . '.city_id',
                $zones_model->info('name') . '.zone_name'
            ]
        );
        $select->columns([
            new Zend_Db_Expr('CONCAT(UPPER(LEFT(operator_name, 1)), SUBSTRING(operator_name, 2)) as operator'),
            new Zend_Db_Expr('FORMAT((price_cent / ' . $cent . '),' . $digits . ') as price'),
            new Zend_Db_Expr('SUM(price_cent) as sum'),
        ]);
        $city_model = new Model_City();
        $select->joinLeft(
            $city_model->info('name'),
            $city_model->info('name') . '.id = ' . $zones_model->info('name') . '.city_id',
            $city_model->info('name') . '.CityName as city'
        );
        $user_id = new Model_UserCities();
        $select->joinLeft(
            $user_id->info('name'),
            $user_id->info('name') . '.city_id = ' . $zones_model->info('name') . '.city_id',
            $user_id->info('name') . '.user_id'
        );

        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchAll($select);
    }

    public function getActiveParking($phone)
    {
        $zone = new Model_Zone();
        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zone->info('name'),
            $zone->info('name') . '.id = ' . $this->_name . '.zone',
            $zone->info('name') . '.usms_zone'
        );
        $select->where("phone = '$phone' and date_start <= NOW() and date_end >= NOW() ")->order('date_inserted DESC');

        return $this->fetchAll($select);
    }

    public function getOrdersForModus($where)
    {
        $zone_model = new Model_Zone();

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->joinLeft(
            $zone_model->info('name'),
            $zone_model->info('name') . '.id =' . $this->_name . '.zone',
            [$zone_model->info('name') . '.zone_name']
        );

        if (!empty($where)) {
            foreach ($where as $w) {
                $select->where($w);
            }
        }

        return $this->fetchAll($select);
    }

    public function getParkingsByPeriodAndUser($from, $to, $user)
    {
        $zones = new Model_UserCities();
        $visibleCities = $zones->select()->from(['z' => $zones->info('name')],
            ['z.city_id'])->where('active = 1 and user_id = ?',
            (int)$user);//gaunu prisijungusio varotojo matomus miestus,
        $zones_model = new Model_Zone();
        $visibleZones = $zones_model->select()->from(['z' => $zones_model->info('name')],
            ['z.id'])->where("city_id in ($visibleCities)");
        $domain = new Model_Domain();

        $select = $this->select()->setIntegrityCheck(false)->from($this->_name);
        $select->where("date_start BETWEEN '$from' AND '$to'");
        $select->where("zone in ($visibleZones)");
        $select->order('date_start DESC');
        $select->joinLeft(
            $zones_model->info('name'),
            $zones_model->info('name') . '.id = ' . $this->_name . '.zone',
            [$zones_model->info('name') . '.zone_code as zone_name']
        );

        return $this->fetchAll($select);
    }

    public function checkEstoniaTeliaParking($plate, $zone)
    {
        $domain = new Model_Domain();
        $keyModel = new Model_EstoniaTeliaKey();
        $options = $domain->options();
        if (strpos($options['name'], 'parkingee.unipark.lt') !== false) {
            $client = new SoapClient('https://web.emt.ee:9112/?wsdl', array("soap_version" => SOAP_1_1, "trace" => 1));
            $soapVar = new SoapVar(["session_id" => $keyModel->getSessionId()], SOAP_ENC_OBJECT);
            $header = new SoapHeader('http://soapinterop.org/echoheader/', 'SessionHeader', $soapVar);

            $params = array(
                "search" => $plate,
            );

            $client->__setSoapHeaders([$header]);

            try {
                $response = $client->__soapCall("ParkingCheck", [$params]);
            } catch (SoapFault $e) {
                die($e);
            }

            $model = new Model_Zone();
            $zoneObject = $model->fetchRow('id =' . $zone);

            if ($response->ParkingStatus == 'Y' && $response->ParkingZone == $zoneObject['zone_code'] && $response->ParkingEndTime == '') {
                $from = (new DateTime($response->ParkingStartTime))->format('Y-m-d H:i:s');
                if($response->ParkingEndTime){
                $to = (new DateTime($response->ParkingEndTime))->format('Y-m-d H:i:s');
                }else{
                    $to = null;
                }
                return ['zone' => $response->ParkingZone, 'from' => $from, 'to' => $to];
            }
        }
        return false;

    }
}
