<?php

/**
 * @category  Models
 * @name WhiteListGroups
 * @copyright Darius Matulionis
 * @author Darius Matulionis <darius@matulionis.lt>
 * @since : 2013-11-26, 15:26:33
 */
class Model_WhiteListGroups extends Core_Db_Table
{
    protected $_primary = 'id';

    protected $_name = 'box_white_list_groups';
}
