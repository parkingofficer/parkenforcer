<?php

/**
 * Crete/update Estonian Telia session id.
 *
 * @author Viktoras
 */
class Model_EstoniaTeliaKey extends Core_Db_Table
{
    protected $_name = 'box_estonia_key';
    protected $_primary = 'id';

    public function getSessionId ()
    {
        $options = $this->fetchRow()->toArray();
        if ( count ( $options ) ) {
           return $options['session_id'];
        } else {
            return false;
        }

    }
    public function getKey ()
    {
        $options = $this->fetchAll()->toArray();
        if ( count ( $options ) ) {
           return true;
        } else {
            return false;
        }

    }

    public function setKey ( $sessionId )
    {
        $getKey = $this->getKey();
        if ( $getKey ) {
            try {
                $dataArray = $this->fetchAll()->toArray();
                foreach ( $dataArray as $data )
                {
                    $this->update( array( 'session_id' => $sessionId, 'date' => date("Y-m-d H:i:s") ), 'id ='. $data['id'] );
                }
            } catch (Exception $e) {
                die($e->getMessage());
            }
        } else {
            try {
                $this->insert( array( 'session_id' => $sessionId, 'date' => date("Y-m-d H:i:s") ) );
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

    }
}
