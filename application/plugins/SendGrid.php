<?php

class Plugin_SendGrid extends Zend_Controller_Plugin_Abstract
{

    protected $apiKey;
    protected $sender;
    protected $subject;

    public function __construct()
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');

        $this->apiKey = $config->getOption('SENDGRID_API_KEY');
        $this->sender = $config->getOption('SENDGRID_EMAIL_SENDER');
        $this->subject = $config->getOption('SENDGRID_EMAIL_SUBJECT');
        //$this->getToken();

    }


    public function sendMail($data, $receiver, $templateId){

        //example data
        //$data = [
        //"-name-" => "Example User 3",
        //"-city-" => "Redwood City"
        //];

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($this->sender);
        $email->setSubject($this->subject);
        $email->addTo($receiver,null,$data);
        $email->setTemplateId($templateId);


        $sendgrid = new \SendGrid($this->apiKey);
        $log = new Model_SendgridLog();
        try {

            $response = $sendgrid->send($email);
            $log->save([
                'request' => json_encode($email),
                'response'=> json_encode($response->statusCode().' '.$response->body()),
                'timestamp' => (new DateTime())->format('Y-m-d H:i:s'),
                'template_id' => $templateId
            ]);
        } catch (Exception $e) {
            $log->save([
                'request' => json_encode($email),
                'response'=> json_encode($e->getMessage()),
                'timestamp' => new DateTime(),
                'template_id' => $templateId
            ]);
        }

    }
}
