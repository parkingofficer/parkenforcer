<?php

class Plugin_Init extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        //$this->_initSessionParams();
        //$this->_initViewHelpers();
        $this->_initDefaultViewParams();
    }

    protected function _initSessionParams()
    {
    }

    protected function _initDefaultViewParams()
    {
    }

    protected function _initViewHelpers()
    {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout('layout')->setLayoutPath(APPLICATION_PATH.'/views/scripts');
        $view = $layout->getView();

        $view->addHelperPath('Core/View/Helper/', 'Core_View_Helper');

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
    }
}
