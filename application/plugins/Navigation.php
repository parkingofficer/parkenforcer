<?php

/**
 * Description of Navigation.
 *
 * @author Darius Matulionis
 */
class Plugin_Navigation extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->view->t = Zend_Registry::get('translator');
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
    }

    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() == 'default' && $request->getActionName() != 'image') {
            $model = new Model_Articles(); // some workaround for not connecting every time an image is loaded

            $obj = new Core_Db_Tree_Structure([
                'db' => 'db',
                'table' => 'box_articles',
                'title' => 'title_'.LANG,
                'pid' => 'pid',
            ]);

            //die(print_r($obj->getStructure(1, 1)));
            //die(var_dump($obj->getNavigationArray($obj->getStructure(1, 0))));

            $arr = $obj->getNavigationArray($obj->getStructure(1, 0));

            $container = new Zend_Navigation($arr);

            $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');

            $viewRenderer->view->navigation($container);

            $namespace = new Zend_Session_Namespace(); // default namespace

            $viewRenderer->view->session = $namespace;

            /**
             * generating path.
             */
            $path = [];
            $i = 0;

            $container = $viewRenderer->view->navigation()->getContainer();

            $active = $viewRenderer->view->navigation()->findActive($container);

            if (isset($active['page'])) {
                $active = $active['page'];

                $path[] = $active;

                while ($parent = $active->getParent()) {
                    if ($parent instanceof Zend_Navigation_Page) {
                        // prepend crumb to html
                        $path[] = $parent;
                    }

                    if ($parent === $container) {
                        // at the root of the given container
                        break;
                    }

                    $active = $parent;
                }

                $pth = $path;
                $count = count($pth) - 1;

                for ($i = 0; $i <= $count; $i++) {
                    $path[$i] = $pth[$count - $i];
                }
            }

            $viewRenderer->view->path = $path;
            Zend_Registry::set('path', $path);
        }
    }
}
