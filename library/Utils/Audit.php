<?php
class Audit{

	public static function contractAudit($cntr,$cntr_id){
		$audit_model = new models_contracts_contractsAuditModel ();
		$session = Zend_Registry::get('session');

		$audit_cntr = array();
		$audit_cntr['cntr_id_FK'] = $cntr_id;
		$audit_cntr['user_id_FK'] = '';
		$audit_cntr['audit_time'] = date('Y-m-d H:m:s');
		$audit_cntr['audit_ip'] = $_SERVER['REMOTE_ADDR'];

		foreach ($cntr as $k=>$c){
			$audit_cntr["audit_".$k] = $c;
		}

		$audit_model->insertOne($audit_cntr);
	}
}
?>