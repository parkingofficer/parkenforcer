<?php
class Utils_Seo extends Zend_Controller_Plugin_Abstract
{

	public function __construct($ctrl)
	{
		$request = new Zend_Controller_Request_Http();
		$url = $request->getRequestUri();

		$uriComponents = explode('/', $url);

		if(!is_file(ZETS_PATH_ROOT."/application/controllers/".ucfirst($uriComponents[1])."Controller.php")){
			$view = Zend_Registry::get('view');
			$view->path = $uriComponents[1];
			$route = new Zend_Controller_Router_Route(
			':p_link',
			array(
				'controller'    => 'pages',
				'action'        => 'view',
			),
			array('link' => '/^[a-zA-Z0-9-]+$/')
			);
			$ctrl->addRoute('pages', $route);
		}

	}
}
?>