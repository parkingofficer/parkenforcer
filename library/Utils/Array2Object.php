<?php

class Array2Object
{
    public static function get($array)
    {
        //$obj = new Array2Object();

        $return = new stdClass();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$return->$k = Array2Object::get($v);
			}
			else {
				$return->$k = $v;
			}
		}

		return $return;
    }

    public static function getArray($object)
    {
    	$arr = array();
		if(is_object($object)){
	    	foreach($object as $k => $v) {
	    		$arr[$k] = $v;
	    	}
	
	    	return $arr;
		}
		return array();
    }

    public function __get($key)
    {
        return isset($this->$key) ? $this->$key : '';
    }
}
?>