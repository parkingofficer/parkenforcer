<?php
/**
 * @name mPdf.php
 * @author darius.matulionis
 */
set_time_limit(0);
class Core_Pdf_FromHtml extends \Mpdf\Mpdf
{
    public $tblHeaderCss = 'text-align:left;vertical-align:middle;border-color:#000;font-weight:bold;border-bottom:0.5px solid #000;background-color:#a6a6a6;';
    public $tblHeaderCssNoBg = 'text-align:left;vertical-align:middle;border-color:#000;font-weight:bold;border-bottom:0.5px solid #000;';
    public $tblRowCss = 'vertical-align:middle;border-color:#000;border-top:0.5px solid #000;border-bottom:0.5px solid #000;';
    public $brdRight = 'border-right:0.5px solid #000;';
    public $brdLeft = 'border-left:0.5px solid #000;';
    public $brdBtn = 'border-bottom:0.5px solid #000;';

    /**
     * Core_Pdf_FromHtml constructor.
     *
     * @param string $mode
     * @param string $format
     * @param int $default_font_size
     * @param string $default_font
     * @param int $mgl
     * @param int $mgr
     * @param int $mgt
     * @param int $mgb
     * @param int $mgh
     * @param int $mgf
     * @param string $orientation
     *
     * @throws \Mpdf\MpdfException
     */
    public function __construct($mode = '', $format = 'A4', $default_font_size = 0, $default_font = '', $mgl = 15, $mgr = 10, $mgt = 10, $mgb = 16, $mgh = 9, $mgf = 9, $orientation = 'P')
    {
        $format = $orientation == 'L' ? $format.'-'.$orientation : $format;

        parent::__construct([
            'mode' => $mode,
            'format' => $format,
            'default_font_size' => $default_font_size,
            'default_font' => $default_font,
            'margin_left' => $mgl,
            'margin_right' => $mgr,
            'margin_top' => $mgt,
            'margin_bottom' => $mgb,
            'margin_header' => $mgh,
            'margin_footer' => $mgf,
            'orientation' => $orientation
        ]);
    }

    public function splitToRows($title, $spiltRow)
    {
        $words = explode(' ', $title);
        $titles = [];
        $row = '';
        foreach ($words as $k => $w) {
            if (strlen($row.''.$w) < $spiltRow) {
                $row = $row.' '.$w;
                //var_dump($row);
            } else {
                $titles[] = trim($row);
                $row = $w;
            }

            if ($k + 1 == count($words) && $row) {
                $titles[] = trim($row);
            }
        }

        return $titles;
    }
}
