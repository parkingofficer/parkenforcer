<?php

/**
 * Uteliu klase.
 */
class Core_Utils
{
    /**
     * @param string $buffer
     * @param unknown $fileName
     * @param string $fileTitle
     * @param string $contentType
     * @throws \Exception
     */
    public static function serveFilePartial($buffer = null, $fileName, $fileTitle = null, $contentType = 'application/pdf')
    {
        //define( 'APPLICATION_ENV', 'development');

        $fileName = TMP_FILES_PATH.'/'.$fileName;
        if ($buffer) {
            //die(var_dump($fileName));
            $f = fopen($fileName, 'wb');
            if (! $f) {
                throw new \Exception(sprintf('Unable to create output file:  %s', $fileName));
            }
            fwrite($f, $buffer, strlen($buffer));
            fclose($f);
        }
        //var_dump($fileName);
        //die();

        if (! file_exists($fileName)) {
            throw new \Exception(sprintf('File not found: %s', $fileName));
        }
        if (! is_readable($fileName)) {
            throw new \Exception(sprintf('File not readable: %s', $fileName));
        }
        ### Remove headers that might unnecessarily clutter up the output
        header_remove('Cache-Control');
        header_remove('Pragma');

        $fm = fopen($fileName, 'rb');
        $size = filesize($fileName);
        $time = date('r', filemtime($fileName));
        $begin = 0;
        $end = $size - 1;

        if (isset($_SERVER['HTTP_RANGE'])) {
            if (preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches)) {
                $begin = intval($matches[1]);
                if (! empty($matches[2])) {
                    $end = intval($matches[2]);
                }
            }
        }
        if (isset($_SERVER['HTTP_RANGE'])) {
            header('HTTP/1.1 206 Partial Content');
        } else {
            header('HTTP/1.1 200 OK');
        }

        header("Content-Type: $contentType");
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Pragma: no-cache');
        header('Accept-Ranges: bytes');
        header('Content-Length:'.(($end - $begin) + 1));
        if (isset($_SERVER['HTTP_RANGE'])) {
            header("Content-Range: bytes $begin-$end/$size");
        }
        header("Content-Disposition: inline; filename=$fileTitle");
        header('Content-Transfer-Encoding: binary');
        header("Last-Modified: $time");
        header("Buff-sizes: $begin-$end");

        $cur = $begin;
        fseek($fm, $begin, 0);
        $bufferSize = 512 * 16; ### Just a reasonable buffer size
        //ignore_user_abort(true);
        //if($end >= $size || feof($fm) || connection_aborted())
        //unlink($fileName);

        while (! feof($fm) && $cur <= $end && (connection_status() == 0)) {
            print fread($fm, min($bufferSize, ($end - $cur) + 1));
            $cur += $bufferSize;
        }

        //if($cur > $end || feof($fm) || connection_aborted())
        if ( file_exists( $fileName ) ) { unlink($fileName); }

        exit();
    }

    /**
     * Convert a comma separated file into an associated array.
     * The first row should contain the array keys.
     *
     * Example:
     *
     * @param string $filename Path to the CSV file
     * @param string $delimiter The separator used in the file
     * @return array
     * @link http://gist.github.com/385876
     * @author Jay Williams <http://myd3.com/>
     * @copyright Copyright (c) 2010, Jay Williams
     * @license http://www.opensource.org/licenses/mit-license.php MIT License
     */
    public static function csv_to_array($filename = '', $delimiter = ',')
    {
        if (! file_exists($filename) || ! is_readable($filename)) {
            return false;
        }
        $header = null;
        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (! $header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }

        return $data;
    }

    /**
     * @desc assignina laukus i objekta
     * @return object jeigu ok, exception string jeigu no good
     */
    public static function assignObjectValues($obj, $arr)
    {
        try {
            if (! is_array($arr) || empty($arr)) {
                throw new Exception('Invalid parameter array!');
            }
            foreach ($arr as $key => $value) {
                if (isset($obj->$key)) {
                    $obj->$key = $value;
                }
            }

            return $obj;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function makeDatePlus($date, $plusYear = null, $plusMoth = null, $plusDays = null)
    {
        $date = str_replace('-', '', $date);
        $year = substr($date, 0, 4);
        $moth = substr($date, 4, 2);
        $days = substr($date, 6, 2);

        return date('Y-m-d', mktime(0, 0, 0, $moth + $plusMoth, $days + $plusDays, $year + $plusYear));
    }

    public static function dateDiff($LaterDate, $EarlierDate, $time)
    {
        $LaterDate = str_replace('-', '', $LaterDate);
        $EarlierDate = str_replace('-', '', $EarlierDate);

        $L_year = substr($LaterDate, 0, 4);
        $L_moth = substr($LaterDate, 4, 2);
        $L_days = substr($LaterDate, 6, 2);

        $E_year = substr($EarlierDate, 0, 4);
        $E_moth = substr($EarlierDate, 4, 2);
        $E_days = substr($EarlierDate, 6, 2);

        $dateDiff = mktime(0, 0, 0, $E_moth, $E_days, $E_year) - mktime(0, 0, 0, $L_moth, $L_days, $L_year);

        if ($time == 'days') {
            return floor(($dateDiff / 60 / 60 / 24));
        } elseif ($time == 'months') {
            $dateLow = date('Y-m-d', mktime(0, 0, 0, $L_moth, $L_days, $L_year));
            $in_dateHigh = date('Y-m-d', mktime(0, 0, 0, $E_moth, $E_days, $E_year));
            $dateHigh = strftime('%m/%Y', strtotime($in_dateHigh));

            $periodDiff = 0;
            while (strftime('%m/%Y', strtotime($dateLow)) != $dateHigh) {
                $periodDiff++;
                $dateLow = date('Y-m-d', strtotime('+1 month', strtotime($dateLow)));
            }

            return $periodDiff;
        } elseif ($time == 'years') {
            return floor($dateDiff / 365 / 60 / 60 / 24);
        } else {
            return false;
        }
    }

    /**
     * @desc generuoja microtime rand skaiciu
     * @return randNumber
     */
    public static function random()
    {
        $timearray = explode(' ', microtime());
        $time = $timearray [0].$timearray [1];
        $time = str_replace('0.', '', $time);

        return $time;
    }

    /**
     * @desc password generator
     * @param $length int password lenght
     * @param $strength int sudetingumo lygis (1,2,4,8)
     * @return object
     */
    public static function generatePassword($length = 9, $strength = 4)
    {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= 'AEUY';
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$%';
        }

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants [(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels [(rand() % strlen($vowels))];
                $alt = 1;
            }
        }

        return $password;
    }

    public static function cuttext($value, $length)
    {
        if (is_array($value)) {
            list($string, $match_to) = $value;
        } else {
            $string = $value;
            $match_to = $value {0};
        }
        if (! empty($string) && ! empty($match_to)) {
            $match_start = stristr($string, $match_to);
            $match_compute = strlen($string) - strlen($match_start);

            if (strlen($string) > $length) {
                if ($match_compute < ($length - strlen($match_to))) {
                    $pre_string = substr($string, 0, $length);
                    $pos_end = strrpos($pre_string, ' ');
                    if ($pos_end === false) {
                        $string = $pre_string.'...';
                    } else {
                        $string = substr($pre_string, 0, $pos_end).'...';
                    }
                } elseif ($match_compute > (strlen($string) - ($length - strlen($match_to)))) {
                    $pre_string = substr($string, (strlen($string) - ($length - strlen($match_to))));
                    $pos_start = strpos($pre_string, ' ');
                    $string = '...'.substr($pre_string, $pos_start);
                    if ($pos_start === false) {
                        $string = '...'.$pre_string;
                    } else {
                        $string = '...'.substr($pre_string, $pos_start);
                    }
                } else {
                    $pre_string = substr($string, ($match_compute - round(($length / 3))), $length);
                    $pos_start = strpos($pre_string, ' ');
                    $pos_end = strrpos($pre_string, ' ');
                    $string = '...'.substr($pre_string, $pos_start, $pos_end).'...';
                    if ($pos_start === false && $pos_end === false) {
                        $string = '...'.$pre_string.'...';
                    } else {
                        $string = '...'.substr($pre_string, $pos_start, $pos_end).'...';
                    }
                }

                $match_start = stristr($string, $match_to);
                $match_compute = strlen($string) - strlen($match_start);
            }
        }

        return $string;
    }
}

?>
