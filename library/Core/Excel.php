<?php
/**
 * @name Excel
 * @since : 2011-08-23, 12.10.54
 */
require_once APPLICATION_PATH.'/../library/PHPExcel/PHPExcel.php';
class Core_Excel extends PHPExcel
{
    /**
     * @var PHPExcel_Style
     */
    protected $styler;
    /**
     * @var array
     */
    protected $stylerParams;
    public function __construct()
    {
        parent::__construct();
        $this->__setDefaultHeaders();
    }
    public function __setDefaultHeaders()
    {
        $this->getProperties()
            ->setCreator('PPMI')
            ->setLastModifiedBy('PPMI')
            ->setTitle('PPMI')
            ->setSubject('PPMI document')
            ->setDescription('PPMI document')
            ->setKeywords('PPMI')
            ->setCategory('PPMI');
    }
    /**
     * @return Array
     */
    public function getSylerParams()
    {
        return $this->stylerParams;
    }
    /**
     * @return Core_Excel
     */
    public function unsetSylerParams()
    {
        $this->stylerParams = null;

        return $this;
    }
    /**
     * @return PHPExcel_Style
     */
    public function getStyler()
    {
        $this->styler = new PHPExcel_Style();
        $this->styler->applyFromArray($this->stylerParams, true);
        $this->unsetSylerParams();

        return $this->styler;
    }
    /**
     * Set Fill color to cell.
     *
     * @param $rgb String
     *       	 RGB color code
     * @return Core_Excel
     */
    public function setFill($rgb = '808080')
    {
        $this->stylerParams ['fill'] = [
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => [
                'argb' => $rgb, ], ];

        return $this;
    }
    /**
     * Set Bold to text.
     *
     * @return Core_Excel
     */
    public function setBold()
    {
        $this->stylerParams ['font'] ['bold'] = true;

        return $this;
    }
    /**
     * Set Italic to text.
     *
     * @return Core_Excel
     */
    public function setItalic()
    {
        $this->stylerParams ['font'] ['italic'] = true;

        return $this;
    }
    /**
     * Set Underline to text.
     *
     * @param $doubleUnderline Bool
     *       	 Underline with double line
     * @return Core_Excel
     */
    public function setUnderline($doubleUnderline = false)
    {
        if ($doubleUnderline) {
            $this->stylerParams ['font'] ['underline'] = PHPExcel_Style_Font::UNDERLINE_DOUBLE;
        } else {
            $this->stylerParams ['font'] ['underline'] = true;
        }

        return $this;
    }
    /**
     * Set Strike to text.
     *
     * @return Core_Excel
     */
    public function setStrike()
    {
        $this->stylerParams ['font'] ['strike'] = true;

        return $this;
    }
    /**
     * Set Color to text.
     *
     * @param $color String
     *       	 RGB color code
     * @return Core_Excel
     */
    public function setColor($color = '000000')
    {
        $this->stylerParams ['font'] ['color'] = [
            'argb' => $color, ];

        return $this;
    }
    /**
     * Set Cell's borders.
     *
     * @param $color String
     *       	 RGB color code
     * @param $borderPositions array
     *       	 left, top, right, bottom, outline, inside, allborders
     * @param $borderStyle PHPExcel_Style_Border
     * @return Core_Excel
     */
    public function setBorders($color = '808080', array $borderPositions = ['allborders'], $borderStyle = null)
    {
        if (! $borderStyle) {
            $borderStyle = PHPExcel_Style_Border::BORDER_THIN;
        }
        foreach ($borderPositions as $key => $borderPosition) {
            $this->stylerParams ['borders'] [$borderPosition] = [
                'style' => $borderStyle,
                'color' => [
                    'argb' => $color, ], ];
        }

        return $this;
    }

    /**
     * @param  $sheet
     * @param  $fromCol
     * @param  $toCol
     */
    public function autoFitColumnWidthToContent($sheet, $fromCol, $toCol)
    {
        if (empty($toCol)) {//not defined the last column, set it the max one
            $toCol = $sheet->getColumnDimension($sheet->getHighestColumn())->getColumnIndex();
        }

        for ($i = $fromCol; $i <= $toCol; $i++) {
            $sheet->getColumnDimension($i)->setAutoSize(true);
        }
        $sheet->calculateColumnWidths();
    }

    public function getColNames()
    {
        return [
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            'AA',
            'AB',
            'AC',
            'AD',
            'AE',
            'AF',
            'AG',
            'AH',
            'AI',
            'AJ',
            'AK',
            'AL',
            'AM',
            'AN',
            'AO',
            'AP',
            'AQ',
            'AR',
            'AS',
            'AT',
            'AU',
            'AV',
            'AW',
            'AX',
            'AY',
            'AZ',
            'BA',
            'BB',
            'BC',
            'BD',
            'BE',
            'BF',
            'BG',
            'BH',
            'BI',
            'BJ',
            'BK',
            'BL',
            'BM',
            'BN',
            'BO',
            'BP',
            'BQ',
            'BR',
            'BS',
            'BT',
            'BU',
            'BV',
            'BW',
            'BX',
            'BY',
            'BZ', ];
    }
}
