<?php

class Core_Form extends Zend_Form
{
    public $flTagDeco = [
            'ViewHelper',
            'Description',
            'Errors',
            ['Label'],
            ['HtmlTag', ['tag' => 'div','class' => 'pull-left mr20px','style' => 'height:60px;']],
        ];

    public $controlGroupDeco = [
            'ViewHelper',
            'Description',
            'Errors',
            [['elementDiv' => 'HtmlTag'], ['tag' => 'div','class' => 'controls']],
            ['Label',['class' => 'control-label']],
            ['HtmlTag', ['tag' => 'div','class' => 'control-group']],
        ];

    public function __construct()
    {
        parent::__construct();
        $this->setTranslator(Zend_Registry::get('translate'));
    }

    public function addClear()
    {
        $empty = $this->createElement('text', 'empty'.rand(1, 1000));
        $empty->setDecorators([
            ['HtmlTag', ['tag' => 'div','class' => 'clearboath h10']],
        ]);
        $this->addElement($empty);

        return $this;
    }

    public function addSubmitButtons()
    {
        /*
         * <div class="clearboath h10"></div>
                    <button type="submit" class="btn btn-alt btn-large btn-primary ml10px pull-right">Saugoti</button>
                    <div class="clearboath h10"></div>
         */
        $submit = $this->createElement('button', 'save');
        $submit->setLabel('Save')
               ->setAttrib('class', 'btn btn-alt btn-large btn-primary ml10px pull-right')
               ->setAttrib('type', 'submit')
               ->removeDecorator('label')
               ->removeDecorator('HtmlTag')
               ->removeDecorator('DtDdWrapper')
               ->addDecorator('HtmlTag', ['tag' => 'div','class' => 'clearboath h10','placement' => 'prepend']);
        $this->addElement($submit);

        return $this;
    }

    public function addFieldsetDecorator($subform = null)
    {
        if ($subform) {
            $subform->setDecorators([
                'FormElements',
                'Fieldset',
                ['HtmlTag',['tag' => 'div', 'class' => 'parameters']],
            ]);
        } else {
            $this->addDecorators([['HtmlTag', ['tag' => 'fieldset', 'class' => 'parameters']]]);
        }
    }
}
