<?php

/**
 * Description of Acc.
 *
 * @author Darius Matulionis
 */
class Filter
{
    public $Name;
    public $Value;
}

class ArrayOfFilter
{
    public $Filter;
}

class GetProductListRequest
{
    public $Filters;
}

class GetProductList
{
    public $GetProductListRequest;
}

class GetInvoiceListRequest
{
    public $Filters;
}

class GetInvoiceList
{
    public $GetInvoiceListRequest;
}

class GetInvoiceDetailsRequest
{
    public $Filters;
}

class GetInvoiceProductSerialList
{
    public $GetInvoiceProductSerialListRequest;
}

class GetInvoiceProductSerialListRequest
{
    public $Filters;
}

class GetInvoiceDetails
{
    public $GetInvoiceDetailsRequest;
}

class GetProductSpecification
{
    public $GetProductSpecificationRequest;
}

class GetProductSpecificationRequest
{
    public $Filters;
}

class GetProductResourcesRequest
{
    public $Filters;
}

class GetProductResources
{
    public $GetProductResourcesRequest;
}

class GetProductBarcodeList
{
    public $GetProductBarcodeListRequest;
}

class GetProductBarcodeListRequest
{
    public $Filters;
}

class GetAddressList
{
    public $GetAddressListRequest;
}

class GetAddressListRequest
{
    public $Filters;
}

class GetCustomerUserList
{
    public $GetCustomerUserListRequest;
}

class GetCustomerUserListRequest
{
    public $Filters;
}

class GetInvoiceShipments
{
    public $GetInvoiceShipmentsRequest;
}

class GetInvoiceShipmentsRequest
{
    public $Filters;
}

class GetOrderDetails
{
    public $GetOrderDetailsRequest;
}

class GetOrderDetailsRequest
{
    public $Filters;
}

class PurchaseProducts
{
    public $PurchaseProductsRequest;
}

class PurchaseProductsRequest
{
    public $Filters;
    public $PurchaseLines;
}

class ProductPurchaseLine
{
    public $ProductPurchaseLineId;
    public $SupplierCode;
    public $Quantity;
    public $DeliveryTypeId;
    public $DeliveryAddress;
    public $Note;
}

class Address
{
    public $AddressId;
    public $Street;
    public $City;
    public $PostalCode;
    public $CountryCode;
}

class GetTreeNodes
{
    public $GetTreeNodesRequest;
}

class GetTreeNodesRequest
{
    public $Filters;
}

class GetTreeNodeProducts
{
    public $GetTreeNodeProductsRequest;
}

class GetTreeNodeProductsRequest
{
    public $Filters;
}

class Core_Api_Acc
{
    public $licenseKey = 'D171136B-5A48-45DF-B47E-E2B803BD8972';
    private $soapClient = null;
    private $soapHeader = null;
    private $wsdl = 'https://api.acme.lt/1.0/commerce.asmx?WSDL';

    public function init()
    {
        $map = [
            'Filter' => 'Filter',
            'ArrayOfFilter' => 'ArrayOfFilter',
            'GetProductListRequest' => 'GetProductListRequest',
            'GetProductList' => 'GetProductList',
            'GetInvoiceList' => 'GetInvoiceList',
            'GetInvoiceListRequest' => 'GetInvoiceListRequest',
            'GetProductSpecification' => 'GetProductSpecification',
            'GetProductSpecificationRequest' => 'GetProductSpecificationRequest',
            'GetProductResources' => 'GetProductResources',
            'GetInvoiceDetails' => 'GetInvoiceDetails',
            'GetInvoiceDetailsRequest' => 'GetInvoiceDetailsRequest',
            'GetInvoiceProductSerialList' => 'GetInvoiceProductSerialList',
            'GetInvoiceProductSerialListRequest' => 'GetInvoiceProductSerialListRequest',
            'GetAddressList' => 'GetAddressList',
            'GetAddressListRequest' => 'GetAddressListRequest',
            'GetCustomerUserList' => 'GetCustomerUserList',
            'GetCustomerUserListRequest' => 'GetCustomerUserListRequest',
            'GetInvoiceShipments' => 'GetInvoiceShipments',
            'GetInvoiceShipmentsRequest' => 'GetInvoiceShipmentsRequest',
            'GetOrderDetails' => 'GetOrderDetails',
            'GetOrderDetailsRequest' => 'GetOrderDetailsRequest',
            'PurchaseProducts' => 'PurchaseProducts',
            'PurchaseProductsRequests' => 'PurchaseProductsRequest',
            'ProductPurchaseLine' => 'ProductPurchaseLine',
            'DeliveryAddress' => 'Address',
            'GetTreeNodes' => 'GetTreeNodes',
            'GetTreeNodeProducts' => 'GetTreeNodeProducts',
        ];

        $this->soapClient = new SoapClient($this->wsdl, ['classmap' => $map, 'trace' => 1, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS]);
        $this->soapHeader = new SoapHeader('http://schemas.acme.eu/', 'LicenseHeader', ['LicenseKey' => $this->licenseKey], false);
        $this->soapClient->__setSoapHeaders([$this->soapHeader]);
    }

    public function getProductResources($SupplierCode, $ResourceType = 8)
    {
        try {
            $this->init();
            $request = new GetProductResourcesRequest();
            $request->Filters = new ArrayOfFilter();
            $language = new Filter();
            $language->Name = 'Language';
            $language->Value = 'en-us';
            $product = new Filter();
            $product->Name = 'SupplierCode';
            $product->Value = $SupplierCode;
            $parameters = $request->Filters->Filter = [$language, $product];
            $params = new GetProductResources();
            $params->GetProductResourcesRequest = $request;
            $result = $this->soapClient->GetProductResources($params);

            $image_url = null;
            foreach ($result->GetProductResourcesResult->ProductResources as $res) {
                if (isset($res->ProductResource)) {
                    foreach ($res->ProductResource as $r) {
                        if ($r->ResourceType == $ResourceType) {
                            return $r->ResourceURL;
                        }
                    }
                }
            }

            return $image_url;
            //echo "<pre>". 	print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getCategory($SupplierCode)
    {
        try {
            $this->init();
            $request = new GetProductResourcesRequest();
            $request->Filters = new ArrayOfFilter();
            $language = new Filter();
            $language->Name = 'Language';
            $language->Value = 'lt-lt';
            $product = new Filter();
            $product->Name = 'SupplierCode';
            $product->Value = $SupplierCode;
            $parameters = $request->Filters->Filter = [$language, $product];
            $params = new GetTreeNodeProducts();
            $params->GetTreeNodeProductsRequest = $request;
            $result = $this->soapClient->GetTreeNodeProducts($params);

            $NodeID = null;
            foreach ($result->GetTreeNodeProductsResult->NodeProductList->NodeProduct as $key => $value) {
                if ($value->SupplierCode == $SupplierCode) {
                    $NodeID = $value->NodeId;
                }
            }
            if ($NodeID) {
                $request = new GetTreeNodesRequest();
                $request->Filters = new ArrayOfFilter();
                $NodeId = new Filter();
                $NodeId->Name = 'NodeId';
                $NodeId->Value = $NodeID;
                $language = new Filter();
                $language->Name = 'Language';
                $language->Value = 'en-us';
                $request->Filters->Filter = [$language, $NodeId];
                $params = new GetTreeNodes();
                $params->GetTreeNodesRequest = $request;
                $result = $this->soapClient->GetTreeNodes($params);

                if (! empty($result->GetTreeNodesResult->NodeList->TreeNode)) {
                    foreach ($result->GetTreeNodesResult->NodeList->TreeNode as $key => $value) {
                        if ($value->NodeId == $NodeID
                            );

                        return $value->Name;
                    }
                }
            }

            return;
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getVendors($vendorId)
    {
        try {
            $this->init();
            $result = $this->soapClient->GetVendorList();
            foreach ($result->GetVendorListResult->VendorList->Vendor as $b) {
                if ($b->VendorId == $vendorId) {
                    return $b->VendorName;
                }
            }
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getProductSpecification()
    {
        try {
            $this->init();
            $request = new GetProductSpecificationRequest();
            $request->Filters = new ArrayOfFilter();
            $language = new Filter();
            $language->Name = 'Language';
            $language->Value = 'en-us';
            $product = new Filter();
            $product->Name = 'SupplierCode';
            $product->Value = '000044';
            $parameters = $request->Filters->Filter = [$language, $product];
            $params = new GetProductSpecification();
            $params->GetProductSpecificationRequest = $request;
            $result = $this->soapClient->GetProductSpecification($params);

            $data = '';
            if (! empty($result->GetProductSpecificationResult->ProductSpecification)) {
                foreach ($result->GetProductSpecificationResult->ProductSpecification as $spec) {
                    foreach ($spec->ProductProperty as $property) {
                        $data .= $property->PropertyName.': '.$property->PropertyValue.'<br />';
                    }
                }
            }

            return $data;
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getProductClassification()
    {
        try {
            $this->init();
            $result = $this->soapClient->GetProductClassification();
            $db = Zend_Registry::get('db');

            // Groups
            print 'Group list:<br />';
            foreach ($result->GetProductClassificationResult->GroupList->Group as $b) {
                print $b->GroupId.' '.$b->GroupName.'<br />';
                $db->query("UPDATE suppliers_acc_products SET `group` = '".$b->GroupName."' WHERE group_id = ".$b->GroupId);
            }

            print '-----------------------------------------------<br />';
            // Classes
            print 'Class list:<br />';
            foreach ($result->GetProductClassificationResult->ClassList->Class as $b) {
                print $b->ClassId.' '.$b->ClassName.'<br />';
                $db->query("UPDATE suppliers_acc_products SET `class` = '".$b->ClassName."' WHERE class_id = ".$b->ClassId);
            }

            print '-------------------------------------------------<br />';
            // Series
            print 'Series list:<br />';
            foreach ($result->GetProductClassificationResult->SeriesList->Series as $b) {
                print $b->SeriesId.' '.$b->SeriesName.'<br />';
                $db->query("UPDATE suppliers_acc_products SET `serie` = '".$b->SeriesName."' WHERE series_id = ".$b->SeriesId);
            }

            //echo "<pre>". print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getProductList()
    {
        try {
            $this->init();

            $request = new GetProductListRequest();
            $request->Filters = new ArrayOfFilter();
            $language = new Filter();
            $language->Name = 'Language';
            $language->Value = 'lt-lt';
            $currency = new Filter();
            $currency->Name = 'Currency';
            $currency->Value = 'LTL';
            $request->Filters->Filter = [$language, $currency];
            $params = new GetProductList();
            $params->GetProductListRequest = $request;
            $result = $this->soapClient->GetProductList($params);

            $data = [];
            $model = new models_SuppliersAccProduct();
            //$model->truncate();

            foreach ($result->GetProductListResult->ProductList->Product as $p) {
                $prod = $model->fetchRow("supplier_code = '".$p->SupplierCode."'");
                if (! $prod) {
                    $d = [
                        'supplier_code' => $p->SupplierCode,
                        'segment_id' => $p->SegmentId,
                        'group_id' => $p->GroupId,
                        'class_id' => $p->ClassId,
                        'series_id' => $p->SeriesId,
                        'vendor' => $this->getVendors($p->VendorId),
                        'part_number' => $p->PartNumber,
                        'name' => $p->Name,
                        'warranty' => $p->Warranty,
                        'price' => $p->Price,
                        'quantity' => $p->Quantity,
                        'gross_weight' => $p->GrossWeight,
                        'date_expected' => $p->DateExpected,
                        'recommended_retail_price' => $p->RecommendedRetailPrice,
                        'is_new_product' => $p->IsNewProduct,
                        'image' => $this->getProductResources($p->SupplierCode),
                        'cat' => $this->getCategory($p->SupplierCode),
                        'spec' => $this->GetProductSpecification($p->SupplierCode),
                    ];
                    if (! empty($d['cat']) && ! empty($d['supplier_code'])) {
                        $model->save($d);
                    }
                }
            }
            //echo "<pre>". print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getInvoiceShipments()
    {
        try {
            $this->init();
            $request = new GetInvoiceShipmentsRequest();
            $request->Filters = new ArrayOfFilter();
            $invoice = new Filter();
            $invoice->Name = 'InvoiceNumber';
            $invoice->Value = 'ACC-00761335'; // set invoice number here

            $parameters = $request->Filters->Filter = [$invoice];
            $params = new GetInvoiceShipments();
            $params->GetInvoiceShipmentsRequest = $request;
            $result = $this->soapClient->GetInvoiceShipments($params);

            foreach ($result->GetInvoiceShipmentsResult->ShipmentList->Shipment as $ps) {
                print $ps->CarrierId.' '.$ps->Number.'</br>';
            }

            //echo "<pre>". 	print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getOrderDetails()
    {
        try {
            $this->init();
            $request = new GetOrderDetailsRequest();
            $request->Filters = new ArrayOfFilter();
            $parameters = $request->Filters->Filter = [];
            $params = new GetOrderDetails();
            $params->GetOrderDetailsRequest = $request;
            $result = $this->soapClient->GetOrderDetails($params);

            if (isset($result->GetOrderDetailsResult->OrderList->Order)) {
                foreach ($result->GetOrderDetailsResult->OrderList->Order as $o) {
                    print 'OrderId: '.$o->OrderHeader->OrderId.' <br />';
                    foreach ($o->OrderLine as $line) {
                        print 'OrderLineId: '.$line->OrderLineId.' SupplierCode: '.$line->SupplierCode.' LineAmount:  '.$line->LineAmount.' <br />';
                    }
                    print '----------------------------------------------------<br />';
                }
            } else {
                print 'no orders found';
            }

            //echo "<pre>". 	print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function purchaseProducts()
    {
        try {
            $this->init();
            $request = new PurchaseProductsRequest();
            $request->Filters = new ArrayOfFilter();
            $request->Filters->Filter = [];

            $line = new ProductPurchaseLine();
            $line->ProductPurchaseLineId = 'demo';
            $line->SupplierCode = '003315';
            $line->Quantity = 1;
            $line->DeliveryTypeId = 0;
            $line->Note = 'test order';

            $address = new Address();
            $address->AddressId = 'ecommtest-001';
            $line->DeliveryAddress = $address;

            $request->PurchaseLines = [$line];
            $params = new PurchaseProducts();
            $params->PurchaseProductsRequest = $request;
            $result = $this->soapClient->PurchaseProducts($params);

            foreach ($result->PurchaseProductsResult->StatusList->ProductPurchaseStatus as $st) {
                print $st->ProductPurchaseLineId.' '.$st->Status.'<br />';
            }

            //echo "<pre>". 	print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getAddressList()
    {
        try {
            $this->init();
            $request = new GetAddressListRequest();
            $request->Filters = new ArrayOfFilter();
            $request->Filters->Filter = [];
            $params = new GetAddressList();
            $params->GetAddressListRequest = $request;
            $result = $this->soapClient->GetAddressList($params);

            foreach ($result->GetAddressListResult->AddressList->Address as $b) {
                print $b->AddressId.' '.$b->Street.' '.$b->City.'<br />';
            }

            //echo "<pre>". 	print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }

    public function getCustomerUserList()
    {
        try {
            $this->init();
            $request = new GetCustomerUserListRequest();
            $request->Filters = new ArrayOfFilter();
            $request->Filters->Filter = [];
            $params = new GetCustomerUserList();
            $params->GetCustomerUserListRequest = $request;
            $result = $this->soapClient->GetCustomerUserList($params);

            if (isset($result->GetCustomerUserListResult->UserList->User)) {
                foreach ($result->GetCustomerUserListResult->UserList->User as $b) {
                    print $b->UserId.' '.$b->FirstName.' '.$b->LastName.'<br />';
                }
            }
            // uncomment this line to see raw result of this method response
            //echo "<pre>". 	print_r($result,1) . '</pre>';
        } catch (SoapFault $e) {
            echo '<xmp>'.$this->soapClient->__getLastRequestHeaders().$this->soapClient->__getLastRequest().'</xmp>';
            echo '<pre>'.print_r($e, 1).'</pre>';
        }
    }
}

//$client = new Core_Api_Acc() ;
//$client->getVendors();
//$client->getProductClassification();
//$client->getProductList();
//$client->getProductBarcodes();
//$client->getInvoiceList();
//$client->GetInvoiceDetails();
//$client->GetInvoiceProductSerials();
//$client->getProductSpecification();
//$client->getProductResources();
//$client->getAddressList();
//$client->getCustomerUserList();
//$client->getInvoiceShipments();
//$client->getOrderDetails();
//$client->purchaseProducts();
?>


