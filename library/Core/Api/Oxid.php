<?php

class Core_Api_Oxid
{
    protected $user;
    protected $pass;
    protected $baseUrl;
    protected $tokenUrl;
    protected $token;

    public function __construct()
    {
        $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $apikeys = $config->getOption('billing');

        $this->user = $apikeys['username'];
        $this->pass = $apikeys['password'];
        $this->baseUrl = $apikeys['address'];
        $this->tokenUrl = $apikeys['address_token'];
        $this->getToken();

    }

    public function getContractType($white_list_group_id)
    {
        //open connection
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl.'contract_types/'.$white_list_group_id.'.json?key='.$this->key);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute
        return json_decode(curl_exec($ch));
    }

    // Send fines to billing
    public function sendToBilling($data, $type)
    {
        $model = new Model_Zone();
        $zone = $model->fetchrow('id = ' . $data['zone']);

        if ($zone && isset($zone['billing_id'])) {

            if ($type == Model_BillingQueue::CREATE_FINE) {
                return $this->sendFine($data, $zone);
            }
            if ($type == Model_BillingQueue::UPDATE_FINE) {
                return $this->updateFine($data, $zone);
            }
            if ($type == Model_BillingQueue::CREATE_TRANSACTION) {
                return $this->sendTransaction($data, $zone);
            }
        }
    }

    protected function sendFine($data, $zone)
    {
        // request by documentation https://docs.unipark.lt:8443/pages/viewpage.action?spaceKey=UPD&title=Fines+API
        $post_data = [
            "FineId" => $data['id'],
            "FineNumber" => $data['id_public'],
            "ParkingZoneId" => $zone['billing_id'],
            "NumberPlate" => $data['plate'],
            "StatusId" => $data['status_id'],
            "ReasonId" => $data['causes_id'],
            "Date" => $data['date'],
            "Address" => $data['street'],
            "Amount" => $data['amount']
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl . '/FinesAPI/CreateFine');

        //request logging
        $billing = new Model_Billing();
        $log = $billing->save([
            'type' => Model_BillingQueue::CREATE_FINE,
            'licensePlateNumber' => $data['plate'],
            'request' => json_encode($post_data)
        ]);

        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type:application/json',
                'Authorization: Bearer ' . $this->token
            )
        );
        curl_setopt($ch, CURLOPT_POST, count($post_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        //if (!$result || $httpcode != 200) {
        if ($httpcode != 200) {
            if ($result) {
                if ($result->error) {
                    $billing->update(['response' => json_encode($result->error)], 'id = ' . $log['id']);
                }
            }
            return $post_data;
        }
        //response logging if returned
        $billing->update(['response' => json_encode($result)], 'id = ' . $log['id']);
        return false;
    }

    protected function updateFine($data, $zone)
    {
        // request by documentation https://docs.unipark.lt:8443/pages/viewpage.action?spaceKey=UPD&title=Fines+API
        $post_data = [
            "FineId" => $data['id'],
            "FineNumber" => $data['id_public'],
            "ParkingZoneId" => $zone['billing_id'],
            "NumberPlate" => $data['plate'],
            "StatusId" => $data['status_id'],
            "ReasonId" => $data['causes_id'],
            "Date" => $data['date'],
            "Address" => $data['street'],
            "Amount" => $data['amount']
        ];
        $ch = curl_init();

        //checking if payment
        $model2 = new Model_FinesPayments();

        $payments = $model2->fetchAll('fine_id = ' . $data['id']);
        if ($payments->count()) {
            foreach ($payments as $payment) {
                switch ($payment['payer_bank']) {
                    case 'Hectronic':
                        $paymentType = 7;
                        break;
                    default:
                        $paymentType = 8;
                        break;
                }

                $paymentsArray[] = [
                    "PaymentId" => $payment['id'],
                    "Amount" => (float)($payment['amount_cnt'] / 100),
                    "Date" => $payment['time'],
                    "PaymentType" => $paymentType
                ];
            }

            $post_data['Payments'] = $paymentsArray;
        }
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl . '/FinesAPI/UpdateFine');

        //request logging
        $billing = new Model_Billing();
        $log = $billing->save([
            'type' => Model_BillingQueue::UPDATE_FINE,
            'licensePlateNumber' => $data['plate'],
            'request' => json_encode($post_data)
        ]);

        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type:application/json',
                'Authorization: Bearer ' . $this->token
            )
        );
        curl_setopt($ch, CURLOPT_POST, count($post_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        //if (!$result || $httpcode != 200) {
        if ($httpcode != 200) {
            if ($result) {
                if ($result->error) {
                    $billing->update(['response' => json_encode($result->error)], 'id = ' . $log['id']);
                } else { $billing->update(['response' => json_encode($result)], 'id = ' . $log['id']); }
            }
            return $post_data;
        }
        //response logging if returned
        $billing->update(['response' => json_encode($result)], 'id = ' . $log['id']);
        return false;
    }

    protected function sendTransaction($data, $zone)
    {
        //https://docs.unipark.lt:8443/pages/viewpage.action?spaceKey=UPD&title=Transaction+import+from+Enforcer
        $post_data = [
            "NumberPlate" => $data['plate'],
            "PhoneNumber" => $data['phone'] ? $data['phone'] : '000000000', // kolkas neturime tokios info
            "StartDateTime" => $data['date_start'],
            "EndDateTime" => $data['date_end'],
            "ZoneId" => $zone['billing_id'],
            "PaymentType" => ($data['operator_name'] === 'hectronic') ? 7 : 3,
            "OperatorName" => ($data['operator_name'] !== 'omnitel') ? $data['operator_name'] : 'telia',
            "Amount" => (float)($data['price_cent'] / 100),
            "PaymentDateTime" => $data['date_inserted']
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl . '/ImportAPI/ImportTransaction');

        //request logging
        $billing = new Model_Billing();
        $log = $billing->save([
            'type' => Model_BillingQueue::CREATE_TRANSACTION,
            'licensePlateNumber' => $data['plate'],
            'request' => json_encode($post_data)
        ]);

        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type:application/json',
                'Authorization: Bearer ' . $this->token
            )
        );
        curl_setopt($ch, CURLOPT_POST, count($post_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if (!$result || $httpcode != 200) {
            if ($result) {
                if ($result->error) {
                    $billing->update(['response' => json_encode($result->error)], 'id = ' . $log['id']);
                }
            }
            return $post_data;
        }
        $model = new Model_Parking();
        $model->update(['billing_id' => $result->TransactionId], "id = '" . $data['id'] . "'");
        $billing->update(['response' => json_encode($result)], 'id = ' . $log['id']);
        return false;
    }

    private function getToken()
    {
        $ch = curl_init();
        $post_data =
            [
                "username" => $this->user,
                "password" => $this->pass,
                "grant_type" => "password",

            ];
        curl_setopt($ch, CURLOPT_URL, $this->tokenUrl . '/token');
        curl_setopt($ch, CURLOPT_POST, count($post_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->build_http_query($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute post
        $result = json_decode(curl_exec($ch));
        if (isset($result)) {
            $this->token = $result->access_token;
        }
    }

    /**
     * Builds an http query string.
     *
     * @param array $query // of key value pairs to be used in the query
     *
     * @return string       // http query string.
     **/
    private function build_http_query($query)
    {
        $query_array = [];

        foreach ($query as $key => $key_value) {
            $query_array[] = urlencode($key) . '=' . urlencode($key_value);
        }

        return implode('&', $query_array);
    }

    public  function AdditionalServiceStatusChange ( $post_data = array () )
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->baseUrl . '/porsche-api/change-additional-service-status');

        //request logging
        $billing = new Model_Billing();
        $log = $billing->save([
            'type' => Model_BillingQueue::PORSCHE_STATUS_CHANGE,
            'licensePlateNumber' => $post_data['OrderId'],
            'request' => json_encode($post_data)
        ]);

        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                'Content-Type:application/json',
                'Authorization: Bearer ' . $this->token
            )
        );
        curl_setopt($ch, CURLOPT_POST, count($post_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($ch));
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if (!$result || $httpcode != 200) {
            if ($result) {
                if ($result->error) {
                    $billing->update(['response' => json_encode($result->error)], 'id = ' . $log['id']);
                }
            }
            return $post_data;
        }

        $billing->update(['response' => json_encode($result)], 'id = ' . $log['id']);
        return false;
    }
}

