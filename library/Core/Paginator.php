<?php

/**
 * Description of Paginator.
 *
 * @author Darius Matulionis
 */
class Core_Paginator extends Zend_Paginator
{
    /**
     * @param mixed $model
     * @param string $view
     * @param int $currentPage
     * @param int $itemsInPage
     * @param null $where
     * @param array $order
     * @param null $rowSelect
     * @param string $paginationHtml
     * @param null $join
     *
     * @return \Zend_Paginator
     * @throws \Zend_Paginator_Exception
     */
    public static function create($model, $view, $currentPage = 1, $itemsInPage = DEFAULT_PAGINATION_ITEMS, $where = null, $order = [], $rowSelect = null, $paginationHtml = DEFAULT_PAGINATION_SCRIPT, $join = null)
    {
        $db = $model->getAdapter();

        if (! $rowSelect) {
            $rowSelect = $db->select()->from($model->info('name'));

            if (! empty($where) && is_array($where)) {
                foreach ($where as $k => $v) {
                    if (is_numeric($k)) {
                        $rowSelect->where($v);
                    } else {
                        $rowSelect->where($k.' = ?', $v);
                    }
                }
            } elseif (is_string($where)) {
                $rowSelect->where($where);
            }

            $rowSelect->order($order);
            //var_dump($rowSelect->assemble());die();

            if (! empty($join) && is_array($join)) {
                if (isset($join[0])) {
                    foreach ($join as $key => $j) {
                        if (is_string($j['select'])) {
                            $rowSelect->joinLeft([$j['as'] => $j['table']], $j['on'], [$j['select']]);
                        }
                        if (is_array($j['select'])) {
                            $rowSelect->joinLeft([$j['as'] => $j['table']], $j['on'], $j['select']);
                        }
                    }
                } else {
                    if (is_string($join['select'])) {
                        $rowSelect->joinLeft([$join['as'] => $join['table']], $join['on'], [$join['select']]);
                    }
                    if (is_array($join['select'])) {
                        $rowSelect->joinLeft([$join['as'] => $join['table']], $join['on'], $join['select']);
                    }
                }
            }

            $select = $db->select()
                ->from($model->info('name'))
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(new Zend_Db_Expr('COUNT(*) AS '.$db->quoteIdentifier(Zend_Paginator_Adapter_DbSelect::ROW_COUNT_COLUMN)));

            if (! empty($where) && is_array($where)) {
                foreach ($where as $k => $v) {
                    if (is_numeric($k)) {
                        $select->where($v);
                    } else {
                        $select->where($k.' = ?', $v);
                    }
                }
            } elseif (! empty($where) && is_string($where)) {
                $select->where($where);
            }

            if (! empty($join) && is_array($join)) {
                if (isset($join[0])) {
                    foreach ($join as $key => $j) {
                        if (is_string($j['select'])) {
                            $select->joinLeft([$j['as'] => $j['table']], $j['on'] ,"");
                        }
                        if (is_array($j['select'])) {
                            $select->joinLeft([$j['as'] => $j['table']], $j['on'] ,"");
                        }
                    }
                } else {
                    if (is_string($join['select'])) {
                        $select->joinLeft([$join['as'] => $join['table']], $join['on'] ,"");
                    }
                    if (is_array($join['select'])) {
                        $select->joinLeft([$join['as'] => $join['table']], $join['on'] ,"");
                    }
                }
            }

            $select->order($order);
        } else {
            $select = clone $rowSelect;
            $select = $select
                //->from(array('iner'=>$model->info('name')))
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns(new Zend_Db_Expr('COUNT(*) AS '.$db->quoteIdentifier(Zend_Paginator_Adapter_DbSelect::ROW_COUNT_COLUMN)));
        }

        $adapter = new Zend_Paginator_Adapter_DbSelect($rowSelect);
        $adapter->setRowCount($select);

        $paginator = new Zend_Paginator($adapter);
        $paginator->setItemCountPerPage($itemsInPage);
        $paginator->setCurrentPageNumber($currentPage); // Must be done last

        if ($paginationHtml) {
            Zend_Paginator::setDefaultScrollingStyle('Sliding');
            Zend_View_Helper_PaginationControl::setDefaultViewPartial($paginationHtml);
        }
        $paginator->setView($view);

        return $paginator;
    }
}
