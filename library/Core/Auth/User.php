<?php

/**
 * Description of User.
 *
 * @author Darius Matulionis
 */
class Core_Auth_User extends Zend_Auth
{
    protected static $_instance = null;

    protected function __construct()
    {
    }

    final public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    final public function hasIdentity()
    {
        if (parent::hasIdentity() && ! isset($this->getStorage()->read()->admin)) {
            return true;
        }
        return false;
    }
}
