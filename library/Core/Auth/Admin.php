<?php
/**
 * Description of Admin.
 *
 * @author Darius Matulionis
 */
//require_once('Zend/Auth.php');

class Core_Auth_Admin extends Zend_Auth
{
    protected static $_instance = null;

    protected function __construct()
    {
        parent::__construct();
        $controller = Zend_Controller_Front::getInstance()->getRequest()->getControllerName();
        $module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
        if ($controller != 'auth' && ! parent::hasIdentity() && $module == 'admin') {
            header('location:/admin/auth/login');
        }
    }

    final public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    final public function hasIdentity()
    {
        if (parent::hasIdentity() && isset($this->getStorage()->read()->id)) {
            return true;
        }
        return false;
    }
}
