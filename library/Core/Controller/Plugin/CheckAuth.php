<?php

/**
 * Check if admin is logged in. If not, then.
 */
class Core_Controller_Plugin_CheckAuth extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() == 'api') {
            $filename = APPLICATION_PATH.'/api/configs/auth.ini';
            $config = new Zend_Config_Ini($filename);
            $auth_params = $config->auth->toArray();
            $key = $request->getParam('key');
            if (! $key || ! in_array($key, $auth_params)) {
                header('Content-type: text/json');
                header('Content-type: application/json');
                echo json_encode([
                    'success' => false,
                    'msg' => 'Bad authentification or API key',
                ]);
                die();

                return false;
            }

            return true;
        }

        if ($request->getModuleName() == 'admin' && ! Core_Auth_Admin::getInstance()->hasIdentity()) {
            if ('auth' != $request->getControllerName()) {
                $request->setActionName('login')
                        ->setControllerName('auth')
                        ->setModuleName('admin');

                $request->setDispatched(false);
            }
        }
    }
}
