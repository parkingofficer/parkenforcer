<?php


/**
 * @desc Controller navigation interface. 
 */
interface Core_Controller_Navigation_Interface
{
    public static function getNavigation();
}
