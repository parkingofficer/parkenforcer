<?php

class Core_Controller_Auth_Action extends Zend_Controller_Action
{
    public function preDispatch()
    {
        if (! Core_Auth_Admin::getInstance()->hasIdentity()) {
            $this->_redirect('');
        }
    }
}
