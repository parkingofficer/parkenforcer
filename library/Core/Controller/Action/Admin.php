<?php

class Core_Controller_Action_Admin extends Zend_Controller_Action
{
    /**
     * @var Zend_Translate
     */
    protected $_t;

    public function init()
    {
        parent::init();
        $this->registerTranslations();
    }

    public function registerListAction($model, $where = [], $order = ['position DESC'], $itemsInPage = 30)
    {
        $this->registerStandartActions($model);

        $paginator = Core_Paginator::create($model, $this->view, $this->_getParam('page'), $itemsInPage, $where, $order);

        $this->view->list = $paginator->getCurrentItems();
        $ths->view->paginator = $paginator;

        $this->view->page = $this->_getParam('page');
    }

    public function registerInsertAction($model, $form)
    {
        $id = (int) $this->_getParam('id');

        if ($this->_request->isPost() && $form->isValid($this->_request->getPost())) {
            $row = $model->save($form->getValues());
            $id = $row->id;
        }

        $row = $model->find((int) $id)->current();

        if (null !== $row) {
            $form->populate($row->toArray());
        }

        $this->view->form = $form;
    }

    public function registerBulkImages($model)
    {
        $product_id = (int) $this->_getParam('product_id');

        $this->registerStandartActions($model, null, null, ['product_id' => $product_id]);

        $form = new Admin_Form_CatalogProductImage();
        $form->setAction('/admin/'.$this->_request->getControllerName().'/'.$this->_request->getActionName().'/product_id/'.$product_id);

        $productImages = $model->fetchImages($product_id, false);
        $form->createSubForms($productImages);

        if ($this->_request->isPost() && $form->isValid($_POST)) {
            /**
             * dealing with the new image.
             */
            $mainValues = $form->getValues();
            $product_id = $mainValues['product_id'];

            if ($form->getSubForm('image')->isValid($_POST)) {
                $values = $form->getSubForm('image')->getValues();

                if (! empty($values['image']['image'])) {
                    $values['image']['product_id'] = $product_id;
                    $row = $model->save($values['image']);
                }
            }

            /**
             * dealing with all the array.
             */
            $subform = $form->getSubForm('images');
            $values = $subform->getValues();

            if (! empty($values['images'])) {
                $count = count($values['images']);
                foreach ($values['images'] as $k => $image) {
                    $image['product_id'] = $product_id;

                    $row = $model->save($image);
                }
            }

            $this->_redirect('/admin/'.$this->_request->getControllerName().'/'.$this->_request->getActionName().'/product_id/'.$product_id);
        }

        if ($product_id) {
            $form->getElement('product_id')->setValue((int) $product_id);
        }

        $this->view->form = $form;
    }

    public function registerStandartActions($object, $perform = ['up', 'down', 'visible', 'delete', 'search' => []], $deleteCall = [], $where = null)
    {
        if (null === $perform) {
            $perform = ['up', 'down', 'visible', 'delete', 'search' => []];
        }

        if (isset($perform['search']) && count($perform) == 1) {
            $perform = array_merge(['up', 'down', 'visible', 'delete'], $perform);
        }

        if ($this->_getParam('moveup')) {
            if (in_array('up', $perform) && method_exists($object, 'moveUp')) {
                $object->moveUp($this->_getParam('moveup'), $where);
            }
        }

        if ($this->_getParam('movedown')) {
            if (in_array('down', $perform) && method_exists($object, 'moveDown')) {
                $object->moveDown($this->_getParam('movedown'), $where);
            }
        }

        if ($this->_getParam('visible')) {
            if (in_array('visible', $perform) && method_exists($object, 'toggleVisibility')) {
                $object->toggleVisibility($this->_getParam('visible'));
            }
        }

        if ($this->_getParam('delete')) {
            if (in_array('delete', $perform)) {
                if (method_exists($object, 'customDelete')) {
                    $object->customDelete($this->_getParam('delete'));
                } elseif (! empty($deleteCall)) {
                    call_user_func($deleteCall);
                } elseif (method_exists($object, 'delete')) {
                    $object->getAdapter()->beginTransaction();
                    try {
                        $object->delete($object->getPrimary()." = '".$this->_getParam('delete')."'");
                        $object->getAdapter()->commit();
                    } catch (Exception $e) {
                        $object->getAdapter()->rollBack();
                        //var_dump( $e->getMessage() );
                        //echo ( $e->getMessage() );
                        if (strpos($e->getMessage(), 'constraint')) {
                            echo('<br/><br/><br/><pre style="color:red;">Šio lauko informacija yra naudojama kituose objektuose todel jo isštrinti negalima.</pre>');
                        }
                    }
                }
            }
        }

        $status = [];
        $status['itemsInPage'] = 100;
        $status['search'] = null;

        $keyword = $this->_getParam('keyword');
        if ($keyword && ! empty($keyword)) {
            $status['search'] = '';

            foreach ($object->info('cols') as $ks => $name) {
                if ($name != 'id' && $name != 'visible' && $name != 'position') {
                    $status['search'] .= $name.' LIKE \'%'.mysql_real_escape_string($this->_getParam('keyword')).'%\' OR ';
                }
            }

            $status['search'] = '( '.substr($status['search'], 0, -3).' ) ';

            $status['itemsInPage'] = 1000;
        }

        return $status;
    }

    public function registerTreeActions(Core_Db_Tree $tree)
    {
        if ($this->_getParam('moveup')) {
            $tree->moveUp($this->_getParam('moveup'));
        } elseif ($this->_getParam('movedown')) {
            $tree->moveDown($this->_getParam('movedown'));
        } elseif ($this->_getParam('visible')) {
            $node = $tree->getNode($this->_getParam('visible'));

            if ($node) {
                $visible = 1;
                if ($node->getData('visible') == 1) {
                    $visible = 0;
                }

                $db = Zend_Registry::get('db');
                $db->update($tree->_table, ['visible' => $visible], 'id = '.intVal($node->getId()));
            }
        } elseif ($this->_getParam('delete')) {
            $tree->removeNode($this->_getParam('delete'));
        }
    }

    private function registerTranslations()
    {
    }
}
