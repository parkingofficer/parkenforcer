<?php
/**
 * @name Core_Controller_Action_Api
 * @author darius.matulionis
 * @since : 2013.07.09, 15:02:25
 */
class Core_Controller_Action_Api extends Zend_Controller_Action
{
    /**
     * @var Zend_Controller_Action_Helper_ContextSwitch
     */
    protected $contextSwitch;

    protected $authenticated = false;
    protected $authenticated_admin;
    protected $authenticated_admin_id;

    public function preDispatch()
    {
        // change view encoding
        $this->view->setEncoding('UTF-8');
        $model = new Model_Fines();
        $model->getAdapter()->query("SET NAMES 'utf8' ");
    }

    public function init()
    {
        parent::init();

        if (! $this->_request->getParam('disable_context')) {
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(true);

            $this->contextSwitch = $this->_helper->getHelper('contextSwitch');
            $this->contextSwitch->addActionContext($this->getRequest()->getActionName(), ['json']);
            $this->contextSwitch->setDefaultContext('json')->setAutoJsonSerialization(true)->initContext();
            $this->view->setEncoding('UTF-8');
            $this->view->success = 'true';
            $this->view->version = '1.0';
        }
    }

    public function checkSession()    {

        if (! $this->_request->getParam('disable_context') && $this->_request->isPost()) {
            $domain_model = new Model_Domain();
            $options = $domain_model->options();
            $request = json_decode($this->_request->getRawBody(), true);

            $device_id = ( $request['adm_device_id'] ) ? $request['adm_device_id'] : $this->_request->getParam('adm_device_id');

            if ($device_id) {
                $model = new Model_Admins();
                $select = $model->select();
                $select->where('adm_device_id = ?', $device_id);

                if ($options['apps_username'] == true) {
                    $username = $this->_request->getParam('username');
                    if($username){
                    $select->where('adm_login = ?', $username);
                    }
                }

                $admin = $model->fetchRow($select);
                if ($admin) {
                    $this->authenticated = true;
                    $this->authenticated_admin = $admin->adm_name.' '.$admin->adm_surname;
                    $this->authenticated_admin_id = $admin->id;
                } else {
                    $this->authenticated = false;
                    $this->view->success = false;
                    $this->view->has_session = false;
                    $this->view->msg = 'Device ID No Found';
                }
            } else {
                $this->authenticated = false;
                $this->view->has_session = false;
                $this->view->success = 'false';
                $this->view->msg = 'No Device ID';
            }
        } else {
            $this->view->success = 'false';
            $this->view->msg = 'Bad request';
        }
    }
}
