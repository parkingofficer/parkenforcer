<?php

/**
 * Description of Aggregator.
 *
 * @author Darius Matulionis
 */
class Core_Navigation_Aggregator
{
    private $front;
    private $module;

    public function __construct($front, $module = null)
    {
        $this->front = $front;
        $this->module = $module;
    }

    /**
     * loop all controllers and return Navigation object.
     *
     * @return Zend_Navigation
     */
    public function getNavigation()
    {
        $pages = [[
                'label' => 'Pradžia',
                'module' => 'admin',
                'controller' => 'index',
                'action' => 'index',
                'lang' => LANG,
                'route' => 'defaults',
                ]];

        $controllers = $this->getControllers();
        $domain = new Model_Domain();
        $domain_options = $domain->options();
        if (! empty($domain_options['modules'])) {
            $modules = json_decode($domain_options['modules']);

            if (! empty($controllers)) {
                foreach ($controllers as $controller => $actions) {
                    if (! in_array($controller, $modules)) {
                        continue;
                    }
                    if (in_array('Core_Controller_Navigation_Interface', class_implements($controller.'Controller'))) {
                        $controllerName = $controller.'Controller';
                        if (function_exists('call_user_func')) {
                            $navigation = call_user_func($controllerName.'::getNavigation');
                        } else {
                            $navigation = call_user_method('getNavigation', $controllerName);
                        }
                        $pages[0]['pages'][$controller] = $navigation;
                    }
                }
            }
        } else {
            die('no modules available');
        }

        $group_id = Zend_Auth::getInstance()->getStorage()->read()->admin_groups_id;
        if (! Zend_Auth::getInstance()->getStorage()->read()->adm_root && Core_Auth_Admin::getInstance()->hasIdentity() && $group_id) {
            /**
             * var Zend_Acl.
             */
            $acl = Zend_Registry::get('acl');

            foreach ($pages[0]['pages'] as $controller => $page) {
                if (isset($page ['pages']) && ! empty($page ['pages']) && $controller) {
                    foreach ($page ['pages'] as $k => $p) {
                        $perm = null;
                        if (isset($p ['pages']) && ! empty($p ['pages'])) {
                            foreach ($p ['pages'] as $kk => $v) {
                                $perm = null;
                                $perm = str_replace('-', '_', $v ['controller'].'_'.$v ['action']);
                                if (in_array($perm, $acl->getResources())) {
                                    if (! $acl->isAllowed($group_id, $perm)) {
                                        //Zend_Debug::dump($pages[0]['pages'][$controller]['pages'][$k]['pages'][$kk]);
                                        unset($pages[0]['pages'] [$controller] ['pages'] [$k] ['pages'] [$kk]);
                                    }
                                }
                            }
                            if (empty($pages[0]['pages'] [$controller] ['pages'] [$k] ['pages'])) {
                                unset($pages[0]['pages'] [$controller] ['pages'] [$k]);
                            }
                        }
                        $perm = str_replace('-', '_', $p ['controller'].'_'.$p ['action']);

                        if (in_array($perm, $acl->getResources())) {
                            if (! $acl->isAllowed($group_id, $perm)) {
                                //var_dump($perm);
                                // Zend_Debug::dump($pages[0]['pages'][$controller]['pages'][$k]['pages'][$kk]);
                                if (empty($pages[0]['pages'] [$controller] ['pages'] [$k] ['pages'])) {
                                    unset($pages[0]['pages'] [$controller] ['pages'] [$k]);
                                }
                            }
                        }
                    }
                    if (empty($pages[0]['pages'] [$controller] ['pages'])) {
                        unset($pages[0]['pages'] [$controller]);
                    }
                }
            }
        }

        $nav = new Zend_Navigation($pages);

        return $nav;
    }

    public function getControllers()
    {
        $acl = [];

        foreach ($this->front->getControllerDirectory() as $mdl => $path) {
            foreach (scandir($path) as $file) {
                if (strstr($file, 'Controller.php') !== false) {
                    include_once $path.DIRECTORY_SEPARATOR.$file;
                }
            }
        }

        $declaredClasses = get_declared_classes();

        sort($declaredClasses);

        foreach ($declaredClasses as $class) {
            if (is_subclass_of($class, 'Zend_Controller_Action')) {
                $controller = substr($class, 0, strpos($class, 'Controller'));

                if ($this->module && substr($controller, 0, strlen($this->module)) != $this->module) {
                    continue;
                }

                $actions = [];

                foreach (get_class_methods($class) as $action) {
                    if (strstr($action, 'Action') !== false) {
                        $acl[$controller][] = $action;
                    }
                }
            }
        }

        return $acl;
    }
}
