<?php
/**
 * Description of Adapter.
 *
 * @author Darius Matulionis
 */
class Core_Ftp_Transfere_Adapter
{
    protected $_host;
    protected $_userName;
    protected $_password;
    protected $_ftpConnection;
    protected $_ftpLogin;
    protected $_baseFtpPath;
    protected $folderTree = [];
    /**
     * @param string $host
     * @param string $username
     * @param string $password
     * @return Core_Ftp_Transfere_Adapter
     */
    public function __construct($host, $username, $password)
    {
        $this->_host = $host;
        $this->_userName = $username;
        $this->_password = $password;
    }

    /**
     * @param string $filename
     * @param string $dir
     * @param bool $passive_mode
     */
    public function getFile($filename, $save_file, $dir, $passive_mode = false)
    {
        $this->_ftpConnection = ftp_connect($this->_host);
        $this->_ftpLogin = ftp_login($this->_ftpConnection, $this->_userName, $this->_password);
        ftp_pasv($this->_ftpConnection, $passive_mode);
        if (! $this->_ftpLogin) {
            ftp_quit($this->_ftpConnection);
            die('Connection failed, check your login and password');
        }

        if ($dir) {
            if (! ftp_chdir($this->_ftpConnection, $dir)) {
                ftp_quit($this->_ftpConnection);
            }
        }

        ftp_get($this->_ftpConnection, $save_file, $filename, FTP_ASCII);
    }
}
