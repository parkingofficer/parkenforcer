<?php

/**
 * Description of Translate.
 *
 * @author Darius Matulionis
 */
class Core_Translate extends Zend_Translate_Adapter
{
    /**
     * @var Zend_Translate
     */
    protected $translator;

    public function __construct($options = [])
    {
        parent::__construct($options);
    }

    public function _($messageId, $locale = null)
    {
        if (Core_Auth_Admin::getInstance()->hasIdentity()) {
            if (! $this->isTranslated($messageId, $original = false, $locale)) {
                $no_translations = Zend_Registry::get('not_translated');
                $no_translations[LANG][] = $messageId;
                $no_translations[LANG] = array_unique($no_translations[LANG]);
                Zend_Registry::set('not_translated', $no_translations);
            }
        }

        return parent::_($messageId, $locale = null);
        //return $this->_($messageId, $locale = null);
    }

    /**
     * Write Translations to file from data.
     * @param array $translateData
     */
    public function writeTranslate(array $translateData)
    {
        if (! empty($translateData)) {
            $translate_file = APPLICATION_PATH.'/translations/'.LANG.'.php';
            $data = "<?php \r\n";

            foreach ($translateData as $key => $value) {
                if (! empty($value)) {
                    $data .= '$lang["'.$key.'"] = "'.$value.'";'."\n";
                }
            }

            if (file_exists($translate_file)) {
                $ini_handle = fopen($translate_file, 'r');
                $ini_contents = fread($ini_handle, filesize($translate_file));
                fclose($ini_handle);
                //done obtaining initially present data
                //write new data to the file, along with the old data
                $handle = fopen($translate_file, 'w+');
                $writestring = $data.substr($ini_contents, 5);
                Zend_Debug::dump($writestring);
                if (fwrite($handle, $writestring) === false) {
                    throw new Zend_Translate_Exception('Cannot write to file: '.$translate_file);
                }
                fclose($handle);
            } else {
                throw new Zend_Translate_Exception('Translations file: '.$translate_file.' does not exists');
            }
        }
    }

    /**
     * returns the adapters name.
     *
     * @return string
     */
    public function toString()
    {
        return 'Array';
    }

    /**
     * Load translation data.
     *
     * @param  string|array  $data
     * @param  string        $locale  Locale/Language to add data for, identical with locale identifier,
     *                                see Zend_Locale for more information
     * @param  array         $options OPTIONAL Options to use
     * @return array
     */
    protected function _loadTranslationData($data, $locale, array $options = [])
    {
        $this->_data = [];
        if (! is_array($data)) {
            if (file_exists($data)) {
                ob_start();
                $data = include $data;
                ob_end_clean();
            }
        }
        if (! is_array($data)) {
            require_once'Zend/Translate/Exception.php';
            throw new Zend_Translate_Exception("Error including array or file '".$data."'");
        }

        if (! isset($this->_data[$locale])) {
            $this->_data[$locale] = [];
        }

        $this->_data[$locale] = $data + $this->_data[$locale];

        return $this->_data;
    }
}
