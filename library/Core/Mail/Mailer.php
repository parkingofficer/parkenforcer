<?php

class Core_Mail_Mailer extends Zend_Mail
{
    public static $fromName = 'PPMI';

    public static $fromEmail = 'no-reply@ppmi.lt';
    public static $mailServer = '';
    public static $_port = 25;
    public static $_userName = '';
    public static $_password = '';

    /**
     * @var Zend_Mail_Transport_Smtp
     */
    public static $transport;

    /**
     * @var Zend_View
     */
    public static $_defaultView;

    /**
     * current instance of our Zend_View.
     * @var Zend_View
     */
    protected $_view;

    public function __construct($charset = 'UTF-8', $mail_type = 'info')
    {
        parent::__construct($charset);

        if (! empty(self::$mailServer) && ! empty(self::$_userName) && ! empty(self::$_password) && ! empty(self::$_port)) {
            $config = ['auth' => 'login',
                    'username' => self::$_userName,
                    'password' => self::$_password,
                    'port' => self::$_port,
                    'name' => self::$mailServer,
            ];
            self::$transport = new Zend_Mail_Transport_Smtp(self::$mailServer, $config);
        }

        $this->setFrom(self::$fromEmail, self::$fromName);
        $this->_view = self::getDefaultView();
    }

    public function sendMail()
    {
        $this->send(self::$transport);
    }

    public function sendHtmlTemplate($template, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        $html = $this->_view->render($template);
        $this->setBodyHtml($html, $this->getCharset(), $encoding);
        $this->send();
    }

    public function setViewParam($property, $value)
    {
        $this->_view->__set($property, $value);

        return $this;
    }

    protected static function getDefaultView()
    {
        if (self::$_defaultView === null) {
            self::$_defaultView = new Zend_View();
            self::$_defaultView->setScriptPath(APPLICATION_PATH.
                    '/views/scripts/mails');
        }

        return self::$_defaultView;
    }
}
