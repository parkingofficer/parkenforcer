<?php
/**
 * Description of UserExists.
 *
 * @author Darius Matulionis
 */
class Core_Form_Validate_ClientCode extends Zend_Validate_Abstract
{
    const USER_EXISTS = 'Toks kodas jau egzistuoja';
    /**
     * @var array
     */
    protected $_messageTemplates = [
        self::USER_EXISTS => 'Toks kodas jau egzistuoja', ];
    /**
     * Defined by Zend_Validate_Interface.
     *
     * @param $value mixed
     * @return bool
     */
    public function isValid($value)
    {
        $model = new Model_Clients();
        if ($model->checkCodeExists($value)) {
            $this->_error(self::USER_EXISTS);

            return false;
        }

        return true;
    }
}
