<?php
/**
 * @name PasswordConfirmation
 * @copyright Baltic Aviation Academy
 * @author Darius Matulionis <d.matulionis@balticaa.com>
 * @since : 2011-04-18, 13.31.45
 */
class Core_Form_Validate_PasswordConfirmation extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';
    protected $_messageTemplates = [
        self::NOT_MATCH => 'Slaptažodžiai nesutampa', ];
    protected $_belongsTo = null;
    public function __construct($options = null)
    {
        if (is_array($options) && array_key_exists('belongsTo', $options)) {
            $this->setBelongs($options ['belongsTo']);
        }
    }
    public function setBelongs($field)
    {
        $this->_belongsTo = $field;
    }
    public function isValid($value, $context = null)
    {
        $value = (string) $value;
        $this->_setValue($value);
        if (is_array($context)) {
            if ($this->_belongsTo) {
                if (isset($context [$this->_belongsTo] ['password_confirm']) && ($value == $context [$this->_belongsTo] ['password_confirm'])) {
                    return true;
                }
            } else {
                if (isset($context ['password_confirm']) && ($value == $context ['password_confirm'])) {
                    return true;
                }
            }
        } elseif (is_string($context) && ($value == $context)) {
            return true;
        }
        $this->_error(self::NOT_MATCH);

        return false;
    }
}
