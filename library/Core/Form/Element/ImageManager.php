<?php

class Core_Form_Element_ImageManager extends Zend_Form_Element
{
    /**
     * Use formTextarea view helper by default.
     * @var string
     */
    public $helper = 'formImageManager';
}
