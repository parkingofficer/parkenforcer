<?php

class Core_Form_Element_TinyMceOff extends Zend_Form_Element_Textarea
{
    /**
     * Use formTextarea view helper by default.
     * @var string
     */
    public $helper = 'formTinyMceOff';
}
