<?php


class Core_Form_SubForm extends Zend_Form_SubForm
{
    public function __construct($options = null)
    {
        parent::__construct($options);
    }

    public function addSubmitButtons()
    {
        $submit = $this->createElement('submit', 'save');
        $submit->setLabel('Saugoti')
               ->setAttrib('class', 'submitSave')
               ->removeDecorator('label')
               ->removeDecorator('HtmlTag')
               ->removeDecorator('DtDdWrapper');
        $this->addElement($submit);

        return $this;
    }

    public function addFieldsetDecorator($subform = null)
    {
        if ($subform) {
            $subform->setDecorators([
                'FormElements',
                'Fieldset',
                ['HtmlTag',['tag' => 'div', 'class' => 'parameters']],
            ]);
        } else {
            $this->addDecorators([['HtmlTag', ['tag' => 'fieldset', 'class' => 'parameters']]]);
        }
    }
}
