<?php

class Core_Log_Writer_View extends Zend_Log_Writer_Stream
{
    public function write($event)
    {
        $translations = Zend_Registry::get('not_translated');
        $event = explode('#', $event['message']);
        $translations[$event[0]][$event[1]] = $event[1];
        Zend_Registry::set('not_translated', $translations);
    }
}
