<?php

class Core_Cache_Adapter extends Zend_Controller_Plugin_Abstract
{
    protected $frontEnd;
    protected $backEnd;
    protected $cache;

    public function __construct($config)
    {
        $this->backEnd = $config['backend'];
        $this->frontEnd = $config['frontend'];
        $this->initCache();
    }

    private function initCache()
    {
        $this->cache = Zend_Cache::factory('Core', 'File', $this->frontEnd, $this->backEnd);
        Zend_Registry::set('cache', $this->cache);
    }
}
