<?php
/**
 * @category   Core
 * @copyright  Copyright (c) 2009-2012 Bal‡zs Micskey
 * @license    New BSD License
 */
class Core_Translate_Adapter_Db extends Zend_Translate_Adapter
{
    private $_data = [];

    /**
     * Load translation data.
     *
     * @param  string        $locale  Locale/Language to add data for, identical with locale identifier,
     *                                see Zend_Locale for more information
     * @param  array         $options Not used in this context
     * @return array         translation data in associative array
     */
    public function loadTranslationData($locale, array $options = [], $noCache = null)
    {
        return $this->_loadTranslationData($noCache, $locale, $options);
    }

    /**
     * @desc returns the adapters name
     * @return string
     */
    public function toString()
    {
        return 'Db';
    }

    /**
     * Load translation data.
     *
     * @param  string        $data    Not used in this context
     * @param  string        $locale  Locale/Language to add data for, identical with locale identifier,
     *                                see Zend_Locale for more information
     * @param  array         $options Not used in this context
     * @return array         translation data in associative array
     */
    protected function _loadTranslationData($data, $locale, array $options = [])
    {
        $_translations = new Model_Translate();
        $_translations->getAdapter()->query('SET NAMES UTF8');

        $disable = false;
        if ($data == 'noCache') {
            $disable = true;
        }

        if ($this->hasCache()) {
            $translations = $_translations->getListByLocaleNoCache($locale);
        } else {
            $translations = $_translations->getListByLocale($locale);
        }

        foreach ($translations as $translation) {
            if ($this->hasCache()) {
                $this->_data[$translation['locale']][$translation['msgid']] = $translation['msgstring'];
            } else {
                $this->_data[$locale][$translation['msgid']] = $translation['msgstring'];
            }
        }

        if (! is_array($this->_data[$locale])) {
            $this->_data[$locale] = ['0' => false];
        }

        return $this->_data;
    }
}
