<?php

class Core_Translate_Adapter_Logger extends Zend_Log_Writer_Abstract
{
    /**
     * Database adapter instance.
     *
     * @var Zend_Db_Table
     */
    protected $_db;

    /**
     * Name of the log table in the database.
     *
     * @var string
     */
    protected $_table;

    /**
     * Relates database columns names to log data field keys.
     *
     * @var null|array
     */
    protected $_columnMap;

    /**
     * Untranslated messages.
     *
     * @var null|array
     */
    protected $_untranslated;

    /**
     * Class constructor.
     *
     * @param Zend_Db_Adapter $db   Database adapter instance
     * @param string $table         Log table in database
     * @param array $columnMap
     * @return void
     */
    public function __construct($db, $table, $columnMap = null)
    {
        $this->_db = $db;
        $this->_table = $table;
        $this->_columnMap = $columnMap;
    }

    /**
     * Create a new instance of Zend_Log_Writer_Db.
     *
     * @param  array|Zend_Config $config
     * @return Zend_Log_Writer_Db
     */
    public static function factory($config)
    {
        $config = self::_parseConfig($config);
        $config = array_merge([
            'db' => null,
            'table' => null,
            'columnMap' => null,
        ], $config);

        if (isset($config['columnmap'])) {
            $config['columnMap'] = $config['columnmap'];
        }

        return new self(
            $config['db'],
            $config['table'],
            $config['columnMap']
        );
    }

    /**
     * Formatting is not possible on this writer.
     *
     * @return void
     * @throws Zend_Log_Exception
     */
    public function setFormatter(Zend_Log_Formatter_Interface $formatter)
    {
        require_once 'Zend/Log/Exception.php';
        throw new Zend_Log_Exception(get_class($this).' does not support formatting');
    }

    /**
     * Remove reference to database adapter.
     *
     * @return void
     */
    public function shutdown()
    {
        $this->_db = null;
    }

    public function getUntranslatedMessages()
    {
        return $this->_untranslated;
    }

    /**
     * Write a message to the log.
     *
     * @param  array  $event  event data
     * @return void
     * @throws Zend_Log_Exception
     */
    protected function _write($event)
    {
        if ($this->_db === null) {
            require_once 'Zend/Log/Exception.php';
            throw new Zend_Log_Exception('Database adapter is null');
        }

        $message = explode(' #&# ', $event['message']);
        if (count($message) == 2) {
            $msgid = $message[0];
            $locale = $message[1];
            $row = $this->_db->fetchRow($this->_db->select()->where('msgid = ?', $msgid)->where('locale = ?', $locale));
            if (! $row) {
                $this->_db->save([
                    'msgid' => $msgid,
                    'msgstring' => $msgid,
                    'locale' => $locale,
                    'is_translated' => 0,
                ]);
            } elseif ($row->is_translated == 0) {
                $this->_untranslated[] = $row->toArray();
            }
        }
    }
}
