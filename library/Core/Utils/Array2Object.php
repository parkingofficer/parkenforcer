<?php

class Core_Utils_Array2Object
{
    public function __get($key)
    {
        return isset($this->$key) ? $this->$key : '';
    }
    public static function get($array)
    {
        $obj = new Array2Object();

        if (is_array($array)) {
            foreach ($array as $k => $v) {
                $obj->$k = $v;
            }
        }

        return $obj;
    }

    public static function getArray($object)
    {
        $arr = [];
        if (is_object($object)) {
            foreach ($object as $k => $v) {
                $arr[$k] = $v;
            }

            return $arr;
        }

        return [];
    }
}
