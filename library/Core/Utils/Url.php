<?php

/**
 * Description of Url.
 *
 * @author Darius Matulionis
 */
class Core_Utils_Url
{
    private $string;

    public function __construct($string = '')
    {
        $this->string = $string;
    }

    public function __toString()
    {
        return $this->string;
    }
    public static function format($string)
    {
        $convert = [
            'ą' => 'a',
            'Ą' => 'A',
            'č' => 'c',
            'Č' => 'C',
            'ę' => 'e',
            'Ę' => 'E',
            'ė' => 'e',
            'Ė' => 'E',
            'į' => 'i',
            'Į' => 'I',
            'š' => 's',
            'Š' => 's',
            'ų' => 'u',
            'Ų' => 'U',
            'ū' => 'u',
            'Ū' => 'U',
            'ž' => 'z',
            'Ž' => 'Z',
            //rus
            'А' => 'A',
            'а' => 'a',
            'Б' => 'B',
            'б' => 'b',
            'В' => 'V',
            'в' => 'v',
            'Г' => 'G',
            'г' => 'g',
            'Д' => 'D',
            'д' => 'd',
            'Е' => 'E',
            'е' => 'e',
            'Ё' => 'Jo',
            'ё' => 'jo',
            'Ж' => 'Zh',
            'ж' => 'zh',
            'З' => 'z',
            'з' => 'z',
            'И' => 'I',
            'и' => 'i',
            'Й' => 'J',
            'й' => 'j',
            'К' => 'K',
            'к' => 'k',
            'Л' => 'L',
            'л' => 'l',
            'М' => 'M',
            'м' => 'm',
            'Н' => 'N',
            'н' => 'n',
            'О' => 'O',
            'о' => 'o',
            'П' => 'P',
            'п' => 'p',
            'Р' => 'R',
            'р' => 'r',
            'С' => 'S',
            'с' => 's',
            'Т' => 'T',
            'т' => 't',
            'У' => 'U',
            'у' => 'u',
            'ф' => 'F',
            'ф' => 'f',
            'Х' => 'H',
            'х' => 'h',
            'Ц' => 'C',
            'ц' => 'c',
            'Ч' => 'Ch',
            'ч' => 'ch',
            'Ш' => 'Sh',
            'ш' => 'sh',
            'Щ' => 'Th',
            'щ' => 'th',
            'ъ' => '-',
            'Ы' => 'Y',
            'ы' => 'y',
            'ь' => '-',
            'Э' => 'Ey',
            'э' => 'ey',
            'Ю' => 'Ju',
            'ю' => 'ju',
            'Я' => 'JA',
            'я' => 'ja',
            //kiti
            'ç' => 'c',
            'Ç' => 'C',
            'ł' => 'l',
            'Ł' => 'L',
        ];

        $var = $string;
        if ($var) {
            mb_internal_encoding('UTF-8');
            mb_regex_encoding('UTF-8');

            foreach ($convert as $k => $v) {
                $var = self::mb_str_ireplace($k, $v, $var);
            }

            $var = mb_ereg_replace('[^A-Za-z0-9]', '-', strtolower($var), 'utf-8');
            $var = preg_replace('/-+/', '-', $var);

            return $var;
        }

        return $string;
    }

    public static function mb_str_ireplace($co, $naCo, $wCzym)
    {
        if (empty($wCzym)) {
            die();
        }
        $wCzymM = mb_strtolower($wCzym);
        $coM = mb_strtolower($co);
        $offset = 0;

        while (($poz = mb_strpos($wCzymM, $coM, $offset)) !== false) {
            $offset = $poz + mb_strlen($naCo);
            $wCzym = mb_substr($wCzym, 0, $poz).$naCo.mb_substr($wCzym, $poz + mb_strlen($co));
            $wCzymM = mb_strtolower($wCzym);
        }

        return $wCzym;
    }

    public static function trimUrl($string, $limit = 30)
    {
        if (mb_strlen($string, 'utf-8') > $limit) {
            return mb_substr($string, 0, $limit - 10, 'utf-8').'...'.mb_substr($string, -10);
        }

        return $string;
    }
}
