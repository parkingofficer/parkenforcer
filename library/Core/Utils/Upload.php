<?php

class Core_Utils_Upload
{
    /**
     * Max file size.
     *
     * @var int
     */
    public $max_file_size = 40572864; // 5,5 Mb
    /**
     * Max image width.
     *
     * @var int
     */
    public $max_image_width = 1024; // pixels
    /**
     * an array which contains uploaded files.
     *
     * @var array
     */
    public $_files = [];
    /**
     * path to files's icons folder.
     *
     * @var string
     */
    public $icons_path = 'ico/';
    /**
     * Class constructor.
     *
     * @param $file_size integer
     *       	 max file size
     * @param $target_dir string
     *       	 dir to upload
     */
    // }}}
    // {{{ setMaxWidth()
    /**
     * Setting max image width.
     */
    public function setMaxWidth($width)
    {
        $this->max_image_width = $width;
    }
    // }}}
    // {{{ setMaxSize()
    /**
     * Setting max file size.
     */
    public function setMaxSize($size)
    {
        $this->max_file_size = $size;
    }
    // }}}
    // {{{ checkDir()
    /**
     * Check destination dir.
     * If dir not exists, create new one.
     *
     * @param $dir string
     *       	 destination folder
     * @return absolute path to dir
     */
    public function checkDir($dir)
    {
        $dir = preg_replace('/(.*)(\\/)$/', '\\1', $dir);
        // $absolute_dir = $_SERVER['DOCUMENT_ROOT'] . '' . $dir;
        $absolute_dir = $dir;
        if (! is_dir($absolute_dir)) {
            mkdir($absolute_dir, 0777);
            // eval("chmod(\"".$absolute_dir."\", 0777);"); // if php script has
            // special rights to execute chmod
            chmod($absolute_dir, 0777);
        }

        return $absolute_dir;
    }
    // end checkDir()
    // }}}
    // {{{ checkGd()
    /**
     * Check GD library version.
     *
     * @return GD version or false
     */
    public function checkGd()
    {
        $gd_content = get_extension_funcs('gd'); // Grab function list
        if (! $gd_content) {
            $this->message_err('Upload class: GD libarary is not installed!', '', '', E_USER_ERROR);

            return false;
        } else {
            ob_start();
            phpinfo(8);
            $buffer = ob_get_contents();
            ob_end_clean();
            if (strpos($buffer, '2.0')) {
                return 'gd2';
            } else {
                return 'gd';
            }
        }
    }
    // }}}
    // {{{ deleteFile()
    /**
     * Delete file from server.
     *
     * @param $filename string
     *       	 filename
     * @param $source_dir string
     *       	 the dir in which is file
     * @return bool true|falses
     */
    public function deleteFile($filename, $source_dir)
    {
        $source_dir = $this->checkDir($source_dir);
        if (file_exists($source_dir.'/'.$filename)) {
            if (unlink($source_dir.'/'.$filename)) {
                return true;
            }
        }

        return false;
    }
    // }}}
    // {{{ lantinEncode()
    /**
     * Encode string from russian to latin.
     *
     * @param $str string
     * @return string $new_string
     */
    public function lantinEncode($str)
    {
        $accordance = [
            'a',
            'b',
            'v',
            'g',
            'd',
            'e',
            'zh',
            'z',
            'i',
            'y',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'r',
            's',
            't',
            'u',
            'f',
            'h',
            'ch',
            'c',
            'sh',
            'sh',
            '',
            'i',
            '',
            'e',
            'u',
            'ya', ];
        $cirilyc = [
            'a',
            'į',
            'ā',
            'ć',
            'ä',
            'å',
            'ę',
            'ē',
            'č',
            'é',
            'ź',
            'ė',
            'ģ',
            'ķ',
            'ī',
            'ļ',
            'š',
            'ń',
            'ņ',
            'ó',
            'ō',
            'õ',
            '÷',
            'ö',
            'ų',
            'ł',
            'ś',
            'ū',
            'ü',
            'ż',
            'ž',
            '˙', ];
        $new_str = '';
        for ($i = 0; $i < strlen($str); $i++) {
            $symbol = substr($str, $i, 1);
            for ($j = 0; $j < count($cirilyc); $j++) {
                if ($symbol == $cirilyc [$j]) {
                    $symbol = $accordance [$j];
                }
            }
            $new_str .= $symbol;
        }

        return $new_str;
    }
    // }}}
    // {{{ uploadFile()
    /**
     * Upload file to server.
     *
     * @param $target_dir string
     *       	 destination dir
     * @param $encode string
     *       	 encode type
     * @param $name_length string
     *       	 filename length
     * @param $filesArr Array
     *       	 usualy $_FILES
     */
    public function uploadFile($target_dir = '', $encode = 'md5', $name_length = 10, $filesArr)
    {
        $target_dir = $this->checkDir($target_dir); // print_r($_FILES); exit;
        foreach ($filesArr as $varname => $array) {
            if (! empty($array ['name'])) {
                if (is_uploaded_file($array ['tmp_name'])) {
                    $filename = strtolower(str_replace(' ', '', $array ['name']));
                    $basefilename = preg_replace('/(.*)\\.([^.]+)$/', '\\1', $filename);
                    $ext = preg_replace('/.*\\.([^.]+)$/', '\\1', $filename);
                    if ($array ['size'] > $this->max_file_size) {
                        $this->message_err('Upload class: Impossible upload file size', '', '', E_USER_WARNING);
                        // } elseif (!in_array($ext, $GLOBALS['VALID_TYPES'])) {
                        // $this->message_err('Upload class: Impossible upload
                        // file type', '', '', E_USER_WARNING);
                    } else {
                        // encode filename
                        switch ($encode) {
                            case 'md5' :
                                $basefilename = substr(md5($basefilename.microtime()), 0, $name_length);
                                $filename = $basefilename.'.'.$ext;
                                break;
                            case 'latin' :
                                $basefilename = substr($this->lantinEncode($basefilename), 0, $name_length);
                                $filename = $basefilename.'.'.$ext;
                                break;
                        }
                        if (! move_uploaded_file($array ['tmp_name'], $target_dir.'/'.$filename)) {
                            $this->message_err('Upload class: cannot upload file', '', '', E_USER_NOTICE);
                        }
                        $this->_files [$varname] = $filename;
                    }
                } else {
                    // $this->message_err('Upload class: cannot create tmp
                    // file', '', '', E_USER_NOTICE);
                }
            }
        }
    }
    /**
     * Upload file to server.
     *
     * @param $target_dir string
     *       	 destination dir
     * @param $encode string
     *       	 encode type
     * @param $name_length string
     *       	 filename length
     * @param $filesArr Array
     *       	 usualy $_FILES
     */
    public function uploadFileCustom($target_dir = '', $encode = 'md5', $name_length = 10, $filesArr)
    {
        if (is_array($filesArr ['name']) && ! empty($filesArr ['name'])) {
            $files = [];
            foreach ($filesArr ['name'] as $key => $file) {
                $files [$key] = [
                    'name' => $filesArr ['name'] [$key],
                    'type' => $filesArr ['type'] [$key],
                    'tmp_name' => $filesArr ['tmp_name'] [$key],
                    'error' => $filesArr ['error'] [$key],
                    'size' => $filesArr ['size'] [$key], ];
            }
            if (! empty($files)) {
                $this->uploadFile($target_dir, $encode, $name_length, $files);
            }
        }
    }
    // }}}
    // {{{ imgResize()
    /**
     * Resize image on the server.
     *
     * @param $filename string
     *       	 the filename of image
     * @param $source_dir string
     *       	 the dir where this image stored
     * @param $dest_width string
     *       	 destination image width
     * @param $duplicate bool
     *       	 set true if you want to duplicate image
     * @return bool true|false
     */
    public function imgResize($filename, $source_dir, $dest_width, $duplicate = false)
    {
        $source_dir = $this->checkDir($source_dir);
        $full_path = $source_dir.'/'.$filename;
        $basefilename = preg_replace('/(.*)\\.([^.]+)$/', '\\1', $filename);
        $ext = preg_replace('/.*\\.([^.]+)$/', '\\1', $filename);
        switch ($ext) {
            case 'png' :
                $image = imagecreatefrompng($full_path);
                break;
            case 'jpg' :
                $image = imagecreatefromjpeg($full_path);
                break;
            case 'jpeg' :
                $image = imagecreatefromjpeg($full_path);
                break;
            default :
                $this->message_err('Upload class: the '.$ext.' format is not allowed in your GD version', '', '', E_USER_ERROR);
                break;
        }
        $image_width = imagesx($image);
        $image_height = imagesy($image);
        // resize image pro rata
        $coefficient = ($image_width > $this->max_image_width) ? (real) ($this->max_image_width / $image_width) : 1;
        $dest_width = (int) ($image_width * $coefficient);
        $dest_height = (int) ($image_height * $coefficient);
        // create copy of original image and next working with copy.
        // an original image still old
        if (false !== $duplicate) {
            $filename = $basefilename.'_2.'.$ext;
            copy($full_path, $source_dir.'/'.$filename);
        }
        if ('gd2' == $this->checkGd()) {
            $img_id = imagecreatetruecolor($dest_width, $dest_height);
            imagecopyresampled($img_id, $image, 0, 0, 0, 0, $dest_width + 1, $dest_height + 1, $image_width, $image_height);
        } else {
            $img_id = imagecreate($dest_width, $dest_height);
            imagecopyresized($img_id, $image, 0, 0, 0, 0, $dest_width + 1, $dest_height + 1, $image_width, $image_height);
        }
        switch ($ext) {
            case 'png' :
                imagepng($img_id, $source_dir.'/'.$filename);
                break;
            case 'jpg' :
                imagejpeg($img_id, $source_dir.'/'.$filename);
                break;
            case 'jpeg' :
                imagejpeg($img_id, $source_dir.'/'.$filename);
                break;
        }
        imagedestroy($img_id);

        return true;
    }
    // }}}
    // {{{ displayFiles()
    /**
     * Display files in destination folder.
     *
     * @param $source_dir the
     *       	 dir where files stored
     */
    public function displayFiles($source_dir)
    {
        $source_dir = $this->checkDir($source_dir);
        $contents = opendir($source_dir);
        if ($contents) {
            echo 'Directory contents:<br>';
            while (false !== ($file = readdir($contents))) {
                if (is_dir($file)) {
                    continue;
                }
                $filesize = (real) (filesize($source_dir.'/'.$file) / 1024);
                $filesize = number_format($filesize, 3, ',', ' ');
                $ext = preg_replace('/.*\\.([^.]+)$/', '\\1', $file);
                echo '<p><img src="'.$this->icons_path.''.$ext.'.gif">&nbsp;'.$file.'&nbsp;('.$filesize.') Kb</p>';
            }
        }
    }
    // }}}
    // {{{ fileRename()
    /**
     * Rename target file.
     *
     * @param $source_dir the
     *       	 dir where files stored
     * @param $filename the
     *       	 source filename
     * @param $newname new
     *       	 filename
     */
    public function fileRename($source_dir, $filename, $newname)
    {
        $source_dir = $this->checkDir($source_dir);
        if (rename($source_dir.'/'.$filename, $newname)) {
            return true;
        } else {
            return false;
        }
    }
    // }}}
    // {{{ message_err()
    /**
     * Trigger error.
     */
    public function message_err($error_msg, $err_line, $err_file, $error_type = E_USER_WARNING)
    {
        $error_msg .= (! empty($err_line)) ? 'on line '.$err_line : '';
        $error_msg .= (! empty($err_file)) ? 'in - '.$err_file : '';
        trigger_error($error_msg, $error_type);
    }
}
?>