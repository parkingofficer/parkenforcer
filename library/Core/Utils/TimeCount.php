<?php

class Core_Utils_TimeCount
{
    public static function getDifference($r_date)
    {
        $date_now = date_parse(date('Y-m-d'));
        $date_start = date_parse($r_date);
        $jd_start = gregoriantojd($date_now['month'], $date_now['day'], $date_now['year']);
        $jd_end = gregoriantojd($date_start['month'], $date_start['day'], $date_start['year']);
        $diff = $jd_start - $jd_end;

        return $diff;
    }

    public static function getTimeDifference($minutes)
    {
        $r_timestamp = strtotime($minutes);
        $diff = time() - $r_timestamp;

        return $diff;
    }

    public static function getTime($date)
    {
        $r_date = substr($date, 0, 10);
        $r_time = substr($date, 11, 8);

        $days = self::getDifference($r_date);
        if ($days == 0) {
            $time = self::getTimeDifference($r_time);
            if ($time < 60) {
                return "prieš $time s";
            } elseif ($time >= 3600) {
                $hours = intval($time / 3600);

                return "prieš $hours val.";
            } else {
                $minutes = intval($time / 60);

                return "prieš $minutes min.";
            }

            return $time;
        } elseif ($days < 7) {
            if ($days == 0) {
                return false;
            }
            if ($days == 1) {
                return 'vakar';
            }
            if ($days == 2) {
                return 'užvakar';
            }
            if ($days > 2) {
                return "prieš $days d.";
            }
        } elseif ($days >= 31) {
            $month_count = intval($days / 31);

            return "prieš $month_count mėn.";
        } else {
            $week_count = intval($days / 7);

            return "prieš $week_count sav.";
        }
    }
}
