<?php

class Core_Db_Table extends Zend_Db_Table
{
    /**
     * @return string
     */
    public static function table()
    {
        return (new static)->_name;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getPrimary()
    {
        return $this->_primary;
    }

    public function init()
    {
        /*
         * $front = Zend_Controller_Front::getInstance(); $db =
         * $front->getParam('bootstrap')->getResource('db'); $db->query('SET
         * NAMES "utf8"'); Zend_Registry::set('db', $db);
         * $this->getAdapter()->query('SET NAMES "utf8"');
         */
    }
    public function save($values, $primaryKey = 'id', $audit_save = false)
    {
        $id = isset($values [$primaryKey]) ? $values [$primaryKey] : null;
        $row = $this->find($id)->current();
        if (! $row) {
            $row = $this->fetchNew();
        }
        $row->setFromArray($values);
        if (! $audit_save) {
            if (isset($row->level) && isset($row->parent) && $row->parent != 0 && $row->parent != null) {
                $parent = $this->find($row->parent)->current();
                if ($parent) {
                    $row->level = $parent->level + 1;
                }
            }
            if (isset($row->position) && ($row->position == 0 || $row->position == null)) {
                $ret = $this->getAdapter()->fetchRow("SHOW TABLE STATUS LIKE '".$this->info('name')."'");
                $row->position = $ret ['Auto_increment'];
            }
            if (method_exists($this, '_audit')) {
                $this->_audit($row);
            }
        }
        $row->save();

        return $row;
    }
    public function move($id, $where = null, $direction = 'up')
    {
        if (! $id) {
            return;
        }
        $select = $this->select();
        if ($where) {
            foreach ($where as $k => $v) {
                $select->where($k.' = ?', $v);
            }
        }
        $slct = clone $select;
        $toMove = $this->fetchRow($slct->where('id = ?', $id));
        if ($direction == 'down') {
            $select->where('position > ?', $toMove->position);
            $select->order('position ASC');
        } else {
            $select->where('position < ?', $toMove->position);
            $select->order('position DESC');
        }
        $toChange = $this->fetchRow($select);
        if ($toChange && $toMove) {
            $position = $toChange->position;
            $toChange->position = $toMove->position;
            $toChange->save();
            $toMove->position = $position;
            $toMove->save();

            return $toMove->position;
        }

        return;
    }
    public function moveUp($id, $where = null)
    {
        return $this->move($id, $where, 'up');
    }
    public function moveDown($id, $where = null)
    {
        return $this->move($id, $where, 'down');
    }
    public function toggleVisibility($id)
    {
        $row = $this->find((int) $id)->current();
        if ($row) {
            $row->visible = $row->visible ? 0 : 1;
        }
        $row->save();

        return $row;
    }
    /**
     * Return array of pairs;.
     *
     * @param $rows Zend_Db_Table_Rowset
     * @param $id string
     * @param $title mixed
     * @param $add_empty bool
     * @return array
     */
    public function getPairs($rows, $id, $title, $add_empty = false)
    {
        $pairs = [];
        if ($add_empty) {
            $pairs = [
                '' => '', ];
        }
        foreach ($rows as $key => $value) {
            if (is_string($title)) {
                $pairs [$value [$id]] = $value [$title];
            }
            if (is_array($title)) {
                $label = '';
                foreach ($title as $k => $v) {
                    if (is_array($v)) {
                        if (isset($v['title'])) {
                            if ($v['separator']) {
                                if ($v['placement'] && $v['placement'] == 'prepend') {
                                    $label [] = $v['separator'].' '.$value [$v['title']];
                                } else {
                                    $label [] = $value [$v['title']].' '.$v['separator'];
                                }
                            } else {
                                $label [] = $value [$v['title']];
                            }
                        }
                    } else {
                        if (isset($value [$v])) {
                            $label [] = $value [$v];
                        }
                    }
                }
                $pairs [$value [$id]] = implode(' ', $label);
            }
        }

        return $pairs;
    }
    /**
     * Return array of pairs;.
     *
     * @param $rows array
     * @param $id string
     * @param $title mixed
     * @param $add_empty bool
     * @return array
     */
    public function getPairsFromArray($rows, $id, $title, $add_empty = true)
    {
        $pairs = [];
        if ($add_empty) {
            $pairs = [
                '' => '', ];
        }
        foreach ($rows as $key => $value) {
            if (is_string($title)) {
                $pairs [$value [$id]] = $value [$title];
            }
            if (is_array($title)) {
                $label = '';
                foreach ($title as $k => $v) {
                    if (isset($value [$v])) {
                        $label [] = $value [$v];
                    }
                }
                $pairs [$value [$id]] = implode(' ', $label);
            }
        }

        return $pairs;
    }
}
