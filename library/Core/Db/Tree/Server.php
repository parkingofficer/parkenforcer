<?php


class Core_Db_Tree_Server extends Core_Db_Tree
{
    private $_nodeId;
    private $_nodeRefId;
    private $_nodePrefix = 'node_';
    private $_keys;
    private $_languages;

    public function __construct($config, $languages, $nodeId, $refId = null)
    {
        parent::__construct($config);

        $this->setNodeId($this->_parseId($nodeId));
        $this->setNodeRefId($this->_parseId($refId));
        $this->_keys = $this->getKeys();
        $this->_languages = $languages;
    }

    public function setNodePrefix($prefix)
    {
        $this->_nodePrefix = $prefix;

        return $this;
    }

    public function setNodeId($id)
    {
        $this->_nodeId = $id;

        return $this;
    }

    public function setNodeRefId($id)
    {
        $this->_nodeRefId = $id;

        return $this;
    }

    public function route($action)
    {
        switch ($action) {
            case 'delete':
                return $this->deleteAction();
            case 'create':
            case 'move':
                return $this->moveAction();
            case 'rename':
                return $this->renameAction();
            case 'loadfile':
                return $this->loadFileAction();
            case 'savefile':
                return $this->saveFileAction();
            default:
                return $this->listAction();
        }
    }

    public function listAction()
    {
        if ($this->_nodeId) {
            $node = $this->getNode($this->_nodeId);

            $parents = $this->getChildren(
                $this->_nodeId,
                1
            );
        } else {
            $parents = new Core_Db_Tree_NodeSet();
            $parents->addNode($this->getNode(1));
        }

        $arr = [];

        if (! empty($parents)) {
            $k = 0;
            while ($parents->valid()) {
                $node = $parents->current();

                $arr[$k]['attributes']['id'] = $this->_nodePrefix.$node->getData($this->_keys['id']);

                if ($node->isParent()) {
                    $arr[$k]['state'] = 'closed';
                }

                foreach ($this->_languages as $key => $lang) {
                    $arr[$k]['data'][$lang] = $node->getData(substr($this->_keys['title'], 0, -2).$lang);
                }

                $arr[$k]['data']['title'] = $node->getData($this->_keys['title']);

                if (! ($node->hasChild || $node->getLevel() < 2)) {
                    if ($node->getData('visible') == 1) {
                        $arr[$k]['data']['icon'] = '/img/admin/icon_cat_page.gif';
                    } else {
                        $arr[$k]['data']['icon'] = '/img/admin/icon_cat_page_inactive.gif';
                    }
                } elseif ($node->getData('visible') == 0) {
                    $arr[$k]['data']['icon'] = '/img/admin/icon_cat_inactive.gif';
                }

                $parents->next();

                $k++;
            }
        }

        die(Zend_Json::encode($arr));
    }

    public function deleteAction()
    {
        $this->removeNode($this->_nodeId);
    }

    public function moveAction()
    {
        $type = isset($_REQUEST['move_type']) ? $_REQUEST['move_type'] : '';

        switch ($type) {
            case 'before':
                $this->moveBefore($this->_parseId($this->_nodeId), $this->_parseId($this->_nodeRefId));
                break;
            case 'after':
                $this->moveAfter($this->_parseId($this->_nodeId), $this->_parseId($this->_nodeRefId));
                break;
            case 'inside':
                $this->moveAsLastChild($this->_parseId($this->_nodeId), $this->_parseId($this->_nodeRefId));
                break;
        }

        die('OK');
    }

    public function loadFileAction()
    {
    }

    public function saveFileAction()
    {
    }

    private function _parseId($str)
    {
        return (int) str_replace($this->_nodePrefix, '', $str);
    }
}
