<?php
/**
 * Magento.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Varien
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Core_Db_Tree_Node
{
    public $hasChild = false;
    public $numChild = 0;

    public $children = [];

    private $_left;
    private $_right;
    private $_id;
    private $_pid;
    private $_level;
    private $_title;
    private $_data;

    public function __construct($nodeData = [], $keys)
    {
        if (empty($nodeData)) {
            throw new Core_Db_Tree_Node_Exception('Empty array of node information');
        }
        if (empty($keys)) {
            throw new Core_Db_Tree_Node_Exception('Empty keys array');
        }

        $this->_id = $nodeData[$keys['id']];
        $this->_pid = $nodeData[$keys['pid']];
        $this->_left = $nodeData[$keys['left']];
        $this->_right = $nodeData[$keys['right']];
        $this->_level = $nodeData[$keys['level']];
        $this->_title = $nodeData[$keys['title']];

        $this->_data = $nodeData;
        $rlDiff = $this->_right - $this->_left;
        if ($rlDiff > 1) {
            $this->hasChild = true;
            $this->numChild = ($rlDiff - 1) / 2;
        }

        return $this;
    }

    public function setChildren($children)
    {
        $this->children = $children;
    }

    public function getChildren()
    {
        return $this->children;
    }

    public function getData($name)
    {
        if (isset($this->_data[$name])) {
            return $this->_data[$name];
        } else {
            return;
        }
    }

    public function getLevel()
    {
        return $this->_level;
    }

    public function getTitle()
    {
        return $this->_title;
    }

    public function getLeft()
    {
        return $this->_left;
    }

    public function getRight()
    {
        return $this->_right;
    }

    public function getPid()
    {
        return $this->_pid;
    }

    public function getId()
    {
        return $this->_id;
    }

    /**
     * Return true if node have chield.
     *
     * @return bool
     */
    public function isParent()
    {
        if ($this->_right - $this->_left > 1) {
            return true;
        } else {
            return false;
        }
    }
}
