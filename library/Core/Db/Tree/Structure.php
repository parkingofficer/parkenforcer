<?php

class Core_Db_Tree_Structure extends Core_Db_Tree
{
    public function getStructure($parent = null, $startLevel = 0, $endLevel = 0)
    {
        $parent = intVal($parent);

        $children = $this->getChildren($parent, $startLevel);

        if (! empty($children)) {
            foreach ($children as $k => $v) {
                $children->current()->setChildren($this->getStructure($v->getId(), 1));
            }
        }

        return $children;
    }

    public function getNavigationArray($structure, $onlyVisible = true)
    {
        $arr = [];

        if (! empty($structure)) {
            foreach ($structure as $k => $v) {
                $module = $v->getData('module');

                if ($v->getData('type') == 'module' && ! empty($module)) {
                    $catalog = 'default';
                    if (strstr($module, '_')) {
                        $exp = explode('_', $module);

                        $catalog = strtolower($exp[0]);
                        $module = $exp[1];
                    }

                    $controllerName = $module.'Controller';

                    require_once APPLICATION_PATH.'/'.$catalog.'/controllers/'.$controllerName.'.php';

                    $arr[$k] = call_user_func([$v->getData('module').'Controller', 'getNavigation']);
                    $arr[$k]['label'] = $v->getTitle();
                    $arr[$k]['params']['id'] = $v->getId();
                } else {
                    $link = $v->getData('link_'.LANG);

                    if (empty($link)) {
                        $link = Core_Utils_Url::format($v->getTitle());
                    }

                    if ($v->getData('type') == 'link') {
                        $arr[$k] = [
                            'id' => 'article_'.$v->getData('id'),
                            'visible' => $v->getData('visible'),
                            'image' => $v->getData('image'),
                            'label' => $v->getTitle(),
                            'image' => $v->getData('image'),
                            'galery_id' => $v->getData('gallery_id'),
                            'uri' => $v->getData('redirect_link') ? $v->getData('redirect_link') : $v->getData('redirect_link_'.LANG),
                        ];
                        //}elseif($v->getData('type') == 'module'){
                    } else {
                        $arr[$k] = [
                            'id' => 'article_'.$v->getData('id'),
                            'visible' => $v->getData('visible'),
                            'text' => $v->getData('text_'.LANG),
                            'second_text' => $v->getData('second_text_'.LANG),
                            'image' => $v->getData('image'),
                            'galery_id' => $v->getData('gallery_id'),
                            'label' => $v->getTitle(),
                            'module' => 'default',
                            'controller' => 'articles',
                            'action' => 'view',
                            'params' => [
                                'lang' => LANG,
                                'id' => $v->getId(),
                                'link' => $link,
                            ],
                            'route' => 'articlesView',
                        ];
                    }
                    $arr[$k]['pages'] = $this->getNavigationArray($v->getChildren());
                }
            }
        }

        return $arr;
    }
}
