<?php

class Core_Db_SimpleTree extends Core_Db_Table
{
    protected $_idName = 'id';
    protected $_levelName = 'level';
    protected $_parentName = 'parent';
    protected $_positionName = 'position';
    protected $_titleName = 'title';

    public function __construct($config = [])
    {
        if (! empty($config ['_idName'])) {
            $this->_idName = $config ['_idName'];
        }
        if (! empty($config ['_levelName'])) {
            $this->_levelName = $config ['_levelName'];
        }
        if (! empty($config ['_parentName'])) {
            $this->_parentName = $config ['_parentName'];
        }
        if (! empty($config ['_positionName'])) {
            $this->_positionName = $config ['_positionName'];
        }
        if (! empty($config ['_titleName'])) {
            $this->_titleName = $config ['_titleName'];
        }
        parent::__construct();
    }
    final public function fetchAllTree($maxLevel = 2, $where = [])
    {
        $select = $this->select();
        if (! empty($where) && is_array($where)) {
            foreach ($where as $k => $v) {
                $select->where($k.' = ?', (string) $v);
            }
        }
        if (! isset($where [$this->_parentName])) {
            $select->where($this->_parentName.' = ?', 0);
        }
        $select->order($this->_positionName.' ASC');
        $items = $this->getAdapter()->fetchAll($select);
        if (! empty($items)) {
            foreach ($items as $k => $v) {
                if ($v [$this->_levelName] < $maxLevel) {
                    $where [$this->_parentName] = $v [$this->_idName];
                    $items [$k] ['children'] = $this->fetchAllTree($maxLevel, $where);
                } else {
                    $items [$k] ['children'] = [];
                }
            }
        }

        return $items;
    }
    public function fetchPlainTree($parent = 0, $where = null, $tree = [], $order = null)
    {
        $select = $this->select();
        if ($parent == 0 || $parent == null) {
            $select->where("$this->_parentName = 0 OR $this->_parentName IS NULL");
        } else {
            $select->where($this->_parentName.' = ?', $parent);
        }
        if ($order) {
            $select->order($order);
        } else {
            $select->order($this->_positionName.' ASC');
        }
        if (! empty($where)) {
            foreach ($where as $k => $v) {
                $select->where($k.' = ?', $v);
            }
        }
        // var_dump($select->assemble());
        // die();
        $items = $this->getAdapter()->fetchAll($select);
        if (! empty($items)) {
            foreach ($items as $k => $v) {
                array_push($tree, $v);
                $tree = $this->fetchPlainTree($v [$this->_idName], $where, $tree);
            }
        }

        return $tree;
    }
    public function fetchPairs($plainTree, $title = 'title')
    {
        $arr = [];
        $arr [0] = '';
        if (! empty($plainTree)) {
            foreach ($plainTree as $k => $value) {
                if (is_string($title)) {
                    $arr [$value [$this->_idName]] = str_repeat('-', 2 * ($value [$this->_levelName])).' '.$value [$title];
                }
                if (is_array($title)) {
                    $label = '';
                    foreach ($title as $k => $v) {
                        if (is_array($v)) {
                            if (isset($v['title'])) {
                                if ($v['separator']) {
                                    if ($v['placement'] && $v['placement'] == 'prepend') {
                                        $label [] = $v['separator'].' '.$value [$v['title']];
                                    } else {
                                        $label [] = $value [$v['title']].' '.$v['separator'];
                                    }
                                } else {
                                    $label [] = $value [$v['title']];
                                }
                            }
                        } else {
                            if (isset($value [$v])) {
                                $label [] = $value [$v];
                            }
                        }
                    }
                    $arr [$value [$this->_idName]] = str_repeat('-', 2 * ($value [$this->_levelName])).' '.implode(' ', $label);
                }
            }
        }

        return $arr;
    }
    public function fetchNodeSet($plainTreeArr)
    {
        $nodeSet = new Core_Db_Tree_NodeSet();
        if (! empty($plainTreeArr)) {
            $keys ['id'] = 'id';
            $keys ['pid'] = 'parent';
            $keys ['level'] = 'level';
            $keys ['title'] = 'title_'.LANG;
            $keys ['left'] = 'id';
            $keys ['right'] = 'id';
            foreach ($plainTreeArr as $k => $v) {
                $node = new Core_Db_Tree_Node($v, $keys);
                $nodeSet->addNode($node);
            }
        }

        return $nodeSet;
    }

    public function getParent($parent, $return = null)
    {
        if ((int) $parent) {
            $row = $this->find($parent)->current();
            if ($row) {
                $return = $row;
                if ((int) $row->{$this->_parentName}) {
                    $return = $this->getParent($row->{$this->_parentName}, $return);
                }
            }
        }

        return $return;
    }

    /**
     * @param $parent Int
     */
    public function getParentPath($parent, $titles = [])
    {
        if ((int) $parent) {
            $row = $this->find($parent)->current();
            if ($row) {
                $titles [] = $row->{$this->_titleName};
                if ((int) $row->{$this->_parentName}) {
                    $titles = array_reverse($this->getParentPath($row->{$this->_parentName}, $titles));
                }
            }
        }

        return $titles;
    }
    public function getUnorderedListTree($view, $tree = null, $unorderedList = null, $url = '#', $actions = [])
    {
        if (! $tree) {
            $tree = $this->fetchAllTree(1000);
        }
        $search = [
            '{id}',
        ];
        if (! empty($tree)) {
            $unorderedList .= '<ul>';
            foreach ($tree as $key => $value) {
                $replace = [
                    $value [$this->_idName],
                ];
                $unorderedList .= "<li><a href='".str_replace($search, $replace, $url)."' class='popup nyroModal'>".$value [$this->_titleName].'</a>';
                if (! empty($actions)) {
                    $unorderedList .= "<div class='jstree-actions'>";
                    if ($actions ['edit']) {
                        $act = $actions ['edit'];
                        $act ['action'] = str_replace($search, $replace, $act ['action']);
                        $unorderedList .= "<a class='action_edit action' href='".$view->url($act, null, true, false)."'>edit</a>";
                    }
                    if ($actions ['view']) {
                        $act = $actions ['view'];
                        $act ['action'] = str_replace($search, $replace, $act ['action']);
                        $unorderedList .= "<a class='action_view action' href='".$view->url($act, null, true, false)."'>view</a>";
                    }
                    if ($actions ['delete']) {
                        $act = $actions ['delete'];
                        $act ['action'] = str_replace($search, $replace, $act ['action']);
                        $unorderedList .= "<a class='action_delete action' href='".$view->url($act, null, true, false)."'>view</a>";
                    }
                    if ($actions ['moveup']) {
                        $act = $actions ['moveup'];
                        $act ['action'] = str_replace($search, $replace, $act ['action']);
                        $unorderedList .= "<a class='action_up action' href='".$view->url($act, null, true, false)."'>up</a>";
                    }
                    if ($actions ['movedown']) {
                        $act = $actions ['movedown'];
                        $act ['action'] = str_replace($search, $replace, $act ['action']);
                        $unorderedList .= "<a class='action_down action' href='".$view->url($act, null, true, false)."'>down</a>";
                    }
                    $unorderedList .= '</div>';
                }
                if (! empty($value ['children'])) {
                    $unorderedList = $this->getUnorderedListTree($view, $value ['children'], $unorderedList, $url, $actions);
                }
                $unorderedList .= '</li>';
            }
            $unorderedList .= '</ul>';
        }

        return $unorderedList;
    }
}
