<?php

/**
 * Magento.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Varien
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Varien Library.
 *
 *
 * @category   Varien
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Core_Db_Tree extends Core_Db_Table
{
    public $_table;

    /**
     * Zend_Db_Adapter.
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db;

    private $_id;
    private $_left;
    private $_right;
    private $_level;
    private $_pid;
    private $_title;
    private $_nodesInfo = [];

    /**
     * Array of additional tables.
     *
     * array(
     *  [$tableName] => array(
     *              ['joinCondition']
     *              ['fields']
     *          )
     * )
     *
     * @var array
     */
    private $_extTables = [];

    public function __construct($config = [])
    {
        // set a Zend_Db_Adapter connection
        if (! empty($config['db'])) {
            // convenience variable
            $db = $config['db'];

            // use an object from the registry?
            if (is_string($db)) {
                $db = Zend_Registry::get($db);
            }

            // make sure it's a Zend_Db_Adapter
            if (! $db instanceof Zend_Db_Adapter_Abstract) {
                throw new Core_Db_Tree_Exception('db object does not implement Zend_Db_Adapter_Abstract');
            }

            // save the connection
            $this->_db = $db;
            $conn = $this->_db->getConnection();
            if ($conn instanceof PDO) {
                $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
            } elseif ($conn instanceof mysqli) {
                //TODO: ???
            }
        } elseif (Zend_Registry::isRegistered('db')) {
            $this->_db = Zend_Registry::get('db');
        } else {
            throw new Core_Db_Tree_Exception('db object is not set in config');
        }

        if (! empty($config['table'])) {
            $this->setTable($config['table']);
        }

        if (! empty($config['id'])) {
            $this->setIdField($config['id']);
        } else {
            $this->setIdField('id');
        }

        if (! empty($config['left'])) {
            $this->setLeftField($config['left']);
        } else {
            $this->setLeftField('left_key');
        }

        if (! empty($config['right'])) {
            $this->setRightField($config['right']);
        } else {
            $this->setRightField('right_key');
        }

        if (! empty($config['level'])) {
            $this->setLevelField($config['level']);
        } else {
            $this->setLevelField('level');
        }

        if (! empty($config['pid'])) {
            $this->setPidField($config['pid']);
        } else {
            $this->setPidField('parent_id');
        }

        if (! empty($config['title'])) {
            $this->setTitleField($config['title']);
        } else {
            $this->setTitleField('title');
        }
    }

    /**
     * set name of id field.
     *
     * @param string $name
     * @return Core_Db_Tree
     */
    public function setIdField($name)
    {
        $this->_id = $name;

        return $this;
    }

    /**
     * set name of left field.
     *
     * @param string $name
     * @return Core_Db_Tree
     */
    public function setLeftField($name)
    {
        $this->_left = $name;

        return $this;
    }

    /**
     * set name of left field.
     *
     * @param string $name
     * @return Core_Db_Tree
     */
    public function setTitleField($name)
    {
        $this->_title = $name;

        return $this;
    }

    /**
     * set name of right field.
     *
     * @param string $name
     * @return Core_Db_Tree
     */
    public function setRightField($name)
    {
        $this->_right = $name;

        return $this;
    }

    /**
     * set name of level field.
     *
     * @param string $name
     * @return Core_Db_Tree
     */
    public function setLevelField($name)
    {
        $this->_level = $name;

        return $this;
    }

    /**
     * set name of pid Field.
     *
     * @param string $name
     * @return Core_Db_Tree
     */
    public function setPidField($name)
    {
        $this->_pid = $name;

        return $this;
    }

    /**
     * set table name.
     *
     * @param string $name
     * @return Core_Db_Tree
     */
    public function setTable($name)
    {
        $this->_table = $name;

        return $this;
    }

    public function getKeys()
    {
        $keys = [];
        $keys['id'] = $this->_id;
        $keys['left'] = $this->_left;
        $keys['right'] = $this->_right;
        $keys['pid'] = $this->_pid;
        $keys['level'] = $this->_level;
        $keys['title'] = $this->_title;

        return $keys;
    }

    /**
     * Cleare table and add root element.
     */
    public function clear($data = [])
    {
        // clearing table
        $this->_db->query('TRUNCATE '.$this->_table);
        //$this->_db->delete($this->_table,'');

        // prepare data for root element
        $data[$this->_pid] = 0;
        $data[$this->_left] = 1;
        $data[$this->_right] = 2;
        $data[$this->_level] = 0;

        try {
            $this->_db->insert($this->_table, $data);
        } catch (PDOException $e) {
            //echo $e->getMessage();
            return false;
        }

        return $this->_db->lastInsertId();
    }

    public function getNodeInfo($id)
    {
        if (empty($this->_nodesInfo[$id])) {
            $sql = 'SELECT * FROM '.$this->_table.' WHERE '.$this->_id.' = :id';
            $res = $this->_db->query($sql, ['id' => $id]);
            $data = $res->fetch();
            $this->_nodesInfo[$id] = $data;
        } else {
            $data = $this->_nodesInfo[$id];
        }

        return $data;
    }

    public function appendChild($id, $data)
    {
        if (! $info = $this->getNodeInfo($id)) {
            return false;
        }

        $data[$this->_left] = $info[$this->_right];
        $data[$this->_right] = $info[$this->_right] + 1;
        $data[$this->_level] = $info[$this->_level] + 1;
        $data[$this->_pid] = $id;

        // creating a place for the record being inserted
        if ($id) {
            $this->_db->beginTransaction();
            try {
                $sql = 'UPDATE '.$this->_table.' SET'
                .' `'.$this->_left.'` = IF( `'.$this->_left.'` > :left, `'.$this->_left.'` + 2, `'.$this->_left.'`),'
                .' `'.$this->_right.'` = IF( `'.$this->_right.'`>= :right, `'.$this->_right.'` + 2, `'.$this->_right.'`)'
                .' WHERE `'.$this->_right.'` >= :right';

                $this->_db->query($sql, ['left' => $info[$this->_left], 'right' => $info[$this->_right]]);

                $ret = $this->_db->insert($this->_table, $data);

                $this->_db->commit();
            } catch (PDOException $p) {
                $this->_db->rollBack();
                echo $p->getMessage();
                exit();

                return false;
            } catch (Exception $e) {
                $this->_db->rollBack();
                echo $e->getMessage();
                echo $sql;
                var_dump($data);
                exit();

                return false;
            }
            // TODO: change to ZEND LIBRARY
            $res = $this->_db->fetchOne('SELECT LAST_INSERT_ID()');

            return $res;
            //return $this->_db->fetchOne('select last_insert_id()');
            //return $this->_db->lastInsertId();
        }

        return false;
    }

    public function checkNodes()
    {
        $sql = $this->_db->select();

        $sql->from(['t1' => $this->_table], ['t1.'.$this->_id, new Zend_Db_Expr('COUNT(t1.'.$this->_id.') AS rep')])
        ->from(['t2' => $this->_table])
        ->from(['t3' => $this->_table], new Zend_Db_Expr('MAX(t3.'.$this->_right.') AS max_right'));

        $sql->where('t1.'.$this->_left.' <> t2.'.$this->_left)
        ->where('t1.'.$this->_left.' <> t2.'.$this->_right)
        ->where('t1.'.$this->_right.' <> t2.'.$this->_right);

        $sql->group('t1.'.$this->_id);
        $sql->having('max_right <> SQRT(4 * rep + 1) + 1');

        return $this->_db->fetchAll($sql);
    }

    public function insertBefore($id, $data)
    {
    }

    public function removeNode($id)
    {
        if (! $info = $this->getNodeInfo($id)) {
            return false;
        }

        if ($id) {
            $this->_db->beginTransaction();
            try {
                // DELETE FROM my_tree WHERE left_key >= $left_key AND right_key <= $right_key
                $this->_db->delete($this->_table, $this->_left.' >= '.$info[$this->_left].' AND '.$this->_right.' <= '.$info[$this->_right]);

                // UPDATE my_tree SET left_key = IF(left_key > $left_key, left_key – ($right_key - $left_key + 1), left_key), right_key = right_key – ($right_key - $left_key + 1) WHERE right_key > $right_key
                $sql = 'UPDATE '.$this->_table.'
                SET
                '.$this->_left.' = IF('.$this->_left.' > '.$info[$this->_left].', '.$this->_left.' - '.($info[$this->_right] - $info[$this->_left] + 1).', '.$this->_left.'),
                '.$this->_right.' = '.$this->_right.' - '.($info[$this->_right] - $info[$this->_left] + 1).'
                WHERE
                '.$this->_right.' > '.$info[$this->_right];
                $this->_db->query($sql);
                $this->_db->commit();

                return new Core_Db_Tree_Node($info, $this->getKeys());
            } catch (Exception $e) {
                $this->_db->rollBack();
                //echo $e->getMessage();
                return false;
            }
        }
    }

    public function moveNodeAsLastChild($srcId, $dstId)
    {
        $srcInfo = $this->getNodeInfo($srcId);
        $dstInfo = $this->getNodeInfo($dstId);

        $srcLeft = $srcInfo[$this->_left];
        $srcRight = $srcInfo[$this->_right];
        $srcLevel = $srcInfo[$this->_level];

        $dstLeft = $dstInfo[$this->_left];
        $dstRight = $dstInfo[$this->_right];
        $dstLevel = $dstInfo[$this->_level];

        if (! $this->_canMove($srcInfo, $dstInfo)) {
            return false;
        }

        if ($dstLeft < $srcLeft && $dstRight > $srcRight && $dstLevel < $srcLevel - 1) {
            $sql = 'UPDATE '.$this->_table.' SET '
            .$this->_level.' = CASE WHEN '.$this->_left.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_level.sprintf('%+d', -($srcLevel - 1) + $dstLevel).' ELSE '.$this->_level.' END, '
            .$this->_right.' = CASE WHEN '.$this->_right.' BETWEEN '.($srcRight + 1).' AND '.($dstRight - 1).' THEN '.$this->_right.'-'.($srcRight - $srcLeft + 1).' '
            .'WHEN '.$this->_left.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_right.'+'.((($dstRight - $srcRight - $srcLevel + $dstLevel) / 2) * 2 + $srcLevel - $dstLevel - 1).' ELSE '.$this->_right.' END, '
            .$this->_left.' = CASE WHEN '.$this->_left.' BETWEEN '.($srcRight + 1).' AND '.($dstRight - 1).' THEN '.$this->_left.'-'.($srcRight - $srcLeft + 1).' '
            .'WHEN '.$this->_left.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_left.'+'.((($dstRight - $srcRight - $srcLevel + $dstLevel) / 2) * 2 + $srcLevel - $dstLevel - 1).' ELSE '.$this->_left.' END '
            .'WHERE '.$this->_left.' BETWEEN '.($dstLeft + 1).' AND '.($dstRight - 1);
        } elseif ($dstLeft < $srcLeft) {
            $sql = 'UPDATE '.$this->_table.' SET '
            .$this->_level.' = CASE WHEN '.$this->_left.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_level.sprintf('%+d', -($srcLevel - 1) + $dstLevel).' ELSE '.$this->_level.' END, '
            .$this->_left.' = CASE WHEN '.$this->_left.' BETWEEN '.$dstRight.' AND '.($srcLeft - 1).' THEN '.$this->_left.'+'.($srcRight - $srcLeft + 1).' '
            .'WHEN '.$this->_left.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_left.'-'.($srcLeft - $dstRight).' ELSE '.$this->_left.' END, '
            .$this->_right.' = CASE WHEN '.$this->_right.' BETWEEN '.$dstRight.' AND '.$srcLeft.' THEN '.$this->_right.'+'.($srcRight - $srcLeft + 1).' '
            .'WHEN '.$this->_right.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_right.'-'.($srcLeft - $dstRight).' ELSE '.$this->_right.' END '
            .'WHERE ('.$this->_left.' BETWEEN '.$dstLeft.' AND '.$srcRight.' '
            .'OR '.$this->_right.' BETWEEN '.$dstLeft.' AND '.$srcRight.')';
        } else {
            $sql = 'UPDATE '.$this->_table.' SET '
            .$this->_level.' = CASE WHEN '.$this->_left.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_level.sprintf('%+d', -($srcLevel - 1) + $dstLevel).' ELSE '.$this->_level.' END, '
            .$this->_left.' = CASE WHEN '.$this->_left.' BETWEEN '.$srcRight.' AND '.$dstRight.' THEN '.$this->_left.'-'.($srcRight - $srcLeft + 1).' '
            .'WHEN '.$this->_left.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_left.'+'.($dstRight - 1 - $srcRight).' ELSE '.$this->_left.' END, '
            .$this->_right.' = CASE WHEN '.$this->_right.' BETWEEN '.($srcRight + 1).' AND '.($dstRight - 1).' THEN '.$this->_right.'-'.($srcRight - $srcLeft + 1).' '
            .'WHEN '.$this->_right.' BETWEEN '.$srcLeft.' AND '.$srcRight.' THEN '.$this->_right.'+'.($dstRight - 1 - $srcRight).' ELSE '.$this->_right.' END '
            .'WHERE ('.$this->_left.' BETWEEN '.$srcLeft.' AND '.$dstRight.' '
            .'OR '.$this->_right.' BETWEEN '.$srcLeft.' AND '.$dstRight.')';
        }

        $this->_db->beginTransaction();
        try {
            $this->_db->query($sql);
            $this->_db->commit();

            return true;
        } catch (Exception $e) {
            $this->_db->rollBack();

            return false;
        }
    }

    //nstrees.php
    public function moveBefore($srcId, $dstId)
    {
        $srcInfo = $this->getNodeInfo($srcId);
        $dstInfo = $this->getNodeInfo($dstId);

        if (! $this->_canMove($srcInfo, $dstInfo)) {
            return false;
        }

        return $this->_moveSubtree($srcInfo, $dstInfo, $dstInfo[$this->_left]);
    }

    //nstrees.php
    public function moveAfter($srcId, $dstId)
    {
        $srcInfo = $this->getNodeInfo($srcId);
        $dstInfo = $this->getNodeInfo($dstId);

        if (! $this->_canMove($srcInfo, $dstInfo)) {
            return false;
        }

        return $this->_moveSubtree($srcInfo, $dstInfo, $dstInfo[$this->_right] + 1);
    }

    //nstrees.php
    public function moveAsFirstChild($srcId, $dstId)
    {
        $srcInfo = $this->getNodeInfo($srcId);
        $dstInfo = $this->getNodeInfo($dstId);

        if (! $this->_canMove($srcInfo, $dstInfo)) {
            return false;
        }

        return $this->_moveSubtree($srcInfo, $dstInfo, $dstInfo[$this->_left] + 1);
    }

    //nstrees.php
    public function moveAsLastChild($srcId, $dstId)
    {
        $srcInfo = $this->getNodeInfo($srcId);
        $dstInfo = $this->getNodeInfo($dstId);

        if (! $this->_canMove($srcInfo, $dstInfo)) {
            return false;
        }

        return $this->_moveSubtree($srcInfo, $dstInfo, $dstInfo[$this->_right]);
    }

    public function addTable($tableName, $joinCondition, $fields = '*')
    {
        $this->_extTables[$tableName] = [
        'joinCondition' => $joinCondition,
        'fields' => $fields,
        ];
    }

    public function getPairs($tree, $id)
    {
        $nd = $this->getNode($id);

        $lft = $rgt = null;

        if ($nd) {
            $lft = $nd->getLeft();
            $rgt = $nd->getRight();
        }

        $pairs = [];

        foreach ($tree as $node) {
            if ($lft == 1 || ! ($lft && $rgt && $node->getLeft() >= $lft && $node->getRight() <= $rgt)) {
                $pairs[$node->getId()] = '';

                for ($i = 0; $i < $node->getLevel() * 4;$i++) {
                    $pairs[$node->getId()] .= '-';
                }

                $pairs[$node->getId()] .= ' '.$node->getTitle();
            }
        }

        return $pairs;
    }

    public function getPairsWithDepth($tree, $id)
    {
        $nd = $this->getNode($id);

        $lft = $rgt = null;

        if ($nd) {
            $lft = $nd->getLeft();
            $rgt = $nd->getRight();
        }

        $pairs = [];

        foreach ($tree as $node) {
            if ($lft == 1 || ! ($lft && $rgt && $node->getLeft() >= $lft && $node->getRight() <= $rgt)) {
                $node_info = $this->getNodeInfo($node->getId());
                $pairs[$node->getId()]['title'] = $node->getTitle();
                $pairs[$node->getId()]['depth'] = $node->getLevel();
                $pairs[$node->getId()]['id'] = $node->getId();
                $pairs[$node->getId()]['link_'.LANG] = $node_info['link_'.LANG];
                $pairs[$node->getId()]['link'] = $node_info['link'];
                $pairs[$node->getId()]['type'] = $node_info['type'];

                //$pairs[$node->getId()]['info'] = $node_info;
            }
        }

        return $pairs;
    }

    public function getTree($title_keyword = null)
    {
        try {
            $info = $this->getNodeInfo($this->_id);
        } catch (Exception $e) {
            return false;
        }

        $sql =
        'SELECT '.
        'node.*, node.id, node.'.$this->_title.' AS title, (COUNT(parent.id) - 1) AS level '.
        'FROM '.$this->_table.' AS node, '.$this->_table.' AS parent '.
        'WHERE '.
        'node.'.$this->_left.' '.
        'BETWEEN '.
        'parent.'.$this->_left.' AND '.
        'parent.'.$this->_right.' ';

        if ($title_keyword) {
            $sql .= " AND  node.$this->_title LIKE '%$title_keyword%'";
        }

        $sql .= 'GROUP BY node.id '.
        'ORDER BY node.'.$this->_left;

        $data = $this->_db->fetchAll($sql);

        $nodeSet = new Core_Db_Tree_NodeSet();

        foreach ($data as $node) {
            $nodeSet->addNode(new Core_Db_Tree_Node($node, $this->getKeys()));
        }

        return $nodeSet;
    }

    public function getChildren($id, $start_level = 0, $end_level = 0)
    {
        try {
            $info = $this->getNodeInfo($id);
        } catch (Exception $e) {
            return false;
        }

        $dbSelect = new Zend_Db_Select($this->_db);
        $dbSelect
        ->from($this->_table)
        ->where($this->_left.' > :left')
        ->where($this->_right.' < :right')
        ->order($this->_left);

        $this->_addExtTablesToSelect($dbSelect);

        $data = [];
        $data['left'] = $info[$this->_left];
        $data['right'] = $info[$this->_right];

        if (! empty($start_level) && empty($end_level)) {
            $dbSelect->where($this->_level.' = :minLevel');
            $data['minLevel'] = $info[$this->_level] + $start_level;
        }

        $data = $this->_db->fetchAll($dbSelect, $data);

        $nodeSet = new Core_Db_Tree_NodeSet();
        foreach ($data as $node) {
            $nodeSet->addNode(new Core_Db_Tree_Node($node, $this->getKeys()));
        }

        return $nodeSet;
    }

    public function getNode($nodeId)
    {
        $dbSelect = new Zend_Db_Select($this->_db);
        $dbSelect
        ->from($this->_table)
        ->where($this->_table.'.'.$this->_id.' >= :id');

        $this->_addExtTablesToSelect($dbSelect);

        $data = [];
        $data['id'] = $nodeId;

        $data = $this->_db->fetchRow($dbSelect, $data);

        return new Core_Db_Tree_Node($data, $this->getKeys());
    }

    public function getParents($id, $order = true)
    {
        try {
            $info = $this->getNodeInfo($id);
        } catch (Exception $e) {
            return false;
        }

        $sql = '
        SELECT
        parent.*
        FROM
        '.$this->_table.' AS node,
        '.$this->_table.' AS parent
        WHERE
        node.'.$this->_left.'
        BETWEEN parent.'.$this->_left.' AND parent.'.$this->_right.'
        AND node.'.$this->_id.' = :id';
        if (true == $order) {
            $sql .= '
            ORDER BY
            parent.'.$this->_left;
        }

        return $this->_db->fetchAll($sql, ['id' => $id]);
    }

    public function getParent($id)
    {
        try {
            $info = $this->getNodeInfo($id);

            return $info[$this->_pid];
        } catch (Exception $e) {
            return false;
        }
    }

    public function moveUp($id)
    {
        $currentNode = $this->getNode($id);

        if (empty($currentNode)) {
            return false;
        }
        $upperNodeId = $this->_db->fetchOne('SELECT '.$this->_id.' FROM '.$this->_table.' WHERE '.$this->_right.' = '.($currentNode->getLeft() - 1));

        if (empty($upperNodeId)) {
            return false;
        }
        $this->moveBefore($currentNode->getId(), $upperNodeId);

        return true;
    }

    public function moveDown($id)
    {
        $currentNode = $this->getNode($id);

        if (empty($currentNode)) {
            return false;
        }
        $lowerNodeId = $this->_db->fetchOne('SELECT '.$this->_id.' FROM '.$this->_table.' WHERE '.$this->_left.' = '.($currentNode->getRight() + 1));

        if (empty($lowerNodeId)) {
            return false;
        }
        $this->moveAfter($currentNode->getId(), $lowerNodeId);

        return true;
    }

    protected function _canMove($srcInfo, $dstInfo)
    {
        return true;
        $srcLeft = $srcInfo[$this->_left];
        $srcRight = $srcInfo[$this->_right];
        $srcLevel = $srcInfo[$this->_level];

        $dstLeft = $dstInfo[$this->_left];
        $dstRight = $dstInfo[$this->_right];
        $dstLevel = $dstInfo[$this->_level];

        $result = ! (
        $srcId == $dstId ||
        $srcLeft == $dstLeft ||
        (
        $dstLeft >= $srcLeft &&
        $dstLeft <= $srcRight
        ) ||
        (
        $srcLevel == $dstLevel + 1 &&
        $srcLeft > $dstLeft &&
        $srcRight < $dstRight
        )
        );

        return $result;
    }

    //nstrees.php
    protected function _moveSubtree($srcInfo, $dstInfo, $dstId)
    {
        $this->_db->beginTransaction();

        try {
            $treeSize = $srcInfo[$this->_right] - $srcInfo[$this->_left] + 1;
            $this->_shiftRLValues($dstId, $treeSize);

            if ($srcInfo[$this->_left] >= $dstId) { // src was shifted too?
                $srcInfo[$this->_left] += $treeSize;
                $srcInfo[$this->_right] += $treeSize;
            }
            /* now there's enough room next to target to move the subtree*/
            $newpos = $this->_shiftRLRange(
            $srcInfo[$this->_left],
            $srcInfo[$this->_right],
            $dstId - $srcInfo[$this->_left]
            );
            /* correct values after source */
            $this->_shiftRLValues($srcInfo[$this->_right] + 1, -$treeSize);
            if ($srcInfo[$this->_left] <= $dstId) { // dst was shifted too?
                $newpos[$this->_left] -= $treeSize;
                $newpos[$this->_right] -= $treeSize;
            }

            $this->_updateLevel($newpos);
            $this->_updatePid($newpos);

            $this->_db->commit();
            //return $newPos;
            return true;
        } catch (Exception $e) {
            $this->_db->rollBack();

            return false;
        }
    }

    protected function _updatePid($srcInfo)
    {
        $left = $srcInfo[$this->_left];
        $right = $srcInfo[$this->_right];

        $row = $this->_db->fetchRow('SELECT * FROM '.$this->_table.' WHERE '.$this->_left.' < '.$left.' AND '.$this->_right.' > '.$right.' ORDER BY '.$this->_left.' DESC');

        $this->_db->query('UPDATE '.$this->_table.' SET '.$this->_pid.' = '.$row[$this->_id].' WHERE '.$this->_left.' = '.$left.' AND '.$this->_right.' = '.$right);
    }

    //
    protected function _updateLevel($srcInfo)
    {
        $srcLeft = $srcInfo[$this->_left];
        $srcRight = $srcInfo[$this->_right];

        $level =
        'SELECT node.'.$this->_id.', node.'.$this->_left.', node.'.$this->_right.', '.
        '	   (COUNT(parent.'.$this->_id.') - 1) AS depth '.
        'FROM '.$this->_table.' AS node, '.
        $this->_table.' AS parent '.
        'WHERE node.'.$this->_left.' BETWEEN parent.'.$this->_left.' AND parent.'.$this->_right.' '.
        'GROUP BY node.'.$this->_id.' '.
        'HAVING node.'.$this->_left.' >= '.$srcLeft.' AND node.'.$this->_right.' <= '.$srcRight.' '.
        'ORDER BY node.'.$this->_left;
        $tst = $this->_db->fetchAll($level);

        if (! empty($tst)) {
            foreach ($tst as $k => $v) {
                $sql = 'UPDATE '.$this->_table.' SET '.$this->_level.' = '.$v['depth'].' WHERE '.$this->_id.' = '.$v[$this->_id];
                $this->_db->query($sql);
            }
        }
    }

    //nstrees.php
    protected function _shiftRLValues($first, $delta)
    {
        $sql =
        'UPDATE '.$this->_table.' SET '.
        $this->_left.' = '.$this->_left.' + :delta '.
        'WHERE '.$this->_left.' >= :first';
        $this->_db->query($sql, ['first' => $first, 'delta' => $delta]);
        $sql =
        'UPDATE '.$this->_table.' SET '.
        $this->_right.' = '.$this->_right.' + :delta '.
        'WHERE '.$this->_right.' >= :first';
        $this->_db->query($sql, ['first' => $first, 'delta' => $delta]);
    }

    //nstrees.php
    protected function _shiftRLRange($first, $last, $delta)
    {
        $sql =
        'UPDATE '.$this->_table.' SET '.
        $this->_left.' = '.$this->_left.' + :delta '.
        'WHERE '.$this->_left.' >= :first AND '.$this->_left.' <= :last';
        $this->_db->query($sql, ['first' => $first, 'last' => $last, 'delta' => $delta]);
        $sql =
        'UPDATE '.$this->_table.' SET '.
        $this->_right.' = '.$this->_right.' + :delta '.
        'WHERE '.$this->_right.' >= :first AND '.$this->_right.' <= :last';
        $this->_db->query($sql, ['first' => $first, 'last' => $last, 'delta' => $delta]);

        return [
        $this->_left => $first + $delta,
        $this->_right => $last + $delta,
        ];
    }

    protected function _addExtTablesToSelect(Zend_Db_Select &$select)
    {
        foreach ($this->_extTables as $tableName => $info) {
            $select->joinInner($tableName, $info['joinCondition'], $info['fields']);
        }
    }
}
