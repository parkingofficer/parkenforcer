<?php

class Core_Locale
{
    protected $_locale = 'en'; //default language
    public function __construct($locale = null)
    {
        if (! $locale) {
            $this->_locale = LANG;
        } else {
            $this->_locale = $locale;
        }
    }

    /* get locale */
    public function getLocale()
    {
        return $this->_locale;
    }

    public static function translate($string)
    { //a custom function for translation
        $translate = Zend_Registry::get('translate');

        return $translate->translate($string);
    }
}
