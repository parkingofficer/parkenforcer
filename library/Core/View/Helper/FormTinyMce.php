<?php

class Core_View_Helper_FormTinyMce extends Zend_View_Helper_FormTextarea
{
    public $isOff = false;
    protected $_tinyMce;

    public function FormTinyMce($name, $value = null, $attribs = null)
    {
        $info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable

        $disabled = '';
        if ($disable) {
            $disabled = ' disabled="disabled"';
        }

        if (empty($attribs['rows'])) {
            $attribs['rows'] = (int) $this->rows;
        }

        if (empty($attribs['cols'])) {
            $attribs['cols'] = (int) $this->cols;
        }

        if (isset($attribs['editorOptions'])) {
            if ($attribs['editorOptions'] instanceof Zend_Config) {
                $attribs['editorOptions'] = $attribs['editorOptions']->toArray();
            }

            $this->view->tinyMce()->setOptions($attribs['editorOptions']);
            unset($attribs['editorOptions']);
        }

        $this->view->tinyMce()->render();

        //$xhtml = '<a href="#" onclick="toggleEditor(\''.$this->view->escape($id).'\'); return false;">Redatorius</a><textarea';
        $xhtml = '<textarea';

        if (! $this->isOff) {
            $xhtml .= ' class="tinymce_area span12" ';
        } else {
            $xhtml .= ' class="inputTextarea span12"';
        }

        $xhtml .= ' name="'.$this->view->escape($name).'"'
               .' id="'.$this->view->escape($id).'"'
               .$disabled
               .$this->_htmlAttribs($attribs).'>'
               .$this->view->escape($value).'</textarea>';

        return $xhtml;
    }
}
