<?php

class Core_View_Helper_TinyMce extends Zend_View_Helper_Abstract
{
    public static $enabled = false;
    protected $_defaultScript = '/js/tiny_mce/tiny_mce_src.js';

    protected $_supported = [
    'mode' => ['textareas', 'specific_textareas', 'exact', 'none'],
    'theme' => ['simple', 'advanced'],
    'format' => ['html', 'xhtml'],
    'languages' => ['en', 'lt'],
    'plugins' => ['style', 'layer', 'table', 'save',
    'advhr', 'advimage', 'advlink', 'emotions',
    'iespell', 'insertdatetime', 'preview', 'media',
    'searchreplace', 'print', 'contextmenu', 'paste',
    'directionality', 'fullscreen', 'noneditable', 'visualchars',
    'nonbreaking', 'xhtmlxtras', 'imgmanager', 'filemanager','template','paste', ], ];

    protected $_config = [
    'mode' => 'specific_textareas',
    'theme_advanced_toolbar_location ' => 'top',
    'theme_advanced_toolbar_align  ' => 'left',
    'paste_use_dialog' => 'false',
    'paste_auto_cleanup_on_paste' => 'true',
    'editor_selector' => 'tinymce_area',
    'theme' => 'advanced',
    'element_format' => 'xhtml',
    'plugins' => 'safari,pagebreak,style,layer,table,advhr,advimage,advlink,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imgmanager,filemanager',
    'theme_advanced_resizing' => true,
    'language' => 'en',
    'theme_advanced_buttons1' => 'pastetext,pasteword,selectall,|,undo,redo,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,styleselect,formatselect,forecolor,backcolor',
    'theme_advanced_buttons2' => 'tablecontrols,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,anchor,cleanup,code,',
    'theme_advanced_buttons3' => 'hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media,advhr,|,fullscreen,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,blockquote,pagebreak,imgmanager,filemanager',
    'extended_valid_elements' => 'embed[width|height|name|flashvars|src|bgcolor|align|play|loop|quality|allowscriptaccess|type|pluginspage],div[class|id],b[class,id],iframe[src|width|height|name|align]',
    //'valid_elements' => "*[*]",
    ];
    //imgmanager,image

    protected $_scriptPath;
    protected $_scriptFile;
    protected $_useCompressor = false;

    public function __set($name, $value)
    {
        $method = 'set'.$name;
        if (! method_exists($this, $method)) {
            throw new Zend_Exception('Invalid tinyMce property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get'.$name;
        if (! method_exists($this, $method)) {
            throw new Zend_Exception('Invalid tinyMce property');
        }

        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set'.ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            } else {
                if ($key == 'theme' && $value == 'simple') {
                    $this->_config['theme_advanced_buttons3'] = '';
                    $this->_config['theme_advanced_buttons2'] = '';
                    $this->_config['theme_advanced_buttons1'] = 'bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,imgmanager,filemanager';
                } else {
                    $this->_config[$key] = $value;
                }
            }
        }

        return $this;
    }

    public function TinyMce()
    {
        return $this;
    }

    public function setScriptPath($path)
    {
        $this->_scriptPath = rtrim($path, '/');

        return $this;
    }

    public function setScriptFile($file)
    {
        $this->_scriptFile = (string) $file;
    }

    public function useCompressor($switch)
    {
        $this->_useCompressor = (bool) $switch;

        return $this;
    }

    public function render()
    {
        if (false === self::$enabled) {
            $this->_renderScript();
            $this->_renderCompressor();
            $this->_renderEditor();
        }

        self::$enabled = true;
    }

    protected function _renderScript()
    {
        if (null === $this->_scriptFile) {
            $script = $this->_defaultScript;
        } else {
            $script = $this->_scriptPath.'/'.$this->_scriptFile;
        }

        $this->view->minifyHeadScript()->appendFile('http://'.$_SERVER['HTTP_HOST'].$script);

        return $this;
    }

    protected function _renderCompressor()
    {
        if (false === $this->_useCompressor) {
            return;
        }
        $script = 'tinyMCE_GZ.init({'.PHP_EOL
        .'themes: "'.implode(',', $this->_supportedTheme).'"'.PHP_EOL
        .'plugins: "'.implode(',', $this->_supportedPlugins).'"'.PHP_EOL
        .'languages: "'.implode(',', $this->_supportedLanguages).'"'.PHP_EOL
        .'disk_cache: true'.PHP_EOL
        .'debug: false'.PHP_EOL
        .'theme_advanced_toolbar_location : top'.PHP_EOL
        .'});';

        $this->view->headScript()->appendScript($script);

        return $this;
    }

    protected function _renderEditor()
    {
        $script = 'tinyMCE.init({'.PHP_EOL;

        foreach ($this->_config as $name => $value) {
            if (is_array($value)) {
                $value = implode(',', $value);
            }
            if (! is_bool($value)) {
                $value = '"'.$value.'"';
            }
            $script .= $name.': '.$value;
            $script .= ','.PHP_EOL;
        }

        $script = substr($script, 0, -2);
        $script .= '});';

        $this->view->headScript()->appendScript($script);

        return $this;
    }
}
