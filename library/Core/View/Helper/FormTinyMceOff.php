<?php


class Core_View_Helper_FormTinyMceOff extends Core_View_Helper_FormTinyMce
{
    public $isOff = true;

    public function FormTinyMceOff($name, $value = null, $attribs = null)
    {
        return $this->FormTinyMce($name, $value, $attribs);
    }
}
