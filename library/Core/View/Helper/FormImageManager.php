<?php

class Core_View_Helper_FormImageManager extends Zend_View_Helper_FormElement
{
    public function formImageManager($name, $value = null, $attribs = null)
    {
        $info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable

        $disabled = '';
        if ($disable) {
            $disabled = ' disabled="disabled"';
        }

        $xhtml = '<button class="image_manager_button btn  btn-success btn-large" onclick="window.open (\'http://'.$_SERVER['HTTP_HOST'].'/js/ImageManager/manager.php?var_name='.$this->view->escape($id).'\', 620, 500);return false;" name=""'
            .$disabled
            .$this->_htmlAttribs($attribs).'>Paveikslėlis</button>';
        //onmouseover="$(\'#span-'.$this->view->escape($id).'\').show();" onmouseout="$(\'#span-'.$this->view->escape($id).'\').hide();"
        if (! empty($value)) {
            $xhtml .= '<a id="img_delete_btn" class="btn btn-large btn-primary ml10px" onclick="$(\'#'.$this->view->escape($id).'\').attr(\'value\', \'\');$(\'#span-'.$this->view->escape($id).'\').remove();$(this).remove();return false;">Trinti</a>';
        }

        $xhtml .= '<input type="hidden" value="'.$this->view->escape($value).'" name="'.$this->view->escape($name).'" id="'.$this->view->escape($id).'" />';

        if (! empty($value)) {
            $xhtml .= '<img id="span-'.$this->view->escape($id).'" style="margin-left:15px;" src="'.($this->view->url(['module' => 'admin', 'controller' => 'show', 'action' => 'image', 'catalog' => 'img', 'image' => urlencode($value)])).'" />';
        }

        return $xhtml;
    }
}
