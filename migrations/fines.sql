ALTER TABLE `box_fines` ADD `amount` DECIMAL(10,2) NULL DEFAULT 0.00 AFTER `date`,
  ADD `rate` INT(3) NULL DEFAULT 100 AFTER `amount`,
  ADD `paid` DECIMAL(10,2) NULL DEFAULT 0.00 AFTER `rate`
;

CREATE TABLE `box_fine_rates` (
  `id` int(11) NOT NULL,
  `visible` tinyint(1) DEFAULT '1',
  `position` int(11) DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `rate` INT(3) DEFAULT 100
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `box_fine_rates` ADD PRIMARY KEY(`id`);
ALTER TABLE `box_fine_rates` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `box_fines_log` (
  `id` int(11) NOT NULL,
  `fine_id` int(11) NOT NULL,
  `who` varchar(100) NOT NULL,
  `message` TEXT NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `box_fines_log` ADD PRIMARY KEY(`id`);
ALTER TABLE `box_fines_log` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `box_zone_causes_rates` (
  `zone_id` int(11) NOT NULL,
  `cause_id` int(11) NOT NULL,
  `rate` decimal(10,2) NOT NULL DEFAULT '24.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;