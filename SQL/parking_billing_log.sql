create table billing_log
(
  id                 int auto_increment
    primary key,
  type               varchar(30)                         not null,
  licensePlateNumber varchar(20)                         null,
  request            varchar(800)                        null,
  response           text                                null,
  request_time       timestamp default CURRENT_TIMESTAMP null
)