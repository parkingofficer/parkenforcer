-- auto-generated definition
create table billing_queue
(
  id        int auto_increment
    primary key,
  type      varchar(30)     null,
  system_id varchar(30)     null,
  status    tinyint         null,
  try       int default '0' null
);

