ALTER TABLE `box_fines`
ADD COLUMN `change_status_date` DATETIME NULL DEFAULT NULL AFTER `date`;

ALTER TABLE `box_zone`
ADD COLUMN `published_time` INT(11) NULL AFTER `municipality_id`,
ADD COLUMN `enforcement_time` INT(11) NULL AFTER `published_time`;
