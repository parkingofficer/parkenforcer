<?php
/*
Uploadify
Copyright (c) 2012 Reactive Apps, Ronnie Garcia
Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
files/training/material
*/

// Define a destination
date_default_timezone_set("Europe/Vilnius");
$verifyToken = md5('xk0q2389rCcn98N*yr2398ryuc23c$%adf' . $_POST['timestamp']);

if (!empty($_FILES) && $_POST['token'] == $verifyToken) {
	$tempFile = $_FILES['Filedata']['tmp_name'];
	$targetPath = $_POST['files_path'];
	if(!file_exists($targetPath)){
		mkdir($targetPath);
		chmod($targetPath, 0777);
	}
	$targetFile = rtrim($targetPath,'/') . '/' . $_FILES['Filedata']['name'];

	$fileParts = pathinfo($_FILES['Filedata']['name']);
	move_uploaded_file($tempFile,$targetFile);
	$newFileName =  md5(microtime()).".".$fileParts['extension'];
	rename($targetFile,$targetPath."/".$newFileName);
	die($newFileName);
}
?>