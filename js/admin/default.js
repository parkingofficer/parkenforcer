// Functions ===================================


// Callbacks ====================================


// Document ready ===============================

$(document).ready(function() {

    $('select').select2();

	$('.datepicker').datepicker({format:"yyyy-mm-dd",dateFormat:"yy-mm-dd",weekStart:1,firstDay: 1});
	$('.datetimepicker').datetimepicker({
        format:"yyyy-mm-dd",
        dateFormat:"yy-mm-dd",
        weekStart:1,
        firstDay: 1,
        timeFormat: "HH:mm"
    });

	// Tooltips for nav badge
	$('.main-navigation .badge').tooltip({
		placement: 'bottom'
	});

	
	//TODO LIST
	$('.todo-block input[type="checkbox"]').click(function(){
		$(this).closest('tr').toggleClass('done');
	});
	$('.todo-block input[type="checkbox"]:checked').closest('tr').addClass('done');

    $('ul.errors').each(function(){
        $(this).prev().addClass('error');
    });

});

