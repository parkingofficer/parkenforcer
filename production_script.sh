#!/usr/bin/env bash

RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color


echo -e "${CYAN}╔══════════════════════════════╗${NC}\n"
echo -e "${CYAN}║ -== RUNNING PHP CS FIXER ==- ║${NC}\n"
echo -e "${CYAN}╚══════════════════════════════╝${NC}\n"
php -n vendor/bin/php-cs-fixer fix -vvv --show-progress=estimating
